function initBanners() {

    (function ($) {
        $.fn.banners = function (options) {

            if (this.length > 0) {
                var elementsData = new Array();

                for (i = 0; i < this.length; i++) {

                    elementsData.push(this[i]);
                }
                ;

                setElementsData(elementsData);
            }
        }


        // Configura os dados dos elementos encontrados pelo plugin
        function setElementsData(arrEl) {
            var productsIDs = '';

            // Adiciona os IDs dos produtos a variï¿½vel productsIDs
            for (var i in arrEl) {
                if ($(arrEl[i]).data().product_id != undefined && !isNaN($(arrEl[i]).data().product_id))
                    productsIDs += (i == 0) ? $(arrEl[i]).data().product_id : ',' + $(arrEl[i]).data().product_id;
            }

            if(productsIDs != '') {

                /*
                 * Passa o param com os IDs por ajax
                 * Retorna o objeto de produtos pelo callback
                 */
                getProductInfo(productsIDs, function (json) {

                    //Percorre os elementos do template
                    for (var i in arrEl) {
                        var settings = {
                            'background': '#fff',
                            'font_color': '#fff',
                            'banner_title': null,
                            'banner_url': null,
                            'banner_subtitle': null
                        };

                        data = $(arrEl[i]).data();

                        for (p in data) {
                            settings[p] = data[p];
                        }

                        //Add css ao elemento .app-banners
                        $(arrEl[i]).css({
                            'background': settings.background,
                            'color': settings.font_color
                        });

                        // se for para exibir o desconto, recebe a classe 'display_discount'
                        display_discount = (settings.banner_type == 2) ? 'show-discount' : '';

                        //Add as classes do banner
                        $(arrEl[i]).addClass(checkBrand() + '-bannerStyle' + settings.banner_type + ' ' + display_discount);

                        if(settings.banner_class != undefined){
                            $(arrEl[i]).addClass(settings.banner_class);
                        }

                        if ($(arrEl[i]).data().product_id != undefined) {

                            produtos = json.products; 

                            /*
                             * Percorre o objeto de produtos
                             * Compara se o produtoID atual ï¿½ o mesmo do templateID
                             * Sendo igual passa por param os dados do produto e do template atual para gerar o html
                             */
                            for (var x in produtos) {
                                if (produtos[x].id == data.product_id) {
                                    createHTML(produtos[x], $(arrEl[i]), settings);
                                }
                            }
                        }
                    }
                });

            }

        }

        function getProductInfo(strIDs, callback) {
            $.ajax({
                url: 'http://www.' + checkBrand() + '.com.br/productinfo-v2?itens=' + strIDs + '&full=true&callback=?',
                jsonpCallback: 'jsonpCallback',
                cache: true,
                dataType: 'jsonp',
                beforeSend: function (xhr) {
                    xhr.overrideMimeType("text/javascript; charset=UTF-8");
                },
                success: function (json) {
                    callback(json);
                },
                error: function (a, b, c) {
                    getProductInfo(strIDs);
                }
            });
        }

        // Gera o html do banner
        function createHTML(objProd, elWrap, settings) {
            var $html = '';
	
			if(objProd.new){
				objProdType = objProd.new;
			} else {
				objProdType = objProd.reembalados;
			}

            $bannerURL = (settings.banner_url == '' || settings.banner_url == null) ? objProd.url : settings.banner_url;

            // Add o elemento "A" ao .app-banners
            $(elWrap).append('<a class="banners-url" href="' + $bannerURL + '" style="display:block; color:' + settings.font_color + '; "></a>');

            //Add o elemento que envolve o banner e suas informacoes
            $classInvoicePrive = (settings.invoice_price) ? 'inovice_price_on' : 'inovice_price_off';
            $('a', elWrap).append('<div class="banners-content ' + $classInvoicePrive + '" style="display:block; margin:0 auto; "></div>');

            $name = truncateText(objProd.name, 50);

            //Verifica se tem tÃ­tulo no template, se nÃ£o tiver retorna o tÃ­tulo do productinfo
            $name = (settings.banner_title == '' || settings.banner_title == null) ? $name : settings.banner_title;
			
			
            if (objProdType.default_price != 'null' && objProdType.default_price != '') {
                $preco_padrao = objProdType.default_price.split(' ');
                $preco_padrao = $preco_padrao[1].split(',');
            }

            $preco_venda = objProdType.sales_price.split(' ');
            $preco_venda = $preco_venda[1].split(',');
            $installmentValue = objProdType.installment.installment_value.split(' ');
            $installmentValue = $installmentValue[1].split(',');
			
			// Add imagem pequena tpl1 soub
            $html += ( (settings.banner_type == 1) && checkBrand() == 'soubarato') ? '<div class="image"><img src="'+objProd.images[0].url+'" /></div>' : '';
			$html += ( (settings.banner_type == 2 || settings.banner_type == 4) && checkBrand() == 'soubarato') ? '<div class="image"><img src="'+objProd.mqImage+'" /></div>' : '';
			$html += ( (settings.banner_type == 3) && checkBrand() == 'soubarato') ? '<div class="image"><img src="'+objProd.images[0].url+'" /></div>' : '';

            // Html do banner
            $html += '<div class="information">';
            $html += '<div class="name">' + $name + '</div>';
            $html += (settings.banner_subtitle == '' || settings.banner_subtitle == null) ? '' : '<div class="subtitle">' + settings.banner_subtitle + '</div>';
			
            $html += '</div>';

            if(objProd.stock == true){
                $html += '<div class="price">';

                // add tags soubarato tpl 2
				var productLabel = (objProd.new != undefined ) ? 'novo' : 'reembalado';
                $html += '<div class="tag-label ' +productLabel+ '"></div>';
				
                hasLabels = ( settings.banner_type == 2 || settings.banner_type == 4) ? '' : 'noHasLabel';

                // add Frete
                $html += ( (settings.banner_type == 2 || settings.banner_type == 3 || settings.banner_type == 4) && objProd.freight != '') ? '<div class="freight '+hasLabels+'"><img src="' + objProd.freight + '" /></div>' : '';

                if (objProdType.default_price != 'null' && objProdType.default_price != '') {
                    $html += '<div class="defaultPrice"><span class="prefix">de</span> <span class="moeda">R$</span>  <span class="unidade">' + $preco_padrao[0] + '</span><span class="decimal">,' + $preco_padrao[1] + '</span></div>';
                }

                //Bloco do preço por boleto
                if (settings.invoice_price) {
                    $html += '<div class="priceInvoice">' + $txtAPartirDe + '<span class="prefix">por</span> <span class="moeda">R$</span>  <span class="unidade">' + $preco_boleto[0] + '</span><span class="decimal">,' + $preco_boleto[1] + '</span> <span class="text">no boleto</span></div>';
                } else {
                    $html += '<div class="priceInvoice"><span class="prefix">por</span> <span class="moeda">R$</span>  <span class="unidade">' + $preco_venda[0] + '</span><span class="decimal">,' + $preco_venda[1] + '</span></div>';
                }

                //Bloco do parcelamento
                var txtEmAte = (
                                ((settings.banner_type == 2 || settings.banner_type == 4) && (checkBrand() == 'soubarato'))
                            ) ? '<span class="emate">em at&eacute</span>' : '';

                var parcelAfter = ((settings.banner_type == 2 || settings.banner_type == 4) && (checkBrand() == 'soubarato')) ? '<p class="parcelAfter">ou em at&eacute; 12x com juros*</p>' : '';

                $html += '<div class="salePrice"><span class="prefix">a prazo por</span> <span class="moeda">R$</span>  <span class="unidade">' + $preco_venda[0] + '</span><span class="decimal">,' + $preco_venda[1] + '</span></div>';
                $html += '<div class="parcel"><span class="prefix"><span class="ou">ou</span> ' + txtEmAte + '</span> <span class="parcelas">' + objProdType.installment.total_installments + 'x</span> <span class="moeda">R$</span> <span class="unidade">' + $installmentValue[0] + '</span><span class="decimal">,' + $installmentValue[1] + '</span> <span class="text">sem juros</span>'+parcelAfter+'</div>';

                //Bloco de valores em porcentagem
                $html += ( checkBrand() == 'soubarato' && (settings.banner_type == 1 || settings.banner_type == 2 || settings.banner_type == 4) ) ? '<div class="discount"><span>' + convertToPercent($preco_padrao[0] + ',' + $preco_padrao[1], $preco_venda[0] + ',' + $preco_venda[1], true) + '%</span></div>' : '';

                //Bloco que insere o botão compre agora
                $html += (
                    ( (settings.banner_type == 2 || settings.banner_type == 3 || settings.banner_type == 4) && (checkBrand() == 'soubarato') )
                    ) ? '<div class="btn-compreagora"></div>' : '';

                if(settings.timer) {
                    $html += timerTemplate(settings.timer);
                }

                $html += '</div>';
            }

            //Insere o html gerado no content do fullbanner
            $('.banners-content', elWrap).append($html);
        }

        /*
         * function truncateText
         * @param str: string
         * @param limit: Char limit
         * @param separator: separator style
         */
        function truncateText(str, limit, separator) {
            separator = (separator) ? separator : ' ';

            if (str.length > limit) {
                str = str.substr(0, limit);
                str = str.split(' ');
                $html = '';

                for ($i = 0; $i < (str.length - 1); $i++) {
                    $html += ($i == 0) ? str[$i] : ' ' + str[$i];
                }

                $html += separator;

                return $html;

            } else {
                return str;
            }
        }
    })(jQuery);

    $(".app-banners").banners();

}

/*
 * Retorna a marca atual a partir da url
 */
function checkBrand() {
    str = location.href;

    if (str.search("americanas") > -1) {
        return 'americanas';
    }
    if (str.search("submarino") > -1) {
        return 'submarino';
    }
    if (str.search("shoptime") > -1) {
        return 'shoptime';
    }
    if (str.search("soubarato") > -1) {
        return 'soubarato';
    }
}

/*
 * Método para retornar a porcentagem entre dois valores
 * @param1: string/number
 * @param2: string/number
 * @param3 (optional): boolean
 */
function convertToPercent(val1, val2) {
    var toInt = arguments[2];

    val1 = val1.replace('.', '').replace(',', '.');
    val2 = val2.replace('.', '').replace(',', '.');

    var result = ((val1 - val2) / val1) * 100;

    if (toInt) {
        return parseInt(result);
    } else {
        return result;
    }
}

// Template para uso do app-timer
function timerTemplate(dateTime) {
    var html = '';

    html += '<div class="timer-box">';
    html += '<div class="app-timer" data-timer="'+dateTime+'">';
    html += '<span class="icon-timer"></span>';
    html += '<span class="tit-timer">Faltam</span>';

    html += '<div class="timer-hour">';
    html += '<span class="timer-tens"></span>';
    html += '<span class="timer-units"></span>';
    html += 'h';
    html += '</div>';
    html += '<div class="timer-minute">';
    html += '<span class="timer-tens"></span>';
    html += '<span class="timer-units"></span>';
    html += 'm';
    html += '</div>';
    html += '<div class="timer-second">';
    html += '<span class="timer-tens"></span>';
    html += '<span class="timer-units"></span>';
    html += 's';
    html += '</div>';
    html += '<div class="timer-milliseconds">c</div>';
    html += '<span class="tit-timer-end">para o t&eacute;rmino desta oferta ou enquanto durar o estoque</span>';
    html += '<span class="btn-check"></span>';
    html += '</div>';
    html += '</div>';

    return html;
}

// Inicia o app-banners
if (checkBrand() == 'soubarato') {
    $(function () {
        initBanners();
    });
} else {
    onReady(function () {
        initBanners();
    });
}