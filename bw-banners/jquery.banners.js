function initBanners() {

    (function ($) {
        $.fn.banners = function (options) {

            if (this.length > 0) {
                var elementsData = new Array();

                for (i = 0; i < this.length; i++) {

                    elementsData.push(this[i]);
                }
                ;

                setElementsData(elementsData);
            }
        }


        // Configura os dados dos elementos encontrados pelo plugin
        function setElementsData(arrEl) {
            var productsIDs = '';

            // Ativa o video no TPL 6 do shop
            if (checkBrand() == 'shoptime') {
                $(document).on('click', '.app-banners .video', function(){
                    $(this).html('<iframe width="384" height="228" src="//www.youtube.com/embed/'+$(this).attr('videoid')+'?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>');
                });
            }

            // Adiciona os IDs dos produtos a variï¿½vel productsIDs
            for (var i in arrEl) {
                if ($(arrEl[i]).data().product_id != undefined && !isNaN($(arrEl[i]).data().product_id))
                    productsIDs += (i == 0) ? $(arrEl[i]).data().product_id : ',' + $(arrEl[i]).data().product_id;
            }

            if(productsIDs != '') {

                /*
                 * Passa o param com os IDs por ajax
                 * Retorna o objeto de produtos pelo callback
                 */
                getProductInfo(productsIDs, function (json) {

                    //Percorre os elementos do template
                    for (var i in arrEl) {
                        var settings = {
                            'background': '#fff',
                            'font_color': '#fff',
                            'banner_title': null,
                            'banner_url': null,
                            'banner_subtitle': null,
                            'discount': null,
                            'invoice_price': true
                        };

                        data = $(arrEl[i]).data();

                        for (p in data) {
                            settings[p] = data[p];
                        }
							
                        //Trata o background se o valor for uma imagem
                        if (/\.(jpg|png|gif)$/.test(settings.background) && settings.banner_type != 2) {
                            settings.background = 'url(' + settings.background + ') left top no-repeat';
                        }

                        //Add css ao elemento .app-banners
                        $(arrEl[i]).css({
                            'background': settings.background,
                            'color': settings.font_color
                        });

                        // se for para exibir o desconto, recebe a classe 'display_discount'
                        display_discount = (settings.discount != '' && settings.discount != null) ? 'display-count' : '';

                        //Add as classes do banner
                        $(arrEl[i]).addClass(checkBrand() + '-bannerStyle' + settings.banner_type + ' ' + display_discount);

                        if(settings.banner_class != undefined){
                            $(arrEl[i]).addClass(settings.banner_class);
                        }

                        if ($(arrEl[i]).data().product_id != undefined) {

                            produtos = json.products;

                            /*
                             * Percorre o objeto de produtos
                             * Compara se o produtoID atual ï¿½ o mesmo do templateID
                             * Sendo igual passa por param os dados do produto e do template atual para gerar o html
                             */
                            for (var x in produtos) {
                                if (produtos[x].id == data.product_id) {
                                    createHTML(produtos[x], $(arrEl[i]), settings);
                                }
                            }
                        }
                    }
                });

            }

        }

        function getProductInfo(strIDs, callback) {
            $.ajax({
                url: 'http://www.' + checkBrand() + '.com.br/productinfo?itens=' + strIDs + '&full=true&callback=?',
                jsonpCallback: 'jsonpCallback',
                cache: true,
                dataType: 'jsonp',
                beforeSend: function (xhr) {
                    xhr.overrideMimeType("text/javascript; charset=UTF-8");
                },
                success: function (json) {
                    callback(json);
                },
                error: function (a, b, c) {
                    getProductInfo(strIDs);
                }
            });
        }

        // Gera o html do banner
        function createHTML(objProd, elWrap, settings) {
            var $html = '';

            // Tpl 6 Video Shoptime
            if (settings.video_yt_id) {
                $(elWrap).prepend('<div class="video" videoid="'+settings.video_yt_id+'"></div>');
            }

            $bannerURL = (settings.banner_url == '' || settings.banner_url == null) ? objProd.url : settings.banner_url;
			$bannerQueryString = (settings.banner_querystring == '' || settings.banner_querystring == null) ? '' : settings.banner_querystring;
			
            // Add o elemento "A" ao .app-banners
            $(elWrap).append('<a class="banners-url" href="' + $bannerURL + $bannerQueryString + '" style="display:block; color:' + settings.font_color + '; "></a>');

            //Add o elemento que envolve o banner e suas informacoes
            $classInvoicePrive = (settings.invoice_price) ? 'inovice_price_on' : 'inovice_price_off';
            $('a', elWrap).append('<div class="banners-content ' + $classInvoicePrive + '" style="display:block; margin:0 auto; "></div>');

            if(
                checkBrand() == 'shoptime' && (settings.banner_type==4 || settings.banner_type==5 || settings.banner_type==6)
            ) {
                $name = truncateText(objProd.name, 80);
            } 
			else if(checkBrand() == 'shoptime' && (settings.banner_type==2 || settings.banner_type==8 || settings.banner_type==9 || settings.banner_type==10 ) ) {
                $name = truncateText(objProd.name, 70);
            }
			else if(checkBrand() == 'shoptime' && settings.banner_type==3) {
                $name = truncateText(objProd.name, 56);
            }	
			else { 
                $name = truncateText(objProd.name, 50);
            }

            //Verifica se tem tÃ­tulo no template, se nÃ£o tiver retorna o tÃ­tulo do productinfo
            $name = (settings.banner_title == '' || settings.banner_title == null) ? $name : settings.banner_title;

            if (objProd.default_price != 'null' && objProd.default_price != '') {
                $preco_padrao = objProd.default_price.split(' ');
                $preco_padrao = $preco_padrao[1].split(',');
            }

            $preco_boleto = objProd.invoice_price.split(' ');
            $preco_boleto = $preco_boleto[1].split(',');
            $preco_venda = objProd.sales_price.split(' ');
            $preco_venda = $preco_venda[1].split(',');
            $installmentValue = objProd.installment.installment_value.split(' ');
            $installmentValue = $installmentValue[1].split(',');
			
			// Add imagem pequena tpl1 shop
            $html += ( (settings.banner_type == 1 || settings.banner_type == 3 || settings.banner_type == 10) && checkBrand() == 'shoptime') ? '<div class="image"><img src="'+objProd.image+'" /></div>' : '';	
			
            // Add imagem grande tpl2 and tpl5 shop
            $html += ( (settings.banner_type == 2 || settings.banner_type == 5 || settings.banner_type == 8 || settings.banner_type == 9) && checkBrand() == 'shoptime') ? '<div class="image"><img src="'+objProd.mqImage+'" /></div>' : '';
			
			//Troca o tamanho das imagens do tpl7
			var tpl7Img = (settings.banner_class != undefined && settings.banner_class == 'tpl7-full') ? objProd.images[0].url : objProd.mqImage ;
			 $html += ( (settings.banner_type == 7) && checkBrand() == 'shoptime') ? '<div class="image"><div class="mask"></div><img src="'+tpl7Img+'" /></div>' : '';

            // Selo de KM Ipiranga Shoptime
            if (settings.invoice_price) {
                var km = ($preco_boleto[1] == '00') ? $preco_boleto[0] : parseInt($preco_boleto[0]) + 1 ;
            } else {
                var km = ($preco_venda[1] == '00') ? $preco_venda[0] : parseInt($preco_venda[0]) + 1 ;
            }
            $html += (checkBrand() == 'shoptime' && settings.seal_km) ? '<div class="seal_km"><span>'+km+' KM</span></div>' : '';

            // Html do banner
            $html += '<div class="information">';
            $html += '<div class="name">' + $name + '</div>';
            $html += (settings.banner_subtitle == '' || settings.banner_subtitle == null) ? '' : '<div class="subtitle">' + settings.banner_subtitle + '</div>';
            // add Frete
            $html += (checkBrand() == 'shoptime' && ( (settings.banner_type == 5 || settings.banner_type == 7) && objProd.freight != '')) ? '<div class="freight"><img src="' + objProd.freight + '" /></div>' : '';
            $html += '</div>';

            if(objProd.stock == true){
                $html += '<div class="price">';

                // add tags soubarato tpl 2
                $html += ( settings.labels && checkBrand() == 'soubarato') ? '<div class="tag-label ' +settings.labels+ '"></div>' : '';
                hasLabels = ( settings.labels && checkBrand() == 'soubarato') ? '' : 'noHasLabel';

                // add Frete
                $html += (settings.banner_type == 2 && objProd.freight != '') ? '<div class="freight '+hasLabels+'"><img src="' + objProd.freight + '" /></div>' : '';

                if (objProd.default_price != 'null' && objProd.default_price != '') {
                    $html += '<div class="defaultPrice"><span class="prefix">de</span> <span class="moeda">R$</span>  <span class="unidade">' + $preco_padrao[0] + '</span><span class="decimal">,' + $preco_padrao[1] + '</span></div>';
                }

                //Bloco do preço por boleto
				$txtAPartirDe = '';
				
                if (settings.invoice_price) {
                    $html += '<div class="priceInvoice">' + $txtAPartirDe + '<span class="prefix">por</span> <span class="moeda">R$</span>  <span class="unidade">' + $preco_boleto[0] + '</span><span class="decimal">,' + $preco_boleto[1] + '</span> <span class="text">no boleto</span></div>';
                } else {
                    $html += '<div class="priceInvoice"><span class="prefix">por</span> <span class="moeda">R$</span>  <span class="unidade">' + $preco_venda[0] + '</span><span class="decimal">,' + $preco_venda[1] + '</span></div>';
                }

                //Bloco do parcelamento
                var txtEmAte = '';
                var parcelAfter = '';

                $html += '<div class="salePrice"><span class="prefix">a prazo por</span> <span class="moeda">R$</span>  <span class="unidade">' + $preco_venda[0] + '</span><span class="decimal">,' + $preco_venda[1] + '</span></div>';
                $html += '<div class="parcel"><span class="prefix"><span class="ou">ou</span> ' + txtEmAte + '</span> <span class="parcelas">' + objProd.installment.total_installments + 'x</span> <span class="moeda">R$</span> <span class="unidade">' + $installmentValue[0] + '</span><span class="decimal">,' + $installmentValue[1] + '</span> <span class="text">sem juros</span>'+parcelAfter+'</div>';

                //Bloco que insere o botão compre agora
                $html += (
                    ( (settings.banner_type == 6 || settings.banner_type == 9) && (checkBrand() == 'shoptime') )
                    ) ? '<div class="btn-compreagora"></div>' : '';

                if(settings.timer) {
                    $html += timerTemplate(settings.timer);
                }

                $html += '</div>';
            }

            //Insere o html gerado no content do fullbanner
            $('.banners-content', elWrap).append($html);
			
			// Add a largura de 50% na LI que conter o template de vídeo
			if(checkBrand() == 'shoptime' && (settings.banner_type==6))
				$(elWrap).parent('li').css('width', '50%');
        }

        /*
         * function truncateText
         * @param str: string
         * @param limit: Char limit
         * @param separator: separator style
         */
        function truncateText(str, limit, separator) {
            separator = (separator) ? separator : ' ';

            if (str.length > limit) {
                str = str.substr(0, limit);
                str = str.split(' ');
                $html = '';

                for ($i = 0; $i < (str.length - 1); $i++) {
                    $html += ($i == 0) ? str[$i] : ' ' + str[$i];
                }

                $html += separator;

                return $html;

            } else {
                return str;
            }
        }
    })(jQuery);

    $(".app-banners").banners();

}

/*
 * Retorna a marca atual a partir da url
 */
function checkBrand() {
    str = location.href;

    if (str.search("americanas") > -1) {
        return 'americanas';
    }
    if (str.search("submarino") > -1) {
        return 'submarino';
    }
    if (str.search("shoptime") > -1) {
        return 'shoptime';
    }
    if (str.search("soubarato") > -1) {
        return 'soubarato';
    }
}

/*
 * Método para retornar a porcentagem entre dois valores
 * @param1: string/number
 * @param2: string/number
 * @param3 (optional): boolean
 */
function convertToPercent(val1, val2) {
    var toInt = arguments[2];

    val1 = val1.replace('.', '').replace(',', '.');
    val2 = val2.replace('.', '').replace(',', '.');

    var result = ((val1 - val2) / val1) * 100;

    if (toInt) {
        return parseInt(result);
    } else {
        return result;
    }
}

// Template para uso do app-timer
function timerTemplate(dateTime) {
    var html = '';

    html += '<div class="timer-box">';
    html += '<div class="app-timer" data-timer="'+dateTime+'">';
    html += '<span class="icon-timer"></span>';
    html += '<span class="tit-timer">Faltam</span>';

    html += '<div class="timer-hour">';
    html += '<span class="timer-tens"></span>';
    html += '<span class="timer-units"></span>';
    html += 'h';
    html += '</div>';
    html += '<div class="timer-minute">';
    html += '<span class="timer-tens"></span>';
    html += '<span class="timer-units"></span>';
    html += 'm';
    html += '</div>';
    html += '<div class="timer-second">';
    html += '<span class="timer-tens"></span>';
    html += '<span class="timer-units"></span>';
    html += 's';
    html += '</div>';
    html += '<div class="timer-milliseconds">c</div>';
    html += '<span class="tit-timer-end">para o t&eacute;rmino desta oferta ou enquanto durar o estoque</span>';
    html += '<span class="btn-check"></span>';
    html += '</div>';
    html += '</div>';

    return html;
}

// Inicia o app-banners
if (checkBrand() == 'soubarato') {
    $(function () {
        initBanners();
    });
} else {
    onReady(function () { 
        initBanners();
    });
}