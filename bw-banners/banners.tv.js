/**
 * Created by Rodrigo Carneiro on 07/07/14.
 */
var APP_TV =(function() {
    var app = {};

    // Realiza chamada ajax JSONP passando uma url
    app.getAjax = function(ajaxUrl, jsonpName, callback){
        $.ajax({
            url: ajaxUrl+'&callback=?',
            jsonpCallback: jsonpName + '_' + Math.floor((Math.random() * 10000) + 1),
            cache: false,
            dataType: 'jsonp',
            success: function(json){
                callback(json);
            },
            error: function() {
                console.log('err');
            }
        });
    }

    // Inicializa o APP
    app.init = function(){
        window.onload=function(){
            alert('asdasd')
        }
    }

    return {
        app: app.init()
    }

});