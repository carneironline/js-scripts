/**
 * Created by Rodrigo Carneiro on 07/07/14.
 */
var APP_TV = (function() {
    var app = {};

    // Realiza chamada ajax JSONP passando uma url
    app.getAjax = function(ajaxUrl, jsonpName, callback){

        var useJquery = (arguments[3] === undefined) ? true : false;
        var callbackName = jsonpName + '_' + Math.floor((Math.random() * 10000) + 1);

        if(useJquery){
            $.ajax({
                url: ajaxUrl+'&callback=?',
                jsonpCallback: callbackName,
                cache: false,
                dataType: 'jsonp',
                success: function(json){
                    callback(json);
                },
                error: function() {
                    console.log('err');
                }
            });
        } else {
            var script = document.createElement('script');
            script.src = ajaxUrl + '&callback=' + callbackName;

            window[callbackName] = function (data) {
                callback(data)
            }

            document.getElementsByTagName('head')[0].appendChild(script);
        }
    }

    // Retorna os produtos da TV e gera o html
    app.getProductsTv = function(){
        var ajaxUrl = 'http://www.shoptime.com.br/produtosTV/10?timestamp=' + (+new Date());

        app.getAjax(ajaxUrl, 'produtosTvDestaque', function(data){
            app.template(data);
        }, false);
    }

    // Gera o template para cada item de produto
    app.template = function(obj){
        var objAux = (arguments[1] == typeof 'undefined') ? false : arguments[1];
        var product = obj.products;
        var html = '';

        for(i=0; i<product.length; i++){
            html += '<li>';
            html += '    <div class="app-banners"';
            html += '    data-product_id="'+product[i].id+'"';
            html += '    data-banner_class="tpl5-full"';
            html += '    data-invoice_price="false"';
            html += '    data-banner_type="5"></div>';
            html += '</li>';
        }

        var target = document.getElementById('masterBanner');
        target.getElementsByClassName("bList")[0].innerHTML = target.getElementsByClassName("bList")[0].innerHTML + html;
    }

    // Inicializa o APP
    app.init = function(){
        if(location.href.indexOf('ofertasdatv') != -1){
            app.getProductsTv();
        }
    }

    return {
        app: app.init()
    }

})();