/**
 * Created by Rodrigo Carneiro on 21/05/14.
 */

$(function(){

    $('.box').css('display', 'block');

    $('html, body').animate({
        scrollTop: $(document).height()
    }, 100, function(){
        $('#loadPage').fadeOut(3000);
    });

   $('#menu li a').on('click', function(e){
        e.preventDefault();

        var target = $(this).attr('href');
        var targetTop =  $(target).offset().top;

        $('html, body').animate({
           scrollTop: targetTop
        }, 500);

   })
});