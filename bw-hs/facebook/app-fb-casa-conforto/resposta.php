<?php
    // Verifica se o formulario foi enviado com o atributo form_sent, e encaminha
    // para a pagina principal do Shoptime caso nao tenha sido setado.
    if($_POST['form_sent'] != "true") {
        $redirect = "http://www.shoptime.com.br";         
         header("location:$redirect");
    } else {
        require_once "functions.php";

        // Recebe as respostas como uma string, separadas por virgulas e transforma em um array
        $array_respostas = explode(',', $_POST['array_respostas']);
        
        // Define o quadro de acordo com as respostas informadas
        $quadro = get_quadro($array_respostas);
        var_dump($quadro);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Casa e Conforto - Shoptime</title>

    <!-- Meta Tags -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Casa e Conforto - Shoptime" />
    
    <link rel="icon" href="img/favicon.png" />
    <link href="css/reset.min.css" rel="stylesheet" type="text/css">
    <link href="css/fonts.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="hs-resposta" id="hs-app-fb-casa-conforto">
        <header>
            <h1>Casa e Conforto - Shoptime</h1>
        </header>
        <div class="hs-content" id="hs-content-resposta">
            <div class="title-content"><h2>CONFIRA AQUI <strong>O SEU KIT IDEAL:</strong></h2></div>
        </div>
        <div class="box-images">
            <ul class="clearFix">
                <li><img alt="" src="img/resposta/image-01.jpg"></li>
                <li><img alt="" src="img/resposta/image-02.jpg"></li>
                <li><img alt="" src="img/resposta/image-03.jpg"></li>
            </ul>
        </div>
        <div class="box-buttons">
            <ul class="clearFix">
                <li><a class="comprar-kit" href="#">Comprar Kit</a></li>
                <li><a class="outros-produtos" href="#">Outros produtos</a></li>
                <li><a class="compartilhar" href="#">Compartilhar</a></li>
            </ul>
        </div>
    </div>
</body>
</html>