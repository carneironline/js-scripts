$(document).ready(function(){
    
    (function(){
        var totalAsks = $('.box-form').length,
            arrayRespostas = new Array(totalAsks),
            arrayTitleForm = [
                "Quais artigos de decoração não podem faltar em sua casa?",
                "Qual estampa você costuma usar nos enxovais da sua cama?",
                "Que tipo de iluminação você mais gosta para o quarto?",
                "Na próxima semana você receberá uma visita de um amigo, como vai deixar a casa?",
                "Você está cansada dos mesmos enxovais, o que faz para dar uma repaginada?",
                "Que tipo de cor combina mais com você?",
                "Pra você, um final de semana perfeito é?",
                "Como é a decoração do seu quarto?"
            ],
            PREVIOUS = 'prev',
            NEXT = 'next',
            currentForm = 0,
            prevForm,
            nextForm,
            possibleValuesForm = ["a", "b", "c", "d"],
            title_error_form = "Erro",
            msg_error_form = "Corrija o formulário para continuar o quiz.",
            title_confirmation_form = "Confirmação",
            msg_confirmation_form = "Deseja continuar com as opções selecionadas?";

        // Inicializa todas as respostas como undefined
        for (var i=0; i<arrayRespostas.length; i++){
            arrayRespostas[i] = undefined;
        };

        // Envio do formulario da home
        $("#form-home").submit(function(e){
            var aceito_regulamento = $("input[name=aceito_regulamento]").prop('checked');

            // Testa se o usuario clicou em aceitar o regulamento
            if(aceito_regulamento === true){
                $(this).attr("action","perguntas.php");
                
                return true;
            } else{
                var msg_title = "Atenção";
                var msg_content = "Você precisa aceitar o regulamento para começar o quiz.";

                // Exibe mensagem de erro
                showFancyMessage(msg_title, msg_content);
            }
            return false;
        });

        // Eventos dos botoes de navegacao anterior e proximo
        $(".box-nav").click(function(e){
            var action,
                box_title_form = $('.box-title-form h2'),
                msg_title,
                msg_content;
            
            // Desabilita a acao default
            e.preventDefault();

            // Define a acao escolhida
            if ($(this).hasClass('hs-prev')){
                action = PREVIOUS;
            } else{
                action = NEXT;
            }

            // Define os forms anterior e posterior
            prevForm = parseInt(currentForm - 1);
            nextForm = parseInt(currentForm + 1);

            // Acao para o envento 'anterior' ou 'proximo', caso contrario
            if(action === PREVIOUS){
                if (currentForm > 0) {
                    // Esconde o form atual
                    $('#form_' + currentForm).hide();

                    // Exibe o form anterior
                    $('#form_' + prevForm).fadeIn();

                    // Atualiza o estado dos botoes de navegacao
                    $('.hs-next').removeClass('disabled');
                    
                    // Checa se a nova resposta esta selecionada e desabilita a acao para o form anterior
                    //if (arrayRespostas[prevForm] === undefined || prevForm === 0) {
                    if (prevForm === 0) {
                        $('.hs-prev').addClass('disabled');
                    }

                    // Atualiza o titulo do form
                    box_title_form.text(arrayTitleForm[prevForm]);

                    // Adiciona uma classe para ajustar o tamanho do titulo dos forms 3 e 4
                    if (prevForm === 3 || prevForm === 4) {
                        box_title_form.addClass('title-large-text');
                    } else {
                        box_title_form.removeClass('title-large-text');
                    };

                    // Atualiza a barra de status
                    updateStatusBar(PREVIOUS);

                    // Atualiza o form atual
                    currentForm--;
                }
            } else{
                // Verifica se algum item do form atual foi marcado
                if (arrayRespostas[currentForm] !== undefined) {
                    // Verifica o form atual nao e' o ultimmo
                    if(currentForm < (totalAsks-1)){
                        // Esconde o form atual
                        $('#form_' + currentForm).hide();

                        // Incrementa o form atual e exibe o proximo form
                        $('#form_' + nextForm).fadeIn();

                        // Atualiza o estado dos botoes de navegacao
                        $('.hs-prev').removeClass('disabled');

                        // Checa se a nova resposta atual esta selecionada e desabilita a acao para o proximo form
                        if (arrayRespostas[nextForm] === undefined || nextForm === (totalAsks-1)) {
                            $('.hs-next').addClass('disabled');
                        }

                        // Atualiza o titulo do form
                        $('.box-title-form h2').text(arrayTitleForm[nextForm]);

                        // Adiciona uma classe para ajustar o tamanho do titulo dos forms 3 e 4
                        if (nextForm === 3 || nextForm === 4) {
                            box_title_form.addClass('title-large-text');
                        } else {
                            box_title_form.removeClass('title-large-text');
                        };

                        // Atualiza a barra de status
                        updateStatusBar(NEXT);

                        // Atualiza o form atual
                        currentForm++;
                    }
                }
            }
        });

        // Evento de selecao de alternativas
        $(".row-alternative").click(function(){
            var form = $(this).parent().parent();
            var form_id = form.attr('id');
            var selector = '#' + form_id + ' .row-alternative';
            
            // Define o form atual a partir do indice final do id do form
            currentForm = (form.attr('id')).split('_');
            currentForm = parseInt(currentForm[1]);

            // Verifica se uma alternativa ja esta selecionada
            if($(this).hasClass('selected')) {
                // Desmarca o item que estava selecionado
                $(this).removeClass('selected');

                // Desabilita o botao de 'proximo'
                $('.hs-next').addClass('disabled');
                
                arrayRespostas[currentForm] = undefined;
            } else {
                // Desmarca todos os itens
                $.each( $(selector), function(key, value){
                    $(value).removeClass('selected');
                });
                
                // Marca o item como selecionado
                $(this).addClass('selected');

                // Habilita o botao de proximo caso nao seja o ultimo form ou habilita o botao de responder caso contrario
                if(currentForm < (totalAsks-1)) {
                    $('.hs-next').removeClass('disabled');
                }
                
                // Guarda a resposta selecionada
                var optionValue = $(this).attr("value");

                // Determina se o valor selecionado e valido
                if (isValidOption(optionValue)){
                    // Guarda a resposta selecionada
                    arrayRespostas[currentForm] = optionValue;
                } else{
                    showFancyMessage(title_error_form, msg_error_form);
                }
            }

            // Habilita o botao de responder caso o form esteja valido para ser enviado
            if(isReadyToSubmit()) {
                $('.box-submit-button').show();
            } else {
                $('.box-submit-button').hide();
            }
        });

        // Define o evento de clicar em responder o form
        $('.submit-button').click(function(){
            if(isReadyToSubmit()) {
                showFancyMessage(title_confirmation_form, msg_confirmation_form, true);
            } else {
                showFancyMessage(title_error_form, msg_error_form);
            }
        });

        // Insere o codigo responsavel por receber o fancybox para exibicao
        var fancyBoxCode = "" +
            "<div id='app-fb-casa-conforto-box-msg' class='app-fb-casa-conforto-box-msg'>" +
                "<div class='box-fancy-title'></div>" +
                "<div class='box-fancy-content'><span></span></div>" +
                "<div class='box-fancy-buttons'>" +
                    "<a class='fancy-confirm-action' href='javascript:;'>Continuar</a>" +
                    "<a class='fancy-close-action' href='javascript:;'>Fechar</a>" +
                "</div>" +
            "</div>" +
            "<a class='fancybox' href='#app-fb-casa-conforto-box-msg'></a>";

        $(fancyBoxCode).insertAfter('.hs-content')


        // Definicao padrao da caixa de mensagem do fancybox
        $('.fancybox').fancybox({
            //'minWidth': 290,
            'minHeight': 40,
            'padding': 0,
            'closeBtn': false
        });

        // Define o evento de fechar o fancybox
        $('.fancy-close-action').click(function(){
            $.fancybox.close(true);
        });

        // Define o evento de confirmar o envio do form
        $('.fancy-confirm-action').click(function(){
            var validForm = true;
            
            // Checa se os valores a serem enviados sao validos
            for (var i=0; i<arrayRespostas.length; i++) {
                if(!isValidOption(arrayRespostas[i])){
                    validForm = false;
                }
            };

            // Checa se o form esta valido para ser enviado, ou imprime uma mensagem de erro caso contrario
            if(validForm) {
                var form = $("#form_asks");

                // Define a action do form
                form.attr('action', 'resposta.php');

                // Cria um elemento para receber as respostas do usuario e um como flag de enviado, e adicionar ao form
                $('<input>').attr({
                    type: 'hidden',
                    name: 'array_respostas',
                    value: arrayRespostas
                }).appendTo(form);

                $('<input>').attr({
                    type: 'hidden',
                    name: 'form_sent',
                    value: true
                }).appendTo(form);

                // Submete o formulario
                $(form).submit();
            } else {
                // Fecha o fancybox atual e exibe uma outra box com uma mensagem de erro
                $.fancybox.close(true);
                showFancyMessage(title_error_form, msg_error_form);
            }
        });

        // Retorna true caso a opcao informada e' valida ou false caso contrario
        function isReadyToSubmit(){
            var validForm = true;
            
            for (var i=0; i<arrayRespostas.length; i++) {
                if(!isValidOption(arrayRespostas[i])) {
                    validForm = false;
                }
            };
            
            return validForm;
        }

        // Retorna true caso a opcao informada e' valida ou false caso contrario
        function isValidOption(option){
            return $.inArray(option, possibleValuesForm) > -1;
        }

        // Funcao que exibe uma mensagem do Fancybox
        function showFancyMessage(title, content, submit){
            var fancy_confirm_action = $('.fancy-confirm-action');

            submit = typeof submit !== 'undefined' ? submit : false;

            // Define o titulo da mensagem
            $('.box-fancy-title').text(title);

            // Define o conteudo da mensagem
            $('.box-fancy-content span').text(content);

             // Insere o botao de confirmar na caixa de dialogo do fancybox se for para realizar submit
             if(submit) {
                fancy_confirm_action.show();
             } else {
                fancy_confirm_action.hide();
             }

            // Chama o fancybox
            $('.fancybox').trigger('click');
        }

        // Funcao que exibe uma mensagem do Fancybox
        function updateStatusBar(action){
            // Verifica se foi clicado o botao 'anterior' e 'proximo' caso contrario
            if(action === PREVIOUS) {
                $('.ask_' + (currentForm) + ' span').removeClass('marked');
            } else {
                $('.ask_' + (currentForm+1) + ' span').addClass('marked');
            }
        }
    })($);
});
