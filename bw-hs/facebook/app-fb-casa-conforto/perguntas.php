<?php
    // Verifica se o formulario foi enviado com o atributo form_sent, e encaminha
    // para a pagina principal do Shoptime caso nao tenha sido setado.
    if($_POST['form_sent'] != "true") {
        $redirect = "http://www.shoptime.com.br";         
         header("location:$redirect");
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Casa e Conforto - Shoptime</title>

    <!-- Meta Tags -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Casa e Conforto - Shoptime" />
    
    <link rel="icon" href="img/favicon.png" />
    <link href="css/reset.min.css" rel="stylesheet" type="text/css">
    <link href="css/fonts.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 8]>
        <link href="css/styleIE.css" rel="stylesheet" type="text/css">
    <![endif]–->
    
    <!-- Biblioteca Jquery e Script inicial -->
    <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <!-- Adiciona fancyBox arquivos JS and CSS -->
    <script type="text/javascript" src="lib/fancybox/js/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="lib/fancybox/css/jquery.fancybox.css?v=2.1.5" media="screen" />
</head>
<body>
    <div class="hs-perguntas" id="hs-app-fb-casa-conforto">
        <header>
            <h1>Casa e Conforto - Shoptime</h1>
        </header>
        <div class="hs-content" id="hs-content-perguntas">
            <form id="form_asks" method="post">
                <!-- Barra de status -->
                <div class="box-status-bar">
                    <ul class="clearFix">
                        <li class="ask_0"><span class="marked"></span></li>
                        <li class="ask_1"><span></span></li>
                        <li class="ask_2"><span></span></li>
                        <li class="ask_3"><span></span></li>
                        <li class="ask_4"><span></span></li>
                        <li class="ask_5"><span></span></li>
                        <li class="ask_6"><span></span></li>
                        <li class="ask_7"><span></span></li>
                    </ul>
                </div>

                <!-- Titulo do form -->
                <div class="box-title-form"><h2>QUAIS ARTIGOS DE DECORAÇÃO NÃO PODEM FALTAR EM SUA CASA?</h2></div>
                
                <!-- Pergunta 1 -->
                <ul class="box-form clearFix" id="form_0">
                    <li>
                        <div class="row-alternative" value="a">
                            <div class="letter-choice"><span>A</span></div>
                            <div class="content-choice"><span>Objetos de antiguidades e clássicos.  Um belo vaso deixa minha sala linda.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="b">
                            <div class="letter-choice"><span>B</span></div>
                            <div class="content-choice"><span>Objetos de antiguidades e clássicos.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="c">
                            <div class="letter-choice"><span>C</span></div>
                            <div class="content-choice"><span>Adoro objetos cheios de estilo, por exemplo, uma poltrona com design moderno.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="d">
                            <div class="letter-choice"><span>D</span></div>
                            <div class="content-choice"><span>Curto peças coloridas. No quarto, também adoro um mural com fotos de amigos e viagens!</span></div>
                        </div>
                    </li>
                </ul>
                
                <!-- Pergunta 2 -->
                <ul class="box-form clearFix" id="form_1">
                    <li>
                        <div class="row-alternative" value="a">
                            <div class="letter-choice"><span>A</span></div>
                            <div class="content-choice"><span>Florida.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="b">
                            <div class="letter-choice"><span>B</span></div>
                            <div class="content-choice"><span>Não gosto muito de estampas, então quase todos são lisos.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="c">
                            <div class="letter-choice"><span>C</span></div>
                            <div class="content-choice"><span>Geométricas.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="d">
                            <div class="letter-choice"><span>D</span></div>
                            <div class="content-choice"><span>Misturo desde floral até estampas temáticas, sempre com muito estilo.</span></div>
                        </div>
                    </li>
                </ul>

                <!-- Pergunta 3 -->
                <ul class="box-form clearFix" id="form_2">
                    <li>
                        <div class="row-alternative" value="a">
                            <div class="letter-choice"><span>A</span></div>
                            <div class="content-choice"><span>Colorida,  é claro!</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="b">
                            <div class="letter-choice"><span>B</span></div>
                            <div class="content-choice"><span>Discreta.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="c">
                            <div class="letter-choice"><span>C</span></div>
                            <div class="content-choice"><span>Com lustres de cristal.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="d">
                            <div class="letter-choice"><span>D</span></div>
                            <div class="content-choice"><span>Quer iluminação mais linda que a da natureza? Abro a janela e deixo o Sol entrar.</span></div>
                        </div>
                    </li>
                </ul>

                <!-- Pergunta 4 -->
                <ul class="box-form clearFix" id="form_3">
                    <li>
                        <div class="row-alternative" value="a">
                            <div class="letter-choice"><span>A</span></div>
                            <div class="content-choice"><span class="large-text">Linda! Momento perfeito para usar minhas tradicionais louças, talheres de prata e, claro, as taças para um bom vinho. Afinal, meu amigo merece muito requinte.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="b">
                            <div class="letter-choice"><span>B</span></div>
                            <div class="content-choice"><span>Mudo a posição de alguns móveis.  Coloco no meu IPod o som da nossa banda favorita.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="c">
                            <div class="letter-choice"><span>C</span></div>
                            <div class="content-choice"><span>Não faço nada. Gosto do meu estilo despojado e meu amigo também.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="d">
                            <div class="letter-choice"><span>D</span></div>
                            <div class="content-choice"><span class="large-text">Vou espalhar algumas flores para perfumar a casa. Se ele for passar a noite, coloco um enxoval delicado no quarto de hóspedes.</span></div>
                        </div>
                    </li>
                </ul>

                <!-- Pergunta 5 -->
                <ul class="box-form clearFix" id="form_4">
                    <li>
                        <div class="row-alternative" value="a">
                            <div class="letter-choice"><span>A</span></div>
                            <div class="content-choice"><span class="large-text">Normalmente não faço mudanças radicais. Talvez algumas colchas aconchegantes, com 200 fios, darão um novo astral ao quarto.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="b">
                            <div class="letter-choice"><span>B</span></div>
                            <div class="content-choice"><span>Se é para mudar, então escolho logo os últimos lançamentos. Modernidade é meu sobrenome!</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="c">
                            <div class="letter-choice"><span>C</span></div>
                            <div class="content-choice"><span>Novas peças delicadas e com um toque suave ficarão perfeitas no meu quarto.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="d">
                            <div class="letter-choice"><span>D</span></div>
                            <div class="content-choice"><span>Vou escolher enxovais que retratam bem minha personalidade, ou seja, divertida e despojada.</span></div>
                        </div>
                    </li>
                </ul>

                <!-- Pergunta 6 -->
                <ul class="box-form clearFix" id="form_5">
                    <li>
                        <div class="row-alternative" value="a">
                            <div class="letter-choice"><span>A</span></div>
                            <div class="content-choice"><span class="large-text">Gosto da combinação branco e preto. Também não me importo em ousar na cor! Objetos de tons coloridos e de traços dinâmicos deixam a casa divertida.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="b">
                            <div class="letter-choice"><span>B</span></div>
                            <div class="content-choice"><span>Gosto de amarelo, laranja e vermelho. Cores cheias de energia!</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="c">
                            <div class="letter-choice"><span>C</span></div>
                            <div class="content-choice"><span>As cores escuras! Elas deixam o ambiente com um ar clássico.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="d">
                            <div class="letter-choice"><span>D</span></div>
                            <div class="content-choice"><span>Tons pastel e suaves, por exemplo,  nude e lilás.</span></div>
                        </div>
                    </li>
                </ul>

                <!-- Pergunta 7 -->
                <ul class="box-form clearFix" id="form_6">
                    <li>
                        <div class="row-alternative" value="a">
                            <div class="letter-choice"><span>A</span></div>
                            <div class="content-choice"><span>Ir a uma exposição de antiguidades.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="b">
                            <div class="letter-choice"><span>B</span></div>
                            <div class="content-choice"><span>Chamar a família e os amigos para um reunião intimista na sua casa.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="c">
                            <div class="letter-choice"><span>C</span></div>
                            <div class="content-choice"><span>Ir a uma exposição de arte moderna.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="d">
                            <div class="letter-choice"><span>D</span></div>
                            <div class="content-choice"><span>Viajar com os amigos.</span></div>
                        </div>
                    </li>
                </ul>

                <!-- Pergunta 8 -->
                <ul class="box-form clearFix" id="form_7">
                    <li>
                        <div class="row-alternative" value="a">
                            <div class="letter-choice"><span>A</span></div>
                            <div class="content-choice"><span>Clássica e refinada.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="b">
                            <div class="letter-choice"><span>B</span></div>
                            <div class="content-choice"><span>Romântica e suave.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="c">
                            <div class="letter-choice"><span>C</span></div>
                            <div class="content-choice"><span>Jovem e despojada.</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="row-alternative" value="d">
                            <div class="letter-choice"><span>D</span></div>
                            <div class="content-choice"><span>Moderna e cheia de estilo.</span></div>
                        </div>
                    </li>
                </ul>
                <div class="box-submit-button"><a class="submit-button" href="javascript:;">RESPONDER</a></div>

                <!-- Botoes de navegacao anterior e proximo -->
                <a href="javascript:;" class="box-nav hs-prev disabled"></a>
                <a href="javascript:;" class="box-nav hs-next disabled"></a>
            </form>
        </div>
    </div>
</body>
</html>