<?php
/**
 * Retorna o quadro de acordo com as respostas informada.
 *
 * @param $asks
 * @return string
 */
function get_quadro($asks){
    $quadro = array(
        'a' => 0,
        'b' => 0,
        'c' => 0,
        'd' => 0
    );

    /* Pergunta 1 */
    switch ($asks[0]) {
        case 'a':
            $quadro['a']++;
            break;

        case 'b':
            $quadro['b']++;
            break;

        case 'c':
            $quadro['c']++;
            break;

        case 'd':
            $quadro['d']++;
            break;
    }

    /* Pergunta 2 */
    switch ($asks[1]) {
        case 'a':
            $quadro['b']++;
            break;

        case 'b':
            $quadro['d']++;
            break;

        case 'c':
            $quadro['c']++;
            break;

        case 'd':
            $quadro['a']++;
            break;
    }

    /* Pergunta 3 */
    switch ($asks[2]) {
        case 'a':
            $quadro['c']++;
            break;

        case 'b':
            $quadro['b']++;
            break;

        case 'c':
            $quadro['a']++;
            break;

        case 'd':
            $quadro['d']++;
            break;
    }

    /* Pergunta 4 */
    switch ($asks[3]) {
        case 'a':
            $quadro['a']++;
            break;

        case 'b':
            $quadro['c']++;
            break;

        case 'c':
            $quadro['d']++;
            break;

        case 'd':
            $quadro['b']++;
            break;
    }

    /* Pergunta 5 */
    switch ($asks[4]) {
        case 'a':
            $quadro['a']++;
            break;

        case 'b':
            $quadro['c']++;
            break;

        case 'c':
            $quadro['b']++;
            break;

        case 'd':
            $quadro['d']++;
            break;
    }

    /* Pergunta 6 */
    switch ($asks[5]) {
        case 'a':
            $quadro['c']++;
            break;

        case 'b':
            $quadro['d']++;
            break;

        case 'c':
            $quadro['a']++;
            break;

        case 'd':
            $quadro['b']++;
            break;
    }

    /* Pergunta 7 */
    switch ($asks[6]) {
        case 'a':
            $quadro['a']++;
            break;

        case 'b':
            $quadro['b']++;
            break;

        case 'c':
            $quadro['c']++;
            break;

        case 'd':
            $quadro['d']++;
            break;
    }

    /* Pergunta 8 */
    switch ($asks[7]) {
        case 'a':
            $quadro['a']++;
            break;

        case 'b':
            $quadro['b']++;
            break;

        case 'c':
            $quadro['d']++;
            break;

        case 'd':
            $quadro['c']++;
            break;
    }

    // Determina qual opcao foi mais selecionada
    $maxs = array_keys($quadro, max($quadro));

    // Retorna apenas o primeiro resultado
    return $maxs[0];
}
?>
