<!DOCTYPE html>
<html>
<head>
    <title>Casa e Conforto - Shoptime</title>

    <!-- Meta Tags -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Casa e Conforto - Shoptime" />

    <link rel="icon" href="img/favicon.png" />
    <link href="css/reset.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    
    <!-- Biblioteca Jquery e Script inicial -->
    <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

    <!-- Adiciona fancyBox arquivos JS and CSS -->
    <script type="text/javascript" src="lib/fancybox/js/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="lib/fancybox/css/jquery.fancybox.css?v=2.1.5" media="screen" />
</head>
<body>
    <div class="hs-home" id="hs-app-fb-casa-conforto">
        <header>
            <h1>Casa e Conforto - Shoptime</h1>
        </header>
        <div class="hs-content" id="hs-content-home">
            <form id="form-home" method="post">
                <input type="hidden" name="form_sent" value="true"/>
                <ul class="clearFix">
                    <li class="row-regulamento"><input name="aceito_regulamento" type="checkbox"><span>Li e aceito o <a href="#">regulamento</a></span></li>
                    <li><input class="input-submit" type="submit" value="RESPONDER AO QUIZ"></li>
                </ul>
            </form>
        </div>
    </div>
</body>
</html>