<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Content-Type: application/json');

if(isset($HTTP_RAW_POST_DATA)) {
	$parts = explode('&', $HTTP_RAW_POST_DATA);
	
	foreach ( $parts as $part ) {
	    list($key, $value) = explode('=', $part, 2);
	    $_POST[$key] = $value;
	}
	
	print_r($_POST);
}
else if (empty($_POST))
{
	// SE NAO TIVER DADOS, DÁ ERRO E MORRE
	$json['valor']=0;
	$json['txt']='Nenhum dado enviado';
	echo json_encode($json);
	die;
}

// VALORES PARA ERRO NO BANCO

$json['valor']=0;
$json['txt']='Problema para gravar no banco de dados';

include("../../conectar_bd_sorteios.php");

function limpar($numero)
{
	if (empty($numero)){return "";}
	$tirar=array('-', ',', '_', ' ', '/', '.');
	$numero=str_replace($tirar, '', $numero);
	return $numero;
}

$banco['hs-torcer-para-ganhar-acom-visa']='001';
$banco['hs-torcer-para-ganhar-acom-sony']='002';
$banco['hs-torcer-para-ganhar-shop-sony']='003';
$banco['hs-torcer-para-ganhar-suba-sony']='004';


if (empty($_POST['sorteio']) OR empty($banco[$_POST['sorteio']]))
{
	// JÁ EXISTE
	$json['valor']=0;
	$json['txt']='Este Sorteio não existe';
	echo json_encode($json);
	die;
}


$n_bd=$banco[$_POST['sorteio']];

// VERIFICAR SE ESSE CPF NÃO EXISTE NO BANCO
$query=mysql_query("SELECT cpf FROM ".$n_bd."_login WHERE cpf='".addslashes(limpar($_POST['cpf']))."'");
if (mysql_num_rows($query)>0)
{
	// JÁ EXISTE
	$json['valor']=0;
	$json['txt']='Este CPF já está cadastrado em nossa base de dados';
	echo json_encode($json);
	die;
}


// GRAVAR. O CAMPO GET VAI GRAVAR A REQUISIÇÃO POR VIA DAS DÚVIDAS.
$gravar = mysql_query ("
	INSERT INTO ".$n_bd."_login (nome, cpf, cep, telefone, nascimento, endereco, estado, email, data, ip, GET) 
	VALUES ('".addslashes($_POST['nome'])."', '".addslashes(limpar($_POST['cpf']))."', '".addslashes(limpar($_POST['cep']))."', '".addslashes(limpar($_POST['telefone']))."', '".addslashes($_POST['nascimento'])."', '".addslashes($_POST['endereco'])."', '".addslashes($_POST['estado'])."', '".addslashes($_POST['email'])."', now(), '".addslashes($_SERVER ["REMOTE_ADDR"])."', '".addslashes(json_encode($_POST))."')") OR DIE (json_encode($json));
	
$json['valor']=1;
$json['txt']='Seus dados foram gravados com sucesso';
echo json_encode($json);
