<?php
include_once('acom-hs-tpl/header.php');

include_once('config.php');
?>

<!--BEGIN HS-->

<link rel="stylesheet" type="text/css" href="<?php echo PATHURL?>css/style.css?1.3"/>

<div class="hs-dia-dos-pais">
	<div class="hs-top-navigation">
		<div class="box-top-social-media">
			<ul class="clearFix">
				<li>
                    <div class="fb-like" data-href="http://www.americanas.com.br/pais-presente-certo/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
				</li>
	    		<li>
	    			<div class="g-plusone" data-size="medium" data-annotation="none"></div>
			        <!-- Place this tag after the last +1 button tag. -->
			        <script type="text/javascript">
			          window.___gcfg = {lang: 'pt-BR'};
			        
			          (function() {
			            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			            po.src = 'https://apis.google.com/js/platform.js';
			            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			          })();
			        </script>
	    		</li>
	    		<li style="width: 90px;">
	    			<a href="https://www.twitter.com/americanascom" class="twitter-share-button" data-lang="en">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	    		</li>
			</ul>
		</div>
	</div><!-- FIM hs-menu-top -->

	<div class="hs-content">