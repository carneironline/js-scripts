<?php
include_once ('shop-hs-tpl/header.php');

include_once('config.php');
?>

<link rel="stylesheet" type="text/css" href="<?php echo PATHURL?>css/style.css"/>
<div class="hs-dia-das-maes">
	<div class="hs-top-navigation">
		<div class="box-top-menu">
			<ul class="clearFix">
				<li>
					<a class="menu-top-home" href="<?php echo $menu['home']; ?>">Home</a>
				</li>
				<li><img src="<?php echo PATHURL?>img/menu-top-separator.jpg" alt=""/>
				</li>
				<li>
					<a class="menu-top-produtos selected" href="<?php echo $menu['produtos']; ?>">Produtos</a>
				</li>
			</ul>
		</div>
		<div class="box-top-social-media">
			<ul class="clearFix">
				<li><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fshoptime&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:20px; width:51px;" allowTransparency="true"></iframe></li>
	    		<li>
	    			<div class="g-plusone" data-size="medium" data-annotation="none"></div>
			        <!-- Place this tag after the last +1 button tag. -->
			        <script type="text/javascript">
			          window.___gcfg = {lang: 'pt-BR'};
			        
			          (function() {
			            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			            po.src = 'https://apis.google.com/js/platform.js';
			            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			          })();
			        </script>
	    		</li>
	    		<li style="width: 90px;">
	    			<a href="https://www.twitter.com/shoptime" class="twitter-share-button" data-lang="en">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	    		</li>
			</ul>
		</div>
	</div><!-- FIM hs-menu-top -->
	<div class="hs-header colorful">
		<h1>No Dia das M�es do Shoptime, sua m�e faz a lista e voc� acerta no presente!</h1>
		<img src="<?php echo PATHURL?>img/header-top-produtos-passo2.png" alt=""/>
	</div><!-- FIM hs-header -->

	<div class="hs-content internals colorful">
