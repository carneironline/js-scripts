<?php
define('DEBUG', true); //@boolean if false == production

if(DEBUG) :
	//Path base para os arquivos
	define('PATHURL', 'http://localhost/rodrigo/Estudos/js-scripts/bw-hs/acom/hs-diadospais/');
	
	$menu = array(
		'home'=>'home.php', 
		'produtos'=>'produtos.php'
	);

	$actionPasso1 = PATHURL.'produtos-passo-2.php';
	$actionPasso2 = PATHURL.'produtos-passo-3.php';

else :
	//Path base para os arquivos
	define('PATHURL', 'http://img.americanas.com.br/mktacom/hotsite/hs-diadospais2014/');
	
	$menu = array(
		'home'=>'http://www.americanas.com.br/pais-presente-certo',
		'produtos'=>'http://www.americanas.com.br/pais-presente-certo/dia-dos-pais-passo1'
	);
	
	$actionPasso1 = 'http://www.americanas.com.br/pais-presente-certo/dia-dos-pais-passo2';
	$actionPasso2 = 'http://www.americanas.com.br/pais-presente-certo/dia-dos-pais-passo3';
	
endif;