<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width" />
		<title>Americanas.com </title>
		<meta charset="UTF-8" />
		<script type="text/javascript">
			var NREUMQ = NREUMQ || [];
			NREUMQ.push(["mark", "firstbyte", new Date().getTime()]);
		</script>
		<link href="http://iacom.s8.com.br/statics-1.62.4.2626/css/acom_components.css" rel="stylesheet" type="text/css" media="all" />
		<!--[if lte IE 7]><link href="http://iacom.s8.com.br/statics-1.62.4.2626/css/acom_components_IE.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
		<link href="http://iacom.s8.com.br/statics-1.62.4.2626/catalog/css/catalog.css" rel="stylesheet" type="text/css" media="all" />
		<!--[if lte IE 7]><link href="http://iacom.s8.com.br/statics-1.62.4.2626/catalog/css/catalog_IE.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->

		<link media="only screen and (max-width: 640px)" href="http://m.americanas.com.br" />
		<link media="handheld" href="http://m.americanas.com.br" />

		<script type="text/javascript">
			// <![CDATA[
			var readyEvents = []
			var exceptionEvents = []
			function onReady(f) {
				readyEvents.push(f)
			}

			function exceptionEvent(f) {
				exceptionEvents.push(f)
			}

			function handleException(x) {
				for ( i = 0; i < exceptionEvents.length; i++) {
					exceptionEvents[i](x);
				}
			}

			exceptionEvent(function(f) {
				if ( typeof (console) != "undefined")
					console.debug(f);
			})

			document.getElementsByTagName("html")[0].className = "js" + ((window.location.hash.indexOf("redir") > 0) ? ' redir' : '');

			// ]]>
		</script>
		<link href="http://img.americanas.com.br/catalog/css/secondary.css" rel="stylesheet" type="text/css" media="all">
		<!--[if IE]><link href="http://img.americanas.com.br/catalog/css/secondary_IE.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
		<link rel="stylesheet" type="text/css" href="http://img.americanas.com.br/catalog/skins/Null/css/skinHeaderFooter.css" media="all" />
		<meta name="robots" content="noindex,follow">

		<meta http-equiv="x-ua-compatible" content="IE=9" >
		<!--TESTE 123-->
		<link rel="stylesheet" href="http://iacom.s8.com.br/mktacom/apps/v0.1.7/styles/main.css"/>
		<script type="text/javascript">
			var APP_DEBUG = true;
			var OAS_url = 'http://oas.americanas.com.br/RealMedia/ads/adstream_mjx.ads/';
			//Apenas para desenvolvimento
		</script>

		<script src="http://iacom.s8.com.br/mktacom/apps/v0.1.7/scripts/app.js"></script>
		<script src="http://iacom.s8.com.br/mktacom/apps/v0.1.7/scripts/americanas.js"></script>

		<script>
			App.on('load', function() {
				var App = this, $ = this.$, _ = this._;

				oas.on('print:end', function() {
					if ($.cookie("crhome") != "true") {
						$('#AbreCRPorteiro').fancybox({
							width : 650,
							height : 305,
							type : 'ajax',
							afterLoad : function() {
								$.cookie('crhome', true, {
									expires : 1,
									path : '/'
								});
								setTimeout(function() {
									var form = $("#lightboxCrPorteiro");
									$.app.trigger('run:cr', form);
									form.find('> .app-cr').one('complete', function() {
										$.cookie('crhome', true, {
											expires : 365,
											path : '/'
										});
										var self = $(this);
										self.one('click', '.cr-msg', function(a, b, c) {
											$.fancybox.close();
										});
									});
								}, 500)
							}
						}).click();
					}

					if ($.fn.fancybox) {
						var link = $('#abreCRPorteiro').eq(0);
						var conf = {
							width : 520,
							height : 250,
							type : 'ajax'
						};
						conf[( typeof $.fn.fancybox.version === "string" ? 'afterLoad' : 'onComplete')] = function() {
							setTimeout(function() {
								var form = $(".lightboxPorteiro");
								$.app.trigger('run:cr', form);
								form.find('> .app-cr').one('complete', function() {
									var self = $(this);
									self.one('click', '.cr-msg', function() {
										$.fancybox.close();
									});
								});
							}, 500)
						};
						link.fancybox(conf).click();
					}
				});
			});
		</script>

		<script type="text/javascript">
			try {

				OAS_url = 'http://oas.submarino.com.br/RealMedia/ads/';
				OAS_listpos = 'Top2,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,BottomLeft,BottomRight';
				OAS_query = 'estatica/Hotsite-moda-slim';
				OAS_sitepage = 'americanas/home';

				OAS_version = 10;
				OAS_rn = '001234567890';
				OAS_rns = '1234567890';
				OAS_rn = new String(Math.random());
				OAS_rns = OAS_rn.substring(2, 11);

			} catch(exception) {
				handleException(exception);
			}

			function OAS_NORMAL(pos) {
				document.write('<A HREF="' + OAS_url + 'click_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + OAS_query + '" TARGET=_top>');
				document.write('<IMG SRC="' + OAS_url + 'adstream_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + OAS_query + '" BORDER=0></A>');
			}
		</script>

		<script type="text/javascript">
			try {
				OAS_version = 11;
				if (navigator.userAgent.indexOf('Mozilla/3') != -1)
					OAS_version = 10;
				if (OAS_version >= 11)
					document.write('<SCR' + 'IPT LANGUAGE=JavaScript1.1 SRC="' + OAS_url + 'adstream_mjx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '?' + OAS_query + '"></SC' + 'RIPT>');
			} catch(exception) {
				handleException(exception);
			}

		</script>

		<script type="text/javascript">
			try {
				function OAS_AD(pos) {
					if (OAS_version >= 11)
						OAS_RICH(pos);
					else
						OAS_NORMAL(pos);
				}

			} catch(exception) {
				handleException(exception);
			}

		</script>

	</head>

	<body>

		<p class="hc">
			<a rel="nofollow" href="#inicio" accesskey="4">você está no início</a>
		</p>
		<div id="page">
			<p class="hc">
				<a rel="nofollow" href="#conteudo">ir para o conteúdo <strong>(atalho + 1)</strong></a>
			</p>
			<div id="header">
				<script src="http://img.americanas.com.br/catalog/skins/inov/arquivos/mobileredirect.js"></script>
				<link rel="stylesheet" href="//img.americanas.com.br/statics-1.62.4.2625/catalog/css/header.min.css"/>
				<style>
					.list-product-linhas {
						margin-bottom: 20px;
					}
					.pure-g {
						display: block !important;
					}
					.a-menu-sanzonal .ams-item:first-child, .a-menu-sanzonal .oferta-do-dia {
						max-width: none;
					}
					.comparator-itens {
						display: none;
					}
					.paginado .comparator-itens {
						display: block;
					}
					.highlight-dpto .top-area-product {
						overflow: hidden;
					}
				</style>
				<header id="a-header" role="banner">
					<div class="a-top-header">
						<div class="banner-top-header">
							<script type="text/javascript">
								try {
									OAS_AD("x71")
								} catch (exp) {
								}
							</script>
						</div>
						<ul class="menu-top-header">
							<li class="mth-item">
								televendas: <strong>4003-4848</strong>
							</li>
							<li class="mth-item">
								<a href="http://www.americanas.com.br/central-de-atendimento?WT.mc_id=internas-atendimentoHome&amp;WT.mc_ev=click"> atendimento </a>
							</li>
							<li class="mth-item">
								<a href="https://carrinho.americanas.com.br/ControlPanelWeb/panel/AllOrders/"> meus pedidos </a>
							</li>
							<li class="mth-item">
								<a href="https://carrinho.americanas.com.br/CustomerWeb/pages/home"> minha conta </a>
							</li>
							<li class="mth-item">
								<a href="http://www.americanas.com.br/estatica-lasa/lasa?DCSext.menu=lojas"> loja mais próxima </a>
							</li>
						</ul>
					</div>
					<div class="a-content-header">
						<div class="header-control">
							<a href="/"><h1 class="spt-logo h-logo">americanas.com</h1></a>
							<div class="h-cart">
								<span class="spt-cesta"></span>
								<span class="cart-itens"> <a href="https://carrinho.americanas.com.br/checkout" class="ci-label">minha cesta</a>
									<div hidden="hidden">
										<div id="template-totalProducts">
											<span class="ci-titens">{{ numberProducts }}</span> {{ numberProductsText }} <span class="spt-seta"></span>
										</div>
									</div> <span class="ci-total"> <span class="ci-titens">0</span> items <span class="spt-seta"></span> </span> <span hidden="hidden" class="hidden-area cart-content-opened">
										<div class="cart-empty">
											<strong class="cart-empty-title">sua cesta esta vazia</strong>

										</div>
										<script type="template/text" id="template-cart-products">
											<ul class="cart-products-list">
											{{#products}}
											<li class="cart-products-item">
											<img src="{{ image }}" class="cart-product-photo"/>
											<span class="cart-product-title">
											<a href="/produto/{{ id }}">
											{{ name }}
											</a>
											</span>
											<span class="cart-product-price">
											<del class="ctp-from">R$ {{ price }}</del>
											<span class="ctp-sale">R$ {{ discountedPrice }}</span>
											</span>
											</li>
											{{/products}}
											</ul>
											{{#showLinkCart}}
											<div class="cart-all-products">
											<a href="https://carrinho.americanas.com.br/checkout/"> ver todos os {{ numberProducts }} produtos da cesta</a>
											</div>
											{{/showLinkCart}}
											<div class="cart-price">
											<strong class="box-cart-price">
											total:
											<span class="cart-price-subtotal">R$ {{ subtotal }}</span>
											</strong>

											<a href="https://carrinho.americanas.com.br/checkout/" class="spt-ir-cesta"></a>
											</div>
										</script> <div class="cart-not-empty">

										</div> </span> </span>
							</div>
							<div class="h-user-login">
								<span class="spt-smile"></span>
								<div class="user-area">
									<div id="user-logged">
										<span class="user-welcome">olá <span id="acomNick"></span>,</span>
										<span class="user-menu"> <a href="https://carrinho.americanas.com.br/CustomerWeb/pages/Logout">sair</a> </span>
									</div>
									<div id="user-not-logged">
										<a href="https://carrinho.americanas.com.br/CustomerWeb/pages/Login/">olá, faça seu <span class="it-user-log">login</span> ou <span class="it-user-log">cadastre-se</span> </a>
									</div>

								</div>
							</div>
							<div class="h-search">
								<div class="ginput">
									<form action="http://busca.americanas.com.br/busca.php">
										<input type="search" class="input-search" name="q" placeholder="buscar" data-position="header"/>
										<input type="submit" class="spt-lupa-buscar" data-position="header"/>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="a-bottom-header">

						<ul class="a-menu-sanzonal">

							<li class="ams-item">
								<div class="a-menu-header">
									<span class="mh-label">compre por departamento <span class="spt-seta-menu"></span></span>
									<nav class="abh-menu-header" role="navigation">
										<!-- <ul class="mh-list list-top">
										<li class="mh-item bfr-arw">
										<a href="#"><span class="spt-seta-submenu"></span>mais vendidos</a>
										</li>
										<li class="mh-item bfr-arw">
										<a href="#"><span class="spt-seta-submenu"></span>lançamentos</a>
										</li>
										<li class="mh-item bfr-arw">
										<a href="#"><span class="spt-seta-submenu"></span>ofertas especiais</a>
										</li>
										</ul> -->
										<ul class="mh-list list-depts">

											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/229187/celulares-e-telefones">celulares e telefones<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/229187/celulares-e-telefones" class="a-title">celulares e telefones</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/350392/celulares-e-telefones/smartphone">Smartphones</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/345399/celulares-e-telefones/iphone">iPhone</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/345396/celulares-e-telefones/samsung-galaxy">Samsung Galaxy</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/345489/celulares-e-telefones/nokia-lumia">Nokia Lumia</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/350373/celulares-e-telefones/smartphone/smartphone-multichips">Smartphone Multichip</a>
																	</li>
																</ul>

															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/229196/celulares-e-telefones/celular">Celulares</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/263550/celulares-e-telefones/celular/celular-dual-chip-2-chips">Dual chip</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/263568/celulares-e-telefones/celular/celular-tri-chip-3-chips">Tri Chip</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/263608/celulares-e-telefones/acessorios-para-celular">Acessórios para celular</a>
																		<ul class="a-snl-sub">
																			<li>
																				<a href="http://www.americanas.com.br/sublinha/263569/celulares-e-telefones/acessorios-para-celular/capas-peliculas">Capas / Peliculas</a>
																			</li>
																		</ul>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/264410/celulares-e-telefones/nextel">Nextel</a>
																	</li>
																</ul>
															</li>
														</ul>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<span class="pooler">TelefonIa fixa</span>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/263555/celulares-e-telefones/telefone-sem-fio">Telefone sem fio</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/263559/celulares-e-telefones/telefone-com-fio">Telefone com fio</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/229187/celulares-e-telefones">Veja mais Celulares e Telefones</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x77")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/228190/informatica">Informática e Tablets<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">

														<div class="pure-g">
															<div class="pure-u-1-2">
																<a href="http://www.americanas.com.br/loja/228190/informatica" class="a-title">informática</a>
																<ul class="a-sn-list">
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/linha/267868/informatica/notebook">notebook</a>
																	</li>
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/linha/348528/informatica/ultrabook">ultrabook</a>
																	</li>
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/linha/267908/informatica/tablets-e-ipad">Tablet</a>
																		<ul class="a-snl-sub">
																			<li>
																				<a href="http://www.americanas.com.br/sublinha/267865/informatica/apple/ipad">iPad</a>
																			</li>
																			<li>
																				<a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet-samsung?f_268761=samsung">Tablet Samsung</a>
																			</li>
																		</ul>
																	</li>
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/linha/267889/informatica/computadores-e-all-in-one">Computadores</a>
																	</li>
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/linha/267854/informatica/multifuncionais">Multifuncionais</a>
																	</li>
																</ul>
															</div>
															<div class="pure-u-1-2">
																<a href="http://www.americanas.com.br/loja/228098/informatica-acessorios" class="a-title">Informática e acessórios</a>
																<ul class="a-sn-list">
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/sublinha/268389/informatica/armazenamento/hd-externo">HD externo</a>
																	</li>
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/sublinha/268370/informatica/armazenamento/pen-drives">Pen drive</a>
																	</li>
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/sublinha/268385/informatica/equipamentos-de-rede-wireless/roteador">Roteadores</a>
																	</li>
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/linha/267888/informatica/acessorios-para-notebook">Acessórios para notebook</a>
																	</li>
																	<li class="a-snl-item">
																		<a href="http://www.americanas.com.br/sublinha/267863/informatica/tablets-e-ipad/acessorios-para-tablets">Acessórios para tablet</a>
																	</li>
																	<li class="a-snl-item">
																		<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/228190/informatica">Veja mais Informática</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x78")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/227707/tv-e-home-theater">TVs, Áudio e Home Theater<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/227707/tv-e-home-theater" class="a-title">TVs, Áudio e Home Theater</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/262909/tv-e-home-theater/tv">TV</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/262909/tv-e-home-theater/tv-smart-tv-3d-smart-tv?f_357593=smart+tv+3d-smart+tv">Smart TV</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/262870/tv-e-home-theater/tv/tv-led">TV LED</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/262909/tv-e-home-theater/tv-smart-tv-3d-3d?f_357593=smart+tv+3d-3d">TV 3D</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/262871/tv-e-home-theater/tv/tvs-de-plasma">TV de Plasma</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/357292/tv-e-home-theater/suporte-para-tv">Suporte para parede</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/loja/256408/audio">Áudio</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/262429/audio/micro-system">Micro system</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/262431/audio/som-portatil">Som portátil</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/262488/audio/ipod-e-acessorios">Ipod e acessórios</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/262453/audio/fones-de-ouvido">Fone de ouvido</a>
																	</li>
																</ul>
															</li>
														</ul>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/263029/tv-e-home-theater/home-theater">home theater</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/263030/tv-e-home-theater/home-theater/home-theater-blu-ray-3d">home theater blu-ray 3d</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/263048/tv-e-home-theater/home-theater/home-theater-dvd-player">home theater dvd player</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227707/tv-e-home-theater">veja mais tvs, áudio e home theater</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x79")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/227644/eletrodomesticos">Eletrodomésticos<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/227644/eletrodomesticos" class="a-title">Eletrodomésticos</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/316788/eletrodomesticos/geladeiras-refrigeradores">Geladeiras / Refrigeradores</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/316689/eletrodomesticos/fogao">Fogões</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/316848/eletrodomesticos/cooktop">Cooktop</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/sublinha/317052/eletrodomesticos/forno/forno-de-embutir">Forno de embutir</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/sublinha/317050/eletrodomesticos/coifa-e-depurador/coifas">Coifas</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/316828/eletrodomesticos/micro-ondas">Micro-ondas</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/316829/eletrodomesticos/adega-de-vinho">adega de vinho</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/316690/eletrodomesticos/lava-loucas">Lava-louças</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/316808/eletrodomesticos/lavadora-de-roupa-e-tanquinho">Lavadora de roupas</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/316691/eletrodomesticos/secadora-de-roupa-e-centrifuga">Secadora de roupas</a>
															</li>
														</ul>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/loja/317728/ar-condicionado-e-aquecedores">Ar condicionado e Aquecedor</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/317752/ar-condicionado-e-aquecedores/ar-condicionado/ar-condicionado-split">Ar condicionado split</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/317750/ar-condicionado-e-aquecedores/ar-condicionado/ar-condicionado-janela">Ar condicionado de janela</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/317789/ar-condicionado-e-aquecedores/climatizador-de-ar">Climatizador de ar</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227644/eletrodomesticos">Veja mais Eletrodomésticos</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x80")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/227763/eletroportateis">Eletroportáteis<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/227763/eletroportateis" class="a-title">Eletroportáteis</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278268/eletroportateis/bebedouros-e-purificadores">Bebedouros e Purificadores</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278248/eletroportateis/batedeiras">Batedeiras</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278191/eletroportateis/cafeteiras-e-chaleiras">Cafeteiras</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278192/eletroportateis/centrifugas-e-espremedores-de-fruta">Centrífugas e Espremedores de fruta</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278196/eletroportateis/forno-eletrico">Forno elétrico</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278252/eletroportateis/grill-sanduicheira-e-torradeira">Grill, Sanduicheiras e Torradeiras</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278159/eletroportateis/panificadora-maquina-de-pao">Panificadoras</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278197/eletroportateis/liquidificadores">Liquidificadores</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278190/eletroportateis/aspiradores-e-vassouras">Aspiradores de pó e Vassoura elétrica</a>
															</li>
														</ul>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278250/eletroportateis/ferro-de-passar">Ferro de passar</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278257/eletroportateis/ventiladores-e-circuladores-de-ar">Ventiladores e Circuladores de ar</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278158/eletroportateis/panelas-eletricas">Panela elétrica</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/278198/eletroportateis/maquinas-de-costura">Máquina de costura</a>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227763/eletroportateis">Veja mais Eletroportáteis</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x81")
															} catch (exp) {
															}
														</script>

													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/227109/brinquedos">Brinquedos e Bebês<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/227109/brinquedos" class="a-title">Brinquedos e Bebês</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/loja/227109/brinquedos">Brinquedos</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/279688/brinquedos/bonecas">Bonecas</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/279649/brinquedos/bonecos">Bonecos</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/279748/brinquedos/brinquedos-eletronicos">Brinquedos eletrônicos</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/279690/brinquedos/controle-remoto">Controle remoto</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/279674/brinquedos/mini-veiculos">Mini veículos</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/279670/brinquedos/bicicleta-infantil">Bicicleta infantil</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/279695/brinquedos/quebra-cabeca">Quebra-cabeça</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/279657/brinquedos/playground">Playground</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/280068/brinquedos/esportes/bolas">Bola de futebol</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/279698/brinquedos/praia-e-piscina">Praia e Piscina</a>
																	</li>
																</ul>
															</li>
														</ul>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/loja/226940/bebes">Bebês</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/sublinha/272687/bebes/passeio/carrinhos-de-passeio">Carrinho de passeio</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/272169/bebes/bercario">Berçario</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/272228/bebes/brinquedos-para-bebe">Brinquedos para bebê</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227109/brinquedos">Veja mais Brinquedos</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x83")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/226762/games">Games, Música e Filmes<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/226762/games" class="a-title">Games</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/347990/games/console-xbox-one">Xbox one</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/351258/games/jogos-xbox-one">Jogos Xbox One</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/291045/games/console-xbox-360">Xbox 360</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/291228/games/jogos-xbox-360">Jogos Xbox 360</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/291067/games/console-playstation-3">Playstation 3</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/291236/games/jogos-playstation-3">Jogos Playstation 3</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/351535/games/console-playstation-4">Playstation 4</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/356437/games/jogos-playstation-4">Jogos Playstation 4</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/291327/games/jogos-pc">Jogos para pc</a>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/226762/games">Veja Mais Games</a>
															</li>
														</ul>
													</div>
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/227369/cds-e-dvds-musicais" class="a-title">Música</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/227469/cds-e-dvds-musicais/pop-internacional">pop internacional</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/227545/cds-e-dvds-musicais/rock-internacional">rock internacional</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/230533/cds-e-dvds-musicais/blu-ray">blu-ray musical</a>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227369/cds-e-dvds-musicais">veja mais música</a>
															</li>
														</ul>
														<a href="http://www.americanas.com.br/loja/227609/dvds-e-blu-ray" class="a-title">Filmes</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/315209/dvds-e-blu-ray/lancamentos">lançamentos</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/315442/dvds-e-blu-ray/boxes-e-colecoes">box e coleções</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/315255/dvds-e-blu-ray/series-de-tv">séries de tv</a>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227609/dvds-e-blu-ray">veja mais filmes</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x85")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/228310/livros">Livros e Papelaria<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/228310/livros" class="a-title">Livros</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/230995/livros/mais-vendidos">Mais vendidos</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/231012/livros/lancamentos">Lançamentos</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/loja/228622/livros-importados">Livros importados</a>
															</li>
															<li class="a-snl-item">
																<span class="pooler">Gêneros</span>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/228536/livros/literatura-estrangeira">Literatura estrangeira</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/228548/livros/literatura-nacional">Literatura nacional</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/228312/livros/administracao-e-negocios">Administração e Negócios</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/228559/livros/medicina-e-saude">Medicina e Saúde</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/228511/livros/juvenil">Juvenil</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/228310/livros">Veja mais Livros</a>
															</li>
														</ul>
													</div>
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/228804/papelaria" class="a-title">Papelaria</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/330688/papelaria/mochilas-escolares">mochilas escolares</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/228860/papelaria/cadernos">cadernos</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/330968/papelaria/ficharios-e-acessorios">fichários</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/330193/papelaria/agendas-e-calendarios">agendas e calendários</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/330901/papelaria/estojos-e-necessaires">estojos</a>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/228804/papelaria">veja mais papelaria</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x84")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao">Móveis e Decoração<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao" class="a-title">Móveis</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/282284/moveis-e-decoracao/guarda-roupa-roupeiro">Guarda-roupa / roupeiro</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/282569/moveis-e-decoracao/cadeira-de-escritorio">Cadeira de escritório</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/282125/moveis-e-decoracao/colchao">Colchão</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/282276/moveis-e-decoracao/cama-box-colchao-box">Cama box (colchão + box)</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/sublinha/282622/moveis-e-decoracao/bar/banquetas-cadeiras-para-bar">Banqueta e cadeira de bar</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/282267/moveis-e-decoracao/mesa-para-computador">Mesa para computador</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/282714/moveis-e-decoracao/rack-estante-e-painel">Rack, estante e Painel</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/282908/moveis-e-decoracao/sofas">Sofá</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/282264/moveis-e-decoracao/cozinha-modulada">cozinha modulada</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/282614/moveis-e-decoracao/comoda">cômoda</a>
															</li>
														</ul>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<span class="pooler">decoração</span>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/282621/moveis-e-decoracao/luminarias">luminárias</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/282598/moveis-e-decoracao/quadros">quadros</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/282597/moveis-e-decoracao/painel-de-fotos">painel de fotos</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/348268/moveis-e-decoracao/adesivos">adesivos</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao">Veja mais Móveis e decoração</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x82")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/358352/moda">Moda<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/358352/moda" class="a-title">Moda</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/subloja/358354/moda/feminino">Feminino</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/358357/moda/feminino/roupas">Roupas</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/358358/moda/feminino/calcados">Calçados</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/358360/moda/feminino/bolsas-e-acessorios">Bolsas e Acessórios</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/subloja/358355/moda/masculino">Masculino</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/358361/moda/masculino/roupas">Roupas</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/358394/moda/masculino/calcados">Calçados</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/358362/moda/masculino/acessorios">Acessórios</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/subloja/358432/moda/infantil">Infantil</a>
																<ul class="a-snl-sub">
																	<li>
																		<a href="http://www.americanas.com.br/linha/358414/moda/infantil/meninas">Para meninas</a>
																	</li>
																	<li>
																		<a href="http://www.americanas.com.br/linha/358434/moda/infantil/meninos">Para meninos</a>
																	</li>
																</ul>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/358352/moda">Veja Mais Moda</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x91")
															} catch (exp) {
															}
														</script>

													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/loja/227821/esporte-e-lazer">Esporte e Saúde<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel two-columns">
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/227821/esporte-e-lazer" class="a-title">Esporte</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/341383/esporte-e-lazer/monitor-cardiaco">monitor cardíaco</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/291560/esporte-e-lazer/praia-e-piscina">piscina</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/290286/esporte-e-lazer/bicicleta">bicicleta</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/sublinha/341445/esporte-e-lazer/aparelho-de-musculacao-e-fitness/esteira-eletrica-ergometrica">esteira elétrica</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/sublinha/341439/esporte-e-lazer/aparelho-de-musculacao-e-fitness/aparelho-de-musculacao">aparelho de musculação</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/sublinha/341399/esporte-e-lazer/aparelho-de-musculacao-e-fitness/acessorios-para-malhar">acessórios para malhação</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/sublinha/341469/esporte-e-lazer/aparelho-de-musculacao-e-fitness/halteres">halteres</a>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227821/esporte-e-lazer">Veja mais Esporte</a>
															</li>
														</ul>
													</div>
													<div class="a-snl-opts">
														<a href="http://www.americanas.com.br/loja/227014/beleza-e-saude" class="a-title">saúde</a>
														<ul class="a-sn-list">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/292289/beleza-e-saude/umidificadores-de-ar">umidificador de ar</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/sublinha/292190/beleza-e-saude/balancas/balanca-digital">balança digital</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/linha/292179/beleza-e-saude/medidores-de-pressao">medidor de pressão</a>
															</li>
															<li class="a-snl-item">
																<a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227014/beleza-e-saude">veja mais saúde</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x93")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="http://viagens.americanas.com.br/default.aspx?utm_source=americanasamp;utm_medium=americanas_toda_a_loja_do_viagens_menu_principal_varejo_-_-_home_principalamp;utm_campaing=-_-_home_principalamp;s_cid=americanas_toda_a_loja_do_viagens_menu_principal_varejo__-_-_home_principal">Viagens<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel">
													<div class="a-snl-opts">
														<a href="http://viagens.americanas.com.br/default.aspx?utm_source=americanasamp;utm_medium=americanas_toda_a_loja_do_viagens_menu_principal_varejo_-_-_home_principalamp;utm_campaing=-_-_home_principalamp;s_cid=americanas_toda_a_loja_do_viagens_menu_principal_varejo__-_-_home_principal" class="a-title">viagens</a>
														<ul class="a-snl-sub">
															<li>
																<a href="http://viagens.americanas.com.br/passagens-aereas.aspx?utm_source=americanasamp;utm_medium=americanas_menu_principal_varejo_-_-_home_passagens_-amp;utm_campaing=-_-_home_passagensamp;s_cid=americanas_menu_principal_varejo_-_-_-_home_passagens">Passagens Aéreas</a>
															</li>
															<li>
																<a href="http://viagens.americanas.com.br/hoteis.aspx?utm_source=americanasamp;utm_medium=americanas_menu_principal_varejo_-_-_home_hoteis_-amp;utm_campaing=-_-_home_hoteisamp;s_cid=americanas_menu_principal_varejo_-_-_-_home_hoteis">Hotéis</a>
															</li>
															<li>
																<a href="http://viagens.americanas.com.br/pacotes-turisticos.aspx?utm_source=americanasamp;utm_medium=americanas_menu_principal_varejo_-_-_home_pacotes_-amp;utm_campaing=-_-_home_pacotesamp;s_cid=americanas_menu_principal_varejo_-_-_-_home_pacotes">Pacotes Turísticos</a>
															</li>
															<li>
																<a href="http://viagens.americanas.com.br/cruzeiros-maritimos.aspx?utm_source=americanasamp;utm_medium=americanas_menu_principal_varejo_-_-_home_cruzeiros_-amp;utm_campaing=-_-_home_cruzeirosamp;s_cid=americanas_menu_principal_varejo_-_-_-_home_cruzeiros">Cruzeiros Marítimos</a>
															</li>
															<li>
																<a href="http://viagens.americanas.com.br/seguros.aspx?utm_source=americanasamp;utm_medium=americanas_menu_principal_varejo_-_-_home_seguros-amp;utm_campaing=-_-_home_segurosamp;s_cid=americanas_menu_principal_varejo_-_-_-_home_seguros">Seguros</a>
															</li>
															<li>
																<a href="http://viagens.americanas.com.br/atracoes.aspx?utm_source=americanasamp;utm_medium=americanas_menu_principal_varejo_-_-_home_atracoes-amp;utm_campaing=-_-_home_atracoesamp;s_cid=americanas_menu_principal_varejo_-_-_-_home_atracoes">Atrações</a>
															</li>
															<li>
																<a href="http://viagens.americanas.com.br/resorts.aspx?utm_source=americanasamp;utm_medium=americanas_menu_principal_varejo_-_-_home_resorts-amp;utm_campaing=-_-_home_resortsamp;s_cid=americanas_menu_principal_varejo_-_-_-_home_resorts">Resorts</a>
															</li>
															<li>
																<a href="http://viagens.americanas.com.br/default.aspx?utm_source=americanasamp;utm_medium=americanas_toda_a_loja_do_viagens_menu_principal_varejo_-_-_home_principalamp;utm_campaing=-_-_home_principalamp;s_cid=americanas_toda_a_loja_do_viagens_menu_principal_varejo__-_-_home_principal">Toda a loja do Viagens</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x92")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>
										</ul>
										<ul class="mh-list list-bottom">

											<li class="mh-item afr-arw">
												<a href="http://www.americanas.com.br/estatica/todos-os-departamentos/?WT.mc_id=home-todos-departamentos">todos os departamentos<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel all-depts">
													<a href="http://www.americanas.com.br/estatica/todos-os-departamentos/?WT.mc_id=home-todos-departamentos" class="a-title">todos os departamentos</a>
													<div class="pure-g">
														<div class="pure-u-1-4">
															<ul class="a-snl-sub">
																<li>
																	<a href="http://www.americanas.com.br/loja/226795/alimentos-e-bebidas?WT.mc_id=home-menuLista-alimentos">Alimentos e bebidas</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/256408/audio?WT.mc_id=home-menuLista-audio">Áudio</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/226855/automotivo?WT.mc_id=home-menuLista-automotivo">Automotivo</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/317728/ar-condicionado-e-aquecedores">Ar-condicionado e aquecedores</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/226940/bebes?WT.mc_id=home-menuLista-bebes">Bebês</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/227014/beleza-e-saude?WT.mc_id=home-menuLista-beleza">Beleza e saúde</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/227109/brinquedos?WT.mc_id=home-menuLista-brinquedos">Brinquedos</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/315798/blu-ray?WT.mc_id=home-menuIconeOver-bluRay-linha">Blu-ray e blu-ray 3D</a>
																</li>
															</ul>
														</div>
														<div class="pure-u-1-4">
															<ul class="a-snl-sub">
																<li>
																	<a href="http://www.americanas.com.br/loja/227296/cama-mesa-e-banho?WT.mc_id=home-menuLista-cameba">Cama, mesa e banho</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/227559/cameras-e-filmadoras?WT.mc_id=home-menuLista-cameras" crmwa_mdc="mp|ver_todos_os_departamentos|cameras_e_filmadoras">Câmeras
																	e filmadoras</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/229187/celulares-e-telefones?WT.mc_id=home-menuLista-celulares" crmwa_mdc="mp|ver_todos_os_departamentos|celulares_e_telefones">Celulares e telefones</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/227644/eletrodomesticos?WT.mc_id=home-menuLista-eletrodomesticos" crmwa_mdc="mp|ver_todos_os_departamentos|eletrodomesticos">Eletrodomésticos</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/227763/eletroportateis?WT.mc_id=home-menuLista-eletroportateis" crmwa_mdc="mp|ver_todos_os_departamentos|eletroportateis">Eletroportáteis</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/227821/esporte-e-lazer?WT.mc_id=home-menuLista-esporte" crmwa_mdc="mp|ver_todos_os_departamentos|esporte_e_lazer">Esporte e lazer</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/227999/ferramentas-e-jardim?WT.mc_id=home-menuLista-ferramentas" crmwa_mdc="mp|ver_todos_os_departamentos|ferramentas_e_jardim">Ferramentas e jardim</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/227609/dvds-e-blu-ray?WT.mc_id=home-menuLista-dvdBlu" crmwa_mdc="mp|ver_todos_os_departamentos|filmes_e_series">Filmes e séries</a>
																</li>
															</ul>
														</div>
														<div class="pure-u-1-4">
															<ul class="a-snl-sub">
																<li>
																	<a href="http://www.americanas.com.br/loja/226762/games?WT.mc_id=home-menuLista-games" crmwa_mdc="mp|ver_todos_os_departamentos|games">Games</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228190/informatica?WT.mc_id=home-menuLista-informatica" crmwa_mdc="mp|ver_todos_os_departamentos|informatica">Informática</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228098/informatica-acessorios?WT.mc_id=home-menuLista-infoacess" crmwa_mdc="mp|ver_todos_os_departamentos|informatica_e_acessorios">Informática e acessórios</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228255/instrumentos-musicais?WT.mc_id=home-menuLista-instrumentos" crmwa_mdc="mp|ver_todos_os_departamentos|instrumentos_musicais">Instrumentos musicais</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228310/livros?WT.mc_id=home-menuLista-livros" crmwa_mdc="mp|ver_todos_os_departamentos|livros">Livros</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228641/malas-e-acessorios?WT.mc_id=home-menuLista-malas" crmwa_mdc="mp|ver_todos_os_departamentos|malas_e_acessorios">Malas e acessórios</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228671/moda-e-acessorios?WT.mc_id=home-menuLista-moda" crmwa_mdc="mp|ver_todos_os_departamentos|moda_e_acessorios">Moda e acessórios</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao?WT.mc_id=home-menuLista-moveis" crmwa_mdc="mp|ver_todos_os_departamentos|moveis_e_decoracao">Móveis e decoração</a>
																</li>
															</ul>
														</div>
														<div class="pure-u-1-4">
															<ul class="a-snl-sub">
																<li>
																	<a href="http://www.americanas.com.br/loja/227369/cds-e-dvds-musicais?WT.mc_id=home-menuLista-cds" crmwa_mdc="mp|ver_todos_os_departamentos|musica">Música</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228804/papelaria?WT.mc_id=home-menuLista-papelaria" crmwa_mdc="mp|ver_todos_os_departamentos|papelaria">Papelaria</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/234768/pascoa" crmwa_mdc="mp|ver_todos_os_departamentos|pascoa">Páscoa</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228926/perfumaria?WT.mc_id=home-menuLista-perfumaria" crmwa_mdc="mp|ver_todos_os_departamentos|perfumaria_e_cosmeticos">Perfumaria e cosméticos</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/228975/pet-shop?WT.mc_id=home-menuLista-petshop" crmwa_mdc="mp|ver_todos_os_departamentos|pet_shop">Pet shop</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/229017/relogios?WT.mc_id=home-menuLista-relogios" crmwa_mdc="mp|ver_todos_os_departamentos|relogios">Relógios</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/336268/suplementos-e-vitaminas?WT.mc_id=home-menuLista-suplementos" crmwa_mdc="mp|ver_todos_os_departamentos|suplementos_e_vitaminas">Suplementos e Vitaminas</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/227707/tv-e-home-theater?WT.mc_id=home-menuLista-eletronicos" crmwa_mdc="mp|ver_todos_os_departamentos|tvs_e_audio">TVs e Áudio</a>
																</li>
																<li>
																	<a href="http://www.americanas.com.br/loja/229231/utilidades-domesticas?WT.mc_id=home-menuLista-utilidadeDomestica" crmwa_mdc="mp|ver_todos_os_departamentos|utilidades_domesticas">Utilidades domésticas</a>
																</li>
															</ul>
														</div>
													</div>
													<div class="a-snl-bottom-info">
														<div class="pure-g">
															<div class="pure-u-1-2">
																<span class="a-title">veja também</span>
																<ul class="a-bt-inf-list">
																	<li class="a-bt-inf-it">
																		<a href="http://viagens.americanas.com.br" class="a-bt-inf-lnk amv">Americanas Viagens</a>
																	</li>
																	<li class="a-bt-inf-it">
																		<a href="http://www.ingresso.com.br/" class="a-bt-inf-lnk ing">Ingresso.com</a>
																	</li>
																</ul>
															</div>
															<div class="pure-u-1-2">
																<span class="a-title">parceiros</span>
																<ul class="a-bt-inf-list">
																	<li class="a-bt-inf-it">
																		<a href="http://www.milevo.com.br/" class="a-bt-inf-lnk aml">Milevo.com</a>
																	</li>
																	<li class="a-bt-inf-it">
																		<a href="http://www.soubarato.com.br" class="a-bt-inf-lnk soub">Outlet - Sou Barato</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</li>
											<li class="mh-item afr-arw">
												<a href="#">Serviços<span class="spt-seta-submenu"></span></a>
												<div class="a-subnivel">
													<div class="a-snl-opts">
														<span class="a-title">serviços</span>
														<ul class="a-snl-sub">
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/avaliacao?chave=HM_SV_AVPROD&amp;WT.mc_id=HM_SV_AVPROD">avaliação de produto</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/garantia-estendida?chave=HM_SV_GE&amp;WT.mc_id=HM_SV_GE">garantia estendida</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/estatica/app-iphone?chave=HM_SV_APPIPHONE&amp;WT.mc_id=HM_SV_APPIPHONE">aplicativo para iPhone</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/negocios-corporativos?chave=HM_SV_NEGCORP&amp;WT.mc_id=HM_SV_NEGCORP">negócios corporativos</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/ajato?chave=HM_SV_EAJATO&amp;WT.mc_id=HM_SV_EAJATO">entrega a jato</a>
															</li>
															<li class="a-snl-item">
																<a href="http://www.americanas.com.br/caixa-expresso?chave=HM_SV_CEXPRESSO&amp;WT.mc_id=HM_SV_CEXPRESSO">caixa expresso</a>
															</li>
															<li class="a-snl-item">
																<a href="https://carrinho.americanas.com.br/lista-de-casamento/pages/HomePage?chave=HM_SV_LCASAMENTO&amp;WT.mc_id=HM_SV_LCASAMENTO">lista de casamento</a>
															</li>
														</ul>
													</div>
													<div class="banner-snl-item">
														<script type="text/javascript">
															try {
																OAS_AD("x88")
															} catch (exp) {
															}
														</script>
													</div>
												</div>
											</li>

										</ul>
									</nav>
								</div>
							</li>
							<li class="ams-item">
								<a href="http://www.americanas.com.br/loja/227109/brinquedos?chave=HT_TP_1">planeta criança</a>
							</li>

							<li class="ams-item">
								<a href="http://www.americanas.com.br/a-torcida?chave=HT_TP_2">a torcida</a>
							</li>
							<li class="ams-item">
								<a href="http://www.americanas.com.br/central-de-promocoes-copa?chave=HT_TP_3">central de promoções</a>
							</li>
							<li class="ams-item">
								<a href="http://www.americanas.com.br/troca-tudo?chave=HT_TP_4">troca-tudo eletrodomésticos</a>
							</li>
							<li class="ams-item oferta-do-dia" hidden="hidden">
								<a href="http://www.americanas.com.br/aofertadodia?WT.ac=ofertadodia"><span class="spt-ic-oferta-do-dia"></span>oferta do dia<span class="spt-seta-menu"></span></a>
								<div class="banner-oferta-do-dia">
									<div class="bnr-ofr-img">
										<script type="text/javascript">
											try {
												OAS_AD("Top2")
											} catch (exp) {
											}
										</script>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</header>
				<script>
					onReady(function() {
						$.getScript("http://img.americanas.com.br/statics-1.62.4.2625/catalog/js/v1/header.js")
					})
				</script>
			</div>
			<div id="content">

				<div id="area02">
					<div id="area02-1">