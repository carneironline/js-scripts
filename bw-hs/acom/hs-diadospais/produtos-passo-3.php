<?php
include_once('include/header-prod-passo3.php');
?>
<!--BEGIN PAGE CONTENT-->
<script type="text/javascript" src="<?php echo PATHURL; ?>js/Validations.js"></script>
<script type="text/javascript" src="<?php echo PATHURL; ?>js/scripts-produtos-passo3-v1.js"></script>
<script type="text/javascript" src="<?php echo PATHURL; ?>js/facebook-share.js"></script>
<div id="fb-root"></div>

<div class="box-content-produtos-passo3">
	<div class="box-content-left">
		<div class="box-saudacao">
			<img src="<?php echo PATHURL?>img/produtos-passo3-lista-enviada.jpg" alt=""/>
		</div>
		<div class="box-subtitle">
			<span>Esqueceu alguém?</span>
		</div>
		<div class="box-form">
			<form id="formEnvioEmail">
				<input type="hidden" id="urlToRedirect" value="<?php echo $menu['home']; ?>" />
				<ul class="clearFix">
					<li class="box-label">
						<span class="icon-mail"><img src="<?php echo PATHURL?>img/mail-icon.jpg" alt=""/></span>
						<span class="label">Digite o email deles:</span>
					</li>
					<li>
						<div class="box-nome-email">
							<div class="box-nome-email-inner">
								<input type="text" id="othersName1" name="othersName1" placeholder="Nome"/>
							</div>
							<div>
								<input type="text" id="othersEmail1" name="othersEmail1" placeholder="filho@gmail.com"/>
							</div>
						</div>
					</li>
					<li>
						<div class="box-nome-email">
							<div class="box-nome-email-inner">
								<input type="text" id="othersName2" name="othersName2" placeholder="Nome"/>
							</div>
							<div>
								<input type="text" id="othersEmail2" name="othersEmail2" placeholder="filho@gmail.com"/>
							</div>
						</div>
					</li>
					<li>
						<div class="box-nome-email">
							<div class="box-nome-email-inner">
								<input type="text" id="othersName3" name="othersName3" placeholder="Nome"/>
							</div>
							<div>
								<input type="text" id="othersEmail3" name="othersEmail3" placeholder="filho@gmail.com"/>
							</div>
						</div>
					</li>
				
					<li><input type="submit" id="submit" class="btn-avancar" value="Enviar"/></li>
					
					<li>
						<span class="label" style="padding-top: 40px; padding-bottom:10px;">Conte para todos o que gostaria de ganhar:</span>
						<a id="share-button" href="#" title="Facebook Share Button"><img src="<?php echo PATHURL; ?>img/btn_face.jpg" alt="Facebook Share Button" title="Facebook Share Button" /></a>
					</li>
				</ul>
			</form>
		</div>
		<div class="box-compartilhe-novamente">
			<img src="<?php echo PATHURL?>img/title-compartilhe-novamente.jpg" alt=""/>
			<ul class="clearFix">
				<li><a class="button-facebook" href="https://www.facebook.com/CanalShoptime?chave=bannervert-facebook&WT.mc_id=bannervert-facebook" target="_blank">Facebook</a></li>
				<li><a class="button-twitter" href="https://twitter.com/canalshoptime?chave=bannervert-twitter&WT.mc_id=bannervert-twitter" target="_blank">Twitter</a></li>
				<li><a class="button-gplus" href="https://plus.google.com/+canalshoptime/posts" target="_blank">Google +</a></li>
			</ul>
		</div>
	</div>
	<div class="box-content-right">
		<img src="<?php echo PATHURL?>img/produtos-passo3-content.jpg" alt=""/>
	</div>
	<div class="clearFix"></div>
</div>

<style>
	#popup-box {display:none;}
	#popup-msg {width: 500px; height: 60px; }
	#popup-msg p {font-size: 18px;text-align: center; padding-top: 15px;}
	#popup-msg p .red {color:#f00}
</style>
<a id="active-popup-msg" href="#popup-msg"></a>
<div id="popup-box">
	<div id="popup-msg">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis mi eu elit tempor facilisis id et neque. Nulla sit amet sem sapien. Vestibulum imperdiet porta ante ac ornare. Nulla et lorem eu nibh adipiscing ultricies nec at lacus. Cras laoreet ultricies sem, at blandit mi eleifend aliquam. Nunc enim ipsum, vehicula non pretium varius, cursus ac tortor. Vivamus fringilla congue laoreet. Quisque ultrices sodales orci, quis rhoncus justo auctor in. Phasellus dui eros, bibendum eu feugiat ornare, faucibus eu mi. Nunc aliquet tempus sem, id aliquam diam varius ac. Maecenas nisl nunc, molestie vitae eleifend vel, iaculis sed magna. Aenean tempus lacus vitae orci posuere porttitor eget non felis. Donec lectus elit, aliquam nec eleifend sit amet, vestibulum sed nunc.
	</div>
</div>
<!--END PAGE CONTENT-->

<?php
include_once('include/footer-prod-passo2.php');
?>