<?php
include_once('include/header-home.php');
?>

<!--BEGIN PAGE CONTENT-->
<script type="text/javascript" src="<?php echo PATHURL; ?>js/Products.js"></script>
<script type="text/javascript" src="<?php echo PATHURL; ?>js/Validations.js"></script>
<script type="text/javascript" src="<?php echo PATHURL; ?>js/scripts-produtos-passo2-v2.js"></script>
<script type="text/javascript" src="<?php echo PATHURL; ?>js/facebook-share.js"></script>
<div id="fb-root"></div>

<div class="box-content-produtos-passo2">
    <ul id="stepMenu">
        <li>1º  Passo</li>
        <li>|</li>
        <li class="active">2º  Passo</li>
    </ul>

	<div class="box-content-left">
		<div class="box-form">
			<form id="formEnvioEmail">
				<input type="hidden" id="urlToRedirect" value="<?php echo $actionPasso2; ?>" />
				<input type="hidden" id="produtosID" name="produtosID" value="" />
				<ul class="clearFix">
					<li>
						<span class="label">Nome:</span>
						<input type="text" id="name" name="name" placeholder="Nome"/>
					</li>
					<li>
						<span class="label">Digite o seu email:</span>
						<input type="text" id="email" name="email" placeholder="Email"/>
					</li>
					<li>
						<span class="label">Digite o email do seu filho e quem mais você quiser:</span>
						<div class="box-nome-email">
							<div class="box-nome-email-inner">
								<input type="text" id="othersName1" name="othersName1" placeholder="Nome"/>
							</div>
							<div>
								<input type="text" id="othersEmail1" name="othersEmail1" placeholder="filho@site.com"/>
							</div>
						</div>
						<div class="box-nome-email">
							<div class="box-nome-email-inner">
								<input type="text" id="othersName2" name="othersName2" placeholder="Nome"/>
							</div>
							<div>
								<input type="text" id="othersEmail2" name="othersEmail2" placeholder="filho@site.com"/>
							</div>
						</div>
						<div class="box-nome-email">
							<div class="box-nome-email-inner">
								<input type="text" id="othersName3" name="othersName3" placeholder="Nome"/>
							</div>
							<div>
								<input type="text" id="othersEmail3" name="othersEmail3" placeholder="filho@site.com"/>
							</div>
						</div>
						<div class="box-nome-email">
							<div class="box-nome-email-inner">
								<input type="text" id="othersName4" name="othersName4" placeholder="Nome"/>
							</div>
							<div>
								<input type="text" id="othersEmail4" name="othersEmail4" placeholder="filho@site.com"/>
							</div>
						</div>
						<div class="box-nome-email">
							<div class="box-nome-email-inner">
								<input type="text" id="othersName5" name="othersName5" placeholder="Nome"/>
							</div>
							<div>
								<input type="text" id="othersEmail5" name="othersEmail5" placeholder="filho@site.com"/>
							</div>
						</div>


					</li>
                    <li>
                        <div class="box-button-avancar btn-avancar amarelo down">
                            <a href="javascript:;">enviar</a>
                        </div>
                    </li>

                    <!--<li>
                        <div class="box-receber-novidades">
                            <input type="checkbox" id="btn-declaro" name="btn-declaro" value=""/><span>Desejo receber e-mails com novidades e ofertas do americanas.com</span>
                            <div class="clear"></div>
                        </div>
                    </li>-->
				</ul>
			</form>
		</div>
	</div>
	<div class="box-content-right">
		
		<div id="box-itens" class="box-itens">
		</div>
		

	</div>
	<div class="clearFix"></div>
</div>

<style>
	#popup-box {display:none;} #popup-msg {width: 500px; height: 60px; }
	#popup-msg p {font-size: 18px;text-align: center; padding-top: 15px;}
	#popup-msg p .red {color:#f00}
</style>
<a id="active-popup-msg" href="#popup-msg"></a>
<div id="popup-box">
	<div id="popup-msg">
	</div>
</div>
<!--END PAGE CONTENT-->

<?php
include_once('include/footer-home.php');
?>