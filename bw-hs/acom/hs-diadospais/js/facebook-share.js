onReady(function(){

	(function() {	
	
		var e = document.createElement('script');
		e.src = document.location.protocol + '//connect.facebook.net/pt_BR/all.js';
		e.async = true;
		document.getElementById('fb-root').appendChild(e);
	}());
	 
	window.fbAsyncInit = function () {
		FB.init({
			appId  : '635161149894655',
			status : true, // check login status
			cookie : true, // enable cookies to allow the server to access the session
			xfbml  : true,  // parse XFBML
			oauth  : true // enable OAuth 2.0
		});
	};
	
	
	$('#share-button').click(function(e) {
		e.preventDefault();
		var maeName = (typeof QueryString.maeName != 'undefined') ? QueryString.maeName : $('#name').val();
		
		if(maeName != ''){
			$('#name').removeClass('error');
			
			FB.ui({
				method: 'feed',
				name: 'Dia das mães - Shoptime',
				link: 'http://www.shoptime.com.br/diadasmaes/dia-das-maes-listagem?produtos_selecionados='+QueryString.produtos_selecionados+'&mae='+maeName,
				picture: 'http://img.shoptime.com.br/mktshop/hotsites/hs-shop-dia-das-maes/img/imagem_face.jpg',
				caption: ' ',
				description:  maeName + ' já escolheu o que quer ganhar neste Dia das Mães! Tá lá no Shoptime!'
			},
			function(response) {
			    if (response && response.post_id) {
			    	callPopupMessage('Seus presentes foram compartilhados no Facebook!');
			    } else {
			    	callPopupMessage('Seus presentes não puderam ser compartilhados no momento. Tente novamente.');
			    }
		    });
		} else {
			$('#name').addClass('error');
			callPopupMessage('Mãe! Preencha seu nome no campo destacado.');
		}
	});

});