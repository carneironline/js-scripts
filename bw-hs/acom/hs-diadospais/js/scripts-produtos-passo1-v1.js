onReady(function() {
	var paramTag = [];

    /*
     tag-descolado-rel
     tag-caseiro-rel
     tag-esportista-rel
     tag-classico-rel
     tag-cultural-rel
     */

	var categoryOptions = [ {
		tag : 'tag-descolado-rel',
		categoria : 'Descolado'
	}, 
	{
		tag : 'tag-caseiro-rel',
		categoria : 'Caseiro'
	}, 
	{
		tag : 'tag-esportista-rel',
		categoria : 'Esportista'
	}, 
	{
		tag : 'tag-classico-rel',
		categoria : 'Classico'
	},
	{
		tag : 'tag-cultural-rel',
		categoria : 'Cultural'
	}
	];
	
	// Gera os itens da lista de categorias
	$.each(categoryOptions, function(index, obj) {

		if(QueryString.cat)
		{
			var query = (QueryString.cat == 'gourmet') ? 'sabor' : QueryString.cat;

			if(query.indexOf(toSlug(obj.categoria)) > -1){
				var hasCatClass = false;
				var active = "active";

				if(!hasCatClass){
                    $('#header-produto').addClass(toSlug(obj.categoria));
			    	//$('.hs-header ul li h2').html(obj.categoria);
				}
			}

		} else {
			var active = (index==0) ? "active" : "";
            $('#header-produto').addClass('decolado');
			//$('.hs-header ul li h2').html('Descolado');
		}

        catName = (obj.categoria == 'Classico') ? 'Clássico' : obj.categoria;

		$('#categoryFilterList')
			.append('<li><a class="btn-' + toSlug(obj.categoria) + ' ' + active + ' btn-category-item" data-category-name="'+obj.categoria+'" data-category-slug="'+toSlug(obj.categoria)+'" data-target="#' + obj.tag + '">'+catName+'</a></li>');

		// Popula o array com as tags
		paramTag.push(obj.tag); 
	});
	
	//Insere os boxes com a listagem de produtos no html
	Produtos.getProductsByTag(paramTag, function(data){

		$.each(data, function(index, obj){

			templateHTML(obj.products, obj.tag, index);
			
		});
	});
	
	// Troca a exibição das listas de produtos por categoria
	$('.btn-category-item').on('click', function(){
		el = $(this);
		categorySlug = el.data('category-slug');
		categoryName = el.data('category-name');
		
		$('.btn-category-item').removeClass('active');
		$(this).addClass('active');
		
		$('.productsList.active').fadeOut("fast", function(){
			$(this).addClass('displayNone');
			$(this).removeClass('active');
			
			$(el.data('target')).removeClass('displayNone');
			$(el.data('target')).addClass('active');
			$(el.data('target')).fadeIn();
		});


        $('#header-produto').removeClass()
        $('#header-produto').addClass(categorySlug);

		/*var classList =$('.hs-header ul li span').attr('class').split(/\s+/);
		$.each( classList, function(index, item){
		    if (item != 'icon-top') {
                $('#header-produto').removeClass(item);
                $('#header-produto').addClass(categorySlug);
		    	//$('.hs-header ul li h2').html(categoryName);
		    }
		});*/
	});
	
	/*
	 * Ativa os eventos a clicar nos botões para add o produto
	 */
	$(document).on('click', '.btn-add-lista', function(){
		
		if(GLOBALPRODUTO.total()<5){
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
				GLOBALPRODUTO.remove($(this).data('product-id'));
			} else {
				$(this).addClass('selected');
				GLOBALPRODUTO.add($(this).data('product-id'));
			}		
		} else {
			
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
				GLOBALPRODUTO.remove($(this).data('product-id'));
			} else {
				callPopupMessage('Você já escolheu 5 produtos!');
			}
		}
	});
	
	$('.btn-avancar').on('click', function(){
		if(GLOBALPRODUTO.total()<1){
			restoProd = 5 - GLOBALPRODUTO.total();
			strProd = (GLOBALPRODUTO.total()==4) ? 'produto' : 'produtos';
			strMsg = (GLOBALPRODUTO.total()==0) ? 'Por favor, escolha até '+restoProd+ ' '+strProd+'.' : 'Você pode escolher mais '+restoProd+' '+strProd+'.';
			
			callPopupMessage(strMsg);
		} else {
			location.href = $('#urlToRedirect').val() + '?produtos_selecionados=' + GLOBALPRODUTO.get()
		}
		return false;
	})

});

var GLOBALPRODUTO = (function() {
 
    // criando o objeto (vazio) box
    var products = {};
    
    // adicionando a propriedade queue (fila)
    products.queue = [];
 
    // adicionando o métodos addItem (adicionar item)
    products.addItem = function(itemID) {
    	var arrProdutos = products.queue.push(itemID); 
        return arrProdutos;
    };
    
    // remove um item pelo buscando pelo ID
    products.removeItem = function(itemID){
    	var index = products.queue.indexOf(itemID);
    	
    	if (index > -1) {
    		products.queue.splice(index, 1);
    	}
    };
 
    // adicionando o métodos getQueue (recuperar fila)
    products.getQueue = function() {
        return products.queue.join(",");
    };
    
    // Retorna o total de itens no array
    products.totalItens = function(){
    	return products.queue.length;
    };
 
    // retornando um objeto personalizado (só com o necessário)
    return {
        add: products.addItem,
        get: products.getQueue,
        remove: products.removeItem,
        total: products.totalItens
    };
})();

//Método para retornar templates
function templateHTML(data, templateName, indexBox){
	if(QueryString.cat)
	{
		var displayNone = (templateName == QueryString.cat) ? 'active' : 'displayNone';
	} else {
		var displayNone = (indexBox!=0) ? 'displayNone' : 'active';
	}
	$html = '<div id="'+templateName+'" class="productsList '+displayNone+'"><ul>';
	
	$.each(data, function(index, item){
		var row = (index < 4) ? 'row1' : '';
		
		$html += '<li data-product-id="'+item.id+'" class="'+row+'">';
		$html += '<a href="'+item.url+'" class="prod-image"><img src="'+item.image+'" /></a>';
		$html += '<a href="'+item.url+'" class="prod-name">'+truncateText(item.name, 65)+'</a>';
		$html += (item.default_price != 'null') ? '<span class="prod-default-price">de: <em>'+item.default_price+'</em></span>' : '';
		$html += (item.sales_price != 'null') ? '<span class="prod-sales-price">por: '+item.sales_price+'</span>' : '';
		$html += '<span class="prod-installment">'+item.installment.total_installments+'x de '+item.installment.installment_value+' </span>';
		$html += (item.ratingsImage != undefined) ? '<span class="prod-ratingsImage"><img src="'+item.ratingsImage+'" /></span> <span class="prod-numReviews">('+item.numReviews+')</span>' : '';
		$html += '<a class="btn-add-lista" data-product-id="'+item.id+'"></a>';
		$html += '</li>';
		
		switch(templateName){
			case 'sl-1ludestsub': seletor = '#list1 ul'; break;
			case 'sl-lulojas': seletor = '#list2 ul'; break;
		}
	});	
	
	$html += '</ul></div>';
	
	$('#productsBox').append($html);
}

//Método para pegar um query da url
var QueryString = function () {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    	// If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
    	// If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
    	// If third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
    }
  } 
    return query_string;
} ();

function callPopupMessage(str){
	$('#popup-msg').html('<p>'+str+'</p>');
	
	$('#active-popup-msg').fancybox({
		onClosed: function() {
           $('#active-popup-msg').html('<div id="popup-msg"></div>');
        }
	}).click();
}

//Método para limpar uma string e transformá-la em um slug
function toSlug(str) {
	return str.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '');
}

function truncateText(str, limit) {
	if (str.length > limit) {
		str = str.substr(0, limit);
		str = str.split(' ');
		$html = '';

		for ($i = 0; $i < (str.length - 1); $i++) {
			$html += ($i == 0) ? str[$i] : ' ' + str[$i];
		}

		$html += '...';

		return $html;

	} else {
		return str;
	}
}