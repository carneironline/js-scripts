onReady(function() {	
	$('.btn-avancar').click(function(){ 
		validaForm('formEnvioEmail');
		
		return false;
	})

});

//VALIDAR FORMS
function validaForm(formID) 
{
	
	var formulario = document.getElementById(formID);
	var f = formulario.getElementsByTagName("input");
	var s = formulario.getElementsByTagName("select");
	var txtArea = formulario.getElementsByTagName("textarea");
	var error = 'PREENCHA OS CAMPOS\n\n';
	var cont = 0;
	
	// Ids dos campos a serem ignorados na checagem
	idIgnorado = [
              	  'submit', 
              	  'urlToRedirect',
              	  'produtosID',
              	  'othersName2', 'othersEmail2',
              	  'othersName3', 'othersEmail3',
              	  'othersName4', 'othersEmail4',
              	  'othersName5', 'othersEmail5',
              	  'btn-declaro'
	              ];
	
	// Faz o loop pelo inputs do form
	for(i=0; i<f.length; i++)
	{
		if(idIgnorado.indexOf(f[i].id) < 0)
		{ 
			jQuery(f[i]).removeClass('error');
			
			if(f[i].value == f[i].defaultValue || f[i].value == '')
			{
				jQuery(f[i]).addClass('error');
				error += f[i].id + '\n';	
				cont = 1;
			}
			
			// Verifica email
			if(f[i].id == 'email'){
				if(!$validations.validateEmail(f[i].value)){
					$(f[i]).addClass('error');	
					cont = 1;
				} 
			}
			
			// Verifica CPF
			if(f[i].id == 'cpf'){
				if(!$validations.validateCPF(f[i].value)){
					$(f[i]).addClass('error');	
					cont = 1;
				} 
			}
			
			// Verifica data
			if(f[i].id == 'data'){ 
				if(!$validations.validateDate(f[i].value)){
					$(f[i]).addClass('error');	
					cont = 1;
				} 
			}
		}
	}
	
	// Faz o loop pelos selects do form
	for(i=0; i<s.length; i++)
	{
		if(idIgnorado.indexOf(s[i].id) < 0)
		{ 
			jQuery(s[i]).removeClass('error');
			if(s[i].value == s[i].defaultValue || s[i].value == '')
			{
				$(s[i]).parent().addClass('error');
				cont = 1;
			} else {
				$(s[i]).parent().removeClass('error');
			}
		}
	}
	
	// Faz o loop pelo textareas do form
	for(i=0; i<txtArea.length; i++)
	{
		if(idIgnorado.indexOf(txtArea[i].id) < 0)
		{ 
			jQuery(txtArea[i]).removeClass('error');
			if(txtArea[i].value == txtArea[i].defaultValue || txtArea[i].value == '')
			{
				jQuery(txtArea[i]).addClass('error');
				error += txtArea[i].id + '\n';	
				cont = 1;
			}
		}
	}
	
	if(cont==0)
	{
		var maeName = (typeof QueryString.maeName != 'undefined') ? QueryString.maeName : $('#name').val();
		var maeEmail = (typeof QueryString.maeEmail != 'undefined') ? QueryString.maeEmail : $('#email').val();
		var produtos_selecionados = (typeof QueryString.produtos_selecionados != 'undefined') ? QueryString.produtos_selecionados : $('#produtosID').val();
		
		fieldsHS = {  
			OPT: 'O',
			DIAMAES_LISTA_PRODUTOS: produtos_selecionados, 
			EVENT_ID: 6745,
			EVENT_NAME:'CR_DIADASMAES',
			O: 32,
			STATUS: 'out',
			SEND_EMAIL: 'true'
		}
		fieldsMom = {  
			EMAIL_ADDRESS_MAE: 'MAE',
			MAIL: maeEmail,
			N: maeName,
			GENDER: 2,
		}
		objMom = mergeObject(fieldsMom, fieldsHS);
		
		arrSons = [];
		for(i=1; i<=3; i++){
			if($('#othersEmail'+i).val()!=''){
				eval('fieldSon'+i+'= { EMAIL_ADDRESS_MAE:"'+maeEmail+'", N:"'+$('#othersName'+i).val()+'", MAIL:"'+$('#othersEmail'+i).val()+'" }');
				arrSons[i-1] = mergeObject(eval('fieldSon'+i), fieldsHS);
			}
		}
				
		callPopupMessage('Enviando ...');
		
		setAjax(objMom, function(data){
			
		})
		
		for(i=0; i<arrSons.length; i++){
			setAjax(arrSons[i], function(data){
			
			});
			
			if(i==(arrSons.length-1)){
				$('#popup-msg p').html('<span class="green">Os e-mails foram enviados com sucesso!</span>');
				
				setTimeout(function(){
					location.href = $('#urlToRedirect').val();
				}, 2000)
			}
		}
		
		return true;

	} else {
		
		callPopupMessage('Campos incorretos. Favor verificar os <span class="red">campos destacados</span>.');
		return false;			
	}	
	
}

function setAjax(obj, callback){
	ajaxUrl = 'http://apps.shoptime.com.br/responsys/cr.php';
	
	if ($.browser.msie && window.XDomainRequest) {
		
		var strToSend = "";
	    
		for (var prop in obj) {
			var separator = (strToSend=="") ? '' : '&';
			strToSend +=  separator + prop + "=" + obj[prop];
		}
		
		// Use Microsoft XDR
		var xdr = new XDomainRequest();
		xdr.open("post", ajaxUrl);
		xdr.send(strToSend);
		xdr.onerror = function() {
			console.log("pavc-err-ie");
		};
		xdr.onload = function() {
			// XDomainRequest doesn't provide responseXml, so if you need it:
			var dom = new ActiveXObject("Microsoft.XMLDOM");
			//console.log(dom.async)
			dom.async = false;
			dom.loadXML(xdr.responseText);

			callback(xdr.responseText)

		};
		
	} else {
		$.ajax({
			type : "POST",
			url : ajaxUrl,
			data: obj,
			success : function(data) {
				callback(data)
				
			},
			error : function() {
				console.log("pavc-err");
			}
		});
	}
}

//Método para retornar templates
function templateHTML(data, templateName, indexBox){
	var displayNone = (indexBox!=0) ? 'displayNone' : 'active';
	$html = '<ul class="clearFix">';

	$.each(data, function(index, item){
		var row = (index < 1) ? 'row1' : '';
		
		$html += '<li data-product-id="'+item.id+'" class="'+row+'">';
		$html += '<div class="box-item">';
		$html += '<ul class="clearFix">';
		$html += '<li class="box-item-image"><img src="'+item.image+'" alt="'+item.name+'"/></li>';
		$html += '<li class="box-item-desc"><span>'+truncateText(item.name, 65)+'</span></li>';
		$html += '<li class="box-item-marker"><img src="img/box-item-marker.jpg" alt=""/></li>';
		$html += '</ul>';
		$html += '</div>';
		$html += '</li>';

	});	
	
	$html += '</ul>';
	
	$('#box-itens').append($html);
}

// Método para pegar um query da url
var QueryString = function () {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    	// If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
    	// If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
    	// If third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
    }
  } 
    return query_string;
} ();

// Método que chama o fancybox passando uma string
function callPopupMessage(str){
	$('#popup-msg').html('<p>'+str+'</p>');
	
	$('#active-popup-msg').fancybox({
		onClosed: function() {
           $('#active-popup-msg').html('<div id="popup-msg"></div>');
        }
	}).click();
}

// Método para mesclar dois objetos
function mergeObject(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

//Método para limpar uma string e transformá-la em um slug
function toSlug(str) {
	return str.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '');
}

//Método para remover caracteres do texto
function truncateText(str, limit) {
	if (str.length > limit) {
		str = str.substr(0, limit);
		str = str.split(' ');
		$html = '';

		for ($i = 0; $i < (str.length - 1); $i++) {
			$html += ($i == 0) ? str[$i] : ' ' + str[$i];
		}

		$html += '...';

		return $html;

	} else {
		return str;
	}
}

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}