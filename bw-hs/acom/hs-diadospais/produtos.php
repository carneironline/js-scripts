<?php
include_once('include/header-produtos.php');
?>

<!--BEGIN PAGE CONTENT-->
<link rel="stylesheet" type="text/css" href="<?php echo PATHURL; ?>css/produtos.css"/>
<script type="text/javascript" src="<?php echo PATHURL; ?>js/Products.js"></script>
<script type="text/javascript" src="<?php echo PATHURL; ?>js/scripts-produtos-passo1-v1.js"></script>

<div id="categoryFilter">
	<div class="title">Filtrar:</div>
	<ul id="categoryFilterList"></ul>
</div><!--#categoryFilter-->

<div id="productsBox">
</div>

<input type="hidden" id="urlToRedirect" value="<?php echo $actionPasso1; ?>" />
<a class="btn-avancar right avancar-bottom amarelo">avançar</a>

<style>
	#popup-box {display:none;}
	#popup-msg {width: 500px; height: 80px; }
	#popup-msg p {font-size: 22px;text-align: center; padding-top: 15px;}
</style>
<a id="active-popup-msg" href="#popup-msg"></a>
<div id="popup-box">
	<div id="popup-msg">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis mi eu elit tempor facilisis id et neque. Nulla sit amet sem sapien. Vestibulum imperdiet porta ante ac ornare. Nulla et lorem eu nibh adipiscing ultricies nec at lacus. Cras laoreet ultricies sem, at blandit mi eleifend aliquam. Nunc enim ipsum, vehicula non pretium varius, cursus ac tortor. Vivamus fringilla congue laoreet. Quisque ultrices sodales orci, quis rhoncus justo auctor in. Phasellus dui eros, bibendum eu feugiat ornare, faucibus eu mi. Nunc aliquet tempus sem, id aliquam diam varius ac. Maecenas nisl nunc, molestie vitae eleifend vel, iaculis sed magna. Aenean tempus lacus vitae orci posuere porttitor eget non felis. Donec lectus elit, aliquam nec eleifend sit amet, vestibulum sed nunc.
	</div>
</div>

<!--END PAGE CONTENT-->

<?php
include_once('include/footer-home.php');
?>