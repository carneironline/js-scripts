<?php
include_once('include/header-prod-listagem.php');
?>
<!--BEGIN PAGE CONTENT-->
<script type="text/javascript" src="<?php echo PATHURL; ?>js/Products.js"></script>
<script type="text/javascript" src="<?php echo PATHURL; ?>js/Validations.js"></script>
<script type="text/javascript" src="<?php echo PATHURL; ?>js/scripts-produtos-passo2-v2.js"></script>

<div class="box-content-produtos-passo2">
	<div class="box-content-right box-listagem">
		
		<div class="texto1">
			<p class="txt1">Olá!</p>
			<p class="txt2"><span id="maeNome"></span> contou para a gente o que está louca para ganhar.</p>
			<p class="txt3">veja o que ela escolheu:</p>
		</div>
		
		<div id="box-itens" class="box-itens">
		</div>
		
		<div class="texto2">
			<p class="txt1">Não perca tempo!</p>
			<p class="txt2">Escolha o presente certo para quem te deu o melhor</p>
		</div>
	</div>
	<div class="clearFix"></div>
</div>

<style>
	#popup-box {display:none;}
	#popup-msg {width: 500px; height: 60px; }
	#popup-msg p {font-size: 18px;text-align: center; padding-top: 15px;}
	#popup-msg p .red {color:#f00}
</style>
<a id="active-popup-msg" href="#popup-msg"></a>
<div id="popup-box">
	<div id="popup-msg">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis mi eu elit tempor facilisis id et neque. Nulla sit amet sem sapien. Vestibulum imperdiet porta ante ac ornare. Nulla et lorem eu nibh adipiscing ultricies nec at lacus. Cras laoreet ultricies sem, at blandit mi eleifend aliquam. Nunc enim ipsum, vehicula non pretium varius, cursus ac tortor. Vivamus fringilla congue laoreet. Quisque ultrices sodales orci, quis rhoncus justo auctor in. Phasellus dui eros, bibendum eu feugiat ornare, faucibus eu mi. Nunc aliquet tempus sem, id aliquam diam varius ac. Maecenas nisl nunc, molestie vitae eleifend vel, iaculis sed magna. Aenean tempus lacus vitae orci posuere porttitor eget non felis. Donec lectus elit, aliquam nec eleifend sit amet, vestibulum sed nunc.
	</div>
</div>
<!--END PAGE CONTENT-->

<?php
include_once('include/footer-prod-passo2.php');
?>