<?php
include_once('include/header-home.php');
?>

<!--BEGIN PAGE CONTENT-->

<div class="box-content-home">
	<div class="content">

		<img src="<?php echo PATHURL?>img/pais/home/como-ganhar.gif" alt="" />

        <div class="box-qual-estilo">
            <div class="content">
                <h2>Qual seu estilo?</h2>
                <h3>Selecione até 5 produtos das nossas listas, e garanta o presente perfeito. </h3>

                <div class="links-home">
                    <ul>
                        <li><a href="<?php echo $menu['produtos']; ?>?cat=tag-descolado-rel"><img src="<?php echo PATHURL?>img/pais/produtos/estilo-btn1.png" /><span>clique</span></a></li>
                        <li><a href="<?php echo $menu['produtos']; ?>?cat=tag-caseiro-rel"><img src="<?php echo PATHURL?>img/pais/produtos/estilo-btn2.png" /><span>clique</span></a></li>
                        <li><a href="<?php echo $menu['produtos']; ?>?cat=tag-esportista-rel"><img src="<?php echo PATHURL?>img/pais/produtos/estilo-btn3.png" /><span>clique</span></a></li>
                        <li><a href="<?php echo $menu['produtos']; ?>?cat=tag-classico-rel"><img src="<?php echo PATHURL?>img/pais/produtos/estilo-btn4.png" /><span>clique</span></a></li>
                        <li><a href="<?php echo $menu['produtos']; ?>?cat=tag-cultural-rel"><img src="<?php echo PATHURL?>img/pais/produtos/estilo-btn5.png" /><span>clique</span></a></li>
                    </ul>

                    <div class="clear"></div>
                </div>
            </div><!--.content-->
        </div>
		
	</div><!--.content-->
</div>

<!--END PAGE CONTENT-->

<?php
include_once('include/footer-home.php');
?>