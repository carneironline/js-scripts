onReady(function() {
	$.getScript('http://img.americanas.com.br/mktacom/hotsite/hs-dia-dos-namorados-2014/js/jquery.carouFredSel-6.2.1.js', function(){
		$(".hs-carrossel-01 ul").carouFredSel({
			auto: false,
			circular: false,
			infinite: true,
			scroll:{
				items: 1,
				fx: "slide",
				pauseOnHover: true	
			},				
			prev : {
				button : "#hs1_prev",
				key : "left"
			},
			next : {
				button : "#hs1_next",
				key : "right"
			}
		});
		
		$(".hs-carrossel-02 ul").carouFredSel({
			auto: false,
			circular: false,
			infinite: true,
			scroll:{
				items: 1,
				fx: "slide",
				pauseOnHover: true	
			},				
			prev : {
				button : "#hs2_prev",
				key : "left"
			},
			next : {
				button : "#hs2_next",
				key : "right"
			}
		});
		
		$.getScript('http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/js/app-timer.js', function(){
		$('.hs-timer').timer();
		});
	});
});