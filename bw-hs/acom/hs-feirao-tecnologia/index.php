<?php
include_once('acom-hs-tpl/header.php');

//define('BASEURL', 'http://o.apps.americanas.com.br/media/global/hotsite/hs-torcer-para-ganhar-acom-visa/');
define('BASEURL', '');
?>

<!--SCRIPTS HS-->
<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>css/style.css" />
<script type="text/javascript" src="<?php echo BASEURL; ?>js/scripts.js"></script>
<!--END SCRIPTS HS-->

<!--HS BEGIN-->
<div id="hs-name">

	<div class="hs-wrapper">
		<div class="hs-content">
			<div class="hs-caixa-interna">
				<h1 class="hs-hide">Feirão de tecnologia</h1>
				
				<span class="hs-divisa"><p class="hs-header-text">Tudo de mais moderno para você aproveitar e economizar, é claro!<br />Só não deixe para depois, as ofertas têm tempo limitado.</p></span>
				
				<div class="hs-both"></div>
				
				<div class="hs-box-01-principal">
					<img class="hs-img-destaque" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-destaque.jpg" />
					<strong>Notebooks Samsung</strong>
					<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
					<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
				</div>
				
				<div style="margin-top:0;" class="hs-box hs-box-02">
					<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
					<strong>Notebooks Samsung</strong>
					<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
					<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
				</div>
				
				<div class="hs-box hs-box-03 hs-left">
					<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
					<strong>Notebooks Samsung</strong>
					<div class="hs-cont"><span>acaba em 03d 01h 15m</span>
					</div>
					<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
				</div>
				
				<div class="hs-box hs-box-04">
					<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
					<strong>Notebooks Samsung</strong>
					<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
					<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
				</div>
				
				<div class="hs-box hs-box-05">
					<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
					<strong>Notebooks Samsung</strong>
					<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
					<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
				</div>
				
				<div class="hs-both"></div>
				
				
				<a href="#" class="hs-banner"><img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/banner.jpg" /></a>
				
				<h2>última chance!</h2>
				<div class="hs-carrossel-01">
					<ul>
						<li>
							<div class="hs-box">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
								<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
							</div>
						</li>
						
						<li>
							<div class="hs-box">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
								<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
							</div>
						</li>
						
						<li>
							<div class="hs-box">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
								<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
							</div>
						</li>
						
						<li>
							<div class="hs-box">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
								<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
							</div>
						</li>
						
						<li>
							<div class="hs-box">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
								<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
							</div>
						</li>
						
						<li>
							<div class="hs-box">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
								<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
							</div>
						</li>
						
						<li>
							<div class="hs-box">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
								<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
							</div>
						</li>
						
						<li>
							<div class="hs-box">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
								<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
							</div>
						</li>
						
						<li>
							<div class="hs-box">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>
								<a href="#"><img class="hs-comprar" src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/btn-clique-confira.jpg" /></a>
							</div>
						</li>
					</ul>
				</div>
				<div class="hs-both"></div>	
				<a class="hs1-prev hs-hide" id="hs1_prev" href="#"><span>prev</span></a>
				<a class="hs1-next hs-hide" id="hs1_next" href="#"><span>next</span></a>
				
				
				
				<h2>vem por aí:</h2>
				<div class="hs-carrossel-02">
					<ul>
						<li>
							<div class="hs-box-bottom paulo">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box-bottom.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>							
							</div>
						</li>
						
						<li>
							<div class="hs-box-bottom">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box-bottom.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>						
							</div>
						</li>
						
						<li>
							<div class="hs-box-bottom">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box-bottom.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>						
							</div>
						</li>
						
						<li>
							<div class="hs-box-bottom">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box-bottom.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>							
							</div>
						</li>
						
						<li>
							<div class="hs-box-bottom">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box-bottom.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>						
							</div>
						</li>
						
						<li>
							<div class="hs-box-bottom">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box-bottom.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>						
							</div>
						</li>
						
						<li>
							<div class="hs-box-bottom">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box-bottom.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>							
							</div>
						</li>
						
						<li>
							<div class="hs-box-bottom">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box-bottom.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>							
							</div>
						</li>
						
						<li>
							<div class="hs-box-bottom">
								<img src="http://img.americanas.com.br/mktacom/hotsite/feira-tecnologia/img/img-box-bottom.jpg" />
								<strong>Notebooks Samsung</strong>
								<div class="hs-cont">
					 <span>acaba em &nbsp</span>
						 <div class="box-timer">
							<div class="hs-timer timer-ready" data-timer="29/07/2014 00:00:00">					
								 <div class="timer-day">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>d &nbsp</span>
								<div class="timer-hour">
									<div class="timer-number">
										<div class="timer-tens"></div>
										<div class="timer-units"></div>
									</div>
								</div>
								<span>h &nbsp</span>
									<div class="timer-minute">
									<div class="timer-number">
									<div class="timer-tens"></div>
									<div class="timer-units"></div>
								</div>
								</div>
								<span>m &nbsp</span>	
							</div>				
						</div><!--.box-timer-->
					</div>						
							</div>
						</li>
					</ul>	
					
					<a class="hs2-prev hs-hide" id="hs2_prev" href="#"><span>prev</span></a>
					<a class="hs2-next hs-hide" id="hs2_next" href="#"><span>next</span></a>
					
				</div>
				
				<div class="hs-both"></div>
			</div>	
		</div>
	</div>

</div><!--#hs-name-->

<!--HS END-->

<?php
include_once('acom-hs-tpl/footer.php');
?>
