<?php
header('Content-type: application/json');

if (!is_numeric($_GET['id']))
{
	$dados['total']=0;
	echo json_encode($dados);
	die;
}
$dados['cadastro']=1;
$dados['total']=10;
$dados['cpf']=$_GET['id'];
$dados['name']='Rodrigo Carneiro';

$dados['pedido']['02-547093408'][0]['serie']='1';
$dados['pedido']['02-547093408'][0]['cupom']='00231';

$dados['pedido']['02-547093408'][1]['serie']='1';
$dados['pedido']['02-547093408'][1]['cupom']='10576';

$dados['pedido']['02-547093408'][2]['serie']='1';
$dados['pedido']['02-547093408'][2]['cupom']='74843';

$dados['pedido']['02-547093408'][3]['serie']='1';
$dados['pedido']['02-547093408'][3]['cupom']='32098';

$dados['pedido']['02-547093408'][4]['serie']='1';
$dados['pedido']['02-547093408'][4]['cupom']='98751';

$dados['pedido']['02-546746046'][0]['serie']='1';
$dados['pedido']['02-546746046'][0]['cupom']='03123';

$dados['pedido']['02-546746046'][1]['serie']='1';
$dados['pedido']['02-546746046'][1]['cupom']='54321';

$dados['pedido']['02-546746046'][2]['serie']='1';
$dados['pedido']['02-546746046'][2]['cupom']='75667';

$dados['pedido']['02-546877844'][0]['serie']='1';
$dados['pedido']['02-546877844'][0]['cupom']='94533';

$dados['pedido']['02-546877844'][1]['serie']='1';
$dados['pedido']['02-546877844'][1]['cupom']='11123';

$dados['coupons'] = array(
						array('promotional_code'=>'02-547093408', 
						 	  'numbers' => array(
						 	  				array('created_at'=>'2012-11-12 00:00:00', 'content' => '11123'),
						 	  				array('created_at'=>'2012-11-22 00:00:00', 'content' => '98751'),
						 	  				array('created_at'=>'2012-11-02 00:00:00', 'content' => '32098')
						 	  				)
							  )
						);


echo json_encode($dados);