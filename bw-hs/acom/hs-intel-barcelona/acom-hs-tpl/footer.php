

					</div><!-- #center -->
				</div><!-- #hotsite-destaques -->
			</div>
		</div>
		<div id="area03"></div>
		</div>

		<div id="footer">
			<link rel="stylesheet" href="http://img.americanas.com.br/statics-1.62.4.2625/catalog/css/footer.min.css"/>
			<footer class="a-footer" role="contentinfo" itemscope="itemscope" itemtype="http://data-vocabulary.org/Organization">
				<div class="ft-wp">
					<div class="ft-header">
						<a href="/" title="Americanas.com" class="ft-logo" itemprop="url" crmwa_mdc="Footer|americanas.com|"><span itemprop="name">Americanas.com</span></a>
						<strong class="ft-slogan">a maior loja. os menores preços.</strong>
						<span class="ft-txt"> não encontrou o que procurava? <a href="/estaticapop/deixar-sugestao" data-modal="true" data-modal-maxwidth="550" data-modal-maxheight="400" title="deixe aqui a sua sugestão de produto." class="ft-txt-lnk" crmwa_mdc="Footer|deixe_aqui_a_sua_sugestao_de_produto.|">deixe aqui a sua sugestão de produto.</a> </span>
					</div>
					<div class="ft-info">
						<div class="ft-pure-g">
							<div class="ft-pure-u-1-5">
								<div class="ft-telephones">
									<strong class="ft-tit"> <a href="http://www.americanas.com.br/estaticapop/popCentralAtendimento/" title="Compre pelo telefone" crmwa_mdc="Footer|compre_pelo_telefone|">compre pelo telefone <span itemprop="tel">4003.1000</span></a> </strong>
									<strong class="ft-tit"> fale com a gente <span itemprop="tel">4003.4848</span> </strong>
								</div>
							</div>
							<div class="ft-pure-u-1-5">
								<div class="ft-questions">
									<strong class="ft-tit">dúvidas</strong>
									<ul class="ft-list1">
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/central-de-atendimento" class="ft-lt1-lnk" title="Central de atendimento" crmwa_mdc="Footer|central_de_atendimento|">Central de atendimento</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/estatica/como-comprar" class="ft-lt1-lnk" title="Como comprar" crmwa_mdc="Footer|como_comprar|">Como comprar</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://faq.americanas.com.br/faq/?A=MTU2&amp;B=&amp;all=true&amp;T=VHJvY2FzIGUgZGV2b2x1w6fDtWVz#MTU3" class="ft-lt1-lnk" title="Trocas e devoluções" crmwa_mdc="Footer|trocas_e_devolucoes|">Trocas e devoluções</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://faq.americanas.com.br/faq/?A=MTU0&amp;B=&amp;all=true&amp;T=UHJvY2Vzc28gZGUgZW50cmVnYQ==#MTU1" class="ft-lt1-lnk" title="Processo de Entrega" crmwa_mdc="Footer|processo_de_entrega|">Processo de Entrega</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/estatica/resultadopromocoes" class="ft-lt1-lnk" title="Resultado de promoções" crmwa_mdc="Footer|resultado_de_promocoes|">Resultado de promoções</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/estatica/politica-de-privacidade" title="Política de privacidade" class="ft-lt1-lnk" crmwa_mdc="Footer|politica_de_privacidade|">Politica de privacidade</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/estaticapop/regras-do-site" class="ft-lt1-lnk" crmwa_mdc="Footer|link|">Termos de uso e condições</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="ft-pure-u-1-5">
								<div class="ft-institutional">
									<strong class="ft-tit">institucional</strong>
									<ul class="ft-list1">
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/estatica/sobre-americanas" class="ft-lt1-lnk" title="Sobre a Americanas.com" crmwa_mdc="Footer|sobre_a_americanas.com|">Sobre a Americanas.com</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.b2wdigital.com" title="Investidores B2W" class="ft-lt1-lnk" crmwa_mdc="Footer|investidores_b2w|">Investidores B2W</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://ri.lasa.com.br" title="Lojas Americanas S.A." class="ft-lt1-lnk" crmwa_mdc="Footer|lojas_americanas_s.a.|">Lojas Americanas S.A.</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/estatica-lasa/lasa" class="ft-lt1-lnk" title="Loja mais próxima" crmwa_mdc="Footer|loja_mais_proxima|">Loja mais próxima</a>
										</li>
										<li class="ft-lt1-it">
											<a href="https://site.vagas.com.br/PagEmpr.asp?e=b2w" target="_blank" title="Trabalhe com a gente" class="ft-lt1-lnk" crmwa_mdc="Footer|trabalhe_com_a_gente|">Trabalhe com a gente</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/estatica/assistencia-tecnica-fabricantes" class="ft-lt1-lnk" crmwa_mdc="Footer|link|">Nossos principais fornecedores</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/estatica/seja-nosso-fornecedor" class="ft-lt1-lnk" title="Seja nosso fornecedor" crmwa_mdc="Footer|seja_nosso_fornecedor|">Seja nosso fornecedor</a>
										</li>
										<li class="ft-lt1-it">
											<a href="#" class="ft-lt1-lnk" crmwa_mdc="Footer|link|">Anuncie na Americanas.com</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.afiliados.com.br/americanas/" targe="_blank" class="ft-lt1-lnk" crmwa_mdc="Footer|link|">Programa de Afiliados</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="ft-pure-u-1-5">
								<div class="ft-services">
									<strong class="ft-tit">serviços</strong>
									<ul class="ft-list1">
										<li class="ft-lt1-it">
											<a href="http://viagens.americanas.com.br/default.aspx?utm_source=americanas&amp;utm_medium=link_building&amp;utm_campaign=viagens&amp;s_cid=link_building_viagens" title="Americanas Viagens" class="ft-lt1-lnk" crmwa_mdc="Footer|americanas_viagens|">Americanas Viagens</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://carrinho.americanas.com.br/lista-de-casamento/pages/HomePage" title="Lista de casamento" class="ft-lt1-lnk" crmwa_mdc="Footer|lista_de_casamento|">Lista de Casamento</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.americanas.com.br/negocios-corporativos" title="Vendas Corporativas" class="ft-lt1-lnk" crmwa_mdc="Footer|vendas_corporativas|">Vendas Corporativas</a>
										</li>
									</ul>
									<strong class="ft-tit">parceiros</strong>
									<ul class="ft-list1">
										<li class="ft-lt1-it">
											<a href="http://novosite.ingresso.com/sao-paulo/home/?default=true" title="Ingresso.com" class="ft-lt1-lnk" crmwa_mdc="Footer|ingresso.com|">Ingresso.com</a>
										</li>
										<li class="ft-lt1-it">
											<a href="http://www.soubarato.com.br/?epar=ds_bs_00_am_siteacomsbacom3&amp;opn=SBACOM3" class="ft-lt1-lnk" title="Sou Barato" crmwa_mdc="Footer|sou_barato|">Sou Barato</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="ft-pure-u-1-5">
								<div class="ft-banner">
									<div class="app-oas" data-oas-width="170" data-oas-height="200" data-oas="Right2"></div>
								</div>
							</div>
						</div>
						<div class="ft-pure-g">
							<div class="ft-pure-u-1-2">
								<div class="ft-news-social">
									<strong class="ft-tit side">novidades, ofertas e mais</strong>
									<ul class="ft-list2">
										<li class="ft-lt2-it">
											<a href="https://www.facebook.com/AmericanasCom" target="_blank" title="Facebook" class="ft-lt2-bt bt-fb" crmwa_mdc="Footer|facebook|">Facebook</a>
										</li>
										<li class="ft-lt2-it">
											<a href="https://twitter.com/americanascom" target="_blank" title="Twitter" class="ft-lt2-bt bt-tw" crmwa_mdc="Footer|twitter|">Twitter</a>
										</li>
										<li class="ft-lt2-it">
											<a href="https://plus.google.com/+americanascom/posts" target="_blank" title="Google+" class="ft-lt2-bt bt-gp" crmwa_mdc="Footer|google+|">Google+</a>
										</li>
									</ul>
								</div>
							</div>

							<link type="text/css" rel="stylesheet" href="http://img.americanas.com.br/mktacom/site/footer/css/cr-footer.css" />

							<div class="ft-pure-u-1-2">
								<div class="ft-newsletter a-newsletter">
									<form class="ft-form app-cr"
									data-cr-origin="48"
									data-cr-event-name="CR_FOOTER"
									data-cr-event-id="4255"
									data-cr-opt="out"
									data-cr-confirmation="true">
										<label for="cr-form-email-bottom-home"> <strong class="ft-tit side">ofertas exclusivas para você</strong> </label>
										<input type="text" placeholder="digite seu e-mail" name="cr-email" class="ft-fr-ipt" id="cr-form-email-bottom-home" />
										<input type="submit" value="ok" class="ft-fr-bt"/>

										<div class="cr-msg"></div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="ft-info">
						<div class="ft-if-payment">
							<strong class="ft-tit">formas de pagamento</strong>
							<div class="ft-if-pay cards">
								<span class="ft-small-tit">cartões</span>
								<ul class="ft-if-pay-lst">
									<li>
										Visa
									</li>
									<li>
										MasterCard
									</li>
									<li>
										AmericanExpress
									</li>
									<li>
										Diners Club
									</li>
									<li>
										Aura
									</li>
									<li>
										Elo
									</li>
									<li>
										Casa
									</li>
									<li>
										Caixa
									</li>
								</ul>
							</div>
							<div class="ft-if-pay billet">
								<span class="ft-small-tit">boleto</span>
								<ul class="ft-if-pay-lst">
									<li>
										Boleto Bancário
									</li>
								</ul>
							</div>
							<div class="ft-if-pay debt">
								<span class="ft-small-tit">débito online</span>
								<ul class="ft-if-pay-lst">
									<li>
										Visa
									</li>
									<li>
										Itaú
									</li>
									<li>
										Bradesco
									</li>
									<li>
										Banco do Brasil
									</li>
								</ul>
							</div>
						</div>
						<div class="ft-if-seals">
							<ul class="ft-list2">
								<li class="ft-lt2-it">
									<div id="armored_website" style="width: 115px; height: 32px;">
										<a rel="canonical" href="http://cdn.siteblindado.com/lp_aw/verificar-pt-br.html?url=www.americanas.com.br" onclick="javascript:window.open(this.href);return false;" title="Visualizou o selo Site Blindado? Navegue tranquilamente, esse site esta BLINDADO CONTRA ATAQUES. Realizamos milhares de testes simulando ataques de hacker, para garantir a segurança do site. Clique no selo e confira o certificado." crmwa_mdc="Footer|visualizou_o_selo_site_blindado?_navegue_tranquilamente_esse_site_esta_blindado_contra_ataques._realizamos_milhares_de_testes_simulando_ataques_de_hacker,_para_garantir_a_seguranca_do_site._clique_no_selo_e_confira_o_certificado.|"><img src="http://cdn.siteblindado.com/seals_aw/americanas.com.br/siteblindado_pr.gif" style="border-style: none"/></a>
										<param id="aw_nav_post" value="1"/>
									</div>
								</li>
								<li class="ft-lt2-it">
									<span class="ft-lt2-bt certsign-seal">Site Seguro Validado por Certsign</span>
								</li>
								<li class="ft-lt2-it">
									<span class="ft-lt2-bt safe-internet-seal">Internet Segura</span>
								</li>
								<li class="ft-lt2-it">
									<a href="https://sealinfo.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=CARRINHO.AMERICANAS.COM.BR&amp;lang=pt" class="ft-lt2-bt verisign-seal" onclick="window.open(this.href,'Lojas','toolbar=no,location=no,status=yes,menubar=no,resizable=no,scrollbars=yes,width=650,height=565');return false" crmwa_mdc="Footer|link|">Confirmar certificado de segurança na Verising</a>
								</li>
								<li class="ft-lt2-it">
									<span class="ft-lt2-bt tim-seal">Movido com Intelig Telecom</span>
								</li>
								<li class="ft-lt2-it">
									<span class="ft-lt2-bt green-campaign-seal">Movido com Intelig Telecom</span>
								</li>
							</ul>
						</div>
					</div>
					<div class="ft-address">
						<address class="ft-address-txt">
							<span itemprop="address">B2W - CIA GLOBAL DO VAREJO / CNPJ: 00776574/0001-56 / Inscrição Estadual:
								492.513.778.117 / Endereço: Rua Sacadura Cabral, 102 - Rio de Janeiro, RJ - 20081-902</span><span itemprop="geo" hidden="hidden">latitude:-22.897409, longitude:-43.184372</span>
							<a href="http://www.americanas.com.br/atendimento-email" class="ft-email-lnk" title="email de atendimento" itemprop="email" target="_top">atendimento.acom@americanas.com</a> - <a href="http://www.americanas.com.br/mapa-do-site" title="mapa do site" crmwa_mdc="Footer|mapa_do_site|" class="ft-address-lnk"> mapa do site</a>
						</address>
					</div>
				</div>
			</footer>
			<script>
				if ( typeof jQuery != 'undefined') {
					$(function() {
						$(".home-page .pure-u-3-4 .single-product").each(function(index, b) {
							var href = $(this).find(".prodTitle").attr("href");
							$(this).find(".prodTitle").attr("href", href + "?chave=HM_SAZ_VT_" + (index + 1));
							$(this).find(".url").attr("href", href + "?chave=HM_SAZ_VT_" + (index + 1));
						});

						var count = 0;
						$(".home-page [data-oas='x33']").closest(".grid-control").find(".single-product").each(function(index) {
							var href = $(this).find(".prodTitle").attr("href");
							$(this).find(".prodTitle").attr("href", href + "?chave=HM_BL1_VT_" + (index + 1));
							$(this).find(".url").attr("href", href + "?chave=HM_BL1_VT_" + (index + 1));
							count = index + 1;
						});
						$(".home-page [data-oas='x26']").closest(".grid-control").find(".single-product").each(function(index) {
							var href = $(this).find(".prodTitle").attr("href");
							count++;
							$(this).find(".prodTitle").attr("href", href + "?chave=HM_BL2_VT_" + (count));
							$(this).find(".url").attr("href", href + "?chave=HM_BL2_VT_" + (count));
						});
					});
				}
			</script>
			<div class="sitemap-link">
				a maior loja. os menores preços.
				<a href="http://www.americanas.com.br/mapa-do-site?WT.mc_id=menuFooter-institucional-mapa">mapa do site</a>
			</div>
		</div>

		<p class="hc">
			<a rel="nofollow" href="#inicio">ir para o início <strong>(atalho + 4)</strong></a>
			<a rel="nofollow" href="#conteudo">ir para o conteúdo <strong>(atalho + 1)</strong></a>
			<a rel="nofollow" href="#menu">ir para o menu <strong>(atalho + 2)</strong></a>
		</p>
		</div>
		<script type="text/javascript" src="http://iacom.s8.com.br/statics-1.62.4.2626/catalog/js/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="http://iacom.s8.com.br/statics-1.62.4.2626/catalog/js/jquery-p.js"></script>
		<script type="text/javascript" src="http://iacom.s8.com.br/statics-1.62.4.2626/catalog/js/catalog.js"></script>

		<NOSCRIPT>
			<img alt="•" width="1" height="1" src="http://wtb.americanas.com.br/dcs20cr2u0pkytlvcatlar3yv_5v5h/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.0.2" />
		</NOSCRIPT>
		!-- general static content acom 296555-->

		<!-- Selo -->
		<script type="text/javascript" src="http://apps.americanas.com.br/media/selo/js/selo.js"></script>
		<!-- End -->

		<script type="text/javascript">
			(function() {
try {
var viz = document.createElement('script');
viz.type = 'text/javascript';
viz.async = true;
viz.src = ('https:' == document.location.protocol ?'https://ssl.vizury.com' : 'http://www.vizury.com')+ '/analyze/pixel.php?account_id=VIZVRM863';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(viz, s);
viz.onload = function() {
pixel.parse();
};
viz.onreadystatechange = function() {
if (viz.readyState == "complete" || viz.readyState == "loaded") {
pixel.parse();
}
};
} catch (i) {
}
})();
		</script>

		<!-- Begin Monetate tag v6. Place at start of document head. DO NOT ALTER. -->

		<script type="text/javascript">

var monetateT = new Date().getTime();
(function() {
var p = document.location.protocol;
if (p == "http:" || p == "https:") {
var m = document.createElement('script'); m.type = 'text/javascript'; m.async = true; m.src = (p == "https:" ? "https://s" : "http://") + "b.monetate.net/js/1/a-1ec66ef9/p/americanas.com.br/" + Math.floor((monetateT + 1521017) / 3600000) + "/g";
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(m, s);
}
})();

		</script>
		<!-- End Monetate tag. -->

		<!-- Adobe Marketing Cloud Tag Loader Code -->

		<script type="text/javascript">
var container = "ZDMtYjJ3LTYwMC0yNDgt"
if(location.pathname.indexOf("/blacknight") >=0){
container = "ZDMtYjJ3LTYwMC0zNTkt"
}
//<![CDATA[
var amc=amc||{};if(!amc.on){amc.on=amc.call=function(){}};
document.write("<scr"+"ipt type=\"text/javascript\" src=\"//www.adobetag.com/d3/v2/" + container + "/amc.js\"></sc"+"ript>");
//]]>
		</script>

		<!-- End Adobe Marketing Cloud Tag Loader Code -->

		<!--Maxymiser script start -->
		<script type="text/javascript"src="//service.maxymiser.net/cdn/pakua/submarino/js/mmcore.js"></script>
		<!--Maxymiser script end -->

		<script type="text/javascript">
jQuery(function() {
if(jQuery("#acomNick").text() == "") {
jQuery.getScript("http://img.americanas.com.br/statics-1.62.4.2625/catalog/js/v1/header.js");
}
});
		</script>
		<script type="text/javascript">
if (!NREUMQ.f) { NREUMQ.f=function() {NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:") + "//" + "js-agent.newrelic.com/nr-100.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","e6936dfb0e","1677856,1677857,1830472","MwAGYEBUX0IDAUwKXwpKN0BAQEVCIwFMCl8KSgFHRlRFWAEDFytfEBYNQFcYXF4GAxUQXA0I",0,34,new Date().getTime(),"","","","",""]);
		</script>
	</body>
</html>