<?php
include_once('acom-hs-tpl/header.php');
define('DEBUG', true);

if(DEBUG) :
	define('BASEURL', '');
	define('URL_RULES', 'regulamento.html');
else :
	define('BASEURL', 'http://apps.americanas.com.br/media/global/hotsite/hs-intel-barcelona/');
	define('URL_RULES', 'http://apps.americanas.com.br/media/global/hotsite/hs-intel-barcelona/regulamento.html');
endif;
?>

<!--HS BEGIN-->
<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>css/style.css" />
<script type="text/javascript" src="<?php echo BASEURL; ?>js/scripts.js"></script>

<div id="hs-torcer-para-ganhar">
	
    <div class="topo">
    	<div class="content">
    		<ul class="midiasSociais">
    			<li><a href="http://on.fb.me/1dNDJ2g" target="_blank" class="f"></a></li>
    			<li><a href="http://bit.ly/shgdsgd" target="_blank" class="t"></a></li>
    			<li><a href="http://bit.ly/GPromo" target="_blank" class="g"></a></li>
    		</ul>
    	</div><!--.content-->
    </div><!--.topo-->
    
    <div class="nav">
    	<div class="content">
    		<ul class="menu">
    			<li><a href="#principal">início</a></li>
    			<!--li><a href="#cadastre-se">cadastre-se</a></li-->
    			<li><a href="#premios">prêmios</a></li>
    			<li><a href="#regulamento">regulamento e FAQ</a></li>
    			<li id="menu-logged"><a href="#logged">Cupons</a></li>
    			<li><a href="#vencedores">vencedores</a></li>
    		</ul>
    		
    		<div class="confira-tickets">
    			<form id="formAuthNav" action="">
	        		<ul>
	        			<li style="padding-top: 2px;">Confira seus tickets</li>
	        			<li><input type="text" placeholder="e-mail" class="text promo-login-email placeholder" id="" name="user[email]" /><div class="answer"></div></li>
	        			<li><input type="text" placeholder="cpf" class="text promo-login-cpf placeholder" id="" name="user[cpf]" maxlength="14" onkeypress="MascaraCPF(this, event); return numbersonly(this, event);" onblur="ValidarCPF(this);" /></li>
	        			<li><input type="submit" value="entrar" class="submit" /></li>
	        		</ul>
        		</form>
        		<input type="submit" value="logout" class="promo-logout" />
    		</div><!--.confira-tickets-->
    	</div><!--.content-->
    </div><!--.nav-->
    
    <div class="wrap-content box-principal displayNone">

		<div class="img2">
			<h2 class="title">como participar?</h2>
			
			<a href="<?php echo URL_RULES; ?>" class="btn-regulamento fancybox" target="_blank">confira o regulamento</a>
			
			<img src="<?php echo BASEURL; ?>images/promo/page-principal/img2.jpg" />
		</div>
		
    </div><!--.wrap-content | box-principal-->
    
    <div class="wrap-content box-cadastre-se displayNone">
    	<h2 class="title">participe!</h2>
    	
		<form id="formPromoCadastrar" onsubmit="validaFormNiver(this.id); return false;">
			<div class="box-cadastro">
				<div class="col-left">
					<h2>Faça já o seu cadastro para participar.</h2>
					
					<p>Atenção!<br />
					Mesmo que você já seja cadastrado no site da Americanas.com, é preciso se cadastrar na promoção preenchendo o formulário ao lado.
					</p>
					
					<p>Ah, você deve usar o mesmo CPF para o cadastro no site e na promoção!</p>
					
					<p>As informações precisam estar corretas para garantir a entrega dos prêmios.</p>
				</div><!--.col-left-->
				
				<div class="col-right">

					<div class="left field span2">
						<label for="promo-user-cpf">cpf</label>
						<input type="text" id="promo-user-cpf" class="text" maxlength="14" onkeypress="MascaraCPF(this, event); return numbersonly(this, event);" onblur="ValidarCPF(this);" />
					</div>
					
					<div class="right field span2">
						<label for="promo-user-rg">rg</label>
						<input type="text" id="promo-user-rg" class="text" onkeypress="return numbersonly(this, event);" />
					</div>
					<div class="clear"></div>
					
					<div class="field span1">
						<label for="promo-user-nome">nome completo</label>
						<input type="text" id="promo-user-nome" class="text" />
					</div>
					
					<div class="left field span3">
						<label for="promo-user-cep">cep</label>
						<input type="text" id="promo-user-cep" class="text" onkeypress="$validacao.maskme(this, '########'); return $validacao.somenteNumeros(event);" maxlength="8" />
					</div>
					
					<div class="left field span3 marginLeft10">
						<label for="promo-user-telefone">telefone</label>
						<input type="text" id="promo-user-telefone" class="text" onkeypress="$validacao.maskme(this, '## #########'); return $validacao.somenteNumeros(event); " maxlength="12" />
					</div>
					
					<div class="right field span3">
						<label for="promo-user-nascimento">data de nascimento</label>
						<input type="text" id="promo-user-nascimento" class="text" onkeypress="$validacao.maskme(this, '##/##/####'); return $validacao.somenteNumeros(event); " maxlength="10" />
					</div>
					<div class="clear"></div>
					
					
					<div class="left field span2">
						<label for="promo-user-endereco">endereço</label>
						<input type="text" id="promo-user-endereco" class="text" />
					</div>
					
					<div class="right field span2">
						<label for="promo-user-estado">estado</label>
						<input type="text" id="promo-user-estado" class="text" />
					</div>
					<div class="clear"></div>
					
	
					<div class="field span1">
						<label for="promo-user-email">e-mail</label>
						<input type="text" id="promo-user-email" class="text block" />
					</div>
					
					<div class="field span1">
						<label for="promo-user-email-confirm">confirmar e-mail</label>
						<input type="text" id="promo-user-email-confirm" class="text block" />
					</div>
					
					<div class="field marginBottom10">
						<div class="checkbox"><input type="checkbox" id="promo-declaro" /></div> declaro que li e concordo com o regulamento.
						<div class="clear"></div>
					</div>
					
					<div class="field marginBottom10">
						<div class="checkbox"><input type="checkbox" id="promo-receber-emails" /></div> desejo receber e-mails com novidades e ofertas<br /> da Americanas.com e Lojas Americanas.
						<div class="clear"></div>
					</div>
					
					<input type="hidden" id="raffleName" value="hs-intel-barcelona" />
					
					<input type="submit" value="enviar" id="submit" class="submit" />
					
					<div class="answer"></div>
					
				</div><!--.col-right-->
				<div class="clear"></div>
			</div><!--.box-cadastro-->
		</form>
		
    </div><!--.wrap-content | box-cadastre-se-->
    
    <div class="wrap-content box-premios displayNone">
    	<h2 class="title">prêmios</h2>
		
		<ul>
			<li>11 pares de ingressos para jogo do Barcelona no estádio Camp Nou,<br /> pelo campeonato espanhol (1 par para cada vencedor)</li>
			<li>Passagem aérea para Barcelona.</li>
			<li>Hospedagem de 3 diárias</li>
			<li>Traslados e alimentação</li>
			<li>Visita ao Museu do Clube Barcelona</li>
		</ul>
		
		<img src="<?php echo BASEURL; ?>images/promo/page-premios/premios-bg.jpg" class="premios" />
    </div><!--.wrap-content | premios-->
    
    <div class="wrap-content box-regulamento displayNone">
    	<h2 class="title">regulamento</h2>
    	
    	<a href="<?php echo URL_RULES; ?>" class="btn-regulamento fancybox" target="_blank">ler o regulamento</a>
          	
    	<h2 class="title faq">FAQ</h2>
    	<p>Leia abaixo, perguntas e respostas mais frequentes.</p>
		
		<div class="accordion">
			<h3>1.	Qual o período de participação da promoção?</h3>
			<div>O período de participação na promoção se inicia a partir das 0h00 (zero hora) do dia 15 de maio de 2014 e se estende até às 23h59m59s (vinte e três horas e cinquenta e nove minutos e cinquenta e nove segundos) do dia 30 de junho de 2014, no horário de Brasília.</div>
			
			<h3>2.	Preciso realizar o cadastro para participar da promoção?</h3>
			<div>
				<p>Sim, para concorrer aos  prêmios do sorteio é necessário realizar o cadastro no site da Promoção da  Americanas.com <a href="http://www.americanas.com.br/ole-intel">www.americanas.com.br/ole-intel</a>, mesmo já tendo cadastro no site da loja online.</p>
			</div>
			
			<h3>3.	Quantos e quais são os prêmios?</h3>
			<div>
				<p>Serão distribuídos pela Promoção 11 (onze) prêmios, sendo  um prêmio para cada contemplado, conforme discriminados na tabela a seguir: <strong></strong></p>
				<table border="1" cellspacing="0" cellpadding="0">
				  <tr>
				    <td width="361"><br>
				        <strong>Descrição do prêmio</strong> </td>
				    <td width="102"><p align="center"><strong>Quantidade de prêmios</strong></p></td>
				    <td width="121"><p align="center"><strong>Valor unitário (R$)</strong></p></td>
				  </tr>
				  <tr>
				    <td width="361"><p>1 (um) pacote de viagem com    hospedagem de 3 diárias para a Barcelona, com passagens aéreas, traslados e    alimentação, e 2 (dois) ingressos para jogo do Barcelona no estádio Camp Nou    pelo campeonato espanhol, a realizar-se em Outubro 2014, sendo um para o contemplado    e outro para um acompanhante, com direito a visita ao museu do clube.</p></td>
				    <td width="102"><p>11 (onze)</p></td>
				    <td width="121"><p>&nbsp;</p>
				        <p>R$14.000,00(quatorze mil reais)</p></td>
				  </tr>
				  <tr>
				    <td width="361"><p align="center"><strong>VALOR TOTAL DOS PRÊMIOS (R$)</strong></p></td>
				    <td width="222" colspan="2"><p align="center">R$ 154.000,00 (cento e cinquenta e quatro mil    reais)<strong> </strong></p></td>
				  </tr>
				</table>


			</div>
			
			<h3>4.	Quem pode participar?</h3>
			<div>
				<p>Participam do sorteio desta Promoção apenas as pessoas  físicas maiores de 13 (treze) anos, residentes e domiciliadas no Brasil, que,  ao longo do período de participação, comprarem produtos que contenham  processadores INTEL® nas lojas on-line do grupo B2W, Americanas.com, e que  cumpram as demais condições do Regulamento. </p><p>
  Aos participantes com idade entre 13 (treze) e 17  (dezessete) anos, recomenda-se o acompanhamento dos pais ou responsáveis legais  ao longo de todo o período da promoção.</p>

			</div>
			
			<h3>5.	Como participar?</h3>
			<div>
				<p>Para concorrer aos prêmios, o participante deverá se  cadastrar na promoção e adquirir um produto que contenha processadores INTEL®  na loja virtual do grupo B2W, Americanas.com ao longo do período de  participação e a cada produto adquirido o participante cadastrado receberá 1  (um) elemento sorteável depois de validada a sua participação. Considera-se a  participação validada após a aprovação do pagamento do pedido. </p><p>
  Serão considerados apenas os pedidos realizados por  pessoas físicas.</p><p>
  Apenas participarão da promoção os pedidos que forem  aprovados integralmente até 23h59m59s (vinte e três horas, cinquenta e nove  minutos e cinquenta e nove segundos) do dia 15 de julho de 2014, horário de Brasília  e que forem realizados durante o período de participação.</p><p>
  Quanto mais produtos com processadores INTEL®  comprados cujos números de pedido e demais dados forem devidamente cadastrados  no site, mais chances o participante terá.</p>
	
			</div>
			
			<h3>6.	Como se cadastrar?</h3>
			<div>
				<p>Para cadastrar-se na Promoção, o participante deverá acessar  o site <a href="//www.americanas.com.br/ole-intel">www.americanas.com.br/ole-intel</a> e realizar o cadastro preenchendo  correta e integralmente os dados solicitados (nome completo, CPF, RG, data de  nascimento, endereço completo, e-mail e telefone celular). Para finalizar o  cadastro o participante deverá, ainda, aceitar os termos do Regulamento da  presente Promoção. Na eventualidade de ser contemplado, será considerado  ganhador a pessoa cujos dados constar no cadastro.</p>


			</div>
			
			<h3>7.	O que são elementos sorteáveis?</h3>
			<div>
				<p>Os elementos sorteáveis são os números utilizados para  a escolha dos vencedores. Os prêmios serão atribuídos aos portadores dos  elementos sorteáveis extraídos da Loteria Federal. </p><p>
  Será considerado vencedor o participante cujo campo  numérico da série seja igual à unidade de centena do primeiro prêmio da Loteria  Federal, e cujo elemento sorteável seja igual à unidade de dezena dos 05  prêmios da Loteria Federal, lidos de cima para baixo. <br>
  Supondo que numa extração hipotética da Loteria Federal,  sejam extraídos os seguintes números:</p>
				<ol>
				  <li><strong>1º Prêmio                            6 1. 30 6</strong></li>
				  <li><strong>2º Prêmio                            1 2. 59 4</strong></li>
				  <li><strong>3º Prêmio                            1 3. 7 70</strong></li>
				  <li><strong>4º Prêmio                            7 1. 67 8</strong></li>
				  <li><strong>5º Prêmio                            2 5. 71 5</strong></li>
				</ol>
				<p>Neste caso, seria contemplado o elemento sorteável de  número da sorte <strong>09771 </strong>(em vermelho) da série <strong>3</strong> (em azul). </p><p>
				  Serão contemplados também os 10 (dez) números da sorte  imediatamente seguintes ao número da sorte do primeiro elemento sorteável  contemplado. Portanto, conforme o exemplo acima seriam contemplados os  elementos sorteáveis cujos números da sorte fossem o <strong>09772, 09773, 09774, 09775, 09776, 09777,  09778, 09779, 09780, 09781 </strong>da série contemplada no primeiro elemento  sorteável ganhador.</p>

			</div>
			
			<h3>8.	O que preciso fazer para ganhar os números para o sorteio?</h3>
			<div>
				Já cadastrado na promoção, indicando o prêmio, e uma vez efetuadas compras com o mesmo CPF utilizado no cadastro, a Promotora efetua o cruzamento das informações e o participante passa a concorrer automaticamente aos prêmios, desde que respeitados todos os termos do Regulamento. Não é necessário nenhum passo adicional.	
			</div>
			
			<h3>9.	Como meus números de pedidos são transformados em elementos sorteáveis?</h3>
			<div>
				<p>A cada compra efetuada na  loja online Americanas.com, o cliente receberá um número de pedido. De acordo,  com o número de produtos com processadores Intel comprados, você receberá um  elemento sorteável.<br>
  Exemplo:</p><p>
  1 produto com processador  Intel = 1 elemento sorteável<br>
  2 produtos com  processadores Intel = 2 elementos sorteáveis </p>

			</div>
			
			<h3>10.	Onde consulto meus números da sorte?</h3>
			<div><p>Os elementos sorteáveis serão distribuídos e  divulgados a você em seu extrato pessoal no site <a href="//www.americanas.com.br/ole-intel">www.americanas.com.br/ole-intel</a>  até um dia antes do sorteio, ou seja, dia 15 de julho de 2014. </p>
</div>
			
			<h3>11.	Quando será realizado o sorteio?</h3>
			<div>O sorteio acontecerá em 16/07/2014 e serão sorteados 11 ganhadores que vão ganhar pacote de viagem com hospedagem de 3 diárias, para a Barcelona, com passagens aéreas, traslados e alimentação e 2 (dois) ingressos para jogo do Barcelona no estádio Camp Nou pelo campeonato espanhol, a realizar-se em Outubro 2014, sendo um para o contemplado e outro para um acompanhante, com direito a visita ao museu do clube.</div>
			
			<h3>12.	Como saberei se sou um dos sorteados?</h3>
			<div><p>O resultado da Promoção e o nome dos contemplados serão  divulgados no site da Promoção em <a href="//www.americanas.com.br/ole-intel">www.americanas.com.br/ole-intel</a>  da Promoção em até 25 (vinte e cinco) dias após a apuração.</p>
				<p>Os contemplados serão comunicados do resultado da promoção pelo envio preferencialmente de e-mail e subsidiariamente de telegrama ou carta com aviso de recebimento (AR), bem como por contato telefônico, no prazo máximo de 20 (vinte) dias da data da apuração, de acordo com os dados cadastrais mantidos pela Promotora e pela Aderente.</p>
</div>
			
			<h3>13.	Quais documentos serão necessários para receber meu prêmio?</h3>
			<div>
				<p>Você deverá receber o prêmio  pessoalmente e, neste ato, assinar e preencher a carta compromisso, apresentar  RG e CPF, devendo fornecer ainda qualquer informação adicional que possa vir a  ser requerida, dentro dos limites da lei.</p><p>
  Todo elemento  sorteável estará ligado às informações do Número do Pedido e a Promotora  validará se a Número do Pedido apresentado corresponde às informações  cadastradas no site da Promoção e se o pedido foi faturado, ou seja, não pode  ter ocorrido o cancelamento posterior da compra.</p><p>
  O contemplado será informado dos procedimentos para  retirada dos ingressos posteriormente. </p><p>
  Na hipótese do contemplado não  poder receber pessoalmente o prêmio, por qualquer razão, será admitida sua  representação, por meio de procuração, em conformidade com a legislação  vigente. Deverá o contemplado constituir procurador por meio de mandato, por  instrumento público ou particular, com firma reconhecida e poderes específicos  para o fim que se destina.</p><p>
  A documentação pessoal que se fizer necessária ao  recebimento, retirada e a fruição do prêmio será de inteira responsabilidade do  participante contemplado, inclusive para o caso específico de contemplado que seja  menor de idade. </p>

			</div>
			
			<h3>14.	Onde retiro meus ingressos?</h3>
			<div>Se você for um dos ganhadores da promoção, receberá todas as informações de procedimento para recebimento do seu prêmio.</div>
			
			<h3>15.	Posso trocar os ingressos por dinheiro?</h3>
			<div>Não, os prêmios não podem ser trocados por dinheiro.</div>
			
			<h3>16.	Não moro do Brasil. Posso participar?</h3>
			<div>Não, a promoção é exclusiva para pessoas residentes e domiciliadas no Brasil.</div>
			
			<h3>17.	Posso ganhar mais de uma vez?</h3>
			<div>Sim, se você for sorteado. </div>
			
			<h3>18.	Posso dar os ingressos como presente?</h3>
			<div>Apenas o ganhador ou pessoa que tenha procuração para tanto poderá retirar os ingressos. Depois de retirado, você poderá entrega-los para outra pessoa utilizar, sendo certo que após a retirada dos ingressos encerra-se a responsabilidade da Promotora e das Aderentes quanto à utilização deles. </div>
			
			<h3>19.	Existe limite de cupons?</h3>
			<div>Não, você pode realizar quantas compras quiser ao longo do período de participação e ganhará um elemento sorteável para cada produto com processador Intel comprado.</div>
			
			<h3>20.	Posso cadastrar na promoção com mais de um e mail?</h3>
			<div>Não, você deve se cadastrar apenas um com um e-mail e CPF.</div>
			
			<h3 class="doubleLine">21.	Como posso ter mais informações sobre o processo de auditoria e funcionamento da mecânica da promoção de forma <br /> que eu possa me assegurar que tudo o que o regulamento informa é verídico?  </h3>
			<div>
				<p>A Promotora e  Aderentes zelam pela regularidade da mecânica de suas promoções mediante obtenção  de autorização expedida pela Caixa Econômica Federal, bem como pela contratação  de empresa de auditoria independente, desde o início de todas as suas  promoções.</p><p>
  São  fiscalizadas todas as etapas da promoção, como distribuição dos elementos  sorteáveis, apuração, identificação dos ganhadores etc. </p><p>
  Dúvidas  podem ser esclarecidas no site da promoção. </p>

			</div>			
						
		</div><!--.accordion-->
		
    </div><!--.wrap-content | premios-->
    
    <div class="wrap-content box-vencedores">
    	<h2 class="title">vencedores</h2>
    	
    	<ul class="list-vencedores">
    		<li><h3>Ticket nº: 3-15729</h3></li>
    		<li><h3>Ticket nº: 3-15904</h3></li>
			<li><h3>Ticket nº: 3-16698</h3></li>
			<li><h3>Ticket nº: 3-18328</h3></li>
			<li><h3>Ticket nº: 3-19688</h3></li>
			<li><h3>Ticket nº: 3-20112</h3></li>
    	</ul>
		
    </div><!--.wrap-content | vencedores-->
    
     <div class="wrap-content box-logged displayNone">
    	<h2 class="title">Olá <span id="userName"></span>!</h2>
    	<input type="hidden" id="hdpf" value="" />
    	
    	<div class="tickets-quant"><span id="tickets-total"></span></div><!--.tickets-quant-->

    	<p class="title">Seus tickets para o sorteio:</p>
    	
    	<table id="show-tickets">
    		<thead>
        		<tr>
        			<th class="col1">data da compra</th>
        			<th class="col2">número do pedido </th>
        			<th class="col3">ticket do sorteio</th>
        		</tr>
    		</thead>
    		<tbody>
        		
    		</tbody>
    	</table>
    	
    	<div class="aviso"><strong>Importante:</strong> Apenas os pedidos com pagamento aprovados serão transformados em número da sorte. Nosso sistema precisa de até 24 horas para validar seu código e gerar o ticket do sorteio.</div>
		
		<div class="clear"></div>
		
    </div><!--.wrap-content | vencedores-->
    
    <div class="clear"></div>
    
    <div class="list-publicidade">
		<ul>
			<li class="left"><a href="http://www.americanas.com.br/linha/350792/informatica/2-em-1?chave=pm_hotsite_intel_barca_2em1" target="_blank"><img src="<?php echo BASEURL; ?>images/promo/footer/banner-intel1.jpg" /></a></li>
			<li class="left"><a href="http://www.americanas.com.br/ofertas/HomeLandingPage5/pm_computadores_intel_acom/228190?chave=pm_hotsite_intel_barca_computadores" target="_blank"><img src="<?php echo BASEURL; ?>images/promo/footer/banner-intel2.jpg" /></a></li>
			<li class="right"><a href="http://www.americanas.com.br/ofertas/HomeLandingPage3/pm_tablet_intel_acom/228190?chave=pm_hotsite_intel_barca_tablet" target="_blank"><img src="<?php echo BASEURL; ?>images/promo/footer/banner-intel3.jpg" /></a></li>
		</ul>
		<div class="clear"></div>
    </div><!--.list-publicidade-->
    
    <div class="clear"></div>
    <p style="margin: 60px auto 0px auto; text-align: center;">Período de Participação de 15/05/2014 a 16/07/2014. Consulte o regulamento no site <a href="http://www.americanas.com.br/ole-intel">www.americanas.com.br/ole-intel</a>. Certificado de Autorização CAIXA nº 1-0954/2014</p>
	
	
	<div id="popupAniversario" style="width:542px; display: none;">
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_01.gif" style="display: block;" />
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_02.gif" style="display: block;" />
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_03.gif" style="display: block;" />
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_04.gif" style="display: block;" />
		<a href="http://www.americanas.com/especial/hotsite/aniversario2013/303994/tag_promoniver_patroc/303981?chave=hs_aniver2013_patrocinadores&WT.mc_id=hs_aniver2013_patrocinadores" target="_blank"><img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_05.gif" style="display: block;" /></a>
	</div>

</div><!--#hs-vem-pra-festa-->

<!--HS END-->

<?php
include_once('acom-hs-tpl/footer.php');
?>
