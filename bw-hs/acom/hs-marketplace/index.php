<?php
include_once('acom-hs-tpl/header.php');

define('BASEURL', 'http://img.americanas.com.br/mktacom/hotsite/hs-marketplace/');
//define('BASEURL', '');
?>

<!--HS BEGIN-->

<!--SCRIPTS HS-->
<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>js/colorbox/colorbox.css" />
<script type="text/javascript" src="<?php echo BASEURL; ?>js/validations.js"></script>
<script type="text/javascript" src="<?php echo BASEURL; ?>js/scripts.js"></script>
<script type="text/javascript">
onReady(function() {
	
	$.getScript('<?php echo BASEURL; ?>js/jquery.colorbox-min.js', function(){
	
		$(".inline").colorbox({inline:true, width:"940px"});
	
	});

});
</script>
<!--END SCRIPTS HS-->

<div id="hs-marketplace">
	
	<h1 id="hsTitle"></h1><!--#hsTitle-->
	
	<div id="boxInfo">
		<div class="text">
			<h2>Já imaginou sua loja<br />dentro da Americanas.com?</h2>
			<p>Com o Marketplace Americanas.com, você pode expor
	seus produtos em nosso site e atingir um número muito maior de clientes qualificados, além de contar com a tecnologia, a segurança e o atendimento da maior loja online da América Latina.</p>

			<p>Então, que tal aumentar suas vendas e aproveitar
				essa oportunidade única para o seu negócio?</p>
			<p>Faça parte da maior!</p>
		</div>
	</div><!--#boxInfo-->
	
	<h2>Como funciona?</h2>
	
	<ul id="listComoFunciona">
		<li>
			O vendedor parceiro, depois de cadastrado
e aprovado pela Americanas.com, passa a vender seus produtos no nosso
site e é responsável
pela entrega.
		</li>
		<li class="divisoria"></li>
		<li>
			As vendas acontecem
por meio do modelo de
negócio “one-stop shop”,
cujo objetivo principal
é reunir clientes e
vendedores em  uma
única plataforma.
		</li>
		<li class="divisoria"></li>
		<li>
			O parceiro disponibiliza seus produtos e condições comerciais e os clientes encontram produtos de diversos vendedores no mesmo catálogo, podendo fechar a compra em um único carrinho.
		</li>
		<li class="divisoria"></li>
		<li>
			Nosso parceiro constrói, desde o início, uma relação especial com o cliente, que já tem a Americanas.com como sua marca online preferida, facilitando o processo de decisão
de compra.
		</li>
	</ul><!--#	<ul id="listComoFunciona-->

	<h2>Por que é bom para o parceiro?</h2>
	
	<ul id="listBomParceiro">
		<li>
			<div class="num">1.</div>
			<div class="text">Mais de 45 milhões de acessos por mês.</div>
		</li>
		<li>
			<div class="num">2.</div>
			<div class="text">Plataforma tecnológica de alta qualidade (catálogo, checkout, busca, SEO).</div>
		</li>
		<li>
			<div class="num">3.</div>
			<div class="text">A 1a em atendimento ao cliente dando suporte do início ao fim da compra.</div>
		</li>
		<li>
			<div class="num">4.</div>
			<div class="text">Parcelamos o pagamento para o cliente e pagamos à vista para você.</div>
		</li>
		<li>
			<div class="num">5.</div>
			<div class="text">Mais segurança e análise de fraude no momento da compra.</div>
		</li>
	</ul><!--#listBomParceiro-->
	
	<div class="clear"></div>
	
	<a href="#popup_content" class="inline btn-parceiro"></a>
	
	<!-- This contains the hidden content for inline calls -->
		<div style="display:none;">
			<div id='popup_content'>
				<form id="formMarketplace">
					<div class="form-title"></div>
					<div class="col">
						<span>
							<h2>dados da empresa</h2>
						</span>
						<span>
							<label>nome fantasia</label>
							<input type="text" id="fantasia" class="text" />
						</span>
						<span>
							<label>CNPJ</label>
							<input type="text" id="cnpj" class="text" onkeypress="$validations.addMask(this, '00.000.000/0000-00', event); return $validations.somenteNumeros(event)" maxlength="18" />
						</span>
						<span>
							<label>telefone comercial</label>
							<input type="text" id="telComercial" class="text" onkeypress="$validations.addMask(this, '00 000000000', event); return $validations.somenteNumeros(event)" maxlength="12" />
						</span>
						<span>
							<label>site</label>
							<input type="text" id="site" class="text" />
						</span>
						<span>
							<label>plataforma utilizada</label>
							<input type="text" id="plataforma" class="text" />
						</span>
						<span>
							<label>departamento</label>
							<select id="departamento">
								<option value=""></option>
								<option value="ALIMENTOS BEBIDAS"></option>
								<option value="ALIMENTOS BEBIDAS">ALIMENTOS BEBIDAS</option>
								<option value="AUDIO">AUDIO</option>
								<option value="AUTOMOTIVO">AUTOMOTIVO</option>
								<option value="BEBES">BEBES</option>
								<option value="BELEZA SAUDE">BELEZA SAUDE</option>
								<option value="BRINQUEDOS">BRINQUEDOS</option>
								<option value="CAMA MESA BANHO">CAMA MESA BANHO</option>
								<option value="CASA JARDIM">CASA JARDIM</option>
								<option value="CDS DVDS MUSICAIS">CDS DVDS MUSICAIS</option>
								<option value="CINE FOTO">CINE FOTO</option>
								<option value="CLIMATIZAÇÃO">CLIMATIZAÇÃO</option>
								<option value="CONSOLES GAMES">CONSOLES GAMES</option>
								<option value="DVDS BLU-RAY">DVDS BLU-RAY</option>
								<option value="ELETRODOMESTICOS">ELETRODOMESTICOS</option>
								<option value="ELETRONICOS">ELETRONICOS</option>
								<option value="ELETROPORTATEIS">ELETROPORTATEIS</option>
								<option value="ESPORTE LAZER">ESPORTE LAZER</option>
								<option value="INFORMATICA ACESSORIOS">INFORMATICA ACESSORIOS</option>
								<option value="INSTRUMENTOS MUSICAIS">INSTRUMENTOS MUSICAIS</option>
								<option value="LIVROS REVISTAS">LIVROS REVISTAS</option>
								<option value="MALAS ACESSORIOS">MALAS ACESSORIOS</option>
								<option value="MODA ACESSORIOS">MODA ACESSORIOS</option>
								<option value="MOVEIS DECORACAO">MOVEIS DECORACAO</option>
								<option value="NOTEBOOKS, ULTRABOOKS E NETBOOKS">NOTEBOOKS, ULTRABOOKS E NETBOOKS</option>
								<option value="PAPELARIA">PAPELARIA</option>
								<option value="PERFUMARIA">PERFUMARIA</option>
								<option value="PET SHOP">PET SHOP</option>
								<option value="RELOGIOS PRESENTES">RELOGIOS PRESENTES</option>
								<option value="SUPLEMENTOS VITAMINAS">SUPLEMENTOS VITAMINAS</option>
								<option value="TABLETS, COMPUTADORES E PERIFÉRICOS">TABLETS, COMPUTADORES E PERIFÉRICOS</option>
								<option value="TELEFONIA">TELEFONIA</option>
								<option value="TELEFONIA FIXA">TELEFONIA FIXA</option>
								<option value="UTILIDADES DOMESTICAS">UTILIDADES DOMESTICAS</option>
							</select>
						</span>
					</div>
					
					<div class="col borderNone">
						<span>
							<h2>dados pessoais</h2>
						</span>
						<span>
							<label>nome</label>
							<input type="text" id="nome" class="text" />
						</span>
						<span>
							<label>sobrenome</label>
							<input type="text" id="sobrenome" class="text" />
						</span>
						<span>
							<label>celular</label>
							<input type="text" id="celular" class="text" onkeypress="$validations.addMask(this, '00 000000000', event); return $validations.somenteNumeros(event)" maxlength="12" />
						</span>
						<span>
							<label>telefone fixo</label>
							<input type="text" id="telFixo" class="text" onkeypress="$validations.addMask(this, '00 000000000', event); return $validations.somenteNumeros(event)" maxlength="12" />
						</span>
						<span>
							<label>e-mail</label>
							<input id="email" type="text" class="text" />
						</span>
						<span>
							<a href="#" class="btn-cadastrar"></a>
							
							<div class="formStatus"></div>
						</span>
					</div>
				</form>
			</div><!--#popup_content-->
		</div>
		
</div><!--#hs-marketplace-->

<!--HS END-->

<?php
include_once('acom-hs-tpl/footer.php');
?>
