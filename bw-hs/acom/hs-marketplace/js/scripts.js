onReady(function(){
	//Formulário Cadastro
	$('.btn-cadastrar').click(function(){
		
		if(validaForm('formMarketplace')) {
			
			// Faz o envio do formulário
			$.ajax({
				  type: "POST",
				  url: "http://apps.americanas.com.br/forms/hotsites/suba/marketplace/sendmail/email.php",
				  data: { 	
						    fantasia: $('#fantasia').val(), 
						    cnpj: $('#cnpj').val(),
							telComercial: $('#telComercial').val(),
							site: $('#site').val(),
							plataforma: $('#plataforma').val(),
							departamento: $('#departamento').val(),
							nome: $('#nome').val(),
							sobrenome: $('#sobrenome').val(),
							celular: $('#celular').val(),
							telFixo: $('#telFixo').val(),
							email: $('#email').val()					  
						}
			})
			.done(function( msg ) {
				$('.formStatus').html(msg);
			});
		}
		
		return false;
	});
})

//VALIDAR FORMS
function validaForm(formID)
{
	
	var formulario = document.getElementById(formID);
	var f = formulario.getElementsByTagName("input");
	var s = formulario.getElementsByTagName("select");
	var txtArea = formulario.getElementsByTagName("textarea");
	var wrong = 'PREENCHA OS CAMPOS\n\n';
	var cont = 0;
	
	// Ids dos campos a serem ignorados na checagem
	idIgnorado = [
              	  'submit',
                  'submitLogin',
              	  'file-original', 
              	  'file-falso'
	              ];
	
	// Faz o loop pelo inputs do form
	for(i=0; i<f.length; i++)
	{
		if(idIgnorado.indexOf(f[i].id) < 0)
		{ 
			$(f[i]).removeClass('wrong');
			
			if(f[i].value == f[i].defaultValue || f[i].value == '')
			{
				$(f[i]).addClass('wrong');
				wrong += f[i].id + '\n';	
				cont = 1;
			}
			
			// Verifica email
			if(f[i].id == 'email'){
				if(!$validations.validateEmail(f[i].value)){
					$(f[i]).addClass('wrong');	
					cont = 1;
				} 
			}
			
			// Verifica nascimento
			if(f[i].id == 'nascimento'){
				if(!$validations.validateDate(f[i].value)){
					$(f[i]).addClass('wrong');	
					cont = 1;
				} 
			}
			
					// Verifica nascimento
			if(f[i].id == 'cep-res' || f[i].id == 'cep-trab'){
				if(!$validations.validateCEP(f[i].value)){
					$(f[i]).addClass('wrong');	
					cont = 1;
				} 
			}		
	
			
			// Verifica CPF
			if(f[i].id == 'cpf'){
				if(!$validations.validateCPF(f[i].value)){
					$(f[i]).addClass('wrong');	
					cont = 1;
				} 
			}
			
			// Verifica data
			if(f[i].id == 'data'){ 
				if(!$validations.validateDate(f[i].value)){
					$(f[i]).addClass('wrong');	
					cont = 1;
				} 
			}
		}
	}
	
	// Faz o loop pelos selects do form
	for(i=0; i<s.length; i++)
	{
		if(idIgnorado.indexOf(s[i].id) < 0)
		{ 
			$(s[i]).removeClass('wrong');
			if(s[i].value == s[i].defaultValue || s[i].value == '')
			{ 
				$(s[i]).addClass('wrong');
				cont = 1;
			} else {
				$(s[i]).removeClass('wrong');
			}
		}
	}
	
	// Faz o loop pelo textareas do form
	for(i=0; i<txtArea.length; i++)
	{
		if(idIgnorado.indexOf(txtArea[i].id) < 0)
		{ 
			jQuery(txtArea[i]).removeClass('wrong');
			if(txtArea[i].value == txtArea[i].defaultValue || txtArea[i].value == '')
			{
				jQuery(txtArea[i]).addClass('wrong');
				wrong += txtArea[i].id + '\n';	
				cont = 1;
			}
		}
	}
	
	if(cont==0)
	{
		$('#'+formID+' .formStatus').html('Enviando...');
		$('#'+formID+' .formStatus').fadeIn();
		return true;

	} else {
		
		$('#'+formID+' .formStatus').html('Campos incorretos. Favor verificar os campos destacados.');
		$('#'+formID+' .formStatus').fadeIn();
		setTimeout(function(){
			$('#status').fadeOut();
		}, 10000)
		return false;			
	}	
	
}