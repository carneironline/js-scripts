<?php
include_once('acom-hs-tpl/header.php');

//define('BASEURL', 'http://o.apps.americanas.com.br/media/global/hotsite/hs-torcer-para-ganhar-acom-visa/');
//define('BASEURL', 'http://img.americanas.com.br/mktacom/hotsite/copa-do-mundo-visa/');
define('BASEURL', '');
define('URL_RULES', 'http://apps.americanas.com.br/media/global/hotsite/hs-torcer-para-ganhar-acom-visa/regulamento.html');
?>

<!--SCRIPTS HS-->
<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>css/style.css" />
<script type="text/javascript" src="<?php echo BASEURL; ?>js/scripts.js"></script>
<!--END SCRIPTS HS-->

<!--HS BEGIN-->
<div id="hs-torcer-para-ganhar">
	
    <div class="topo">
    	<div class="content">
    		<ul class="midiasSociais">
    			<li><a href="http://on.fb.me/1dNDJ2g" target="_blank" class="f"></a></li>
    			<li><a href="http://bit.ly/shgdsgd" target="_blank" class="t"></a></li>
    			<li><a href="http://bit.ly/GPromo" target="_blank" class="g"></a></li>
    		</ul>
    	</div><!--.content-->
    </div><!--.topo-->
    
    <div class="nav">
    	<div class="content">
    		<ul class="menu">
    			<li><a href="#principal">início</a></li>
    			<li><a href="#cadastre-se">cadastre-se</a></li>
    			<li><a href="#premios">prêmios</a></li>
    			<li><a href="#regulamento">regulamento e FAQ</a></li>
    			<li id="menu-logged"><a href="#logged">Cupons</a></li>
    			<li><a href="#vencedores">vencedores</a></li>
    		</ul>
    		
    		<div class="confira-tickets">
    			<form id="formAuthNav" action="">
	        		<ul>
	        			<li style="padding-top: 2px;">Confira seus tickets</li>
	        			<li><input type="text" placeholder="e-mail" class="text promo-login-email" id="" name="user[email]" /><div class="answer"></div></li>
	        			<li><input type="text" placeholder="cpf" class="text promo-login-cpf" id="" name="user[cpf]" maxlength="14" onkeypress="MascaraCPF(this, event); return numbersonly(this, event);" onblur="ValidarCPF(this);" /></li>
	        			<li><input type="submit" value="entrar" class="submit" /></li>
	        		</ul>
        		</form>
        		<input type="submit" value="logout" class="promo-logout" />
    		</div><!--.confira-tickets-->
    	</div><!--.content-->
    </div><!--.nav-->
    
    <div class="wrap-content box-principal displayNone">
    	<h2 class="title">quais são os prêmios oferecidos pela Visa?</h2>
    	
		<div class="img1"><img src="<?php echo BASEURL; ?>images/promo/page-principal/img1.jpg" /></div>
		
		<div class="img2">
			<h2 class="title">como participar?</h2>
			
			<a href="<?php echo URL_RULES; ?>" class="btn-regulamento fancybox" target="_blank">confira o regulamento</a>
			
			<img src="<?php echo BASEURL; ?>images/promo/page-principal/img2.jpg" />
			
			<a href="#cadastre-se" class="btn-clique-participe"></a>
		</div>
		
    </div><!--.wrap-content | box-principal-->
    
    <div class="wrap-content box-cadastre-se displayNone">
    	<h2 class="title">participe!</h2>
    	
		<form id="formPromoCadastrar" onsubmit="validaFormNiver(this.id); return false;">
			<div class="box-cadastro">
				<div class="col-left">
					<h2>Faça já o seu cadastro para participar.</h2>
					
					<p>Atenção!<br />
					Mesmo que você já seja cadastrado no site da Americanas.com, é preciso se cadastrar na promoção preenchendo o formulário ao lado.
					</p>
					
					<p>Ah, você deve usar o mesmo CPF para o cadastro no site e na promoção!</p>
					
					<p>As informações precisam estar corretas para garantir a entrega dos prêmios.</p>
				</div><!--.col-left-->
				
				<div class="col-right">

					<div class="left field span2">
						<label for="promo-user-cpf">cpf</label>
						<input type="text" id="promo-user-cpf" class="text" maxlength="14" onkeypress="MascaraCPF(this, event); return numbersonly(this, event);" onblur="ValidarCPF(this);" />
					</div>
					
					<div class="right field span2">
						<label for="promo-user-rg">rg</label>
						<input type="text" id="promo-user-rg" class="text" onkeypress="return numbersonly(this, event);" />
					</div>
					<div class="clear"></div>
					
					<div class="field span1">
						<label for="promo-user-nome">nome completo</label>
						<input type="text" id="promo-user-nome" class="text" />
					</div>
					
					<div class="left field span3">
						<label for="promo-user-cep">cep</label>
						<input type="text" id="promo-user-cep" class="text" onkeypress="$validacao.maskme(this, '########'); return $validacao.somenteNumeros(event);" maxlength="8" />
					</div>
					
					<div class="left field span3 marginLeft10">
						<label for="promo-user-telefone">telefone</label>
						<input type="text" id="promo-user-telefone" class="text" onkeypress="$validacao.maskme(this, '## #########'); return $validacao.somenteNumeros(event); " maxlength="12" />
					</div>
					
					<div class="right field span3">
						<label for="promo-user-nascimento">data de nascimento</label>
						<input type="text" id="promo-user-nascimento" class="text" onkeypress="$validacao.maskme(this, '##/##/####'); return $validacao.somenteNumeros(event); " maxlength="10" />
					</div>
					<div class="clear"></div>
					
					
					<div class="left field span2">
						<label for="promo-user-endereco">endereço</label>
						<input type="text" id="promo-user-endereco" class="text" />
					</div>
					
					<div class="right field span2">
						<label for="promo-user-estado">estado</label>
						<input type="text" id="promo-user-estado" class="text" />
					</div>
					<div class="clear"></div>
					
	
					<div class="field span1">
						<label for="promo-user-email">e-mail</label>
						<input type="text" id="promo-user-email" class="text block" />
					</div>
					
					<div class="field span1">
						<label for="promo-user-email-confirm">confirmar e-mail</label>
						<input type="text" id="promo-user-email-confirm" class="text block" />
					</div>
					
					<div class="field marginBottom10">
						<div class="checkbox"><input type="checkbox" id="promo-declaro" /></div> declaro que li e concordo com o regulamento.
						<div class="clear"></div>
					</div>
					
					<div class="field marginBottom10">
						<div class="checkbox"><input type="checkbox" id="promo-receber-emails" /></div> desejo receber e-mails com novidades e ofertas<br /> da Americanas.com e Lojas Americanas.
						<div class="clear"></div>
					</div>
					
					<input type="hidden" id="raffleName" value="hs-torcer-para-ganhar-acom-visa" />
					
					<input type="submit" value="enviar" id="submit" class="submit" />
					
					<div class="answer"></div>
					
				</div><!--.col-right-->
				<div class="clear"></div>
			</div><!--.box-cadastro-->
		</form>
		
    </div><!--.wrap-content | box-cadastre-se-->
    
    <div class="wrap-content box-premios displayNone">
    	<h2 class="title">prêmios</h2>
		
		<ul>
			<li>4 pares de ingressos para assistir a um jogo da copa do mundo. <br />(um par para cada vencedor)</li>
			<li>Acomodação em hotel 4 estrelas com café da manhã para 4 dias e 3 noites.</li>
			<li>R$ 300,00 pré-pago VISA por pessoa para comprar o que quiser.</li>
			<li>Passagem para cidade do jogo.</li>
			<li>R$ 300,00 em cartão pré-pago Visa para comprar o que quiser.</li>
			<li>Meet & Greet na chegada do aeroporto.</li>
			<li>Transporte terrestre pré-programado.</li>
			<li>City Tour na cidade do jogo.</li>
		</ul>
		
		<img src="<?php echo BASEURL; ?>images/promo/page-premios/premios-bg.jpg" class="premios" />
    </div><!--.wrap-content | premios-->
    
    <div class="wrap-content box-regulamento displayNone">
    	<h2 class="title">regulamento</h2>
    	
    	<a href="<?php echo URL_RULES; ?>" class="btn-regulamento fancybox" target="_blank">ler o regulamento</a>
          	
    	<h2 class="title faq">FAQ</h2>
    	<p>Leia abaixo, perguntas e respostas mais frequentes.</p>
		
		<div class="accordion">
			<h3>1.	Qual o período de participação da promoção?</h3>
			<div>O período de participação na promoção se inicia a partir das 0h00 (zero hora) do dia 01 de abril de 2014 e se estende até às 23h59m59s (vinte e três horas e cinquenta e nove minutos e cinquenta e nove segundos) do dia 11 de maio de 2014, no horário de Brasília.</div>
			
			<h3>2.	Preciso realizar o cadastro para participar da promoção?</h3>
			<div>
				<p>Sim, para concorrer aos  prêmios do sorteio é necessário realizar o cadastro no site da Promoção <a href="http://www.americanas.com.br/copa-do-mundo-visa,">www.americanas.com.br/copa-do-mundo-visa,</a> mesmo já tendo cadastro no  site da Americanas.com. </p>

			</div>
			
			<h3>3.	Quantos e quais são os prêmios?</h3>
			<div>
				<p>Serão distribuídos pela Promoção 04 (quatro) prêmios,  conforme discriminados na tabela a seguir: <strong></strong></p>
				<table border="1" cellspacing="0" cellpadding="0">
				  <tr>
				    <td width="361"><br>
				        <strong>Descrição do prêmio</strong> </td>
				    <td width="102"><p align="center"><strong>Quantidade de prêmios</strong></p></td>
				    <td width="121"><p align="center"><strong>Valor unitário (R$)</strong></p></td>
				  </tr>
				  <tr>
				    <td width="361"><p>1 (um) pacote de    hospitalidade para a Copa do Mundo da FIFA 2014,com passagens aéreas,    traslados e hospedagem em hotel 4 estrelas de 4 dias/3 noites com café da    manhã para a cidade de Brasília com serviço de “Meet &amp; Greet” no momento    de chegada ao aeroporto, hospitalidade no hotel e pré-partida – rota para o    estádio, 2 (dois) ingressos categoria II para jogo da Copa do    Mundo&nbsp;da&nbsp;FIFA&nbsp;Brasil&nbsp;2014, a realizar-se em 23/06/2014,    sendo um para o contemplado e outro para um acompanhante, apoio de staff de    suporte no evento, um cartão pré-pago VISA com o valor de R$ 300,00    (trezentos reais), city tour.</p></td>
				    <td width="102"><p>4 (quatro)</p></td>
				    <td width="121"><p>&nbsp;</p>
				        <p>R$    31.000,00 (trinta e um mil reais)</p></td>
				  </tr>
				</table>

			</div>
			
			<h3>4.	Quem pode participar?</h3>
			<div>
				<p>Participam do sorteio desta Promoção apenas as pessoas  físicas maiores de 13 (treze) anos, residentes e domiciliadas no Brasil, que,  ao longo do período de participação, comprarem produtos na loja on-line do  grupo B2W, Americanas.com – www.americanas.com.br/copa-do-mundo-visa,  utilizando os cartões VISA (“Visa, Americanas Visa e Visa Electron”) e que  cumpram as demais condições do Regulamento. <br>
  Aos  participantes com idade entre 13 (treze) e 17 (dezessete) anos, recomenda-se o  acompanhamento dos pais ou responsáveis legais ao longo de todo o período da promoção.</p>

				
			</div>
			
			<h3>5.	Como participar?</h3>
			<div>
				<p>Para concorrer aos  prêmios, o participante deverá se cadastrar na promoção e adquirir qualquer  produto na loja virtual do grupo B2W, Americanas.com, e efetuar o pagamento  desses produtos com os Cartões Visa (Visa, Americanas Visa e Visa Electron) ao  longo do período de participação e a cada R$ 99,00 (noventa e nove reais)  adquiridos o participante cadastrado receberá 1 (um) elemento sorteável depois  de validada a sua participação. Considera-se a participação validada após a aprovação  do pagamento do pedido. <br>
  Serão considerados apenas os pedidos realizados por  pessoas físicas.<br>
  Apenas participarão da promoção os pedidos que forem  aprovados integralmente até 23h59m59s (vinte e três horas, cinquenta e nove  minutos e cinquenta e nove segundos) do dia 16 de maio de 2014, horário de Brasília  e que forem realizados durante o período de participação.<br>
  Quanto mais compras realizadas cujos números de pedido  e demais dados forem devidamente cadastrados no site, mais chances o  participante terá.</p>
				
			</div>
			
			<h3>6.	Como se cadastrar?</h3>
			<div>
				<p>Para cadastrar-se na Promoção, o participante deverá  acessar o site <a href="http://www.americanas.com">www.americanas.com</a>.br/copa-do-mundo-visa e realizar o cadastro preenchendo correta e  integralmente os dados solicitados (nome completo, CPF, RG, data de nascimento,  endereço completo, e-mail e telefone celular). Para finalizar o cadastro o  participante deverá, ainda, aceitar os termos do Regulamento da presente  Promoção.</p>

			</div>
			
			<h3>7.	E se eu utilizar mais de um meio para efetuar o pagamento?</h3>
			<div>Se o pagamento do pedido for efetuado através de dois ou mais meios de pagamento, será considerada apenas, para efeitos dessa promoção, a parte do pedido que foi paga com os cartões Visa nas funções débito ou crédito.</div>
			
			<h3>8.	O que são elementos sorteáveis?</h3>
			<div>
				<p>Os elementos sorteáveis são os números utilizados para  a escolha dos vencedores. Os prêmios serão atribuídos aos portadores dos  elementos sorteáveis extraídos da Loteria Federal. <br>
				  O primeiro elemento sorteável contemplado será aquele  cujo campo numérico da série seja igual à unidade de centena do primeiro prêmio  da Loteria Federal, e cujo elemento sorteável seja igual à unidade de dezena  dos 05 prêmios da Loteria Federal, lidos de cima para baixo, conforme o exemplo  abaixo:<br>
				  Exemplo: Supondo que numa extração hipotética da Loteria  Federal, sejam extraídos os seguintes números:</p>
				<ol>
				  <li><strong>1º Prêmio                            6 1. 30 6</strong></li>
				  <li><strong>2º Prêmio                            1  2. 59 4</strong></li>
				  <li><strong>3º Prêmio                            1 3. 7 70</strong></li>
				  <li><strong>4º Prêmio                            7 1. 67 8</strong></li>
				  <li><strong>5º Prêmio                            2 5. 71 5</strong></li>
				</ol>
				<p>Neste caso, seria contemplado o elemento sorteável de  número da sorte <strong>09771 </strong>(em vermelho) da série <strong>3</strong> (em azul). <br>
				  Serão contemplados também os 03 (três) números da sorte  imediatamente seguintes ao número da sorte do primeiro elemento sorteável  contemplado. Portanto, conforme o exemplo acima seriam contemplados os  elementos sorteáveis cujos números da sorte fossem o <strong>09772, 09773 e 09774 </strong>da série contemplada  no primeiro elemento sorteável ganhador.<br>
				  Esses elementos sorteáveis serão distribuídos e  divulgados ao participante em seu extrato pessoal no site www.americanas.com.br/copa-do-mundo-visa até um dia antes  do sorteio, ou seja, dia 16 de maio de 2014. </p>
								
			</div>
			
			<h3>9.	O que preciso fazer para ganhar os números para o sorteio?</h3>
			<div>
				<p>Já cadastrado na promoção e uma vez efetuadas compras  com o mesmo CPF utilizado no cadastro, a Promotora efetua o cruzamento das  informações e o participante passa a concorrer automaticamente aos prêmios,  desde que sejam respeitados todos os termos do Regulamento. Não é necessário  nenhum passo adicional.<br>
  É importante lembrar que apenas participarão da  promoção os pedidos que forem aprovados integralmente até 23h59m59s (vinte e  três horas, cinquenta e nove minutos e cinquenta e nove segundos) do dia 16 de  maio de 2014, horário de Brasília.</p>

			</div>
			
			<h3>10.	Como meus números de pedidos são transformados em elementos sorteáveis?</h3>
			<div>
				<p>A cada compra efetuada no  site da Americanas.com o cliente receberá um número de pedido. Esse número será  transformado em números para o sorteio da seguinte forma: <br>
				  1 (um) Número de Pedido  de R$ 99,00 (noventa e nove reais) em produtos = 1 (um) cupom. <br>
				  1 (um) Número de Pedido  de R$ 198,00 (cento e noventa e oito reais) em produtos = 2 (dois) cupons. <br>
				  1 (um) Número de Pedido  de R$ 100,00 (cem reais) = 1 cupom. <strong>O  valor de R$ 1,00 (um real) restantes não permanece como saldo para outra  utilização.</strong><br>
				  O participante pode  utilizar o mesmo Número de Pedido apenas 1 (uma) vez nessa Promoção. </p>
				Não  serão utilizados para obtenção de cupons os valores do número do pedido que não  se referirem a produtos, por exemplo, valores de garantia estendida.
			</div>
			
			<h3>11.	Os ingressos valem para quais jogos?</h3>
			<div>Os ingressos serão válidos para o jogo a ser realizado em Brasília em 23/06/2014.</div>
			
			<h3>12.	Onde consulto meus números da sorte (elementos sorteáveis)?</h3>
			<div>
				<p>Os  elementos sorteáveis serão distribuídos e divulgados a você em seu extrato  pessoal no site www.americanas.com.br/copa-do-mundo-visa  até um dia antes do sorteio, ou seja, dia 16 de maio de 2014. <strong> </strong></p>

			</div>
			
			<h3>13.	Quando será realizado o sorteio?</h3>
			<div>
				O sorteio acontecerá em 17/05/2014 e serão sorteados 04 ganhadores, sendo que cada um vai receber 1 (um) pacote de hospitalidade para a Copa do Mundo da FIFA 2014,com passagens aéreas, traslados e hospedagem em hotel 4 estrelas de 4 dias/3 noites com café da manhã para a cidade de Brasília com serviço de “Meet & Greet” no momento de chegada ao aeroporto, hospitalidade no hotel e pré-partida – rota para o estádio, 2 (dois) ingressos categoria II para jogo da Copa do Mundo da FIFA Brasil 2014, a realizar-se em 23/06/2014, sendo um para o contemplado e outro para um acompanhante, apoio de staff de suporte no evento, um cartão pré-pago VISA com o valor de R$ 300,00 (trezentos reais), city tour.
			</div>
			
			<h3>14.	Como saberei se sou um dos sorteados?</h3>
			<div>
				<p>O  resultado da Promoção e o nome do contemplado serão divulgados no site da  Promoção em <a href="http://www.americanas.com">www.americanas.com</a>.br/copa-do-mundo-visa da Promoção em  até 10 (dez) dias após a apuração.<br>
  O  contemplado será comunicado do resultado da promoção pelo envio  preferencialmente de e-mail e subsidiariamente de telegrama ou carta com aviso  de recebimento (AR), bem como por contato telefônico, no prazo máximo de 05  (cinco) dias da data da apuração, de acordo com os dados cadastrais mantidos  pela Promotora e pela Aderente.</p>

			</div>
			
			<h3>15.	Quais documentos serão necessários para receber meu prêmio?</h3>
			<div>
				<p>A Promotora entregará uma carta-compromisso para que  você assine e apresente RG e CPF. Será necessário  apresentar também o número do pedido com o qual você participou na promoção. <br>
  Além disso, podem ser solicitados documentos  requeridos pela FIFA, bem como qualquer informação adicional, dentro dos  limites da lei.<br>
  Por  fim, para retirada dos ingressos, será necessário apresentar um documento de  identidade válido e com fotografia.</p>

			</div>
			
			<h3>16.	Onde retiro meus ingressos?</h3>
			<div>Se você for um dos ganhadores da promoção, receberá todas as informações de procedimento para recebimento do seu prêmio.</div>
			
			<h3>17.	Posso trocar os ingressos por dinheiro?</h3>
			<div>Não, os prêmios não podem ser trocados por dinheiro.</div>
			
			<h3>18.	Não moro do Brasil. Posso participar?</h3>
			<div>Não, a promoção é exclusiva para pessoas residentes e domiciliadas no Brasil.</div>
			
			<h3>19.	Posso ganhar mais de uma vez?</h3>
			<div>Sim, se você for sorteado. </div>
			
			<h3>20.	Posso dar os ingressos como presente?</h3>
			<div>Apenas o ganhador ou pessoa que tenha procuração para tanto poderá retirar os ingressos. Depois de retirado, você poderá entrega-los para outra pessoa utilizar, sendo certo que após a retirada dos ingressos encerra-se a responsabilidade da Promotora e Aderentes quanto à utilização deles. </div>
			
			<h3>21.	Posso cadastrar na promoção com mais de um e mail?</h3>
			<div>Não, você deve se cadastrar apenas um com um e-mail e um CPF.</div>
			
			<h3>22.	O transporte está incluso no prêmio?</h3>
			<div>
				<p>Sim,  mas caso o ganhador resida em uma cidade  que não possua aeroporto, será providenciado o deslocamento até o aeroporto  mais próximo, sendo de livre escolha das empresas promotoras o meio de  transporte para esse fim. Se o ganhador residir no município onde será  realizado o jogo ou próximo num raio de até 300 km, será providenciado um  traslado de ida e volta do seu domicílio até a respectiva cidade, não terá  direito a passagens e hospedagem.<br>
  O ganhador e seu acompanhante viajarão  obrigatoriamente juntos na mesma data, com o mesmo meio de transporte, partindo  e retornando ao domicilio do ganhador. Fica desde já claro e acertado que os  ganhadores, em nenhuma hipótese, serão reembolsados caso escolham outra forma  de locomoção que não seja aquela oferecida pelas empresas promotoras.<br>
  Toda a documentação pessoal necessária para a viagem  deverá ser providenciada pelo ganhador e seu acompanhante, bem como as despesas  incorridas para este fim. </p>

			</div>
			
			<h3>23.	Os traslados estão incluídos no prêmio?</h3>
			<div>Sim,  o traslado do contemplado e seu acompanhante até o aeroporto de embarque será  realizado no dia do embarque/desembarque, sendo que a Promotora e as Aderentes  não serão responsáveis por hospedagem em hotel, caso o contemplado deseje  estender sua viagem e permanecer na cidade de embarque/desembarque. </div>
			
			<h3 class="doubleLine">24.	Como posso ter mais informações sobre o processo de auditoria e funcionamento da mecânica da promoção de forma<br /> que eu possa me assegurar que tudo o que o regulamento informa é verídico?  </h3>
			<div>
				<p>A Promotora e Aderentes  zelam pela regularidade da mecânica de suas promoções mediante obtenção de  autorização expedida pela Secretaria de Acompanhamento Econômico - SEAE, bem  como pela contratação de empresa de auditoria independente, desde o início de  todas as suas promoções.<br>
  São  fiscalizadas todas as etapas da promoção, como distribuição dos cupons  elementos sorteáveis, apuração, identificação dos ganhadores etc. <br>
  Dúvidas  podem ser esclarecidas no site da promoção. </p>

			</div>
						
		</div><!--.accordion-->
		
    </div><!--.wrap-content | premios-->
    
    <div class="wrap-content box-vencedores">
    	<h2 class="title">vencedores</h2>
    	
    	<ul class="list-vencedores">
    		<li>
    			<h3>Parabéns</h3>
    			<p>Ticket n<sup>o</sup>: 2-5728</p>
                <p>Ticket n<sup>o</sup>: 2-5749</p>
                <p>Ticket n<sup>o</sup>: 2-5730</p>
                <p>Ticket n<sup>o</sup>: 2-5713</p>
    		</li>
    	</ul>
		
    </div><!--.wrap-content | vencedores-->
    
     <div class="wrap-content box-logged displayNone">
    	<h2 class="title">Olá <span id="userName"></span>!</h2>
    	<input type="hidden" id="hdpf" value="" />
    	
    	<div class="tickets-quant"><span id="tickets-total"></span></div><!--.tickets-quant-->

    	<p class="title">Seus tickets para o sorteio:</p>
    	
    	<table id="show-tickets">
    		<thead>
        		<tr>
        			<th class="col1">data da compra</th>
        			<th class="col2">número do pedido </th>
        			<th class="col3">ticket do sorteio</th>
        		</tr>
    		</thead>
    		<tbody>
        		
    		</tbody>
    	</table>
    	
    	<div class="aviso"><strong>Importante:</strong> Apenas os pedidos com pagamento aprovados serão transformados em número da sorte. Nosso sistema precisa de até 24 horas para validar seu código e gerar o ticket do sorteio.</div>
		
		<div class="clear"></div>
		
    </div><!--.wrap-content | vencedores-->
    
    <div class="clear"></div>
    
    <div class="list-publicidade">
		<ul>
			<li class="left"><a href="http://www.americanas.com.br/ole-tvs?chave=hs_centralpromo_hsoletvs&WT.mc_id=hs_centralpromo_hsoletvs"><img src="<?php echo BASEURL; ?>images/promo/footer/banner-oletvs.gif" /></a></li>
			<li class="left"><a href="http://www.americanas.com.br/a-torcida?chave=hs_centralpromo_hsatorcida&WT.mc_id=hs_centralpromo_hsatorcida"><img src="<?php echo BASEURL; ?>images/promo/footer/banner-atorcida.gif" /></a></li>
			<li class="right"><a href="http://viagens.americanas.com.br/default.aspx?utm_source=americanas&utm_medium=link_building&utm_campaign=viagens&s_cid=link_building_viagens"><img src="<?php echo BASEURL; ?>images/promo/footer/banner-viagens.gif" /></a></li>
		</ul>
		<div class="clear"></div>
    </div><!--.list-publicidade-->
    
    <div class="clear"></div>
    <p style="margin: 60px auto 0px auto; text-align: center;">Período de Participação de 01/04/2014 a 11/05/2014. Certificado de Autorização SEAE nº 04/0117/2014.</p>
	
	
	<div id="popupAniversario" style="width:542px; display: none;">
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_01.gif" style="display: block;" />
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_02.gif" style="display: block;" />
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_03.gif" style="display: block;" />
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_04.gif" style="display: block;" />
		<a href="http://www.americanas.com/especial/hotsite/aniversario2013/303994/tag_promoniver_patroc/303981?chave=hs_aniver2013_patrocinadores&WT.mc_id=hs_aniver2013_patrocinadores" target="_blank"><img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_05.gif" style="display: block;" /></a>
	</div>

</div><!--#hs-vem-pra-festa-->

<!--HS END-->

<?php
include_once('acom-hs-tpl/footer.php');
?>
