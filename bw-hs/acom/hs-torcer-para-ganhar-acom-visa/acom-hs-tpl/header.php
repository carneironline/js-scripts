<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width" /> 
		<title>Americanas.com
		</title>
		<meta charset="UTF-8" /><script type="text/javascript">var NREUMQ=NREUMQ||[];NREUMQ.push(["mark","firstbyte",new Date().getTime()]);</script>
		<link href="http://img.americanas.com.br/statics-1.58.1.2147/css/acom_components.css" rel="stylesheet" type="text/css" media="all" />
		<!--[if lte IE 7]><link href="http://img.americanas.com.br/statics-1.58.1.2147/css/acom_components_IE.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
		<link href="http://img.americanas.com.br/statics-1.58.1.2147/catalog/css/catalog.css" rel="stylesheet" type="text/css" media="all" />	
		<!--[if lte IE 7]><link href="http://img.americanas.com.br/statics-1.58.1.2147/catalog/css/catalog_IE.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
	
	<link media="only screen and (max-width: 640px)" href="http://m.americanas.com.br" />
	<link media="handheld" href="http://m.americanas.com.br" />
	
	<script type="text/javascript"> // <![CDATA[
		var readyEvents=[]    
		var exceptionEvents=[]
		function onReady(f)  { readyEvents.push(f)  }
		function exceptionEvent(f) { exceptionEvents.push(f) }
		function handleException(x) {
			for(i=0;i<exceptionEvents.length;i++) {
				exceptionEvents[i](x);
			}						 
	  }
		exceptionEvent(function (f) { if(typeof(console)!="undefined") console.debug(f); })
		
		document.getElementsByTagName("html")[0].className = 
			"js" + ((window.location.hash.indexOf("redir") > 0) ? ' redir' : '');

		// ]]>
	</script>
	<link href="http://img.americanas.com.br/catalog/css/secondary.css" rel="stylesheet" type="text/css" media="all">
<!--[if IE]><link href="http://img.americanas.com.br/catalog/css/secondary_IE.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="http://img.americanas.com.br/catalog/skins/Null/css/skinHeaderFooter.css" media="all" />
<meta name="robots" content="noindex,follow">

<meta http-equiv="x-ua-compatible" content="IE=9" >

<script type="text/javascript"> 
try {
	
	OAS_url = 'http://oas.submarino.com.br/RealMedia/ads/';
	OAS_listpos = 'Top2,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,BottomLeft,BottomRight';
	OAS_query = 'estatica/tiposdefrete-teste'; 
	OAS_sitepage = 'americanas/home';	

	OAS_version = 10;
	OAS_rn = '001234567890'; OAS_rns = '1234567890';
	OAS_rn = new String (Math.random()); OAS_rns = OAS_rn.substring (2, 11); 

} catch(exception) {
	handleException(exception);	
}

function OAS_NORMAL(pos) {
	document.write('<A HREF="' + OAS_url + 'click_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + OAS_query + '" TARGET=_top>');
 	document.write('<IMG SRC="' + OAS_url + 'adstream_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + OAS_query + '" BORDER=0></A>');
}  
</script>

<script type="text/javascript">
try {
	OAS_version = 11;
	if (navigator.userAgent.indexOf('Mozilla/3') != -1)
		OAS_version = 10;
	if (OAS_version >= 11)
		document.write('<SCR'+ 'IPT LANGUAGE=JavaScript1.1 SRC="' + OAS_url + 'adstream_mjx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '?' + OAS_query + '"></SC'+'RIPT>');
} catch(exception) {
	handleException(exception);	
}

</script>

<script type="text/javascript">
try {
	function OAS_AD(pos) {
		if (OAS_version >= 11)
			OAS_RICH(pos);
		else
			OAS_NORMAL(pos);
	}
} catch(exception) {
	handleException(exception);	
}

</script>

</head>

	<body>
	
		<p class="hc"><a rel="nofollow" href="#inicio" accesskey="4">você está no início</a></p>
		<div id="page">
			<p class="hc">
				<a rel="nofollow" href="#conteudo">ir para o conteúdo <strong>(atalho + 1)</strong></a>
			</p>
			<div id="header">
	<script src="http://img.americanas.com.br/catalog/skins/inov/arquivos/mobileredirect.js"></script>

<link rel="stylesheet" type="text/css" href="http://img.americanas.com.br/catalog/skins/inov/header-v1-2013/css/header.min.css"/>
<script type="text/javascript">
    onReady(function () {
        // set qtd cesta carrinho.
        var cart = $.parseJSON($.cookie('ShoppingCartInfo')) || {};
        if (cart.totalQty) {
            $('#quantItensCart').html(cart.totalQty + ' ' + ((cart.totalQty > 1) ? 'itens' : 'item'));
        }

        // set nickname from cookie "acomNick";
        var nick = $.cookie('acomNick');
        if (nick) {
            $('#headerLoggedUserName').text(nick);
            $('#headerAccess').hide();
            $('#headerLogged').show();
        }
$("#headerTop-wrap").find('.televendas').click(function(e){
            var fbUrl=$(this).attr('href');
            $.fancybox({'width':650,'height':420,'autoDimensions':false,'href':fbUrl,'type':'iframe','callBackOnShow':function(){resizeFB();}});
            e.preventDefault();
        });
    });


    var waitjQuery = waitjQuery || [];
    nAcom = '';
    function getAjax(strURL, callback) {
        var xmlHttpReq = false;
        if (window.XMLHttpRequest) {
            xmlHttpReq = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlHttpReq.open('GET', strURL, true);
        xmlHttpReq.setRequestHeader('Content-Type', 'text/html');
        xmlHttpReq.setRequestHeader('charset', 'utf-8');
        xmlHttpReq.onreadystatechange = function () {
            if (xmlHttpReq.readyState == 4) {
                callback.call(this, xmlHttpReq);
            }
        }
        xmlHttpReq.send('');
    }

    function sSearch(frm) {
        var obj = document.getElementById("search"),
                term = frm.getElementsByTagName('input')[0].value;
        if (term != "") {
            dep = (isNaN(parseInt(dep))) ? '' : '?dep=' + dep;
            window.location = 'http://www.americanas.com.br/busca/' + term;
        } else {
            document.getElementById("box-alert").style.display = "block";
            frm.getElementsByTagName('input')[0].style.background = "#ffe202";
            frm.getElementsByTagName('input')[0].style.border = "1px solid #ffe202";
        }
        return false;
    }
    function hideBox() {
        var box = document.getElementById('box-alert');
        var Input = document.getElementsByTagName('input')[0];
        if (box && box.style.display == "block") {
            box.style.display = 'none';
            Input.style.border = "1px solid #cccccc";
            Input.style.background = "#FFFFFF";
        }
    }

</script>

<style type="text/css">
/* START fix 10% barra flutuante */
.headerFlutuante .layer-cart .emptyCart-content{display:none;}
/* END fix 10% barra flutuante */


.all-dpts-menu .list > li > ul {
     padding: 0 8px !important;
}

.listCol .flag-new1 {
	position: absolute;
	background-color: #96E102;
    padding: 4px 5px 3px;
    top: 0;
	left: 105px;
	}
	
.listCol .flag-new2 {
	position: absolute;
	background-color: #96E102;
    padding: 4px 5px 3px;
    top: 0;
	left: 52px;
	}

	#mainMenu>li>.headerNew>.ico10 {
	background-image: url(http://img.americanas.com.br/catalog/skins/inov/api/v1.0.7/images/icon-sdf638dce19.png);
	background-position: -29px -714px;
	background-repeat: no-repeat;
	height: 26px;
	width: 21px;
	margin-top: 3px;
	}
	#mainMenu>li:hover>.headerNew>.ico10 {
		background-position: -29px  -740px;
	}
#mainMenu>li:hover>.headerNew {
   padding: 2px 0 6px !important;
}
#header #doSearch{text-indent: -9999px}
</style>

<!--[if lte IE 8]>
<style type="text/css">
.group{
	border-left: 1px solid #aaa;
	border-right: 1px solid #aaa;
	top: 55px;
}
li.info-dpt-menu  .group,
li.tel-dpt-menu   .group,
li.tv-som-dpt-menu   .group{
	left: -18px;
}
li.moveis-dpt-menu  .group,
li.games-dpt-menu  .group,
li.brinq-dpt-menu  .group{
	right: -5px;
}
.groupOferta   h3   strong,
.groupOferta   h3   span{
	display: block;
	width: 100px;
}
.groupOferta   h3   a{
	float: none;
	clear:both;
}
#headerTop-bottom{
	z-index: 100;
}
#headerTop-bottom  #cestaAmericanas{
	z-index: 5;
}
#headerTop-bottom  #cestaAmericanas  >  div{
	border: 1px solid #aaa;

}
</style>
<![endif]-->

<div id="headerTop-wrap">

<div id="headerTop-upper-wrap">
    <div id="headerTop-upper">

        <ul id="menuTop">
            <li>
                <a href="http://www.americanas.com.br/estaticapop/popCentralAtendimento/" class="televendas">televendas 24h:
                    4003-1000</a></li>
            <li>
                <a href="http://www.americanas.com.br/central-de-atendimento?WT.mc_id=internas-atendimentoHome&WT.mc_ev=click">atendimento</a>
            </li>
            <li><a href="https://carrinho.americanas.com.br/ControlPanelWeb/panel/AllOrders/">meus pedidos</a></li>
            <li><a href="https://carrinho.americanas.com.br/ControlPanelWeb/panel/home">minha conta</a></li>
            <li class="last"><a href="http://www.americanas.com.br/estatica-lasa/lasa?DCSext.menu=lojas">loja mais
                próxima</a></li>
        </ul>
        <!--#menuTop-->

        <ul id="menuEspecial">
            <li>
                <script type="text/javascript">try {OAS_AD('x71')} catch (exp) {handleException(exp);}</script>
            </li>
        </ul>
        <!--#menuEspecial-->

    </div>
    <!--#headerTop-upper-->
</div>
<!--#headerTop-upper-wrap-->

<div id="headerTop-bottom">

<a href="http://www.americanas.com.br" id="logoAmericanas">A maior loja. Os menores preços</a>

<form id="searchAmericanas" action="http://busca.americanas.com.br/busca.php">
    <div>
        <input type="text" value="" data-position="header" id="s" placeholder="buscar" autocomplete="off" name="q" data-neemu="autocomplete-search-top"
                />
        <input type="submit" value="buscar" data-position="header" id="doSearch" />
    </div>
</form>

<div id="loginAmericanas">
    <div id="headerAccess">
        <a href="https://carrinho.americanas.com.br/CustomerWeb/pages/Login">olá, faça seu login<br/>
            ou cadastre-se</a>
    </div>
    <div id="headerLogged" style="display: none;">
        olá, <span id="headerLoggedUserName">Visitante</span><br/>
        <a href="https://carrinho.americanas.com.br/CustomerWeb/pages/Logout" id="headerLogout">sair</a>
    </div>
</div>
<!--#loginAmericanas-->

<div id="cestaAmericanas">
    <a href="http://carrinho.americanas.com.br/checkout/">
        minha cesta:
        <span id="quantItensCart">0 itens</span>
    </a>
</div>
<!--#cestaAmericanas-->

<ul id="mainMenu">
<li class="all-dpts-menu">
    <a href="http://www.americanas.com.br/estatica/todos-os-departamentos?DCSext.menu=todos-departamentos"><strong>ver todos os<br/> departamentos<span class="rodape"></span></strong></a>

    <div class="group">

        <ul class="list">
            <li class="listCol">
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/loja/226795/alimentos-e-bebidas?WT.mc_id=internas-menuLista-alimentos">Alimentos
                            e bebidas</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/256408/audio?WT.mc_id=interna-menuLista-audio">Áudio</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/226855/automotivo?WT.mc_id=internas-menuLista-automotivo">Automotivo</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/317728/ar-condicionado-e-aquecedores?WT.mc_id=internas-menuLista-arcondicionadoeaquecedores">Ar-condicionado
                            e aquecedores</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/226940/bebes?WT.mc_id=internas-menuLista-bebes">Bebês</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/227014/beleza-e-saude?WT.mc_id=internas-menuLista-beleza">Beleza
                            e saúde</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/227109/brinquedos?WT.mc_id=internas-menuLista-brinquedos">Brinquedos</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/315798/blu-ray?WT.mc_id=internas-menuIconeOver-bluRay-linha">Blu-ray
                            e blu-ray 3D</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/227296/cama-mesa-e-banho?WT.mc_id=internas-menuLista-cameba">Cama,
                            mesa e banho</a></li>

                </ul>
            </li>
            <li class="listCol">
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/loja/227559/cameras-e-filmadoras?WT.mc_id=internas-menuLista-cameras">Câmeras
                            e filmadoras</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/229187/celulares-e-telefones?WT.mc_id=internas-menuLista-celulares">Celulares
                            e telefones</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/227644/eletrodomesticos?WT.mc_id=internas-menuLista-eletrodomesticos">Eletrodomésticos</a>
                    </li>
                   
                    <li>
                        <a href="http://www.americanas.com.br/loja/227763/eletroportateis?WT.mc_id=internas-menuLista-eletroportateis">Eletroportáteis</a>
                    </li>
                    
                    <li>
                        <a href="http://www.americanas.com.br/loja/227821/esporte-e-lazer?WT.mc_id=internas-menuLista-esporte">Esporte
                            e lazer</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/227999/ferramentas-e-jardim?WT.mc_id=internas-menuLista-ferramentas">Ferramentas
                            e jardim</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/227609/dvds-e-blu-ray?WT.mc_id=internas-menuLista-dvdBlu">Filmes
                            e séries</a>/li>
					<li style="position:relative">
                        <a href="http://www.americanas.com.br/loja/226762/games?WT.mc_id=internas-menuLista-games">Games</a>
						
                    </li>	
					<li>
                        <a href="http://www.americanas.com.br/loja/228190/informatica?WT.mc_id=internas-menuLista-informatica">Informática</a>
                    </li>					
                  </ul>
            </li>
            <li class="listCol">
                <ul>
				 
                    
                    <li>
                        <a href="http://www.americanas.com.br/loja/228098/informatica-acessorios?WT.mc_id=internas-menuLista-infoacess">Informática
                            e acessórios</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/228255/instrumentos-musicais?WT.mc_id=internas-menuLista-instrumentos">Instrumentos
                            musicais</a></li>
                    <li><a href="http://www.americanas.com.br/loja/228310/livros?WT.mc_id=internas-menuLista-livros">Livros</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/228641/malas-e-acessorios?WT.mc_id=internas-menuLista-malas">Malas e acessórios</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/228671/moda-e-acessorios?WT.mc_id=internas-menuLista-moda">Moda
                            e acessórios</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao?WT.mc_id=internas-menuLista-moveis">Móveis
                            e decoração</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/227369/cds-e-dvds-musicais?WT.mc_id=internas-menuLista-cds">Música</a>
                    </li>
					<li>
                        <a href="http://www.americanas.com.br/loja/228804/papelaria?WT.mc_id=internas-menuLista-papelaria">Papelaria</a>
                    </li>
					
					<!--li><span style="display:block; height: 17px; padding-top:3px;"></span></li-->
					<li>
						<a href="http://www.americanas.com.br/loja/228926/perfumaria?WT.mc_id=internas-menuLista-perfumaria">Perfumaria
						e cosméticos</a></li>
                    
                </ul>
            </li>
            <li class="listCol">
                <ul>
					<li><a href="http://www.americanas.com.br/loja/228975/pet-shop?WT.mc_id=internas-menuLista-petshop">Pet
                        shop</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/229017/relogios?WT.mc_id=internas-menuLista-relogios">Relógios</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/336268/suplementos-e-vitaminas?WT.mc_id=internas-menuLista-suplementos">Suplementos
                            e Vitaminas</a></li>
							 <li>
                        <a href="http://www.americanas.com.br/loja/227707/tv-e-home-theater?WT.mc_id=internas-menuLista-eletronicos">TVs e Áudio</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/loja/229231/utilidades-domesticas?WT.mc_id=internas-menuLista-utilidadeDomestica">Utilidades
                            domésticas</a></li>

                    <li class="services">
                        <h4>nossos serviços:</h4>
                        <ul>
                            <li>
                                <a href="http://carrinho.americanas.com.br/lista-de-casamento/pages/HomePage">Lista de Casamento</a>
                            </li>
							<li>
                                <a href="http://www.americanas.com.br/negocios-corporativos">Serviços Corporativos</a>
                            </li>
                        </ul>
                    </li>

                </ul>

            </li>
        </ul>

        <div class="clear"></div>
        <div class="clear hr-divisoria"></div>
        <!--divisoria-->

        <div class="extraLinks">

            <ul>
                <li><h4>veja também:</h4></li>
                <li class="extraLink-01">
                    <a href="http://viagens.americanas.com.br/default.aspx?utm_source=americanas&utm_medium=todos_departamentos&utm_campaign=home&s_cid=americanas_todosdepartamentos_home">Americanas
                        Viagens</a>
                </li>
                <li class="extraLink-02">
                    <a href="http://novosite.ingresso.com?PARCERIA=AMERICANAS&T_PARCERIA=AMERICANAS">Ingresso.com</a>
                </li>
                <!--li class="extraLink-03">
                    <a href="http://www.blockbusteronline.com.br/">Blockbuster</a>
                </li-->
            </ul>

            <ul>
                <li><h4>parceiros:</h4></li>
                <li class="extraLink-04">
                    <a href="http://www.milevo.com.br/?utm_source=acom&utm_medium=homevarejo&utm_campaign=menudeptos">Milevo.com</a>
                </li>
                <li class="extraLink-05">
                    <a href="http://www.soubarato.com.br/?epar=ds_bs_00_am_siteacomsbacom2&opn=SBACOM2">Outlet - Sou Barato</a>
                </li>
            </ul>
            <div class="clear"></div>
        </div>
        <!--.extraLinks-->

        <div class="btn-campanha">
			<span class="sombra"></span>
			<script type="text/javascript">try {OAS_AD('x72')} catch (exp) {handleException(exp);}</script>
        </div>

    </div>
    <!--.group-->

</li>
<!--.all-dpts-menu-->
<li class="info-dpt-menu cols1">
    <a href="http://www.americanas.com.br/loja/228190/informatica?DCSext.menu=informatica" class="headerNew"><span
            class="icone ico1"></span>

        <p>informática</p></a>

    <div class="group">

        <div class="imgDestaque-dpt">
            <div class="wrapper-imgDestaque-dpt">
                <script type="text/javascript">try {OAS_AD('x73')} catch (exp) {handleException(exp);}</script>
            </div>
        </div>

        <div class="lines">
            <p class="lineTitle">Confira as promoções em <a
                    href="http://www.americanas.com.br/loja/228190/informatica?DCSext.menu=informatica"><strong>informática</strong></a>
            </p>

            <div>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/267868/informatica/notebook?WT.mc_id=internas-menuIconeOver-info-notebooks">Notebook</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/348528/informatica/ultrabook?WT.mc_id=internas-menuIconeOver-info-ultrabooks">Ultrabook</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/267908/informatica/tablets-e-ipad?WT.mc_id=internas-menuIconeOver-info-tablet">Tablet</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/estatica/apple?WT.mc_id=internas-menuIconeOver-info-apple">Apple</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/267889/informatica/computadores-e-all-in-one?WT.mc_id=internas-menuIconeOver-info-computadores">Computadores</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/267881/informatica/computadores-e-all-in-one/all-in-one?WT.mc_id=internas-menuIconeOver-info-all-in-one">All in One</a>
                    </li>
                    <!--li>
                        <a href="http://www.americanas.com.br/linha/267869/informatica/monitor?WT.mc_id=internas-menuIconeOver-info-tv-monitor">Tv
                            monitor</a></li-->
                </ul>
                <h4>
                    <a href="http://www.americanas.com.br/loja/228098/informatica-acessorios?WT.mc_id=internas-menuLista-infoacess">Informática
                        e acessórios</a></h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/268369/informatica/armazenamento/hd-portatil?WT.mc_id=internas-menuIconeOver-info-hds">HD's</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/268370/informatica/armazenamento/pen-drives?WT.mc_id=interna-menuIconeOver-info-pendrives">Pen
                            drive</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/268385/informatica/equipamentos-de-rede-wireless/roteador?WT.mc_id=internas-menuIconeOver-info-roteadores">Roteadores</a>
                    </li>

                </ul>
                <h4>
                    <a href="http://www.americanas.com.br/loja/228190/informatica?WT.mc_id=internas-menuIcon-informatica">Veja
                        mais...</a></h4>
            </div>
        </div>

    </div>

</li>
<!--
<li class="tbl-dpt-menu cols1">
    <a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet" class="headerNew"><span class="icone ico10"></span>

        <p>Tablets</p></a>

    <div class="group">

        <div class="imgDestaque-dpt">
            <div class="wrapper-imgDestaque-dpt">
                <script type="text/javascript">try {OAS_AD('x67')} catch (exp) {handleException(exp);}</script>
            </div>
        </div>

        <div class="lines">
            <p class="lineTitle">Confira as promoções em <a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet"><strong>Tablets</strong></a>
            </p>

            <div>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/350792/informatica/conversivel-2-em-1">Tablet Conversível</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/267863/informatica/tablets-e-ipad/acessorios-para-tablets">Acessórios para Tablets</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/267882/informatica/apple/acessorios-para-ipad">Acessórios para iPad</a>
                    </li>
                </ul>
                <h4>Tablets por Marca</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/267865/informatica/apple/ipad">iPad</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet-samsung?f_268761=%22samsung%22">Tablet Samsung</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet-cce?f_268761=%22cce%22">Tablet CCE</a>
                    </li>
                    <li><a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet-philco?f_268761=%22philco%22">Tablet Philco</a></li>
                    <li><a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet-multilaser?f_268761=%22multilaser%22">Tablet Multilaser</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet-positivo?f_268761=%22positivo%22">Tablet Positivo</a></li>
                </ul>
                <h4>
                    <a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet">Veja
                        mais...</a></h4>
            </div>
        </div>

    </div>

</li>-->

<li class="tel-dpt-menu cols1">
    <a class="headerNew"
       href="http://www.americanas.com.br/loja/229187/celulares-e-telefones?DCSext.menu=celulares"><span
            class="icone ico2"></span>

        <p>telefonia</p></a>

    <div class="group">

        <div class="imgDestaque-dpt">
            <div class="wrapper-imgDestaque-dpt">
                <script type="text/javascript">try {OAS_AD('x74')} catch (exp) {handleException(exp);}</script>
            </div>
        </div>

        <div class="lines">
            <p class="lineTitle">Confira as promoções em <a
                    href="http://www.americanas.com.br/loja/229187/celulares-e-telefones?DCSext.menu=celulares"><strong>telefonia</strong></a>
            </p>

            <div>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/229196/celulares-e-telefones/celular?WT.mc_id=internas-menuIconeOver-celulares-celulares">Celulares</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/350392/celulares-e-telefones/smartphone">Smartphones</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/264410/celulares-e-telefones/nextel?WT.mc_id=internas-menuIconeOver-celulares-nextel">Nextel</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/263608/celulares-e-telefones/acessorios-para-celular?WT.mc_id=internas-menuIconeOver-celulares-acessorios-para-celular">Acessórios</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/345399/celulares-e-telefones/iphone?WT.mc_id=home-menuIconeOver-iphone">iPhone</a></li>
                </ul>
                <h4>Telefonia fixa</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/263555/celulares-e-telefones/telefone-sem-fio?WT.mc_id=internas-menuIconeOver-celulares-semfio">Telefones
                            sem fio</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/263557/celulares-e-telefones/telefone-sem-fio/telefones-sem-fio-com-ramal?WT.mc_id=internas-menuIconeOver-celulares-semFioRamal">Telefones
                            sem fio com ramal</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/263592/celulares-e-telefones/fax?WT.mc_id=internas-menuIconeOver-celulares-fax">Fax</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/263574/celulares-e-telefones/voip?WT.mc_id=internas-menuIconeOver-celulares-voip">Voip/
                            telefonia IP</a></li>
                </ul>
                <h4>
                    <a href="http://www.americanas.com.br/loja/229187/celulares-e-telefones?WT.mc_id=internas-menuIcon-celulares">Veja
                        mais...</a></h4>
            </div>
        </div>


    </div>

</li>
<li class="tv-som-dpt-menu cols1">
    <a class="headerNew" href="http://www.americanas.com.br/loja/227707/tv-e-home-theater?DCSext.menu=eletronicos"><span
            class="icone ico3"></span>

        <p>tvs e áudio</p></a>

    <div class="group">

        <div class="imgDestaque-dpt">
            <div class="wrapper-imgDestaque-dpt">
                <script type="text/javascript">try {OAS_AD('x75')} catch (exp) {handleException(exp);}</script>
            </div>
        </div>

        <div class="lines">
            <p class="lineTitle">Confira as promoções em <a
                    href="http://www.americanas.com.br/loja/227707/tv-e-home-theater?DCSext.menu=eletronicos"><strong>tvs e áudio</strong></a>
            </p>

            <div>
                <h4>
                    <a href="http://www.americanas.com.br/loja/227707/tv-e-home-theater?WT.mc_id=internas-menuIconeOver-eletronicos">Tv
                        e home theater</a></h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/262909/tv-e-home-theater/tv?WT.mc_id=internas-menuIconeOver-eletronicos-tv">Tvs</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/263029/tv-e-home-theater/home-theater?WT.mc_id=internas-menuIconeOver-eletronicos-home-theater">Home
                            theater</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/262988/tv-e-home-theater/blu-ray-player?WT.mc_id=internas-menuIconeOver-eletronicos-bluray-player">Blu-ray
                            player</a></li>

                </ul>
                <h4>
                    <a href="http://www.americanas.com.br/loja/256408/audio?WT.mc_id=internas-menuIconeOver-audio">Áudio</a>
                </h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/262429/audio/micro-system?WT.mc_id=internas-menuIconeOver-audio-micro-system">Micro
                            system</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/262431/audio/som-portatil?WT.mc_id=internas-menuIconeOver-audio-som-portatil">Som
                            portátil</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/262488/audio/ipod-e-acessorios?WT.mc_id=internas-menuIconeOver-audio-ipod-e-acessorios">Ipod
                            e acessórios</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/262435/audio/dock-station-e-caixas-bluetooth?WT.mc_id=internas-menuIconeOver-audio-dock-station">Dock
                            station</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/262453/audio/fones-de-ouvido?WT.mc_id=internas-menuIconeOver-audio-fone-de-ouvido">Fone
                            de ouvido</a></li>
                </ul>
                <h4>
                    <a href="http://www.americanas.com.br/loja/227707/tv-e-home-theater?WT.mc_id=internas-menuIcon-eletronicos">Veja
                        mais...</a></h4>
            </div>
        </div>


    </div>

</li>
<li class="large eletro-dpt-menu">
    <a class="headerNew"
       href="http://www.americanas.com.br/loja/227644/eletrodomesticos?DCSext.menu=eletrodomesticos"><span
            class="icone ico4"></span>

        <p>eletrodomésticos</p></a>

    <div class="group">

        <div class="imgDestaque-dpt">
            <div class="wrapper-imgDestaque-dpt">
                <script type="text/javascript">try {OAS_AD('x76')} catch (exp) {handleException(exp);}</script>
            </div>
        </div>

        <div class="lines">
            <p class="lineTitle">Confira as promoções em <a
                    href="http://www.americanas.com.br/loja/227644/eletrodomesticos?DCSext.menu=eletrodomesticos"><strong>eletrodomésticos</strong></a>
            </p>

            <div class="colLeft">
                <h4>Cozinha</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/316829/eletrodomesticos/adega-de-vinho?WT.mc_id=internas-menuIconeOver-eletrodomesticos-adega">Adega
                            de vinho</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/317050/eletrodomesticos/coifa-e-depurador/coifas?WT.mc_id=internas-menuIconeOver-eletrodomesticos-coifa">Coifa</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/317051/eletrodomesticos/coifa-e-depurador/depurador?WT.mc_id=interna-menuIconeOver-eletrodomesticos-depuradorAr">Depurador
                            de ar</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/316689/eletrodomesticos/fogao?WT.mc_id=interna-menuIconeOver-eletrodomesticos-fogoes">Fogões</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/316848/eletrodomesticos/cooktop?WT.mc_id=internas-menuIconeOver-eletrodomesticos-cooktop">Cooktop</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/316828/eletrodomesticos/micro-ondas?WT.mc_id=internas-menuIconeOver-eletrodomesticos-microondas">Forno
                            microondas</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/317052/eletrodomesticos/forno/forno-de-embutir?WT.mc_id=internas-menuIconeOver-eletrodomesticos-fornoEmbutir">Forno
                            de embutir</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/316868/eletrodomesticos/freezer?WT.mc_id=internas-menuIconeOver-eletrodomesticos-freezer">Freezer</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/316690/eletrodomesticos/lava-loucas?WT.mc_id=internas-menuIconeOver-eletrodomesticos-lavaLouca">Lava
                            louça</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/316788/eletrodomesticos/geladeiras-refrigeradores?WT.mc_id=internas-menuIconeOver-eletrodomesticos-geladeira">Geladeiras
                            / refrigeradores</a></li>
                </ul>
            </div>
            <div class="colLeft">
                <h4>
                    <a href="http://www.americanas.com.br/loja/317728/ar-condicionado-e-aquecedores?WT.mc_id=internas-menuIconeOver-eletrodomesticos-ar-condicionado-aquecedores">Ar
                        condicionado<br/> e aquecedores</a></h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/317750/ar-condicionado-e-aquecedores/ar-condicionado-e-splits/ar-condicionado-janela?WT.mc_id=internas-menuIconeOver-arcondicionado-e-aquecedores-arCondicionado">Ar
                            condicionado de janela</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/317752/ar-condicionado-e-aquecedores/ar-condicionado-e-splits/ar-condicionado-split?WT.mc_id=internas-menuIconeOver-eletrodomesticos-ar-condicionado-split">Ar
                            condicionado split</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/317769/ar-condicionado-e-aquecedores/aquecedores-de-ar?WT.mc_id=internas-menuIconeOver-arcondicionado-e-aquecedores-aquecedores">Aquecedor de ar</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/317789/ar-condicionado-e-aquecedores/climatizador-de-ar?WT.mc_id=internas-menuIconeOver-arcondicionado-e-aquecedores-climatizadores">Climatizador de ar</a>
                    </li>
                </ul>
                <h4>Lavanderia</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/316808/eletrodomesticos/lavadora-de-roupa-e-tanquinho?WT.mc_id=interna-menuIconeOver-eletrodomesticos-lavadoraRoupa">Lavadora
                            de roupas</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/316691/eletrodomesticos/secadora-de-roupa-e-centrifuga?WT.mc_id=internas-menuIconeOver-eletrodomesticos-secadoraRoupa">Secadora
                            de roupas</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/316988/eletrodomesticos/lavadora-de-roupa-e-tanquinho/tanquinho?WT.mc_id=interna-menuIconeOver-eletrodomesticos-tanquinho">Tanquinho</a>
                    </li>
                </ul>
                <h4>
                    <a href="http://www.americanas.com.br/loja/227644/eletrodomesticos?WT.mc_id=internas-menuIcon-eletrodomesticos">Veja
                        mais...</a></h4>
            </div>
        </div>


    </div>

</li>
<li class="moveis-dpt-menu cols1">
    <a class="headerNew"
       href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao?WT.mc_id=internas-menuIcon-moveis"><span
            class="icone ico7"></span>

        <p>móveis</p></a>

    <div class="group">

        <div class="imgDestaque-dpt">
            <div class="wrapper-imgDestaque-dpt">
                <script type="text/javascript">try {OAS_AD('x77')} catch (exp) {handleException(exp);}</script>
            </div>
        </div>

        <div class="lines">
            <p class="lineTitle">Confira as promoções em <a
                    href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao?WT.mc_id=internas-menuIcon-moveis"><strong>móveis</strong></a>
            </p>

            <div>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/282569/moveis-e-decoracao/cadeira-de-escritorio?WT.mc_id=internas-menuIconeOver-moveis-cadeirasEscritorio">Cadeira
                            de escritório</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/282125/moveis-e-decoracao/colchao?WT.mc_id=internas-menuIconeOver-moveis-colchoes">Colchões</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/282276/moveis-e-decoracao/cama-box-colchao-box?WT.mc_id=internas-menuIconeOver-moveis-colchoesBox">Cama
                            box (colchão + box)</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/282263/moveis-e-decoracao/cozinha-compacta?WT.mc_id=internas-menuIconeOver-moveis-cozinhaCompacta">Cozinhas
                            compactas</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/282267/moveis-e-decoracao/mesa-para-computador?WT.mc_id=internas-menuIconeOver-moveis-mesacomputador">Mesa
                            para computador</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/282714/moveis-e-decoracao/rack-estante-e-painel?WT.mc_id=internas-menuIconeOver-moveis-rackHomeEstantes">Rack,
                            home e estante</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/282620/moveis-e-decoracao/puff?WT.mc_id=internas-menuIconeOver-moveis-puff">Puff</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/282908/moveis-e-decoracao/sofas?WT.mc_id=internas-menuIconeOver-moveis-sofas">Sofá</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/282264/moveis-e-decoracao/cozinha-modulada?WT.mc_id=internas-menuIconeOver-moveis-cozinhaModulada">Cozinha
                            modulada</a></li>
                </ul>
                <h4>
                    <a href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao?WT.mc_id=internas-menuIcon-moveis">Veja
                        mais...</a></h4>
            </div>
        </div>


    </div>

</li>
<li class="large port-dpt-menu">
    <a class="headerNew"
       href="http://www.americanas.com.br/loja/227763/eletroportateis?WT.mc_id=internas-menuIcon-eletroportateis"><span
            class="icone ico5"></span>

        <p>eletroportáteis</p></a>

    <div class="group">

        <div class="imgDestaque-dpt">
            <div class="wrapper-imgDestaque-dpt">
                <script type="text/javascript">try {OAS_AD('x78')} catch (exp) {handleException(exp);}</script>
            </div>
        </div>

        <div class="lines">
            <p class="lineTitle">Confira as promoções em <a
                    href="http://www.americanas.com.br/loja/227763/eletroportateis?WT.mc_id=internas-menuIcon-eletroportateis"><strong>eletroportáteis</strong></a>
            </p>

            <div class="colLeft">
                <h4>Cozinha</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278268/eletroportateis/bebedouros-e-purificadores?WT.mc_id=internas-menuIconeOver-eletroportateis-bebedouros-e-purificadores">Bebedouros
                            e purificadores</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278248/eletroportateis/batedeiras?WT.mc_id=internas-menuIconeOver-eletroportateis-batedeiras">Batedeiras</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278191/eletroportateis/cafeteiras-e-chaleiras?WT.mc_id=internas-menuIconeOver-eletroportateis-cafeteiras-e-chaleiras">Cafeteiras</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278192/eletroportateis/centrifugas-e-espremedores-de-fruta?WT.mc_id=internas-menuIconeOver-eletroportateis-centrifugas">Centrífugas
                            e espremedores de<br/> frutas</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278196/eletroportateis/forno-eletrico?WT.mc_id=internas-menuIconeOver-eletroportateis-fornos-eletricos">Forno
                            elétricos</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278252/eletroportateis/grill-sanduicheira-e-torradeira?WT.mc_id=internas-menuIconeOver-eletroportateis-grill">Grill,
                            sanduicheiras e torradeiras</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278159/eletroportateis/panificadora-maquina-de-pao?WT.mc_id=internas-menuIconeOver-eletroportateis-panificadoras">Panificadoras</a>
                    </li>
                </ul>
                <h4>Calculadoras</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/330348/papelaria/calculadoras?WT.mc_id=internas-menuIconeOver-eletroportateis-calculadoras">Calculadoras</a>
                    </li>
                </ul>
            </div>
            <div class="colLeft">
                <h4>Casa</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278190/eletroportateis/aspiradores-e-vassouras?WT.mc_id=internas-menuIconeOver-eletroportateis-aspirador">Aspiradores
                            de pó, vassouras e acessórios</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278250/eletroportateis/ferro-de-passar?WT.mc_id=internas-menuIconeOver-eletroportateis-ferros-passar">Ferro
                            de passar</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278198/eletroportateis/maquinas-de-costura?WT.mc_id=internas-menuIconeOver-eletroportateis-maquinaCostura">Máquinas
                            de costura</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278257/eletroportateis/ventiladores-e-circuladores-de-ar?WT.mc_id=internas-menuIconeOver-eletroportateis-ventiladores">Ventiladores
                            e circuladores de ar</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278428/eletroportateis/vaporizadores-e-higienizadores?WT.mc_id=internas-menuIconeOver-eletroportateis-vaporizador">Vaporizadores
                            e higienizadores</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278158/eletroportateis/panelas-eletricas?WT.mc_id=internas-menuIconeOver-eletroportateis-panelas-eletricas">Panela
                            elétrica</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/333908/eletroportateis/enceradeiras?WT.mc_id=internas-menuIconeOver-eletroportateis-encerradeiras">Enceradeira</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278151/eletroportateis/churrasqueiras-e-utensilios?WT.mc_id=internas-menuIconeOver-eletroportateis-churrasqueiras">Churrasqueiras elétricas</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/278253/eletroportateis/maquinas-de-waffle-e-creperia">Máquinas
                            de waffle e crepeira</a></li>

                </ul>
                <h4>
                    <a href="http://www.americanas.com.br/loja/227763/eletroportateis?WT.mc_id=internas-menuIcon-eletroportateis">Veja
                        mais...</a></h4>
            </div>
        </div>


    </div>

</li>
<!--li>
    <div class="headerNew"><span class="icone ico6"></span><p>câmeras</p></div>
</li-->
<li class="brinq-dpt-menu">
    <a class="headerNew"
       href="http://www.americanas.com.br/loja/227109/brinquedos?WT.mc_id=interna-menuLista-brinquedos"><span
            class="icone ico9"></span>

        <p>brinquedos</p></a>

    <div class="group">

        <div class="imgDestaque-dpt">
            <div class="wrapper-imgDestaque-dpt">
                <script type="text/javascript">try {OAS_AD('x79')} catch (exp) {handleException(exp);}</script>
            </div>
        </div>

        <div class="lines">
            <p class="lineTitle">Confira as promoções em <a
                    href="http://www.americanas.com.br/loja/227109/brinquedos?WT.mc_id=interna-menuLista-brinquedos"><strong>Brinquedos</strong></a>
            </p>

            <div class="colLeft">
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/sublinha/280813/brinquedos/carrinhos-e-veiculos/autoramas-e-pistas?WT.mc_id=internas-menuIconeOver-brinquedos-autoramapista">Autoramas
                            e pistas</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279669/brinquedos/barbie?WT.mc_id=internas-menuIconeOver-brinquedos-barbie">Boneca barbie</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279688/brinquedos/bonecas?WT.mc_id=internas-menuIconeOver-brinquedos-bonecas">Bonecas</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279649/brinquedos/bonecos?WT.mc_id=internas-menuIconeOver-brinquedos-bonecos">Bonecos</a>
                    li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279670/brinquedos/bicicleta-infantil?WT.mc_id=internas-menuIconeOver-brinquedos-bicicletas">Bicicleta infantil</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279748/brinquedos/brinquedos-eletronicos?WT.mc_id=internas-menuIconeOver-brinquedos-brinquedosEletronicos">Brinquedos eletrônicos</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/especial/hotsite/brinquedos-importados/217866/hotsite-brinquedos-importados-Ofertas-Especiais/217867?WT.mc_id=internas-menuIconeOver-brinquedos-importadosExclusivos">Brinquedos
                            importados</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279768/brinquedos/colecionaveis?WT.mc_id=internas-menuIconeOver-brinquedos-colecionaveis">Colecionáveis</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279690/brinquedos/controle-remoto?WT.mc_id=internas-menuIconeOver-brinquedos-controleRemoto">Controle
                            remoto</a></li>
                </ul>
            </div>
            <div class="colLeft">
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279691/brinquedos/esportes?WT.mc_id=internas-menuIconeOver-brinquedos-esportes">Esportes</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279788/brinquedos/hot-wheels?WT.mc_id=internas-menuIconeOver-brinquedos-hotWheels">Hot Wheels</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279674/brinquedos/mini-veiculos?WT.mc_id=internas-menuIconeOver-brinquedos-miniVeiculos">Mini veículos</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279657/brinquedos/playground?WT.mc_id=internas-menuIconeOver-brinquedos-playground">Playground</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279698/brinquedos/praia-e-piscina?WT.mc_id=internas-menuIconeOver-brinquedos-praiaPiscina">Praia
                            e piscina</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279695/brinquedos/quebra-cabeca?WT.mc_id=internas-menuIconeOver-brinquedos-quebraCabeca">Quebra-cabeça</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/272228/bebes/brinquedos-para-bebe?WT.mc_id=internas-menuIconeOver-brinquedos-bebes">Brinquedos
                            para bebês</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279671/brinquedos/blocos-de-montar?WT.mc_id=internas-menuIconeOver-brinquedos-blocos-para-montar">Blocos
                            de montar</a></li>

                </ul>
            </div>
            <div class="colLeft">
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279651/brinquedos/carrinhos-e-veiculos?WT.mc_id=internas-menuIconeOver-brinquedos-carrinhos-veiculos">Carrinhos
                            e veículos</a></li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/279692/brinquedos/fantasias?WT.mc_id=internas-menuIconeOver-brinquedos-fantasias">Fantasias</a>
                    </li>
                </ul>
                <h4><a href="http://www.americanas.com.br/loja/227109/brinquedos?WT.mc_id=internas-menuIcon-brinquedos">Veja
                    mais...</a></h4>

            </div>
        </div>


    </div>
</li>
<li class="games-dpt-menu">
    <a class="headerNew" href="http://www.americanas.com.br/loja/226762/games?WT.mc_id=internas-menuIcon-games"><span
            class="icone ico8"></span>

        <p>games</p></a>

    <div class="group">

        <div class="imgDestaque-dpt">
            <div class="wrapper-imgDestaque-dpt">
                <script type="text/javascript">try {OAS_AD('x80')} catch (exp) {handleException(exp);}</script>
            </div>
        </div>

        <div class="lines">
            <p class="lineTitle">Confira as promoções em <a
                    href="http://www.americanas.com.br/loja/226762/games?WT.mc_id=internas-menuIcon-games"><strong>games</strong></a>
            </p>

            <div class="colLeft">
                <h4>Playstation 4</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/351535/games/console-playstation-4">Console Playstation 4</a>
                    </li>
                </ul> 
                <h4>Playstation 3</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291067/games/console-playstation-3?WT.mc_id=internas-menuIconeOver-games-playstation-3-consoles">Console Playstation 3</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291236/games/jogos-playstation-3?WT.mc_id=internas-menuIconeOver-games-playstation-3-games">Jogos Playstation 3</a>
                    </li>

                </ul>
                <h4>Xbox 360</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291045/games/console-xbox-360?WT.mc_id=internas-menuIconeOver-games-xbox-360-console">Console Xbox 360</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291228/games/jogos-xbox-360?WT.mc_id=internas-menuIconeOver-games-xbox-360-games">Jogos Xbox 360</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291231/games/acessorios-xbox-360?WT.mc_id=internas-menuIconeOver-games-xbox-360-acessorios">Acessórios Xbox 360</a>
                    </li>
                </ul>
                <h4>Xbox One</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/347990/games/console-xbox-one">Console Xbox One</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/351258/games/jogos-xbox-one">Jogos Xbox One</a>
                    </li>
                </ul>
                
            </div>
            <div class="colLeft">
                <h4>Nintendo Wii</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291314/games/console-nintendo-wii?WT.mc_id=internas-menuIconeOver-games-nintendo-wii-consoles">Console Nintendo Wii</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291282/games/jogos-nintendo-wii?WT.mc_id=internas-menuIconeOver-games-nintendo-wii-games">Jogos Nintendo Wii</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291374/games/acessorios-nintendo-wii?WT.mc_id=internas-menuIconeOver-games-nintendo-wii-acessorios">Acessórios Nintendo Wii</a>
                    </li>
                </ul>
                <h4>Nintendo DS</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291432/games/console-nintendo-dsi?WT.mc_id=internas-menuIconeOver-games-nintendo-ds-consoles">Console Nintendo DS</a>
                    </li>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291449/games/jogos-nintendo-dsi-ds?WT.mc_id=internas-menuIconeOver-games-nintendo-ds-games">Games Nintendo DS</a>
                    </li>
                    <!--<li><a href="http://www.americanas.com.br/linha/291286/games/acessorios-nintendo-dsi?WT.mc_id=internas-menuIconeOver-games-nintendo-ds-acessorios">Acessórios</a></li>-->
                </ul>
                <h4>PC</h4>
                <ul>
                    <li>
                        <a href="http://www.americanas.com.br/linha/291327/games/jogos-pc?WT.mc_id=internas-menuIconeOver-games-pc-games">Jogos para PC</a>
                    </li>

                </ul>
                <!--<h4>Outros consoles</h4>
                <ul>
                    <li><a href="http://www.americanas.com.br/linha/293490/games/consoles-classicos?WT.mc_id=internas-menuIconeOver-games-outros-consoles-consoles">Consoles</a></li>
                    <li><a href="http://www.americanas.com.br/linha/293433/games/jogos-classicos?WT.mc_id=internas-menuIconeOver-games-outros-consoles-games">Games</a></li>
                    <li><a href="http://www.americanas.com.br/linha/293493/games/acessorios-consoles-classicos?WT.mc_id=internas-menuIconeOver-games-outros-consoles-acessorios">Acessórios</a></li>
                </ul>-->
                <h4><a href="http://www.americanas.com.br/loja/226762/games?WT.mc_id=internas-menuIcon-games">Veja
                    mais...</a></h4>
            </div>
        </div>


    </div>

</li>
<style type="text/css">
	li.aoferta-menu > .rodape{
		display: block;
		background: url(http://img.americanas.com.br/catalog/skins/inov/header-v1-2013/images/img-oferta-do-dia.gif) no-repeat scroll center center #FEFA1D;
		position: absolute;
		width: 97px;
		height: 55px;
		top: 0;
		left: 0;
		z-index: 9999999;
	}
</style>
<li class="last aoferta-menu">
    <a class="rodape" href="http://www.americanas.com.br/aofertadodia?WT.ac=ofertadodia"></a>

    <div class="groupOferta">

        <h3><strong>oferta do dia</strong> <span><a href="http://www.americanas.com.br/aofertadodia?WT.ac=ofertadodia">Veja
            mais</a></span></h3>

        <div class="wrapper-groupOferta">
            <script type="text/javascript">try {
                OAS_AD('Top2')
            } catch (exp) {
                handleException(exp);
            }</script>
        </div>
    </div>

</li>
</ul>
<!--#mainMenu-->


</div>
<!--#headerTop-bottom-->
</div>
<!--#headerTop-wrap-->

<script type="text/javascript">
    onReady(function () {
        $("#s").focus(function () {
            $(this).parent().addClass("focusInput");
        });
        $("#s").blur(function () {
            $(this).parent().removeClass("focusInput");
        });
    });
</script>


<!-- Niver Acom2013 Responsys -->
<style>
.niverAcom2013 .loader{background-image:url('http://img.americanas.com.br/docs/loading.gif') !important;background-repeat:no-repeat;background-position:0 !important;background-color:transparent !important;border:0;}
.niverAcom2013 .inp-error{background-color:rgb(227, 33, 45);color:#FFF;}
</style>
<script type="text/javascript" src="http://img.americanas.com.br/catalog/skins/inov/api/cr.js"></script>	 
<script type="text/javascript" src="http://img.americanas.com.br/docs/aniversario_2013/js/cr.js?4"></script>
<!-- End -->


<script language="JavaScript">  
var request = { getParameter: _getParameter };  
  
function _getParameter(querystring)  
          {  
                     var querystr = new Array();  
                        loc = window.location.search.substr(1).split('&');  
                           for (query in loc)  
                                        {  
                                                      var q = loc[query].split('=');  
                                                            querystr[q[0]] = q[1];  
                                                                 }  
                              return querystr[querystring];  
                                }  

function createCookie(name, value, days) {

            if (days) {
                            var date = new Date();
                                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                                            var expires = "; expires=" + date.toGMTString();
                                                }
                else var expires = "";
                    document.cookie = name + "=" + value + expires + "; path=/;domain=.americanas.com.br";

}

function setCookieInSession(paramName, cookieName){

        var parameter = _getParameter(paramName)

        if(parameter!=null)
        {       
                createCookie(cookieName, parameter)
        }

}

setCookieInSession("opn","acomOpn");
setCookieInSession("epar","acomEPar");
setCookieInSession("chave","acomChave");
setCookieInSession("par","acomPar");
setCookieInSession("rentId","rentName");
setCookieInSession("opn","b2wOpn");
setCookieInSession("epar","b2wEPar");
setCookieInSession("chave","b2wChave");
setCookieInSession("par","b2wPar");
setCookieInSession("franq","b2wFranq");
setCookieInSession("acomIntChannel","b2wChannel");
setCookieInSession("acomIntChannel","acomChannel");

</script>
			</div>
		<div id="content">
	
	<div id="area02">
		<div id="area02-1">