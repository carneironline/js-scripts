var APP_PAIS =(function() {

    var api = {};
    var shapeOpt = {}; // Shape Options
    var rgb = { r: 0, g: 0, b: 0 } //Skin Color Default

    // Seta a cor RGB para reaplicar na silhueta
    api.setSkinColor = function(hex){
        rgb = api.hexToRgb(hex);
        api.createSkinShape();
    }

    // Cria um shape com base na imagem da silhueta, podendo alterar a sua cor
    api.createSkinShape = function(){
        shapeOpt.SkinColor = rgb;

        var canvas = document.createElement("canvas");
        var sourceImg = document.getElementById("sourceImg");
        var silhouetteImg = document.getElementById("sourceImg");
        var ctx = canvas.getContext('2d');
        canvas.width = sourceImg.width;
        canvas.height = sourceImg.height;
        ctx.drawImage(sourceImg,0,0);
        var imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
        var pix = imgData.data;
        //convert the image into a silhouette

        //var rgb = api.hexToRgb('#f00')
        for (var i=0, n = pix.length; i < n; i+= 4){
            //set red to 0
            pix[i] = rgb.r;
            //set green to 0
            pix[i+1] = rgb.g;
            //set blue to 0
            pix[i+2] = rgb.b;
            //retain the alpha value
            pix[i+3] = pix[i+3];
        }
        ctx.putImageData(imgData,0,0);
        silhouetteImg.src = canvas.toDataURL();
    }

    // Converte cor hexadecimal para RGB
    api.hexToRgb = function(hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    // Inicializa a API
    api.init = function(){
        window.onload=function(){
            api.createSkinShape();
        }
    }

    return {
        api: api.init(),
        shapeOpt: shapeOpt,
        setSkinColor: api.setSkinColor
    }

})();