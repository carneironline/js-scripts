<?php
include_once('suba-hs-tpl/header.php');

//define('BASEURL', 'http://o.apps.americanas.com.br/media/global/hotsite/hs-torcer-para-ganhar-suba-sony/');
//define('BASEURL', 'http://img.submarino.com.br/mktsuba/hotsites/copa-do-mundo-sony/suba/');
define('BASEURL', '');
define('URL_RULES', 'http://apps.submarino.com.br/media/global/hotsite/hs-torcer-para-ganhar-acom-sony/regulamento-sony.html');
?>

<!--SCRIPTS HS-->
<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>css/style.css" />

<!--FANCYBOX2-->
<!--<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
<script type="text/javascript" src="js/jquery.fancybox.js"></script>-->

<script type="text/javascript" src="<?php echo BASEURL; ?>js/scripts.js"></script>
<!--END SCRIPTS HS-->

<!--HS BEGIN-->
<div id="hs-torcer-para-ganhar">
	
    <div class="topo">&nbsp;</div><!--.topo-->
    
    <div class="nav">
    	<div class="content">
    		<ul class="menu">
    			<li><a href="#principal">início</a></li>
    			<li><a href="#cadastre-se">cadastre-se</a></li>
    			<li><a href="#premios">prêmios</a></li>
    			<li><a href="#regulamento">regulamento e FAQ</a></li>
    			<li id="menu-logged"><a href="#logged">Cupons</a></li>
    			<li><a href="#vencedores">vencedores</a></li>
    		</ul>
    		
    		<div class="confira-tickets">
    			<form id="formAuthNav" action="">
	        		<ul>
	        			<li style="padding-top: 2px;">Confira seus tickets</li>
	        			<li><input type="text" placeholder="e-mail" class="text promo-login-email" id="" name="user[email]" /><div class="answer"></div></li>
	        			<li><input type="text" placeholder="cpf" class="text promo-login-cpf" id="" name="user[cpf]" maxlength="14" onkeypress="MascaraCPF(this, event); return numbersonly(this, event);" onblur="ValidarCPF(this);" /></li>
	        			<li><input type="submit" value="entrar" class="submit" /></li>
	        		</ul>
        		</form>
        		<input type="submit" value="logout" class="promo-logout" />
    		</div><!--.confira-tickets-->
    	</div><!--.content-->
    </div><!--.nav-->
    
    <div class="wrap-content box-principal displayNone">
    	<h2 class="title">quais são os prêmios*?</h2>
    	
		<div class="img1"><img src="<?php echo BASEURL; ?>images/promo/page-principal/img1.jpg" /></div>
		
		<div class="img2">
			<h2 class="title">como participar?</h2>
			
			<a href="<?php echo URL_RULES; ?>" class="btn-regulamento fancybox" target="_blank">confira o regulamento</a>
			
			<img src="<?php echo BASEURL; ?>images/promo/page-principal/img2.jpg" />
			
			<a href="#cadastre-se" class="btn-clique-participe"></a>
		</div>
		
    </div><!--.wrap-content | box-principal-->
    
    <div class="wrap-content box-cadastre-se displayNone">
    	<h2 class="title">participe!</h2>
    	
		<form id="formPromoCadastrar" onsubmit="validaFormNiver(this.id); return false;">
			<div class="box-cadastro">
				<div class="col-left">
					<h2>Faça já o seu cadastro para participar.</h2>
					
					<div class="chamada">
						<p>Atenção! Mesmo que você já seja cadastrado no site do Submarino,<br /> 
						é preciso se cadastrar na promoção preenchendo o formulário abaixo.<br />
						As informações precisam estar corretas para garantir a entrega dos prêmios.</p>
					</div><!--.text-->
					<div class="clear"></div>
				</div><!--.col-left-->

				<div class="col-right">
					<p class="formLabel">Preencha o formulário abaixo:</p>
				
					<div class="left field span1 noMarginLeft">
						<label for="promo-user-nome">nome completo</label>
						<input type="text" id="promo-user-nome" class="text" />
					</div>
					
					<div class="left field span2">
						<label for="promo-user-rg">rg</label>
						<input type="text" id="promo-user-rg" class="text" onkeypress="return $validacao.somenteNumeros(event);" />
					</div>
					
					<div class="left field span2">
						<label for="promo-user-cpf">cpf</label>
						<input type="text" id="promo-user-cpf" class="text" maxlength="14" onkeypress="MascaraCPF(this, event); return numbersonly(this, event);" onblur="ValidarCPF(this);" />
					</div>

					<div class="clear"></div>
					
					<div class="left field span4 noMarginLeft">
						<label for="promo-user-endereco">endereço</label>
						<input type="text" id="promo-user-endereco" class="text" />
					</div>
					
					<div class="left field span3">
						<label for="promo-user-estado">estado</label>
						<input type="text" id="promo-user-estado" class="text" />
					</div>
					
					<div class="left field span3">
						<label for="promo-user-cep">cep</label>
						<input type="text" id="promo-user-cep" class="text" onkeypress="$validacao.maskme(this, '########'); return $validacao.somenteNumeros(event);" maxlength="8" />
					</div>
					
					<div class="left field span3">
						<label for="promo-user-telefone">telefone</label>
						<input type="text" id="promo-user-telefone" class="text" onkeypress="$validacao.maskme(this, '## #########'); return $validacao.somenteNumeros(event); " maxlength="12" />
					</div>
					
					<div class="left field span3">
						<label for="promo-user-nascimento">data de nascimento</label>
						<input type="text" id="promo-user-nascimento" class="text" onkeypress="$validacao.maskme(this, '##/##/####'); return $validacao.somenteNumeros(event); " maxlength="10" />
					</div>
					
					
					<div class="clear"></div>
					
	
					<div class="field left span1 noMarginLeft">
						<label for="promo-user-email">e-mail</label>
						<input type="text" id="promo-user-email" class="text block" />
					</div>
					
					<div class="field left span1">
						<label for="promo-user-email-confirm">confirmar e-mail</label>
						<input type="text" id="promo-user-email-confirm" class="text block" />
					</div>
					
					<div class="clear"></div>
					
					<p class="formLabel"><em>Está quase lá!</em><br />
					Para finalizar seu cadastro, escolha o prêmio* que deseja concorrer:</p>
					
					<div class="choiceAward">
						<div class="item noMarginLeft">
							<img src="<?php echo BASEURL; ?>images/promo/page-cadastro/premios-radio-img1.jpg" />
							<div class="radio">
								<input type="radio" class="radioOption" name="premios" value="20-fan-photographers" />
								<p><strong>20</strong> fan photographers + acompanhante<br />
									<span>(1 par para cada vencedor)</span>
								</p>
							</div>
							<div class="clear"></div>
						</div>
						
						<div class="item">
							<img src="<?php echo BASEURL; ?>images/promo/page-cadastro/premios-radio-img2.jpg" />
							<div class="radio">
								<input type="radio" class="radioOption" name="premios" value="5-ingressos-assistir" />
								<p><strong>5 pares</strong> de ingressos para assistir à final da Copa do Mundo<br />
									<span>(1 ingresso com acompanhante para cada vencedor)</span>
								</p>
							</div>
							<div class="clear"></div>
						</div>
						
						<div class="clear"></div>
						
						<p>* Para todos os prêmios estão inclusas passagens e hospedagem.</p>
					</div><!--.choiceAward-->
					
					<div class="field marginBottom10">
						<div class="checkbox"><input type="checkbox" id="promo-declaro" /></div> declaro que li e concordo com o regulamento.
						<div class="clear"></div>
					</div>
					
					<div class="field marginBottom10">
						<div class="checkbox"><input type="checkbox" id="promo-receber-emails" /></div> desejo receber e-mails com novidades e ofertas do Submarino.
						<div class="clear"></div>
					</div>
					
					<input type="hidden" id="raffleName" value="hs-torcer-para-ganhar-suba-sony" />
					
					<input type="submit" value="enviar" id="submit" class="submit" />
					
					<div class="answer"></div>
					
				</div><!--.col-right-->
				<div class="clear"></div>
			</div><!--.box-cadastro-->
		</form>
		
    </div><!--.wrap-content | box-cadastre-se-->
    
    <div class="wrap-content box-premios displayNone">
    	<h2 class="title">prêmios*</h2>
		
		<img src="<?php echo BASEURL; ?>images/promo/page-premios/premios.jpg" />
    </div><!--.wrap-content | premios-->
    
    <div class="wrap-content box-regulamento displayNone">
    	<h2 class="title">regulamento</h2>
    	
    	<a href="<?php echo URL_RULES; ?>" class="btn-regulamento fancybox" target="_blank">ler o regulamento</a>
          	
    	<h2 class="title faq">FAQ</h2>
    	<p>Leia abaixo, perguntas e respostas mais frequentes.</p>
		
		<div class="accordion">
			<h3>1.	Qual o período de participação da promoção?</h3>
			<div>O período de participação na promoção se inicia a partir das 00h00 (zero hora) do dia 25 de março de 2014 e se estende até às 23h59m59s (vinte e três horas cinquenta e nove minutos e cinquenta e nove segundos) do dia 19 de abril de 2014, horário oficial de Brasília. </div>
			
			<h3>2.	Preciso realizar o cadastro para participar da promoção?</h3>
			<div>
				<p>Sim, para concorrer aos  prêmios do sorteio é necessário realizar o cadastro no site da Promoção do  Submarino.com <a href="http://www.submarino.com.br/copa-do-mundo-sony">www.submarino.com.br/copa-do-mundo-sony</a>, mesmo já tendo cadastro  no site da loja online. </p>

			</div>
			
			<h3>3.	Quantos e quais são os prêmios?</h3>
			<div>
				<p>Serão distribuídos pela Promoção 25 (vinte e cinco)  prêmios, conforme discriminados na tabela a seguir: </p>
<table border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="359"><p><strong>Descrição do prêmio</strong></p></td>
    <td width="105"><p align="center"><strong>Quantidade    de prêmios</strong></p></td>
    <td width="120"><p align="center"><strong>Valor    unitário (R$)</strong></p></td>
  </tr>
  <tr>
    <td width="359"><p>01 (um) pacote de    viagem com passagens áreas e hospedagem com 2 (dois) ingressos, destinado ao    ganhador e um acompanhante indicado por ele, o premiado irá retirar uma    Câmera Sony por empréstimo na qual entrará 20 minutos no aquecimento dos    jogadores antes do início da partida, como <strong>FAN PHOTOGRAPHER</strong>, em um dos jogos da Copa do    Mundo&nbsp;da&nbsp;FIFA&nbsp;Brasil&nbsp;2014 destinados aos participantes    que comprarem no <strong>SUBMARINO.COM</strong> </p></td>
    <td width="105"><p align="center">20 (vinte)</p></td>
    <td width="120"><p>Ingresso:    R$500,00<br>
      Pacote:    R$8.171,00</p></td>
  </tr>
  <tr>
    <td width="359"><p>1 (um) pacote de    viagem com passagens áreas e hospedagem para a cidade do Rio de Janeiro com 2    (dois) ingressos, destinado ao ganhador e um acompanhante indicado por ele    para camarote com alimentação inclusa para <strong>JOGO FINAL</strong> da Copa do Mundo da FIFA Brasil 2014 destinados aos    participantes que comprarem na <strong>SUBMARINO.COM.</strong></p></td>
    <td width="105"><p align="center">05 (cinco)</p></td>
    <td width="120"><p>Ingresso:    R$ 352,00<br>
      Pacote:    R$ 8171,00 </p></td>
  </tr>
  <tr>
    <td width="359"><p align="center"><strong>VALOR TOTAL DOS PRÊMIOS (R$)</strong></p></td>
    <td width="225" colspan="2"><p align="center">R$ 216.035,00 (duzentos e dezesseis    mil e trinta e cinco reais) <strong> </strong></p></td>
  </tr>
</table>

			</div>
			
			<h3>4.	Quem pode participar?</h3>
			<div>
				<p>Participam  do sorteio desta Promoção apenas as pessoas físicas maiores de 13 (treze) anos,  residentes e domiciliadas no Brasil que, ao longo do período de participação,  comprarem produtos SONY na  loja on-line do grupo B2W, Submarino e que cumpram as demais condições  do Regulamento.</p>
<p>Aos participantes com idade entre 13 (treze) e 17  (dezessete) anos, recomenda-se o acompanhamento dos pais ou responsáveis  legais. </p>

			</div>
			
			<h3>5.	Como participar?</h3>
			<div>
				<p>Para concorrer aos prêmios, o participante deverá  cadastrar-se na promoção e adquirir os produtos SONY na loja on-line do grupo B2W,  Submarino ao longo do período de  participação.<br>
  A cada R$ 399,00 (trezentos e noventa e nove reais)  adquiridos em produtos SONY o participante cadastrado receberá 1 (um) elemento  sorteável depois de validada a sua participação. Considera-se a participação  validada após a aprovação do pagamento do pedido. <br>
  Serão considerados apenas os pedidos realizados por  pessoas físicas.<br>
  Apenas participarão da promoção os pedidos que forem  aprovados integralmente até 23h59m59s (vinte e três horas, cinquenta e nove  minutos e cinquenta e nove segundos) do dia 2 de maio de 2014, horário de  Brasília e que forem realizados durante o período de participação.<br>
  Quanto mais compras realizadas cujos dados pessoais do  participante forem devidamente cadastrados no site da promoção, mais chances o  participante terá.</p>

			</div>
			
			<h3>6.	Como me cadastrar?</h3>
			<div>
				<p>Para cadastrar-se no site da Promoção, o participante  deverá acessar o site da promoção <a href="http://www.submarino.com.br/copa-do-mundo-sony">www.submarino.com.br/copa-do-mundo-sony</a> e realizar o cadastro preenchendo correta e  integralmente os dados solicitados (nome completo, CPF, RG, data de nascimento,  endereço completo, e-mail e telefone celular). Para finalizar o cadastro o  participante deverá, ainda, aceitar os termos do Regulamento da presente  Promoção.</p>

			</div>
			
			<h3>7.	Quando escolho que prêmio quero concorrer?</h3>
			<div>Após o cadastramento, você vai escolher o prêmio que você deseja ganhar. Você poderá escolher pacote de viagem com acompanhante + Fan Photographer ou pacote de viagem com acompanhante + par de ingresso para o jogo final da Copa do Mundo 2014.</div>
			
			<h3>8.	O que são elementos sorteáveis?</h3>
			<div>
				<p>Os elementos sorteáveis são os números utilizados para  a escolha dos vencedores. Os prêmios serão atribuídos aos portadores dos  elementos sorteáveis extraídos da Loteria Federal. <br>
  Serão considerados vencedores todos os participantes,  dentre as 25 (vinte e cinco) séries distribuídas, que possuírem elementos  sorteáveis cujo número da sorte seja igual à dezena simples dos 05 (cinco)  primeiros prêmios da Loteria Federal lidas de cima para baixo.</p>
<p>Supondo que numa extração hipotética da Loteria Federal,  sejam extraídos os seguintes números:</p>
<p align="center"><strong>1º Prêmio                  6  1. 3 1 6</strong><br>
    <strong>2º Prêmio                  1  2. 5 9 4</strong><br>
    <strong>3º Prêmio                  1  3. 7 7 0</strong><br>
    <strong>4º Prêmio                  7  1. 6 7 8</strong><br>
    <strong>5º Prêmio                  2  5. 7 1 5</strong></p>
<p>Neste caso, serão contemplados os participantes que  tiverem os elementos sorteáveis em todas as 25 (vinte e cinco) séries cujos  números da sorte sejam iguais a <strong>19771.</strong><br>
  As séries relativas aos FAN PHOTOGRAFERS são 20 a 39 e  as séries relativas aos INGRESSOS PARA FINAL DA COPA DO MUNDO são 45 a 49.</p>

			</div>
			
			<h3>9.	Se eu escolher o prêmio FAN PHOTOGRAPHER em qual cidade sede irei assistir ao jogo e em que data?</h3>
			<div>
				<p>Depois de definidos os elementos sorteáveis premiados de  cada série, a <strong>definição do local</strong> onde os premiados com FAN PHOTOGRAFER irão assistir aos jogos será feita  através do mesmo sorteio da Loteria Federal da qual foi extraído o elemento  sorteável premiado de cada série.</p>
				<p>Para isso, os jogos foram numerados e posicionados de  modo sequencial de acordo com a data e cidade de cada jogo, do seguinte modo:</p>
				<div align="center">
				  <table border="0" cellspacing="0" cellpadding="0" width="582">
				    <tr>
				      <td width="170"><p align="center"><strong>JOGO</strong><strong> </strong></p></td>
				      <td width="180"><p align="center"><strong>DATA</strong><strong> </strong></p></td>
				      <td width="233"><p align="center"><strong>CIDADE</strong><strong> </strong></p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">1</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">13/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>SALVADOR</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">2</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">15/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>PORTO ALEGRE</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">3</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">18/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>PORTO ALEGRE</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">4</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">23/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>RECIFE</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">5</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">23/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>BRASÍLIA</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">6</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">24/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>BELO HORIZONTE</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">7</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">24/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>FORTALEZA</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">8</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">28/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>BELO HORIZONTE</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">9</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">28/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>BRASÍLIA</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">10</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">29/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>FORTALEZA</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">11</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">29/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>RECIFE</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">12</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">30/06/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>PORTO ALEGRE</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">13</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">01/07/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>SALVADOR</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">14</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">01/07/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>SÃO PAULO</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">15</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">04/07/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>FORTALEZA</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">16</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">04/07/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>RIO DE JANEIRO</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">17</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">04/07/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>RIO DE JANEIRO</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">18</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">05/07/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>BRASÍLIA</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">19</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">05/07/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>SALVADOR</p></td>
				    </tr>
				    <tr>
				      <td width="170"><p align="center">20</p></td>
				      <td width="180" nowrap valign="bottom"><p align="center">12/07/2014</p></td>
				      <td width="233" nowrap valign="bottom"><p>BRASÍLIA</p></td>
				    </tr>
				  </table>
				</div>
				<p>Os jogos serão distribuídos de acordo com Número de  Apuração que vão de 1 a 9 na coluna par, às qual pertencem os jogos de 1 a 10 e  também de 1 a 9 na coluna ímpar, à qual pertencem os jogos de 11 a 20, conforme  segue: </p>
				<table border="1" cellspacing="0" cellpadding="0" width="600">
				  <tr>
				    <td width="137" nowrap rowspan="2"><p align="center">JOGO</p></td>
				    <td width="140" nowrap><p align="center">COLUNA<br>
				      PAR</p></td>
				    <td width="140"><p align="center">COLUNA<br>
				      ÍMPAR</p></td>
				    <td width="183" nowrap rowspan="2"><p align="center">JOGO</p></td>
				  </tr>
				  <tr>
				    <td width="140" nowrap><p align="center">NÚMERO APURAÇÃO</p></td>
				    <td width="140"><p align="center">NÚMERO APURAÇÃO</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">1</p></td>
				    <td width="140" nowrap><p align="center">0</p></td>
				    <td width="140" nowrap><p align="center">0</p></td>
				    <td width="183" nowrap><p align="center">11</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">2</p></td>
				    <td width="140" nowrap><p align="center">1</p></td>
				    <td width="140" nowrap><p align="center">1</p></td>
				    <td width="183" nowrap><p align="center">12</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">3</p></td>
				    <td width="140" nowrap><p align="center">2</p></td>
				    <td width="140" nowrap><p align="center">2</p></td>
				    <td width="183" nowrap><p align="center">13</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">4</p></td>
				    <td width="140" nowrap><p align="center">3</p></td>
				    <td width="140" nowrap><p align="center">3</p></td>
				    <td width="183" nowrap><p align="center">14</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">5</p></td>
				    <td width="140" nowrap><p align="center">4</p></td>
				    <td width="140" nowrap><p align="center">4</p></td>
				    <td width="183" nowrap><p align="center">15</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">6</p></td>
				    <td width="140" nowrap><p align="center">5</p></td>
				    <td width="140" nowrap><p align="center">5</p></td>
				    <td width="183" nowrap><p align="center">16</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">7</p></td>
				    <td width="140" nowrap><p align="center">6</p></td>
				    <td width="140" nowrap><p align="center">6</p></td>
				    <td width="183" nowrap><p align="center">17</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">8</p></td>
				    <td width="140" nowrap><p align="center">7</p></td>
				    <td width="140" nowrap><p align="center">7</p></td>
				    <td width="183" nowrap><p align="center">18</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">9</p></td>
				    <td width="140" nowrap><p align="center">8</p></td>
				    <td width="140" nowrap><p align="center">8</p></td>
				    <td width="183" nowrap><p align="center">19</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">10</p></td>
				    <td width="140" nowrap><p align="center">9</p></td>
				    <td width="140" nowrap><p align="center">9</p></td>
				    <td width="183" nowrap><p align="center">20</p></td>
				  </tr>
				</table>
				<p>A série 20 será a primeira cujo local e data do jogo  serão determinados. A partir dessa definição serão estabelecidos os jogos que  os premiados da série 21 a 39 irão assistir. </p>
				<p>Para obtenção da ordem de distribuições dos prêmios de  acordo com os locais e datas dos jogos, <strong>primeiramente  será identificada a COLUNA DO NÚMERO DE APURAÇÃO</strong> (se par ou ímpar),  utilizando-se, para tanto, o algarismo da centena simples do 1º prêmio.  Exemplo:</p>
				<table border="1" cellspacing="0" cellpadding="0" width="567">
				  <tr>
				    <td width="198" valign="top"><p>1º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>6</p></td>
				    <td width="76" valign="top"><p>1</p></td>
				    <td width="76" valign="top"><p><strong>3</strong></p></td>
				    <td width="85" valign="top"><p>1</p></td>
				    <td width="66" valign="top"><p>6</p></td>
				  </tr>
				  <tr>
				    <td width="198" valign="top"><p>2º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>1</p></td>
				    <td width="76" valign="top"><p>2</p></td>
				    <td width="76" valign="top"><p>5</p></td>
				    <td width="85" valign="top"><p>9</p></td>
				    <td width="66" valign="top"><p>4</p></td>
				  </tr>
				  <tr>
				    <td width="198" valign="top"><p>3º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>1</p></td>
				    <td width="76" valign="top"><p>3</p></td>
				    <td width="76" valign="top"><p>7</p></td>
				    <td width="85" valign="top"><p>7</p></td>
				    <td width="66" valign="top"><p>0</p></td>
				  </tr>
				  <tr>
				    <td width="198" valign="top"><p>4º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>7</p></td>
				    <td width="76" valign="top"><p>1</p></td>
				    <td width="76" valign="top"><p>6</p></td>
				    <td width="85" valign="top"><p>7</p></td>
				    <td width="66" valign="top"><p>8</p></td>
				  </tr>
				  <tr>
				    <td width="198" valign="top"><p>5º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>2</p></td>
				    <td width="76" valign="top"><p>5</p></td>
				    <td width="76" valign="top"><p>7</p></td>
				    <td width="85" valign="top"><p>1</p></td>
				    <td width="66" valign="top"><p>5</p></td>
				  </tr>
				</table>
				<p>O número 3 é ímpar, então o número de apuração será  encontrado na <strong>coluna ímpar</strong>.</p>
				<p><strong>Em seguida, para a  identificação do número de apuração</strong>, considera-se algarismo da centena do  2º prêmio, conforme o exemplo abaixo:</p>
				<table border="1" cellspacing="0" cellpadding="0" width="567">
				  <tr>
				    <td width="198" valign="top"><p>1º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>6</p></td>
				    <td width="76" valign="top"><p>1</p></td>
				    <td width="76" valign="top"><p>3</p></td>
				    <td width="85" valign="top"><p>1</p></td>
				    <td width="66" valign="top"><p>6</p></td>
				  </tr>
				  <tr>
				    <td width="198" valign="top"><p>2º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>1</p></td>
				    <td width="76" valign="top"><p>2</p></td>
				    <td width="76" valign="top"><p><strong>5</strong></p></td>
				    <td width="85" valign="top"><p>9</p></td>
				    <td width="66" valign="top"><p>4</p></td>
				  </tr>
				  <tr>
				    <td width="198" valign="top"><p>3º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>1</p></td>
				    <td width="76" valign="top"><p>3</p></td>
				    <td width="76" valign="top"><p>7</p></td>
				    <td width="85" valign="top"><p>7</p></td>
				    <td width="66" valign="top"><p>0</p></td>
				  </tr>
				  <tr>
				    <td width="198" valign="top"><p>4º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>7</p></td>
				    <td width="76" valign="top"><p>1</p></td>
				    <td width="76" valign="top"><p>6</p></td>
				    <td width="85" valign="top"><p>7</p></td>
				    <td width="66" valign="top"><p>8</p></td>
				  </tr>
				  <tr>
				    <td width="198" valign="top"><p>5º PRÊMIO</p></td>
				    <td width="66" valign="top"><p>2</p></td>
				    <td width="76" valign="top"><p>5</p></td>
				    <td width="76" valign="top"><p>7</p></td>
				    <td width="85" valign="top"><p>1</p></td>
				    <td width="66" valign="top"><p>5</p></td>
				  </tr>
				</table>
				<p>Dessa forma, o <strong>Jogo</strong> correspondente a “SÉRIE 20” será identificado na tabela abaixo de acordo com a  sua coluna e o seu número de apuração.</p>
				<table border="1" cellspacing="0" cellpadding="0" width="600">
				  <tr>
				    <td width="137" nowrap rowspan="2"><p align="center">JOGO</p></td>
				    <td width="140" nowrap><p align="center">COLUNA<br>
				      PAR</p></td>
				    <td width="140"><p align="center">COLUNA<br>
				      ÍMPAR</p></td>
				    <td width="183" nowrap rowspan="2"><p align="center">JOGO</p></td>
				  </tr>
				  <tr>
				    <td width="140" nowrap><p align="center">NÚMERO APURAÇÃO</p></td>
				    <td width="140"><p align="center">NÚMERO APURAÇÃO</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">1</p></td>
				    <td width="140" nowrap><p align="center">0</p></td>
				    <td width="140" nowrap><p align="center">0</p></td>
				    <td width="183" nowrap><p align="center">11</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">2</p></td>
				    <td width="140" nowrap><p align="center">1</p></td>
				    <td width="140" nowrap><p align="center">1</p></td>
				    <td width="183" nowrap><p align="center">12</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">3</p></td>
				    <td width="140" nowrap><p align="center">2</p></td>
				    <td width="140" nowrap><p align="center">2</p></td>
				    <td width="183" nowrap><p align="center">13</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">4</p></td>
				    <td width="140" nowrap><p align="center">3</p></td>
				    <td width="140" nowrap><p align="center">3</p></td>
				    <td width="183" nowrap><p align="center">14</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">5</p></td>
				    <td width="140" nowrap><p align="center">4</p></td>
				    <td width="140" nowrap><p align="center">4</p></td>
				    <td width="183" nowrap><p align="center">15</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">6</p></td>
				    <td width="140" nowrap><p align="center">5</p></td>
				    <td width="140" nowrap><p align="center">5</p></td>
				    <td width="183" nowrap><p align="center">16</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">7</p></td>
				    <td width="140" nowrap><p align="center">6</p></td>
				    <td width="140" nowrap><p align="center">6</p></td>
				    <td width="183" nowrap><p align="center">17</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">8</p></td>
				    <td width="140" nowrap><p align="center">7</p></td>
				    <td width="140" nowrap><p align="center">7</p></td>
				    <td width="183" nowrap><p align="center">18</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">9</p></td>
				    <td width="140" nowrap><p align="center">8</p></td>
				    <td width="140" nowrap><p align="center">8</p></td>
				    <td width="183" nowrap><p align="center">19</p></td>
				  </tr>
				  <tr>
				    <td width="137" nowrap><p align="center">10</p></td>
				    <td width="140" nowrap><p align="center">9</p></td>
				    <td width="140" nowrap><p align="center">9</p></td>
				    <td width="183" nowrap><p align="center">20</p></td>
				  </tr>
				</table>
				<p>Portanto, temos: coluna ímpar, número de apuração 5 e,  portanto, jogo 16. </p>
				<p>Após a identificação do prêmio da “SERIE 20”, qual seja o  jogo 16, os demais prêmios serão distribuídos sequencialmente nas séries  seguintes.</p>
				<p>Com isso, os prêmios das séries FAN PHOTOGRAPHER passam a  ser:</p>
				<table border="0" cellspacing="0" cellpadding="0" width="596">
				  <tr>
				    <td width="64"><p align="center"><strong>Série</strong></p></td>
				    <td width="172"><p align="center"><strong>JOGO</strong></p></td>
				    <td width="162"><p align="center"><strong>DATA</strong></p></td>
				    <td width="198"><p align="center"><strong>CIDADE</strong></p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">20</p></td>
				    <td width="172"><p align="center">16</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">04/07/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Rio de Janeiro</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">21</p></td>
				    <td width="172"><p align="center">17</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">04/07/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Rio de Janeiro</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">22</p></td>
				    <td width="172"><p align="center">18</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">05/07/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Brasília</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">23</p></td>
				    <td width="172"><p align="center">19</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">05/07/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Salvador</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">24</p></td>
				    <td width="172"><p align="center">20</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">12/07/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Brasília</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">25</p></td>
				    <td width="172"><p align="center">1</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">13/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Salvador</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">26</p></td>
				    <td width="172"><p align="center">2</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">15/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Porto Alegre</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">27</p></td>
				    <td width="172"><p align="center">3</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">18/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Porto Alegre</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">28</p></td>
				    <td width="172"><p align="center">4</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">23/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Recife</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">29</p></td>
				    <td width="172"><p align="center">5</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">23/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Brasília</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">30</p></td>
				    <td width="172"><p align="center">6</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">24/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Belo Horizonte</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">31</p></td>
				    <td width="172"><p align="center">7</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">24/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Fortaleza</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">32</p></td>
				    <td width="172"><p align="center">8</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">28/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Belo Horizonte</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">33</p></td>
				    <td width="172"><p align="center">9</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">28/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Brasília</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">34</p></td>
				    <td width="172"><p align="center">10</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">29/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Fortaleza</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">35</p></td>
				    <td width="172"><p align="center">11</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">29/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Recife</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">36</p></td>
				    <td width="172"><p align="center">12</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">30/06/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Porto Alegre</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">37</p></td>
				    <td width="172"><p align="center">13</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">01/07/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Salvador</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">38</p></td>
				    <td width="172"><p align="center">14</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">01/07/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>São Paulo</p></td>
				  </tr>
				  <tr>
				    <td width="64"><p align="center">39</p></td>
				    <td width="172"><p align="center">15</p></td>
				    <td width="162" nowrap valign="bottom"><p align="center">04/07/2014</p></td>
				    <td width="198" nowrap valign="bottom"><p>Fortaleza</p></td>
				  </tr>
				</table>
		
			</div>
			
			<h3>10.	O que preciso fazer para ganhar os números para o sorteio?</h3>
			<div>Já cadastrado na promoção, indicando o prêmio, e uma vez efetuadas compras com o mesmo CPF utilizado no cadastro, a Promotora efetua o cruzamento das informações e o participante passa a concorrer automaticamente aos prêmios, desde que respeitados todos os temos do Regulamento. Não é necessário nenhum passo adicional.</div>
			
			<h3>11.	Como meus números de pedidos são transformados em elementos sorteáveis?</h3>
			<div>
				<p>A cada compra efetuada na  loja online Submarino.com, o cliente receberá um número de pedido. Esse número  será transformado em números para o sorteio da seguinte forma: <br>
  Exemplos:<br>
  1 (um) Número de Pedido  de R$ 399,00 (trezentos e noventa e nove reais) em produtos participantes = 1  (um) cupom. <br>
  1 (um) Número de Pedido  de R$ 798,00 (setecentos e noventa e oito reais) em produtos participantes = 2  (dois) cupons. <br>
  1 (um) Número de Pedido  de R$ 898,00 (oitocentos e noventa e oito reais) = 2 cupons. <strong>O valor de R$ 100,00 não permanece como  saldo para outra utilização</strong>.<br>
  Não serão utilizados para  obtenção de cupons os valores do número do pedido que não se referirem a  produtos, por exemplo, valores de garantia estendida.<br>
  O participante pode  utilizar o mesmo Número de Pedido apenas 1 (uma) vez nessa Promoção. </p>

			</div>
			
			<h3>12.	Onde consulto meus números da sorte?</h3>
			<div>
				<p>Os  elementos sorteáveis serão distribuídos e divulgados a você em seu extrato  pessoal no site da promoção <a href="http://www.submarino.com.br/copa-do-mundo-sony">www.submarino.com.br/copa-do-mundo-sony</a> até um dia antes do sorteio, ou seja, dia 02 de maio de  2014. <strong> </strong></p>

			</div>
			
			<h3>13.	Quando será realizado o sorteio?</h3>
			<div>O sorteio acontecerá em 03/05/2014 e serão sorteados 25 ganhadores, sendo que dependendo da escolha feita no momento do cadastramento na promoção, cada um vai receber um pacote de viagem com acompanhante + Fan Photographer ou pacote de viagem com acompanhante + par de ingresso para o jogo final da Copa do Mundo 2014.</div>
			
			<h3>14.	Como saberei se sou um dos sorteados?</h3>
			<div>
				<p>O resultado da Promoção e os nomes dos contemplados serão  divulgados no site da promoção <a href="http://www.submarino.com/copa-do-mundo-sony">www.submarino.com.br/copa-do-mundo-sony</a>.  em até 10 (dez) dias após a apuração.</p>
<p>Os contemplados serão comunicados do resultado da  promoção pelo envio preferencialmente de e-mail e subsidiariamente de telegrama  ou carta com aviso de recebimento (AR), bem como por contato telefônico, no  prazo máximo de 05 (cinco) dias da data da apuração, de acordo com os dados  cadastrais mantidos pela Promotora.</p>

			</div>
			
			<h3>15.	O que são os Fan Photographers?</h3>
			<div>
				<p>Os Fan Photographers são pessoas que terão o direito de se posicionar  na lateral do campo, 45 (quarenta e cinco) minutos antes do início do jogo da  Copa do Mundo Fifa 2014 e poderão permanecer por 25 (vinte e cinco) minutos  para fotografar o aquecimento dos jogadores.<br>
    <strong>No caso do ganhador ser Fan Photographer </strong>deverá encaminhar a  seguinte documentação, sua e de seu acompanhante à Promotora:<strong> </strong><br>
  a. Nome completo<br>
  b. Endereço completo<br>
  c. Preenchimento de formulário com informações necessárias(restrições  alimentares, alergias). Este formulário será disponibilizado pela realizadora  para o preenchimento do ganhador.<br>
  Somente o contemplado poderá se posicionar na lateral do campo para  fotografar o aquecimento.<br>
  O contemplado não poderá entrar em campo com nenhum equipamento  eletrônico de fotografia que não seja a câmera emprestada pela Sony. A Sony  disponibilizará uma câmera digital para os ganhadores utilizarem no momento que  estiverem na lateral do campo fotografando, que deverá ser restituída após a  utilização.<br>
  O ganhador não poderá executar nenhum tipo de filmagem dentro do  campo, apenas e tão somente fotografar com a câmera que será emprestada pela  Sony, mesmo que a câmera tenha a função filme.<br>
  Todas as fotos tiradas pelo ganhador serão armazenadas em um DVD-R e  enviadas para o participante via Correio em até 30 dias contados a partir do  dia do evento.<br>
  As fotos tiradas não ficarão armazenadas em nenhum arquivo, sendo  considerada finalizada a reponsabilidade da Promotora no momento da entrega do  DVD-R pelos Correios.<br>
  As fotos tiradas pelo ganhador não poderão em hipótese alguma ser alvo  de comercialização.<br>
  O contemplado e o acompanhante poderão  permanecer no estádio para assistir ao jogo. </p>

			</div>
			
			<h3>16.	Quais documentos serão necessários para receber meu prêmio?</h3>
			<div>
				<p>A Promotora entregará uma carta-compromisso para que  você assine. Será necessário apresentar também o número do pedido com o qual  você participou na promoção. <br>
  Além disso,  podem ser solicitados documentos requeridos pela FIFA, bem como qualquer  informação adicional, dentro dos limites da lei.<br>
  Por fim, para retirada dos ingressos, será necessário  apresentar um documento de identidade válido e com fotografia.<br>
  Na eventualidade de o participante ganhador ser  menor de 18 (dezoito) anos, o seu responsável legal deverá assinar a  carta-compromisso, a qual deverá ser preenchida com os dados do ganhador, e  retirar o prêmio, entregue em nome do menor e, para tanto, deverá comprovar tal  condição mediante a apresentação de documento.</p>

			</div>
			
			<h3>17.	Onde retiro meus ingressos ?</h3>
			<div>Se você for um dos ganhadores da promoção, receberá todas as informações de procedimento para recebimento do seu prêmio.</div>
			
			<h3>18.	Posso trocar os ingressos por dinheiro?</h3>
			<div>Não, os prêmios não podem ser trocados por dinheiro.</div>
			
			<h3>19.	Não moro do Brasil. Posso participar?</h3>
			<div>Não, a promoção é exclusiva para pessoas residentes e domiciliadas no Brasil.</div>
			
			<h3>20.	Posso ganhar mais de uma vez?</h3>
			<div>Sim, se você for sorteado. </div>
			
			<h3>21.	Posso dar os ingressos como presente?</h3>
			<div>Apenas o ganhador ou pessoa que tenha procuração para tanto poderá retirar os ingressos. Depois de retirado, você poderá entrega-los para outra pessoa utilizar, sendo certo que após a retirada dos ingressos encerra-se a responsabilidade da Promotora e das Aderentes quanto à utilização deles. </div>
			
			<h3>22.	Existe limite de cupons?</h3>
			<div>Não, você pode realizar quantas compras quiser ao longo do período de participação e ganhará um elemento sorteável para cada R$ 399,00 em compras.</div>
			
			<h3>23.	Posso cadastrar na promoção com mais de um e mail?</h3>
			<div>Não, você deve se cadastrar apenas um com um e-mail e CPF.</div>
			
			<h3>24.	O transporte e a hospedagem estão inclusos no prêmio?</h3>
			<div>
				<p>A Promotora será responsável pelo  transporte da cidade de domicílio para a cidade onde o jogo da Copa do Mundo  FIFA 2014 será realizado.<br>
  Não fazem jus ao pacote de viagem os  moradores dos locais onde se realizar os jogos para os quais os participantes  foram premiados. Os ganhadores dos jogos a se realizarem às 13 horas, retornarão  para suas cidades no mesmo dia do jogo, pois há disponibilidade de horários de  voos e não terão direito à hospedagem. <br>
  Já os ganhadores dos jogos a se  realizarem às 16h, 17h, 18h e 22h terão direito ao pernoite, pois, devido ao  horário do jogo, não há mais voo disponíveis para retorno no mesmo dia. <br>
  Não fazem jus a passagens aéreas os  ganhadores cujos domicílios estejam a 200 km ou menos do estádio. Nesses casos,  o transporte será terrestre. </p>

			</div>
			
			<h3 class="doubleLine">25.	Como posso ter mais informações sobre o processo de auditoria e funcionamento da mecânica da promoção de forma <br />que eu possa me assegurar que tudo o que o regulamento informa é verídico?  </h3>
			<div>
				<p>A Promotora e  Aderentes zelam pela regularidade da mecânica de suas promoções mediante obtenção  de autorização expedida pela Caixa Econômica Federal, bem como pela contratação  de empresa de auditoria independente, desde o início de todas as suas  promoções.<br>
  São  fiscalizadas todas as etapas da promoção, como distribuição dos elementos  sorteáveis, apuração, identificação dos ganhadores etc. <br>
  Dúvidas  podem ser esclarecidas no site da promoção. </p>

			</div>
			
		</div><!--.accordion-->
		
    </div><!--.wrap-content | premios-->
    
    <div class="wrap-content box-vencedores">
    	<h2 class="title">vencedores</h2>
    	
    	<ul class="list-vencedores">
    		<li>
    			<h3>Fernando Cesar de Souza Lima</h3>
    			<p><span class="label">Cidade:</span> <span class="city">SÃO PAULO</span></p>
                <div class="clear"></div>
    		</li>
            <li>
                <h3>Jacqueline Cristina Braga</h3>
                <p><span class="label">Cidade:</span> <span class="city">FORTALEZA</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Ilton Moreira da Costa</h3>
                <p><span class="label">Cidade:</span> <span class="city">RIO DE JANEIRO</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Gerson Machado de Souza</h3>
                <p><span class="label">Cidade:</span> <span class="city">BRASÍLIA</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Fernando Tadeu Trevisan</h3>
                <p><span class="label">Cidade:</span> <span class="city">SALVADOR</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Alessandro Alves Mesquita</h3>
                <p><span class="label">Cidade:</span> <span class="city">BRASÍLIA</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Cleber Giovani Rodrigues da Cunha</h3>
                <p><span class="label">Cidade:</span> <span class="city">SALVADOR</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Haroldo Tayra Gushiken</h3>
                <p><span class="label">Cidade:</span> <span class="city">PORTO ALEGRE</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Roberto Diez Corte Filho</h3>
                <p><span class="label">Cidade:</span> <span class="city">PORTO ALEGRE</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Diogo Felipe da Silva</h3>
                <p><span class="label">Cidade:</span> <span class="city">RECIFE</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Celso Berri Neto</h3>
                <p><span class="label">Cidade:</span> <span class="city">BRASÍLIA</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Marcus Alexandre da Cunha Silva</h3>
                <p><span class="label">Cidade:</span> <span class="city">BELO HORIZONTE</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Julio Cesar Toledo Fernandes</h3>
                <p><span class="label">Cidade:</span> <span class="city">FORTALEZA</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>David Siqueira Santos</h3>
                <p><span class="label">Cidade:</span> <span class="city">BELO HORIZONTE</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Sueli Aparecida Lobo de Oliveira</h3>
                <p><span class="label">Cidade:</span> <span class="city">RECIFE</span></p>
                <div class="clear"></div>
            </li>
            <li>
                <h3>Eduard de Souza Bandeira</h3>
                <p><span class="label">Cidade:</span> <span class="city">SALVADOR</span></p>
                <div class="clear"></div>
            </li>
    	</ul><!--.list-vencedores-->
		
    </div><!--.wrap-content | vencedores-->
    
     <div class="wrap-content box-logged displayNone">
    	<h2 class="title">Olá <span id="promoUserName"></span>!</h2>
    	<input type="hidden" id="hdpf" value="" />
    	
    	<div class="tickets-quant"><span id="tickets-total"></span></div><!--.tickets-quant-->

    	<p class="title">Seus tickets para sorteio:</p>
    	
    	<table id="show-tickets">
    		<thead>
        		<tr>
        			<th class="col1">data do cadastro</th>
        			<th class="col2">código promocional ou número do pedido </th>
        			<th class="col3">ticket do sorteio</th>
        		</tr>
    		</thead>
    		<tbody>
        		
    		</tbody>
    	</table>
    	
    	<div class="aviso"><strong>Importante:</strong> Apenas os pedidos aprovados serão transformados em número da sorte. Nosso sistema precisa de até 24 horas para validar seu código e gerar o ticket do sorteio.</div>
		
		<div class="clear"></div>
		
    </div><!--.wrap-content | vencedores-->
    
    <div class="clear"></div>
    
    <div class="list-publicidade">
		<ul>
			<li><a href="http://www.submarino.com.br/televisao-nova?WT.mc_id=Rodape1Dest_Hs14&chave=Rodape1Dest_Hs14"><img src="<?php echo BASEURL; ?>images/promo/footer/banner-televisao-nova.gif" /></a></li>
		</ul>
		<div class="clear"></div>
    </div><!--.list-publicidade-->
    
    <div class="clear"></div>
    <p style="margin: 60px auto 20px auto; text-align: center;">Período de Participação de 25/03/2014 a 19/04/2014. Certificado de Autorização CAIXA nº 1-0421/2014.</p>
	
	
	<div id="popupAniversario" style="width:542px; display: none;">
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_01.gif" style="display: block;" />
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_02.gif" style="display: block;" />
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_03.gif" style="display: block;" />
		<img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_04.gif" style="display: block;" />
		<a href="http://www.americanas.com/especial/hotsite/aniversario2013/303994/tag_promoniver_patroc/303981?chave=hs_aniver2013_patrocinadores&WT.mc_id=hs_aniver2013_patrocinadores" target="_blank"><img src="http://img.americanas.com.br/catalog/skins/hotsites/hs-vem-pra-festa/images/popup_hotsite_aniversario_05.gif" style="display: block;" /></a>
	</div>

</div><!--#hs-vem-pra-festa-->

<!--HS END-->

<?php
include_once('suba-hs-tpl/footer.php');
?>
