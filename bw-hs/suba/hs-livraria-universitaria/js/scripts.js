onReady('jQuery', function(){
    var paramTag = ['sl-1ludestsub'];

    Produtos.getProductsByTag(paramTag, function(data){
        /*
         * Percorre cada item do Array/Objeto
         * \n � para inserir quebra de linha
         */
        $.each(data, function(index, obj){

            templateHTML(obj.products, obj.tag);

        });
    });

    // Método para retornar templates
    function templateHTML(data, templateName){
        var html = '';

        $.each(data, function(index, item){
            html += '<li><a href="#">';
            html += '    <img class="hs-product-img" src="'+item.image+'" />';
            html += '    <span class="hs-desc">'+truncateText(item.name, 60)+'</span>';
            html += '    <span class="hs-rs">R$</span>';
            html += '   <span class="hs-preco">'+item.sales_price.substr(2)+'</span>';
            html += '    <span class="hs-parcela">'+item.installment.total_installments+'x de R$ '+item.installment.installment_value.substr(2)+'</span>';
            html += '</a></li>';
        });

        $('.hs-preco-dinamico ul').append(html);

        $.getScript('http://img.submarino.com.br/inov/js/jquery.carouFredSel-5.6.4-packed.js', function(){
            var containerCarrossel = $('.hs-preco-dinamico ul');
            $(containerCarrossel).carouFredSel({
                auto:false,
                scroll:{
                    items: 1,
                    fx: "slide",
                    pauseOnHover: true
                },
                prev: {
                    button: ".hs-prev",
                    key: "left"
                },
                next: {
                    button: ".hs-next",
                    key: "right"
                }
            });
        });
    }

    function truncateText(str, limit, separator) {
        separator = (separator) ? separator : '...';

        if (str.length > limit) {
            str = str.substr(0, limit);
            str = str.split(' ');
            $html = '';

            for ($i = 0; $i < (str.length - 1); $i++) {
                $html += ($i == 0) ? str[$i] : ' ' + str[$i];
            }

            $html += separator;

            return $html;

        } else {
            return str;
        }
    }
});