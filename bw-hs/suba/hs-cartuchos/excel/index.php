<?php
/**
 * Created by PhpStorm.
 * User: rodrigo.carneiro
 * Date: 03/06/14
 * Time: 11:34
 */

/*
 * API PARAMS FULL EXAMPLE
 * ?action=get&table=cartuchos
 * &like=brand,brother;color,ciano
 * &limit=3
 * &cols=dbCol1,dbCol2, ...
 */

require_once('MysqliDb.php');

//$db = new Mysqlidb('mysql.rodrigocarneiro.net.br', 'rodrigocarneir08', 'db12345', 'rodrigocarneir08');
$db = new Mysqlidb('localhost', 'root', '', 'teste_excel');

if(isset($_GET) and isset($_GET['action']) and isset($_GET['table'])){

    // Verifica os params principais da API
    $table = $_GET['table'];
    $cols = (isset($_GET['cols'])) ? explode(',', $_GET['cols']) :  Array ("cartuchos_id, product_id, brand, description, model, color");
    $limit = (isset($_GET['limit'])) ? $_GET['limit'] : null;
    $page = (isset($_GET['page']) and $_GET['page'] != 'undefined') ? $_GET['page'] : 1;
    $action = (isset($_GET['action'])) ? $_GET['action'] : false;
    $like = (isset($_GET['like'])) ? explode('.', $_GET['like']) : false;

    $query = "select ";
    $params = array();

    if(isset($_GET['cols'])){
        $i = 0;
        foreach($cols as $col){
            $coma = ($i > 0) ? ' , ' : '';
            $query .= $coma.$col;
            $i++;
        }

    }

    $query .= " from $table ";

    // add as opções do LIKE na query
    if($like){
        $query .= 'where ';
        $i = 0;
        foreach($like as $l){
            $opt = explode('_', $l);
            $and = ($i > 0) ? ' and ' : '';
            $params[] = '%'.$opt[1].'%';
            $query .= $and.$opt[0]." like ? ";
            $i++;
        }
    }


    if($table != 'modelos'){
        $query .= (!$like) ? 'where ' : '' ;
        $and = ($like) ? ' and' : '';
        $params[] = 'y';
        $query .= $and." active = ? ";
    }

    if(isset($_GET['cols'])){
        if(in_array('distinct model', $cols)){
            $query .= " order by model asc ";
        }
        elseif(in_array('distinct brand', $cols)){
            $query .= " order by brand asc ";
        }
        elseif(in_array('distinct color', $cols)){

            $query .= " order by color asc ";
        }
    }

    # Total de itens na busca
    if($params){
        $count = $db->rawQuery($query, $params);
    } else {
        $count = $db->rawQuery($query);
    }


    $json['totalPages'] = round($db->count/30);

    $offset = abs(($page - 1) * 30);

    $query .= ($limit== null) ? '' : " limit $limit offset $offset";
    $query .= ($limit== null and (isset($_GET['page']) and $_GET['page'] != 'undefined')) ? " limit 30 offset $offset" : '';


    // Retorna o resultado da query
    if($params){
        $json[$table] = $db->rawQuery($query, $params);
    } else {
        $json[$table] = $db->rawQuery($query);
    }

    if(isset($_GET['callback'])){
        $encoded = $_GET['callback'] . '('. json_encode($json) . ');';
    } else {
        $encoded = json_encode($json);
    }

    echo $encoded;
}