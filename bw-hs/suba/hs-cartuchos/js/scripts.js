var API =(function() {
    var api = {};
    var BASEURL = 'http://apps.submarino.com.br/forms/hotsite/hs-cartuchos/excel/';
    var MSG_NOTFOUND = 'No momento sua busca não encontrou nenhum resultado';
    var MSG_SEARCHING = 'Buscando...';
    var DEBUG = false;
    var lastSearch = '';

    // Ativa e desativa o DEBUG passando true ou false
    api.setDebug = function(){
        DEBUG = (arguments[0]  != typeof 'undefined' && arguments[0] == true) ? true : false;
    }

    // Realiza chamada ajax JSONP passando uma url
    api.getAjax = function(ajaxUrl, jsonpName, callback){
		if(jsonpName=='marcas')
			$('#hsProductList .grid').html('<li class="search-not-found">'+MSG_SEARCHING+'</li>');

        $.ajax({
            url: ajaxUrl+'&callback=?',
            jsonpCallback: jsonpName + '_' + Math.floor((Math.random() * 10000) + 1),
            cache: false,
            dataType: 'jsonp',
            success: function(json){
                callback(json);
            }
        });
		
		// Rola a página para o topo
		if(jsonpName=='query'){
			var body = $("html, body");
			body.animate({scrollTop:0}, '500', 'swing');
		}
    }

    // Seta o filtro de marcas
    api.setMarcas = function(){
        var ajaxUrl = BASEURL+'?action=get&table=cartuchos&cols=distinct%20brand';

        api.getAjax(ajaxUrl,'marcas', function(data){
            var obj = data.cartuchos;

            for(i=0; i<obj.length; i++){
                if(obj[i].brand != ''){
                    $('#filterMarcas').append('<option class="item" value="'+obj[i].brand+'">' + obj[i].brand    + '</option>');
                    $('#menuMarcas').append('<li><a href="' + obj[i].brand + '">' + obj[i].brand + '</a></li>');
                }
            }
        });
    }

    // Seta o filtro de Modelo
    api.setModelo = function(){
        $('#btn-filter').addClass('load');

        var strToSearch = (arguments[0] == typeof 'undefined') ? false : arguments[0];

        var ajaxUrl = BASEURL+'?action=get&table=modelos&cols=distinct%20model';
        ajaxUrl += (strToSearch) ? '&like=brand_'+strToSearch : '';

        api.getAjax(ajaxUrl, 'modelo', function(data){
            var obj = data.modelos;

            // remove os options do modelo
            if(strToSearch){
                var modeloCurrent = $('#filterModelo').val();
                $('#filterModelo .item').remove();
            }

            for(i=0; i<obj.length; i++){
                if(obj[i].model != '')
                    $('#filterModelo').append('<option class="item" value="'+obj[i].model+'">' + obj[i].model + '</option>')
            }

            // remove os options da marca
            if(strToSearch){
				
				$("#filterMarcas option").removeAttr('selected');
				$("#filterModelo option").removeAttr('selected');
				
                $("#filterMarcas option[value='"+strToSearch+"']").attr('selected', 'selected');
                $("#filterModelo option[value='"+modeloCurrent+"']").attr('selected', 'selected');
            }

            $('#btn-filter').removeClass('load');
        });
    }

    // Seta o filtro de Cores
    api.setCores = function(){
        $('#btn-filter').addClass('load');

        var strToSearch = (arguments[0] == typeof 'undefined') ? false : arguments[0];
        var searchIn = (arguments[1] == typeof 'undefined') ? 'brand' : arguments[1];

        var ajaxUrl = BASEURL+'?action=get&table=cartuchos&cols=distinct%20color';
        ajaxUrl += (strToSearch) ? '&like='+searchIn+'_'+strToSearch : '';

        api.getAjax(ajaxUrl, 'cor', function(data){
            var obj = data.cartuchos;

            if(strToSearch){
                $('#filterCores .item').remove();
            }

            for(i=0; i<obj.length; i++){
                if(obj[i].color != '')
                    $('#filterCores').append('<option class="item" value="'+obj[i].color+'">' + obj[i].color + '</option>');

                if(searchIn != 'model'){
                    if(obj[i].color != '')
                        $('#menuCor').append('<li><a href="' + obj[i].color + '">' + obj[i].color + '</a></li>');
                }
            }

            $('#btn-filter').removeClass('load');
        });
    }

    // Gera o menu de filtros
    api.doMenuFilter = function(strToSearch, filter){
        // Ajax Loader
        $('#hsProductList .grid').html('<li class="loader"></li>');

        api.setModelo(strToSearch);
        api.setCores(strToSearch);

        if(filter == 'color'){
            $("#filterCores option[value='"+strToSearch+"']").attr('selected', 'selected');
            $("#filterMarcas option").eq(0).attr('selected', 'selected');
        }
        else if(filter == 'brand'){
            $("#filterCores option").eq(0).attr('selected', 'selected');
        }

        var ajaxUrl = BASEURL+'?action=get&table=cartuchos&cols=product_id,color&like='+filter+'_'+ strToSearch;

        api.createProductList(ajaxUrl);
    }

    // Realiza o filtro relacional
    api.doFilter = function(){
        // Ajax Loader
        $('#hsProductList .grid').html('<li class="loader"></li>');

        var sqlLike = [];
        var strLike = '';

        ($('#filterMarcas').val()!= '') ? sqlLike.push('brand_' + $('#filterMarcas').val()) : '' ;
        ($('#filterTipo').val()!= '') ? sqlLike.push('type_' + $('#filterTipo').val()) : '' ;
        ($('#filterModelo').val()!= '') ? sqlLike.push('model_' + $('#filterModelo').val()) : '' ;
        ($('#filterCores').val()!= '') ? sqlLike.push('color_' + $('#filterCores').val()) : ''  ;

        for(i=0; i<sqlLike.length; i++){
            separator = (i>0) ? '.' : '';
            strLike += separator + sqlLike[i];
        }

        var hasFilter = (sqlLike != '') ? true : false;

        if(hasFilter){
            //api.setModelo($('#filterMarcas').val());
            var ajaxUrl = BASEURL+'?action=get&table=cartuchos&cols=product_id,color&like=' + strLike;
        } else {
            //api.setModelo();
            var ajaxUrl = BASEURL+'?action=get&table=cartuchos&cols=product_id,color&like=priority_y&limit=30';
        }

        api.createProductList(ajaxUrl);


    }

    // Gera o html da lista de produtos
    api.createProductList = function(ajaxUrl){

        ajaxUrl = api.removeURLParameter(ajaxUrl, 'page');

        if(arguments[1] == undefined){
			var currentPage = 1; 
		} else {
			ajaxUrl = ajaxUrl+'&page='+arguments[1];
			var currentPage = arguments[1];
		}

        lastSearch = ajaxUrl;

        // getAjax 1: retorna o resultado da consulta
        api.getAjax(ajaxUrl,'query', function(data){
            //paginação
            var pageList = '';
            for(i=1; i<=data.totalPages; i++){
			
				var active = (currentPage == i) ? 'active' : '';
                pageList += '<a data-page="'+i+'" class="'+active+'">'+i+'</a>';
            }
            $('#hsProductList .pagination').html(pageList);


            var obj = data.cartuchos;
            var product_ids = '';

            if(obj.length){
                for(i=0; i<obj.length; i++){
                    separator = (i>0) ? ',' : '';
                    product_ids += separator + obj[i].product_id;
                }

                var paramFull = (DEBUG) ? '&full=true' : '';
                var ajaxUrl = 'http://www.submarino.com.br/productinfo?itens='+product_ids+ paramFull +'&callback=?';

                // getAjax 2: retorna os dados dos produtos buscando pelos IDs
                api.getAjax(ajaxUrl,'query', function(data){
                    api.template(data, obj);
                });
            } else {
                $('#hsProductList .grid').html('<li class="search-not-found">'+MSG_NOTFOUND+'</li>');
            }

        });
    }

    // Gera o template para cada item de produto
    api.template = function(obj){
        var html = '';
        var objAux = (arguments[1] == typeof 'undefined') ? false : arguments[1];
        var product = obj.products;
        var skuCheck = 0;


        for(i=0; i<product.length; i++){
            classe = ((i+1)%3==0) ? 'last' : '' ;

            html += '<li class="li round '+classe+'">';

            // Confere se o produto que está na base é o mesmo retornado pelo productinfo
            if(DEBUG){
                for(x=0; x<objAux.length; x++){
                    if(objAux[x].product_id == product[i].id){
                        var skuColor = product[i].technicalEspecification;

                        for(y=0; y<skuColor.length; y++){
                            for(z=0; z<skuColor[y].group.length; z++){
                                if(skuColor[y].group[z].value){
                                    var str = skuColor[y].group[z].value;
                                    var strSearch = objAux[x].color.toLowerCase();

                                    if (str.toLowerCase().indexOf(strSearch) > -1){

                                        skuCheck++;
                                    }
                                }
                            }
                        }

                        if(!skuCheck){
                            html += '<div class="prod-warning"></div>';
                        }

                        skuCheck = 0;
                    }
                }
            }

            html += '    <div class="hproduct  ui-draggable" itemprop="" itemscope="itemscope" itemtype="http://schema.org/Product">';
            html += '        <input class="productId" type="hidden" value="'+product[i].id+'">';
            html += '            <a itemprop="url" href="'+product[i].url+'" title="'+product[i].name+'" class="url">';
            html += '                <img itemprop="image" width="150" height="150" alt="'+product[i].name+'" src="'+product[i].mqImage+'" data-longdesc="'+product[i].image+'" class="photo">';
            html += '                    <span class="dragPIcon">.</span></a>';
            html += '                <span class="stamps" style="height: 30px;">';
            html += '                    <span class="stpImage">';
            html += '                        <span class="stopperInfo" data-href="/estaticapop/regulamento-frete-gratis" title="clique e saiba mais">';
			
			if(product[i].freight!=''){
            html += '                            <img alt="frete_sul_sudeste" src="'+product[i].freight+'">';
			}
            html += '                            </span>';
            html += '                        </span>';
            html += '                    </span>';
            html += '                    <span class="stpImage">';
            html += '                        <img alt="stopper_oferta_especial" src="http://isuba.s8.com.br/novosuba/img/stopper_oferta_especial.gif">';
            html += '                        </span>';


            html += '                        <a href="'+product[i].url+'" title="'+product[i].name+'" class="url">';
            html += '                            <span class="info">';
            html += '                                <strong itemprop="name" class="n name fn">'+api.truncateText(product[i].name, 74)+'</strong>';

            html += '                                <span class="more"><span class="stp stDetails">Saiba mais sobre esse produto</span></span>';
            html += '                            </span>';
            html += '                        </a>';

            html += '                        <span class="priceBox" itemscope="itemscope" itemtype="http://schema.org/PriceSpecification">';

            html += '                            <span class="sale price money">';
            html += '                                <strong class="amount"><span itemprop="price">'+product[i].sales_price+'</span> </strong>';
            html += '                            </span>';
            html += '                            <span class="p-v interest" itemscope="itemscope" itemtype="http://schema.org/Offer">';
            html += '                                <span class="parcel" itemprop="itemCondition">'+product[i].installment.total_installments+'x</span> de <span class="amount" itemprop="price">'+product[i].installment.installment_value+'</span>';
            html += '                            </span>';
            html += '                        </span>';
            html += '                                </li>';

        }

        $('#hsProductList .grid').html(html);
    }

    // Método para truncar um texto
    api.truncateText = function(str, limit, separator) {
        separator = (separator) ? separator : '...';

        if (str.length > limit) {
            str = str.substr(0, limit);
            str = str.split(' ');
            $html = '';

            for ($i = 0; $i < (str.length - 1); $i++) {
                $html += ($i == 0) ? str[$i] : ' ' + str[$i];
            }

            $html += separator;

            return $html;

        } else {
            return str;
        }
    }

    api.removeURLParameter = function(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts= url.split('?');
        if (urlparts.length>=2) {

            var prefix= encodeURIComponent(parameter)+'=';
            var pars= urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i= pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url= urlparts[0]+'?'+pars.join('&');
            return url;
        } else {
            return url;
        }
    }

    api.removeAccents = function(s){
        var r=s.toLowerCase();
        r = r.replace(new RegExp(/\./g),"");
        r = r.replace(new RegExp(/[àáâãäå]/g),"a");
        r = r.replace(new RegExp(/ç/g),"c");
        r = r.replace(new RegExp(/[èéêë]/g),"e");
        r = r.replace(new RegExp(/[ìíîï]/g),"i");
        r = r.replace(new RegExp(/[òóôõö]/g),"o");
        r = r.replace(new RegExp(/[ùúûü]/g),"u");
        return r;
    }

    api.auxiliar = function(){
        api.doFilter();
        $('#btn-filter').removeClass('load');

        $(document).on('click', '.btn-filter', function(){
            api.doFilter();
        });

        $(document).on('click', '#hsMenuFilter #menuMarcas a', function(e){
            e.preventDefault();
            api.doMenuFilter($(this).attr('href'), 'brand');
        });

        $(document).on('click', '#hsMenuFilter #menuCor a', function(e){
            e.preventDefault();
            api.doMenuFilter($(this).attr('href'), 'color');
        });

        $(document).on('change', '#hsSearchFilter #filterMarcas', function(e){
            api.setModelo($(this).val());
        });

        $(document).on('change', '#hsSearchFilter #filterModelo', function(e){
            api.setCores($(this).val(), 'model');
        });

        $(document).on('click', '#hsProductList .pagination a', function(e){
            e.preventDefault();
            api.createProductList(lastSearch, $(this).data('page'));
        });
		
    }

    // Inicializa a API
    api.init = function(){
        window.onload=function(){
            api.setMarcas();
            api.setModelo();
            api.setCores();
            api.auxiliar();
			
			
        }
    }

    return {
        api: api.init(),
        debug: api.setDebug
    }
})();

