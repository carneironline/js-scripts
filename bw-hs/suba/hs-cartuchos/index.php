<?php
include_once('suba-hs-tpl/header.php');

//define('BASEURL', 'http://apps.submarino.com.br/forms/hotsite/hs-cartuchos/');
define('BASEURL', '');
?>

<!--HS BEGIN-->

<!--SCRIPTS HS-->
<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>css/dropkick.css" />
<script type="text/javascript" src="<?php echo BASEURL; ?>js/dropkick.min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL; ?>js/scripts.js"></script>

<!--END SCRIPTS HS-->

<div id="hsCurrent">

    <div id="hsMenuFilter" class="round">
        <h2 class="title">Cartuchos e Toners</h2>

        <ul>
            <li>
                <p class="subtitle">Marcas</p>
                <ul id="menuMarcas"></ul>
            </li>
            <li>
                <p class="subtitle">Cor</p>
                <ul id="menuCor"></ul>
            </li>
        </ul>
    </div><!--#hsMenuFilter-->

	<div id="hsSearchFilter" class="round">
        <h2 class="title">Encontre Cartuchos e Toners</h2>
        <ul>
            <li>
                <h3 class="subtitle">Por Marca</h3>
                <select id="filterMarcas" name="filterMarcas"  class="round"><option value="">Selecione aqui</option></select>
            </li>
            <li>
                <h3 class="subtitle">Por Cartucho ou Toner</h3>
                <select id="filterTipo" name="filterTipo"  class="round"><option value="">Selecione aqui</option>
                    <option class="item" value="TONERS">Toners</option>
                    <option class="item" value="CARTUCHOS">Cartuchos</option>
                </select>
            </li>
            <li>
                <h3 class="subtitle">Por Impressora</h3>
                <select id="filterModelo" name="filterModelo"  class="round"><option value="">Selecione aqui</option></select>
            </li>
            <li>
                <h3 class="subtitle">Por Cor</h3>
                <select id="filterCores" name="filterCores"  class="round"><option value="">Selecione aqui</option></select>
            </li>
			
            <li><a id="btn-filter" class="btn-filter round">Buscar</a></li>
        </ul>
	</div><!--#hsSearchFilter-->

    <div id="hsProductList" class="round">
        <ul class="grid">
            <li class="loader"></li>
        </ul>

        <div class="pagination"></div>
    </div><!--#hsProductList-->

    <div class="clear"></div>
</div><!--#hs-name-->

<!--HS END-->

<?php
include_once('suba-hs-tpl/footer.php');
?>
