<?php
$estrutura_servidor = '/home/storage/6/dc/83/json/public_html/c/submarino/2014/diadospais/';

$avatar = explode('_',$_GET['avatar']);
$corcorpo = $avatar[0];
$tipocorpo = $avatar[1];
$tipocabeca = $avatar[2];
$corcabelo = $avatar[3];
$tipocabelo = $avatar[4];
$corolhos = $avatar[5];
$tipoolhos = $avatar[6];
$corsombrancelhas = $avatar[7];
$tiposombrancelhas = $avatar[8];
$tiponariz = $avatar[9];
$tipooculos = $avatar[10];
$tipoboca = $avatar[11];
$tipobarba = $avatar[12];
$tipolook = $avatar[13];


$image = imagecreatetruecolor(800, 1294);
imagealphablending($image, false);
$col=imagecolorallocatealpha($image,255,255,255,127);
imagefilledrectangle($image,0,0,800, 1294,$col);
imagealphablending($image,true);


$img_corpo = imagecreatefrompng($estrutura_servidor.'images/corpo_'.$tipocorpo.'_'.$corcorpo.'.png');
imagecopyresampled($image, $img_corpo, 0, 294, 0, 0, 800, 1000, 800, 1000);
imagealphablending($image,true);

$img_look = imagecreatefrompng($estrutura_servidor.'images/look_'.$tipocorpo.'_'.$tipolook.'.png');
imagecopyresampled($image, $img_look, 0, 294, 0, 0, 800, 1000, 800, 1000);
imagealphablending($image,true);

$img_cabeca = imagecreatefrompng($estrutura_servidor.'images/cabeca_'.$corcorpo.'_'.$tipocabeca.'.png');
imagecopyresampled($image, $img_cabeca, 240, 0, 0, 0, 320, 450, 320, 450);
imagealphablending($image,true);

if($tipobarba!='0'){
	$img_barba = imagecreatefrompng($estrutura_servidor.'images/barba_'.$corcorpo.'_'.$tipocabeca.'_'.$tipobarba.'.png');
	imagecopyresampled($image, $img_barba, 240, 0, 0, 0, 320, 500, 320, 500);
	imagealphablending($image,true);
}

$img_boca = imagecreatefrompng($estrutura_servidor.'images/boca_'.$corcorpo.'_'.$tipoboca.'.png');
imagecopyresampled($image, $img_boca, 240, 0, 0, 0, 320, 450, 320, 450);
imagealphablending($image,true);

$img_nariz = imagecreatefrompng($estrutura_servidor.'images/nariz_'.$corcorpo.'_'.$tiponariz.'.png');
imagecopyresampled($image, $img_nariz, 240, 0, 0, 0, 320, 450, 320, 450);
imagealphablending($image,true);

$img_olhos = imagecreatefrompng($estrutura_servidor.'images/olhos_'.$tipoolhos.'_'.$corolhos.'.png');
imagecopyresampled($image, $img_olhos, 240, 0, 0, 0, 320, 450, 320, 450);
imagealphablending($image,true);

$img_sombrancelha = imagecreatefrompng($estrutura_servidor.'images/sombrancelha_'.$tiposombrancelhas.'_'.$corsombrancelhas.'.png');
imagecopyresampled($image, $img_sombrancelha, 240, 0, 0, 0, 320, 450, 320, 450);
imagealphablending($image,true);

if($tipooculos!='0'){
	$img_oculos = imagecreatefrompng($estrutura_servidor.'images/oculos_'.$tipocabeca.'_'.$tipooculos.'.png');
	imagecopyresampled($image, $img_oculos, 240, 0, 0, 0, 320, 450, 320, 450);
	imagealphablending($image,true);
}

if($tipocabelo!='0'){
	$img_cabelo = imagecreatefrompng($estrutura_servidor.'images/cabelo_'.$tipocabeca.'_'.$tipocabelo.'_'.$corcabelo.'.png');
	imagecopyresampled($image, $img_cabelo, 240, 0, 0, 0, 320, 450, 320, 450);
	imagealphablending($image,true);
}

$caminho_imagem = 'imagempai/'.$_GET['avatar'].'.png';

imagealphablending($image,false);
imagesavealpha($image,true);
if(imagepng($image, $caminho_imagem, 1)){
    //gerou a imagem
}
imagedestroy($image);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Monte seu pai - Submarino</title>
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/padrao.js"></script>
</head>

<body>

<div id="monte_seu_pai">
<?php
if($_GET['imprimir']!='pagina'){
?>
	<div class="topo_monte">
    	<div class="ttl_monte_seu_pai"><a href="http://apps.submarino.com.br/media/hotsites/hs-diadospais2014/">MONTE O SEU PAI</a></div>
    </div>
<?php
}
?>
<img src="<?php echo $caminho_imagem; ?>" alt="pai" width="500" style="display:block; margin:0 auto;" />
</div>

<script type="text/javascript">
altura_tela = $(window).height();
$('#monte_seu_pai').css('min-height',altura_tela);
<?php
if($_GET['imprimir']=='pagina'){
	echo 'window.print();';
}
?>
</script>

</body>
</html>
<?php


?>