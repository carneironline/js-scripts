<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Monte seu pai - Submarino</title>
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/padrao.js"></script>
</head>

<body>

<div id="monte_seu_pai">

	<div id="divpreloader_avatar">
    </div>
    
    <div class="topo_monte">
    	<div class="ttl_monte_seu_pai">MONTE O SEU PAI</div>
        <div class="txt_monte">A gente já sabe que ele é o cara, mas como é que ele é?<br />Monte o seu pai e descubra quais presentes mais combinam com ele.</div>
    </div>

	<ul id="menu">
    	<li class="membro ativo">
        	<a href="corpo" class="seleciona_membro">CORPO</a>
        	<ul class="seleciona_cor" id="corcorpo">
            	<li class="ativo"><a id="corcorpo_1" href=""></a></li>
                <li><a id="corcorpo_2" href=""></a></li>
                <li><a id="corcorpo_3" href=""></a></li>
                <li><a id="corcorpo_4" href=""></a></li>
            </ul>
            <ul class="seleciona_tipo" id="tipocorpo">
            	<li class="ativo"><a id="tipocorpo_1" href=""></a></li>
                <li><a id="tipocorpo_2" href=""></a></li>
                <li><a id="tipocorpo_3" href=""></a></li>
            </ul>
        </li>
        <li class="membro">
        	<a href="cabeca" class="seleciona_membro">CABEÇA</a>
            <ul class="seleciona_tipo" id="tipocabeca">
            	<li class="ativo"><a id="tipocabeca_1" href=""></a></li>
                <li><a id="tipocabeca_2" href=""></a></li>
                <li><a id="tipocabeca_3" href=""></a></li>
                <li><a id="tipocabeca_4" href=""></a></li>
            </ul>
        </li>
        <li class="membro">
        	<a href="cabelo" class="seleciona_membro">CABELO</a>
            <ul class="seleciona_cor" id="corcabelo">
            	<li class="ativo"><a id="corcabelo_1" href=""></a></li>
                <li><a id="corcabelo_2" href=""></a></li>
                <li><a id="corcabelo_3" href=""></a></li>
                <li><a id="corcabelo_4" href=""></a></li>
                <li><a id="corcabelo_5" href=""></a></li>
                <li><a id="corcabelo_6" href=""></a></li>
            </ul>
            <ul class="seleciona_tipo" id="tipocabelo">
            	<li class="ativo"><a id="tipocabelo_0" href=""></a></li>
                <li><a id="tipocabelo_1" href=""></a></li>
                <li><a id="tipocabelo_2" href=""></a></li>
                <li><a id="tipocabelo_3" href=""></a></li>
                <li><a id="tipocabelo_4" href=""></a></li>
                <li><a id="tipocabelo_5" href=""></a></li>
                <li><a id="tipocabelo_6" href=""></a></li>
                <li><a id="tipocabelo_7" href=""></a></li>
                <li><a id="tipocabelo_8" href=""></a></li>
            </ul>
        </li>
        <li class="membro"><a href="olhos" class="seleciona_membro">OLHOS</a>
        	<ul class="seleciona_cor" id="corolhos">
            	<li class="ativo"><a id="corolhos_1" href=""></a></li>
                <li><a id="corolhos_2" href=""></a></li>
                <li><a id="corolhos_3" href=""></a></li>
                <li><a id="corolhos_4" href=""></a></li>
            </ul>
            <ul class="seleciona_tipo" id="tipoolhos">
            	<li class="ativo"><a id="tipoolhos_1" href=""></a></li>
                <li><a id="tipoolhos_2" href=""></a></li>
                <li><a id="tipoolhos_3" href=""></a></li>
                <li><a id="tipoolhos_4" href=""></a></li>
            </ul>
        </li>
        <li class="membro"><a href="sombrancelhas" class="seleciona_membro">SOBRANCELHAS</a>
        	<ul class="seleciona_cor" id="corsombrancelhas">
            	<li class="ativo"><a id="corsombrancelhas_1" href=""></a></li>
                <li><a id="corsombrancelhas_2" href=""></a></li>
                <li><a id="corsombrancelhas_3" href=""></a></li>
                <li><a id="corsombrancelhas_4" href=""></a></li>
            </ul>
            <ul class="seleciona_tipo" id="tiposombrancelhas">
            	<li class="ativo"><a id="tiposombrancelhas_1" href=""></a></li>
                <li><a id="tiposombrancelhas_2" href=""></a></li>
                <li><a id="tiposombrancelhas_3" href=""></a></li>
                <li><a id="tiposombrancelhas_4" href=""></a></li>
            </ul>
        </li>
        <li class="membro">
        	<a href="nariz" class="seleciona_membro">NARIZ</a>
            <ul class="seleciona_tipo" id="tiponariz">
            	<li class="ativo"><a id="tiponariz_1" href=""></a></li>
                <li><a id="tiponariz_2" href=""></a></li>
                <li><a id="tiponariz_3" href=""></a></li>
                <li><a id="tiponariz_4" href=""></a></li>
            </ul>
        </li>
        <li class="membro">
        	<a href="oculos" class="seleciona_membro">ÓCULOS</a>
            <ul class="seleciona_tipo" id="tipooculos">
            	<li class="ativo"><a id="tipooculos_0" href=""></a></li>
                <li><a id="tipooculos_1" href=""></a></li>
                <li><a id="tipooculos_2" href=""></a></li>
                <li><a id="tipooculos_3" href=""></a></li>
            </ul>
        </li>        
        <li class="membro">
        	<a href="boca" class="seleciona_membro">BOCA</a>
            <ul class="seleciona_tipo" id="tipoboca">
            	<li class="ativo"><a id="tipoboca_1" href=""></a></li>
                <li><a id="tipoboca_2" href=""></a></li>
                <li><a id="tipoboca_3" href=""></a></li>
            </ul>
        </li>
        <li class="membro">
        	<a href="barba" class="seleciona_membro">BARBA</a>
            <ul class="seleciona_tipo" id="tipobarba">
            	<li class="ativo"><a id="tipobarba_0" href=""></a></li>
                <li><a id="tipobarba_1" href=""></a></li>
                <li><a id="tipobarba_2" href=""></a></li>
                <li><a id="tipobarba_3" href=""></a></li>
                <li><a id="tipobarba_4" href=""></a></li>
                <li><a id="tipobarba_5" href=""></a></li>
            </ul>
        </li>
        <li class="membro">
        	<a href="look" class="seleciona_membro">ROUPA</a>
            <ul class="seleciona_tipo" id="tipolook">
            	<li><a id="tipolook_1" href=""></a></li>
                <li><a id="tipolook_2" href=""></a></li>
                <li><a id="tipolook_3" href=""></a></li>
                <li><a id="tipolook_4" href=""></a></li>
                <!--<li><a id="tipolook_5" href=""></a></li>-->
                <li><a id="tipolook_6" href=""></a></li>
                <li><a id="tipolook_7" href=""></a></li>
                <li><a id="tipolook_8" href=""></a></li>
                <li><a id="tipolook_9" href=""></a></li>
                <li><a id="tipolook_10" href=""></a></li>
                <li><a id="tipolook_11" href=""></a></li>
                <li class="ativo"><a id="tipolook_12" href=""></a></li>
            </ul>
        </li>
    </ul>
    
    <div id="seletores">
    	<a href="" class="anterior"></a>
        <a href="" class="proximo"></a>
    </div>
    
    <div id="btns_dir">
    	<span>Divulgue seu pai na internet</span>
        <ul class="redes_sociais">
        	<li><a href="" class="btn_twitter" title="Twitter" target="_blank"><img src="images/btn_twitter.jpg" alt="twitter" /></a></li>
            <li><a href="" class="btn_facebook" title="Facebook" target="_blank"><img src="images/btn_facebook.jpg" alt="Facebook" /></a></li>
            <li><a href="" class="btn_google" title="Google +" target="_blank"><img src="images/btn_google.jpg" alt="Google +" /></a></li>
        </ul>
        <span class="quer_imprimir">Quer imprimir o seu pai?</span>
        <a href="" class="btn_imprimir" target="_blank"><img src="images/btn_imprimir.jpg" alt="Imprimir" /></a>
        <a href="#pai_montado" id="btn_encontrar_presente">Encontrar Presente!</a>
    </div>

	<div class="avatar">
    	<ul class="corpo">
        	<li class="tipo_corpo">
            	<ul class="cor_corpo">
                	<li></li>
                </ul>
                <ul class="looks">
                	<li></li>
                </ul>
                <ul class="cabeca">
                	<li></li>
                </ul>
                <ul class="barba">
                	<li></li>
                </ul>
                <ul class="boca">
                	<li></li>
                </ul>     
                <ul class="nariz">
                	<li></li>
                </ul>
                <ul class="olhos">
                	<li></li>
                </ul>
                <ul class="sombrancelha">
                	<li></li>
                </ul>
                <ul class="oculos">
                	<li></li>
                </ul>
                <ul class="cabelo">
                	<li></li>
                </ul>
            </li>
        </ul>
    </div>
    
    
    
</div>

<div class="app-giftFinder">
    <div class="topo_monte">
        <div class="ttl_gift_finder">GIFT FINDER</div>
        <div class="subttl_gift_finder">Agora que já sabemos qual é a cara do seu pai,<br />vamos ver se conseguimos acertar o que é a cara dele.</div>
        <input type="hidden" name="tag" id="tag" />
    </div>

	<div id="pai_montado"></div>
    <div class="coracaozinho"></div>
    <div class="canvas-product"></div>
    <div class="box_buttons">
        <div class="change-tag button">Não! Nada a ver com ele.</div>
        <div class="change-product button">Hum, quase lá</div>
        <div class="see-product button">É isso mesmo que queria!</div>
    </div>
</div><!--.app-giftFinder-->

</body>
</html>
