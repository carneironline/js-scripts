// JavaScript Document

$(document).ready(function(e) {
	url_exibicao = 'http://json.monstra.gr/c/submarino/2014/diadospais/pai.php';
	title_redes = 'Dia%20dos%20Pais%20Submarino'
	summary_redes = 'Confira%20como%20ficou%20meu%20pai';
	listaCombinacoes = {
		tipolook_1:'sl-ternogravatasapato',
		tipolook_2:'sl-bermudaregatameias',
		tipolook_3:'sl-social',
		tipolook_4:'sl-havaiana',
		tipolook_5:'sl-havaiana',
		tipolook_6:'sl-roupadecorrer',
		tipolook_7:'sl-sweatercolete',
		tipolook_8:'sl-camisaxadrez',
		tipolook_9:'sl-blusapolobermuda',
		tipolook_10:'sl-moletomcamisavelha',
		tipolook_11:'sl-camisetajeanstenis',
		tipolook_12:'sl-bermudaregatameias'
	}
	formatoImagens = {
		cor_corpo:'tipocorpo_corcorpo',
		looks:'tipocorpo_tipolook',
		cabeca:'corcorpo_tipocabeca',
		barba:'corcorpo_tipocabeca_tipobarba',
		boca:'corcorpo_tipoboca',
		nariz:'corcorpo_tiponariz',
		olhos:'tipoolhos_corolhos',
		sombrancelha:'tiposombrancelhas_corsombrancelhas',
		oculos:'tipocabeca_tipooculos',
		cabelo:'tipocabeca_tipocabelo_corcabelo'
	};
	
	nova_url_exibicao = '';
	
	function monta_avatar(){
		nova_url_exibicao = url_exibicao+'?&avatar=';
		novoavatar = {};	
		$('.seleciona_tipo, .seleciona_cor').each(function(index, element){
			qual = $(this).attr('id');
			valor =  $(this).find('.ativo').find('a').attr('id').split('_')[1];
			novoavatar[qual]=valor;
			nova_url_exibicao = nova_url_exibicao+valor+'_';
		});
		$('#btns_dir .btn_imprimir').attr('href',nova_url_exibicao+'&imprimir=pagina');
		nova_url_exibicao = nova_url_exibicao.replace('?','%3F').replace('&','%26').replace('=','%3D');
		nova_url_exibicao = nova_url_exibicao.replace(':','%3A').replace('/','%2F').replace('/','%2F').replace('/','%2F').replace('/','%2F').replace('/','%2F').replace('/','%2F');
		url_facebook = 'http://www.facebook.com/sharer.php?s=100&p[url]='+nova_url_exibicao+'&p[title]='+title_redes+'&p[summary]='+summary_redes;
		url_twitter = 'https://twitter.com/home?status='+summary_redes+'%20'+nova_url_exibicao;
		url_google = 'https://plus.google.com/share?url='+nova_url_exibicao;
		
		$('#btns_dir .btn_facebook').attr('href',url_facebook);
		$('#btns_dir .btn_twitter').attr('href',url_twitter);
		$('#btns_dir .btn_google').attr('href',url_google);
		
		$.each(formatoImagens, function(key,val){
			sem = false;
			largura = $('.'+key+' li').width();
			altura = $('.'+key+' li').height();
			mudax = 0;
			muday = 0;
			forma = val.split('_');	
			for (var i = 0; i < forma.length; ++i){
				if(key=='barba'){
					if(i==0){
						muday = (muday+novoavatar[forma[i]]-1)*altura*4;
					}
					if(i==1){
						muday = muday+((novoavatar[forma[i]]-1)*altura);
					}
					if(i==2){
						mudax = (mudax+novoavatar[forma[i]]-1)*largura;
					}
				}else if(key=='cabelo'){
					if(i==0){
						muday = (muday+novoavatar[forma[i]]-1)*altura*8;
					}
					if(i==1){
						muday = muday+((novoavatar[forma[i]]-1)*altura);
					}
					if(i==2){
						mudax = (mudax+novoavatar[forma[i]]-1)*largura;
					}
				}else{
					if(i==0){
						muday = (muday+novoavatar[forma[i]]-1)*altura;
					}
					if(i==1){
						mudax = (mudax+novoavatar[forma[i]]-1)*largura;
					}					
				}
				if(novoavatar[forma[i]]=='0'){
					sem = true;
				}
				if(forma[i]=='tipolook'){
					//$('#btns_dir #btn_encontrar_presente').attr('href',listaCombinacoes[forma[i]+'_'+novoavatar[forma[i]]])
					$('#tag').val(listaCombinacoes[forma[i]+'_'+novoavatar[forma[i]]]);
				}
			}
			antigo = $('.'+key+' li').css('background-position');
			novo = '-'+mudax+'px -'+muday+'px';
			if(novo!=antigo){
				$('.'+key+' li').css('background-position',novo);
				if(sem == true){								
					$('.'+key+' li').hide();
				}else{
					$('.'+key+' li').show();
				}
			}
		});
	}
	
	monta_avatar();

	$(document).on('click','#menu .seleciona_membro',function(){
		$('#menu .membro').removeClass('ativo');
		$(this).parent().addClass('ativo');
		return false;
	});
	
	$(document).on('click','#menu .seleciona_cor a',function(){
		$(this).parent().parent().find('.ativo').removeClass('ativo');
		$(this).parent().addClass('ativo');
		monta_avatar();
		return false;
	});
	
	$(document).on('click','#seletores a',function(){
		total = $('#menu .membro.ativo .seleciona_tipo li').size();
		atual = $('#menu .membro.ativo .seleciona_tipo .ativo').index()+1;
		if($(this).hasClass('proximo')){
			novo = atual+1;
		}else{
			novo = atual-1;
		}
		if(novo<1){
			novo=total;
		}
		if(novo>total){
			novo=1;
		}
		$('#menu .membro.ativo .seleciona_tipo .ativo').removeClass('ativo');
		$('#menu .membro.ativo .seleciona_tipo li:nth-child('+novo+')').addClass('ativo');
		monta_avatar();
		return false;
	});
	
	$(document).on('click','#btn_encontrar_presente',function(){
		avatar = $('#monte_seu_pai .avatar').clone();
		$('#pai_montado').html(avatar);
		valor = $('#tag').val();
        $.getScript('http://apps.submarino.com.br/media/global/plugins/jquery.giftFinder/jquery.giftFinder.js', function(){
            giftFinder({
                tag: valor,
                tagsList: ['sl-ternogravatasapato', 'sl-bermudaregatameias', 'sl-social', 'sl-havaiana', 'sl-camisaxadrez', 'sl-blusapolobermuda', 'sl-roupadecorrer', 'sl-sweatercolete', 'sl-moletomcamisavelha', 'sl-camisetajeanstenis']
            });
            $('.app-giftFinder').slideDown('slow');
        });
	});
	
	altura = $('#monte_seu_pai').height();
	largura = $('#monte_seu_pai').width();
	$('#divpreloader_avatar').height(altura).width(largura);
	
});

$(window).load(function(){
	 $('#divpreloader_avatar').fadeOut(1000);
});