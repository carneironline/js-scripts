var giftFinder =(function() {

    var app = {};
    var default_options = {
        canvasProduct: '.canvas-product',
        changeTag: '.change-tag',
        changeProduct: '.change-product'
    }
    var currentTag = 0;
    var currentProduct = 0;
    var productsList;

    // Inicializa a API
    app.init = function(){
        if(typeof arguments[0].tagsList != 'undefined'){
            params = { tagsList: arguments[0].tagsList }
            params['tag'] = (typeof arguments[0].tag != 'undefined') ? arguments[0].tag : false;

            app.createGF();
            app.actionsGF();
        }
    }

    // Cria um shape com base na imagem da silhueta, podendo alterar a sua cor
    app.createGF = function(){
        var action = (typeof arguments[0] == 'undefined') ? false  : arguments[0];

        $('.app-giftFinder').each(function(){

            var tag = (params['tag']) ? params['tag'] : params.tagsList[currentTag];

            if(action == 'changeTag'){
                currentTag = Math.floor((Math.random() * params.tagsList.length) + 0);
                tag = params.tagsList[currentTag];
            }

            getTagData(tag, function(data){
                productsList = data.products;

                currentProduct = Math.floor((Math.random() * productsList.length) + 0);

                app.createHTML( productsList[currentProduct]);

            });
        });
    }

    // Gera o html do banner
    app.createHTML = function(objProd) {
        var $html = '';

        $html += '<div class="gf-image"><img src="'+objProd.image+'" title="'+objProd.name+'" /></div>';
        $html += '<div class="gf-name">'+objProd.name+'</div>';
        $html += '<div class="gf-price">'+objProd.sales_price+'</div>';

        //Insere o html gerado no content do fullbanner
        $(default_options.canvasProduct).html($html);

        $(default_options.canvasProduct).attr('data-url', objProd.url)
    }

    function getTagData(tagName, callback){

        $.ajax({
            url: 'http://www.'+app.checkBrand()+'.com.br/productinfo_bytag?tag='+tagName+'&callback=?',
            jsonpCallback: 'appGF' + '_' + Math.floor((Math.random() * 10000) + 1),
            cache: true,
            dataType: 'jsonp',
            success: function(json){
                callback(json);
            },
            complete: function(a, b){
                /*if(b == 'parsererror'){
                    if(testing<2){
                        getTagData(paramTag, arrObj, callback, index, testing+1);
                    }
                    else if(index < paramTag.length-1) {
                        getTagData(paramTag, arrObj, callback, index+1, 0);
                    } else {
                        callback(arrObj);
                    }
                }*/
            }
        });
    }

    app.checkBrand = function() {
        str = location.href;

        if (str.search("americanas") > -1) {
            return 'americanas';
        }
        else if (str.search("submarino") > -1) {
            return 'submarino';
        }
        else if (str.search("shoptime") > -1) {
            return 'shoptime';
        }
        else if (str.search("soubarato") > -1) {
            return 'soubarato';
        }
        else {
            return 'submarino';
        }
    }

    app.actionsGF = function(){
        $(default_options.changeTag).on('click', function(){
            app.createGF('changeTag');
        });

        $(default_options.changeProduct).on('click', function(){
            currentProduct = Math.floor((Math.random() * productsList.length) + 0)
            app.createHTML( productsList[currentProduct]);
        });

        $(document).on('click', '.see-product', function(){
            $('.canvas-product').removeData('url');
            window.open($('.canvas-product').data('url'));
        });
    }

    return app.init;

})();

