<?php
include_once('shop-hs-tpl/header.php');

//define('BASEURL', 'http://o.apps.americanas.com.br/media/global/hotsite/hs-torcer-para-ganhar-shop-sony/');
//define('BASEURL', 'http://img.shoptime.com.br/mktshop/hotsites/copa-do-mundo-sony/shop/');
define('BASEURL', '');
define('URL_RULES', 'http://apps.shoptime.com.br/media/global/hotsite/hs-torcer-para-ganhar-acom-sony/regulamento-sony.html');
?>

<!--SCRIPTS HS-->
<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>css/style.css" />

<!--FANCYBOX2-->
<!--<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
<script type="text/javascript" src="js/jquery.fancybox.js"></script>-->

<script type="text/javascript" src="<?php echo BASEURL; ?>js/scripts.js"></script>
<!--END SCRIPTS HS-->

<!--HS BEGIN-->
<div id="hs-torcer-para-ganhar">
	
    <div class="topo">&nbsp;</div><!--.topo-->
    
    <div class="nav">
    	<div class="content">
    		<ul class="menu">
    			<li><a href="#principal">início</a></li>
    			<li><a href="#cadastre-se">cadastre-se</a></li>
    			<li><a href="#premios">prêmios</a></li>
    			<li><a href="#regulamento">regulamento e FAQ</a></li>
    			<li id="menu-logged"><a href="#logged">Cupons</a></li>
    			<li><a href="#vencedores">vencedores</a></li>
    		</ul>
    		
    		<div class="confira-tickets">
    			<form id="formAuthNav" action="">
	        		<ul>
	        			<li style="padding-top: 2px;">Confira seus tickets</li>
	        			<li><input type="text" placeholder="e-mail" class="text promo-login-email" id="" name="user[email]" /><div class="answer"></div></li>
	        			<li><input type="text" placeholder="cpf" class="text promo-login-cpf" id="" name="user[cpf]" maxlength="14" onkeypress="MascaraCPF(this, event); return numbersonly(this, event);" onblur="ValidarCPF(this);" /></li>
	        			<li><input type="submit" value="entrar" class="submit" /></li>
	        		</ul>
        		</form>
        		<input type="submit" value="logout" class="promo-logout" />
    		</div><!--.confira-tickets-->
    	</div><!--.content-->
    </div><!--.nav-->
    
    <div class="wrap-content box-principal displayNone">
    	
		<div class="img1"><img src="<?php echo BASEURL; ?>images/promo/page-principal/img1.jpg" /></div>
		
		<div class="img2">
			<h2 class="title2">Como participar?</h2>
			
			<img src="<?php echo BASEURL; ?>images/promo/page-principal/img2.jpg" />
			
			<a href="#cadastre-se" class="btn-clique-participe"></a>
		</div>
		
    </div><!--.wrap-content | box-principal-->
    
    <div class="wrap-content box-cadastre-se displayNone">
    	<h2 class="title">Cadastre-se</h2>
    	
		<form id="formPromoCadastrar" onsubmit="validaFormNiver(this.id); return false;">
			<div class="box-cadastro">
				<div class="col-left">
					<h2>Faça já o seu cadastro para participar.</h2>
					
					<p>Atenção!<br />
					Mesmo que você já seja cadastrado no site do Shoptime, é preciso se cadastrar na promoção preenchendo o formulário ao lado.
					</p>
					
					<p>As informações precisam estar corretas para garantir a entrega dos prêmios.</p>
				</div><!--.col-left-->
				
				<div class="col-right">					
					<div class="left field span2">
						<label for="promo-user-cpf">cpf</label>
						<input type="text" id="promo-user-cpf" class="text" maxlength="14" onkeypress="MascaraCPF(this, event); return numbersonly(this, event);" onblur="ValidarCPF(this);" />
					</div>
					
					<div class="right field span2">
						<label for="promo-user-rg">rg</label>
						<input type="text" id="promo-user-rg" class="text" onkeypress="return numbersonly(this, event);" />
					</div>
					
					<div class="clear"></div>
					
					<div class="field span1">
						<label for="promo-user-nome">nome completo</label>
						<input type="text" id="promo-user-nome" class="text" />
					</div>
					
					<div class="left field span3">
						<label for="promo-user-cep">cep</label>
						<input type="text" id="promo-user-cep" class="text" onkeypress="$validacao.maskme(this, '########'); return $validacao.somenteNumeros(event);" maxlength="8" />
					</div>
					
					<div class="left field span3 marginLeft10">
						<label for="promo-user-telefone">telefone</label>
						<input type="text" id="promo-user-telefone" class="text" onkeypress="$validacao.maskme(this, '## #########'); return $validacao.somenteNumeros(event); " maxlength="12" />
					</div>
					
					<div class="right field span3">
						<label for="promo-user-nascimento">data de nascimento</label>
						<input type="text" id="promo-user-nascimento" class="text" onkeypress="$validacao.maskme(this, '##/##/####'); return $validacao.somenteNumeros(event); " maxlength="10" />
					</div>
					<div class="clear"></div>
					
					
					<div class="left field span2">
						<label for="promo-user-endereco">endereço</label>
						<input type="text" id="promo-user-endereco" class="text" />
					</div>
					
					<div class="right field span2">
						<label for="promo-user-estado">estado</label>
						<input type="text" id="promo-user-estado" class="text" />
					</div>
					<div class="clear"></div>
					
	
					<div class="field span1">
						<label for="promo-user-email">e-mail</label>
						<input type="text" id="promo-user-email" class="text block" />
					</div>
					
					<div class="field span1">
						<label for="promo-user-email-confirm">confirmar e-mail</label>
						<input type="text" id="promo-user-email-confirm" class="text block" />
					</div>
					
					<div class="field marginBottom10">
						<div class="checkbox"><input type="checkbox" id="promo-declaro" /></div> declaro que li e concordo com o regulamento.
						<div class="clear"></div>
					</div>
					
					<div class="field marginBottom10">
						<div class="checkbox"><input type="checkbox" id="promo-receber-emails" /></div> desejo receber e-mails com novidades e ofertas do Shoptime.
						<div class="clear"></div>
					</div>
					
					<input type="hidden" id="raffleName" value="hs-torcer-para-ganhar-shop-sony" />
					
					<input type="submit" value="enviar" id="submit" class="submit" />
					
					<div class="answer"></div>
					
				</div><!--.col-right-->
				<div class="clear"></div>
			</div><!--.box-cadastro-->
		</form>
		
    </div><!--.wrap-content | box-cadastre-se-->
    
    <div class="wrap-content box-premios displayNone">
    	<h2 class="title">Prêmios</h2>
		
		<img src="<?php echo BASEURL; ?>images/promo/page-premios/premios-texto.gif" />
		
		<img src="<?php echo BASEURL; ?>images/promo/page-premios/premios-bg.jpg" class="premios" />
    </div><!--.wrap-content | premios-->
    
    <div class="wrap-content box-regulamento displayNone">
    	<h2 class="title">Regulamento</h2>
    	
    	<a href="<?php echo URL_RULES; ?>" class="btn-regulamento fancybox" target="_blank">ler o regulamento</a>
          	
    	<h2 class="title faq">FAQ</h2>
    	<p>Leia abaixo, perguntas e respostas mais frequentes.</p>
		
		<div class="accordion">
			<h3>1.	Qual o período de participação da promoção?</h3>
			<div>O período de participação na promoção se inicia a partir das 00h00 (zero hora) do dia 25 de março de 2014 e se estende até às 23h59m59s (vinte e três horas cinquenta e nove minutos e cinquenta e nove segundos) do dia 19 de abril de 2014, horário oficial de Brasília. </div>
			
			<h3>2.	Preciso realizar o cadastro para participar da promoção?</h3>
			<div>
				<p>Sim, para concorrer aos  prêmios do sorteio é necessário realizar o cadastro no site da Promoção do  Shoptime.com <a href="http://www.shoptime.com.br/copa-do-mundo-sony">www.shoptime.com.br/copa-do-mundo-sony</a>, mesmo já tendo cadastro  no site da loja online. </p>				
			</div>
			
			<h3>3.	Quantos e quais são os prêmios?</h3>
			<div>
					<p>Serão  distribuídos pela Promoção 5 (cinco) prêmios, conforme discriminados na tabela  a seguir: <strong></strong></p>
	<table border="1" cellspacing="0" cellpadding="0">
	  <tr>
	    <td width="359"><br>
	        <strong>Descrição do prêmio</strong> </td>
	    <td width="105"><p align="center"><strong>Quantidade    de prêmios</strong></p></td>
	    <td width="120"><p align="center"><strong>Valor    unitário (R$)</strong></p></td>
	  </tr>
	  <tr>
	    <td width="359"><p>1 (um) pacote de    viagem com passagens áreas e hospedagem para a cidade do Rio de Janeiro com 2    (dois) ingressos, destinado ao ganhador e um acompanhante indicado por ele    para camarote com alimentação inclusa para o <strong>JOGO FINAL</strong> da Copa do Mundo da FIFA Brasil 2014 destinados aos    participantes que comprarem no <strong>SHOPTIME.COM</strong>.</p></td>
	    <td width="105"><p align="center">05 (cinco)</p></td>
	    <td width="120"><p>Ingresso:    R$ 352,00<br>
	      Pacote:    R$ 8171,00 </p></td>
	  </tr>
	  <tr>
	    <td width="359"><p align="center"><strong>VALOR TOTAL DOS PRÊMIOS (R$)</strong></p></td>
	    <td width="225" colspan="2"><p align="center">R$ 42.615,00 (quarenta e dois mil e    seiscentos e quinze reais) <strong> </strong></p></td>
	  </tr>
	</table>

			</div>
			
			<h3>4.	Quem pode participar?</h3>
			<div>
				<p>Participam  do sorteio desta Promoção apenas as pessoas físicas maiores de 13 (treze) anos,  residentes e domiciliadas no Brasil que, ao longo do período de participação,  comprarem produtos SONY na loja on-line do grupo B2W, Shoptime.com e que  cumpram as demais condições do Regulamento.<br>
  Aos  participantes com idade entre 13 (treze) e 17 (dezessete) anos, recomenda-se o  acompanhamento dos pais ou responsáveis legais. </p>

			</div>
			
			<h3>5.	Como participar?</h3>
			<div>
				<p>Para concorrer aos prêmios, o participante deverá  cadastrar-se na promoção e adquirir os produtos SONY na loja on-line Shoptime.com,  ao longo do período de participação.<br>
  A cada R$ 399,00 (trezentos e noventa e nove reais)  adquiridos em produtos SONY o participante cadastrado receberá 1 (um) elemento  sorteável depois de validada a sua participação. Considera-se a participação  validada após a aprovação do pagamento do pedido. <br>
  Serão considerados apenas os pedidos realizados por  pessoas físicas.<br>
  Apenas participarão da promoção os pedidos que forem  aprovados integralmente até 23h59m59s (vinte e três horas, cinquenta e nove  minutos e cinquenta e nove segundos) do dia 2 de maio de 2014, horário de  Brasília e que forem realizados durante o período de participação.<br>
  Quanto mais compras realizadas cujos dados pessoais do  participante forem devidamente cadastrados no site da promoção, mais chances o  participante terá.</p>

			</div>
			
			<h3>6.	Como me cadastrar?</h3>
			<div>
				<p>Para cadastrar-se no site da Promoção, o participante  deverá acessar o site da promoção <a href="http://www.shoptime.com.br/copa-do-mundo-sony">www.shoptime.com.br/copa-do-mundo-sony</a> e realizar o cadastro preenchendo correta e  integralmente os dados solicitados (nome completo, CPF, RG, data de nascimento,  endereço completo, e-mail e telefone celular). Para finalizar o cadastro o  participante deverá, ainda, aceitar os termos do Regulamento da presente  Promoção.</p>

			</div>
			
			<h3>7.	O que são elementos sorteáveis?</h3>
			<div>
				<p>Os elementos sorteáveis são os números utilizados para  a escolha dos vencedores. Os prêmios serão atribuídos aos portadores dos elementos  sorteáveis extraídos da Loteria Federal. <br>
  Serão considerados vencedores todos os participantes,  dentre as 05 (cinco) séries distribuídas – 50 a 54, que possuírem elementos  sorteáveis cujo número da sorte seja igual à dezena simples dos 05 (cinco) primeiros  prêmios da Loteria Federal lidas de cima para baixo.<br>
  Supondo que numa extração hipotética da Loteria Federal,  sejam extraídos os seguintes números:<br>
  <strong>1º Prêmio                  6 1. 3 1 6</strong><br>
  <strong>2º Prêmio                  1 2. 5 9  4</strong><br>
  <strong>3º Prêmio                  1 3. 7 7 0</strong><br>
  <strong>4º Prêmio                  7 1. 6 7 8</strong><br>
  <strong>5º Prêmio                   2  5. 7 1 5</strong><br>
  Neste caso, serão contemplados os participantes que  tiverem os elementos sorteáveis em todas as 05 (cinco) séries cujos números da  sorte sejam iguais a <strong>19771</strong>.</p>

			</div>
			
			<h3>8.	O que preciso fazer para ganhar os números para o sorteio?</h3>
			<div>Já cadastrado na promoção, indicando o prêmio, e uma vez efetuadas compras com o mesmo CPF utilizado no cadastro, a Promotora efetua o cruzamento das informações e o participante passa a concorrer automaticamente aos prêmios, desde que respeitados os termos do Regulamento. Não é necessário nenhum passo adicional.</div>
			
			<h3>9.	Como meus números de pedidos são transformados em elementos sorteáveis?</h3>
			<div>
				<p>A cada compra efetuada na  loja online Shoptime.com, o cliente receberá um número de pedido. Esse número  será transformado em números para o sorteio da seguinte forma: <br>
  Exemplos:<br>
  1 (um) Número de Pedido  de R$ 399,00 (trezentos e noventa e nove reais) em produtos participantes = 1  (um) cupom. <br>
  1 (um) Número de Pedido  de R$ 798,00 (setecentos e noventa e oito reais) em produtos participantes = 2  (dois) cupons. <br>
  1 (um) Número de Pedido  de R$ 898,00 (oitocentos e noventa e oito reais) = 2 cupons. <strong>O valor de R$ 100,00 não permanece como  saldo para outra utilização</strong>.<br>
  Não serão utilizados para  obtenção de cupons os valores do número do pedido que não se referirem a  produtos, por exemplo, valores de garantia estendida.<br>
  O participante pode  utilizar o mesmo Número de Pedido apenas 1 (uma) vez nessa Promoção. </p>

			</div>
			
			<h3>10.	Os ingressos valem para quais jogos?</h3>
			<div>Serão sorteados 05 (cinco) pares de ingressos para assistir o jogo final da Copa do Mundo FIFA 2014 na cidade do Rio de Janeiro.</div>
			
			<h3>11.	Onde consulto meus números da sorte?</h3>
			<div>
				<p>Os  elementos sorteáveis serão distribuídos e divulgados a você em seu extrato  pessoal no site da promoção <a href="http://www.shoptime.com.br/copa-do-mundo-sony">www.shoptime.com.br/copa-do-mundo-sony</a> até um dia antes do sorteio, ou seja, dia 02 de maio de  2014. <strong> </strong></p>

			</div>
			
			<h3>12.	Quando será realizado o sorteio?</h3>
			<div>O sorteio acontecerá em 03/05/2014 e serão sorteados 5 ganhadores, cada um vai receber par de ingresso para o jogo final da Copa do Mundo 2014.</div>
			
			<h3>13.	Como saberei se sou um dos sorteados?</h3>
			<div>
				<p>O  resultado da Promoção e os nomes dos contemplados serão divulgados no site da  promoção <a href="http://www.shoptime.com/copa-do-mundo-sony">www.shoptime.com.br/copa-do-mundo-sony</a>. em até 10  (dez) dias após a apuração.<br>
  Os  contemplados serão comunicados do resultado da promoção pelo envio  preferencialmente de e-mail e subsidiariamente de telegrama ou carta com aviso  de recebimento (AR), bem como por contato telefônico, no prazo máximo de 05  (cinco) dias da data da apuração, de acordo com os dados cadastrais mantidos  pela Promotora.</p>

			</div>
			
			<h3>14.	Quais documentos serão necessários para receber meu prêmio?</h3>
			<div>
				<p>A Promotora entregará uma carta-compromisso para que  você assine. Será necessário apresentar também o número do pedido com o qual  você participou na promoção. <br>
  Além disso, podem ser solicitados documentos  requeridos pela FIFA, bem como qualquer informação adicional, dentro dos  limites da lei.<br>
  Por  fim, para retirada dos ingressos, será necessário apresentar um documento de  identidade válido e com fotografia.<br>
  Na eventualidade de o participante ganhador ser  menor de 18 (dezoito) anos, o seu responsável legal deverá assinar a  carta-compromisso, a qual deverá ser preenchida com os dados do ganhador, e  retirar o prêmio, entregue em nome do menor e, para tanto, deverá comprovar tal  condição mediante a apresentação de documento.</p>

			</div>
			
			<h3>15.	Onde retiro meus ingressos ?</h3>
			<div>Se você for um dos ganhadores da promoção, receberá todas as informações de procedimento para recebimento do seu prêmio.</div>
			
			<h3>16.	Posso trocar os ingressos por dinheiro?</h3>
			<div>Não, os prêmios não podem ser trocados por dinheiro.</div>
			
			<h3>17.	Não moro do Brasil. Posso participar?</h3>
			<div>Não, a promoção é exclusiva para pessoas residentes e domiciliadas no Brasil.</div>
			
			<h3>18.	Posso ganhar mais de uma vez?</h3>
			<div>Sim, se você for sorteado. </div>
			
			<h3>19.	Posso dar os ingressos como presente?</h3>
			<div>Apenas o ganhador ou pessoa que tenha procuração para tanto poderá retirar os ingressos. Depois de retirado, você poderá entrega-los para outra pessoa utilizar, sendo certo que após a retirada dos ingressos encerra-se a responsabilidade da Promotora e das Aderentes quanto à utilização deles. </div>
			
			<h3>20.	Existe limite de cupons?</h3>
			<div>Não, você pode realizar quantas compras quiser ao longo do período de participação e ganhará um elemento sorteável para cada R$ 399,00 em compras.</div>
			
			<h3>21.	Posso cadastrar na promoção com mais de um e mail?</h3>
			<div>Não, você deve se cadastrar apenas um com um e-mail e CPF.</div>
			
			<h3>22.	O transporte e a hospedagem estão inclusos no prêmio?</h3>
			<div>
				<p>A Promotora será responsável pelo  transporte da cidade de domicílio para a cidade onde o jogo da Copa do Mundo  FIFA 2014 será realizado.<br>
  Não fazem jus ao pacote de viagem os  moradores dos locais onde se realizar os jogos para os quais os participantes  foram premiados. Os ganhadores dos jogos a se realizarem às 13 horas, retornarão  para suas cidades no mesmo dia do jogo, pois há disponibilidade de horários de  voos e não terão direito à hospedagem. <br>
  Já os ganhadores dos jogos a se  realizarem às 16h, 17h, 18h e 22h terão direito ao pernoite, pois, devido ao  horário do jogo, não há mais voo disponíveis para retorno no mesmo dia. <br>
  Não fazem jus a passagens aéreas os  ganhadores cujos domicílios estejam a 200 km ou menos do estádio. Nesses casos,  o transporte será terrestre. </p>

			</div>
			
			<h3 class="doubleLine">23.	Como posso ter mais informações sobre o processo de auditoria e funcionamento da mecânica da promoção de forma <br />que eu possa me assegurar que tudo o que o regulamento informa é verídico?  </h3>
			<div>
				<p>A Promotora e  Aderentes zelam pela regularidade da mecânica de suas promoções mediante obtenção  de autorização expedida pela Caixa Econômica Federal, bem como pela contratação  de empresa de auditoria independente, desde o início de todas as suas  promoções.<br>
  São  fiscalizadas todas as etapas da promoção, como distribuição dos elementos  sorteáveis, apuração, identificação dos ganhadores etc. <br>
  Dúvidas  podem ser esclarecidas no site da promoção. </p>

			</div>
			
		</div><!--.accordion-->
		
    </div><!--.wrap-content | premios-->
    
    <div class="wrap-content box-vencedores">
    	<h2 class="title">Vencedores</h2>
    	
    	<ul class="list-vencedores">
    		<li>
    		    <div>
                    <span class="tipoingresso">TIPO DE INGRESSO:</span>
                    <span class="title">Final da Copa</span>
                    <span class="divisoria">/</span>
                    <span class="ordem">N. DE ORDEM: 86053</span>
                    <span class="divisoria">/</span>
                    <span class="serie">N. DE SÉRIE: 54</span>
    			</div>
    		</li>
    		<li>
                <div>
                    <span class="tipoingresso">TIPO DE INGRESSO:</span>
                    <span class="title">Final da Copa</span>
                    <span class="divisoria">/</span>
                    <span class="ordem">N. DE ORDEM: 79819</span>
                    <span class="divisoria">/</span>
                    <span class="serie">N. DE SÉRIE: 52</span>
                </div>
            </li>
            <li>
                <div>
                    <span class="tipoingresso">TIPO DE INGRESSO:</span>
                    <span class="title">Final da Copa</span>
                    <span class="divisoria">/</span>
                    <span class="ordem">N. DE ORDEM: 80166</span>
                    <span class="divisoria">/</span>
                    <span class="serie">N. DE SÉRIE: 53 </span>
                </div>
            </li>
            <li>
                <div>
                    <span class="tipoingresso">TIPO DE INGRESSO:</span>
                    <span class="title">Final da Copa</span>
                    <span class="divisoria">/</span>
                    <span class="ordem">N. DE ORDEM: 79531</span>
                    <span class="divisoria">/</span>
                    <span class="serie">N. DE SÉRIE: 51</span>
                </div>
            </li>
            <li>
                <div>
                    <span class="tipoingresso">TIPO DE INGRESSO:</span>
                    <span class="title">Final da Copa</span>
                    <span class="divisoria">/</span>
                    <span class="ordem">N. DE ORDEM: 79420</span>
                    <span class="divisoria">/</span>
                    <span class="serie">N. DE SÉRIE: 50</span>
                </div>
            </li>
    	</ul><!--.list-vencedores-->
		
    </div><!--.wrap-content | vencedores-->
    
     <div class="wrap-content box-logged displayNone">
    	<h2 class="title">Olá <span id="promoUserName"></span>!</h2>
    	<input type="hidden" id="hdpf" value="" />
    	
    	<div class="tickets-quant"><span id="tickets-total"></span></div><!--.tickets-quant-->

    	<p class="title">Seus tickets para sorteio:</p>
    	
    	<table id="show-tickets">
    		<thead>
        		<tr>
        			<th class="col1">data do cadastro</th>
        			<th class="col2">código promocional ou número do pedido </th>
        			<th class="col3">ticket do sorteio</th>
        		</tr>
    		</thead>
    		<tbody>
        		
    		</tbody>
    	</table>
    	
    	<div class="aviso"><strong>Importante:</strong> Apenas os pedidos aprovados serão transformados em número da sorte. Nosso sistema precisa de até 24 horas para validar seu código e gerar o ticket do sorteio.</div>
		
		<div class="clear"></div>
		
    </div><!--.wrap-content | vencedores-->
    
    <div class="clear"></div>
    
    <!--<div class="list-publicidade">
		<ul>
			<li><a href=""><img src="<?php echo BASEURL; ?>images/promo/footer/banner-televisao-nova.gif" /></a></li>
		</ul>
		<div class="clear"></div>
    </div><!--.list-publicidade-->
	
	<div class="clear"></div>
    <p style="margin: 60px auto 20px auto; text-align: center;">Período de Participação de 25/03/2014 a 19/04/2014. Certificado de Autorização CAIXA nº 1-0421/2014.</p>
	
</div><!--#hs-vem-pra-festa-->

<!--HS END-->

<?php
include_once('shop-hs-tpl/footer.php');
?>
