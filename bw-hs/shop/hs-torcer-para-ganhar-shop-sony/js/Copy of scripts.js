var TEMPLATE_URL = (location.host == 'localhost') ? '/b2w/Americanas/hotsite/hs-vem-pra-festa/' : 'http://fb.americanas.com.br/portal/hs-vem-pra-festa/'


 onReady(function () {
	  $('.fancybox').fancybox();
    /*
     * Ao clicar nos menus faz a troca de conteúdo.
     */
    $('.menu a').click(function () {
      page = $(this).attr('href').replace('#', '');
      $('.wrap-content').hide();
      $('.box-' + page).fadeIn();
    });

    /*
     * Accordion
     */
    $('.accordion h3').click(function () {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
      } else {
        $(this).addClass('active');
      }

      $(this).next('div').slideToggle();
    });

    /*
     * Exibe os menus ao clicar no logout
     */
    $('.confira-tickets .submit').click(function () {
      userLogin($(this).parent().parent().parent('form'));

      return false;
    });

    /*
     * Exibe os menus ao clicar no logout
     */
    $('.promo-logout').click(function () {
      $('.confira-tickets .answer').html('');
      $('.menu').show();
      $('.confira-tickets form').show();
      $('.btn-chances').show();
      $(this).hide();

      $('.wrap-content').hide();
      $('.box-como-participar').fadeIn();

      $('#promo-login-email').val('');
      $('#promo-login-cpf').val('');
      $('#userName').html('');
      $('#tickets-total').html('');
      $('#show-tickets tbody').empty();

      return false;
    });

    $validacao = new validations();
    $cCupons = new Cupons();
  });


// Valida o user pelo Email e CPF
function userLogin($parent) {
	
  if($parent === 0){
	  var $formID = 'formAuthNav';
	  var $email = $('#promo-user-email').val();
	  var $cpf = clearCPF($('#promo-user-cpf').val());
  } else {
	  var $formID = $parent.attr('id'); 
	  var $email = $('#' + $formID +' .promo-login-email').val();
	  var $cpf = clearCPF($('#' + $formID + ' .promo-login-cpf').val());
  }
  
  var $urlAjax = "http://ws.todomundovai.com.br/usuarios/entrar"

  $tagResposta = '.confira-tickets .answer';

  $($tagResposta).html('enviando...');
  $($tagResposta).show();
  
  /*
   * Se for IE ativa o XDR, senão XHR
   */
  if ($.browser.msie && window.XDomainRequest) 
  {
	  var data = 'user[email]=' + $email + '&user[cpf]=' + $cpf;
			  
      // Use Microsoft XDR
      var xdr = new XDomainRequest();
      xdr.open("post", $urlAjax);
      xdr.send(data);
      xdr.onerror = function()
      {   	  
    	  $($tagResposta).html('No momento não é possível se logar.');
      };
      xdr.onload = function() 
      {
          // XDomainRequest doesn't provide responseXml, so if you need it:
          var dom = new ActiveXObject("Microsoft.XMLDOM");
          //console.log(dom.async)
          dom.async = false;
          dom.loadXML(xdr.responseText);
          
          jsonObj = JSON.parse(xdr.responseText); 
    
          if (jsonObj.authenticated) {
        	    setDataUser($cpf);
				hideMenu();
				
				$($tagResposta).html('Usuário autenticado.');
				
				$('.wrap-content').hide();
				$('.box-logged').fadeIn();
          } else {
	        $($tagResposta).html('Não autenticado.');
	      }
          
          $($tagResposta).fadeIn();
	      setTimeout(function () {
	        $($tagResposta).fadeOut();
	      }, 10000)
      };
      
  } 
  else 
  {  
	  jq182.ajax({
	    type: "POST",
	    url: $urlAjax,
	    data: { user: { email: $email, cpf: $cpf } }
	  }).done(function (data) {
	     
	      //converte o result em dados json
	      jsonObj = data;
	
	      if (jsonObj.authenticated) {
	        setDataUser($cpf);
	        hideMenu();
	
	        $($tagResposta).html('Usuário autenticado.');
	
	        $('.wrap-content').hide();
	        $('.box-logged').fadeIn();
	      } else {
	        $($tagResposta).html('Não autenticado.');
	      }
	
	      $($tagResposta).fadeIn();
	      setTimeout(function () {
	        $($tagResposta).fadeOut();
	      }, 10000)
	
	    });
  }

  return false;
}

/*
 * Esconde o menu principal e o form confira seus ticket
 */
function hideMenu() {
  $('.menu').hide();
  $('.confira-tickets form').hide();
  $('.confira-tickets .promo-logout').show();
  $('.btn-chances').hide();
}

// Retorna o user pelo CPF
function setDataUser($cpf) 
{
	$urlAjax = "http://ws.todomundovai.com.br/usuarios/" + $cpf;

 /*
   * Se for IE ativa o XDR, senão XHR
   */
  if ($.browser.msie && window.XDomainRequest) 
  {			  
      // Use Microsoft XDR
      var xdr = new XDomainRequest();
      xdr.open("get", $urlAjax);
      xdr.send();
      xdr.onload = function() {
          // XDomainRequest doesn't provide responseXml, so if you need it:
          var dom = new ActiveXObject("Microsoft.XMLDOM");
          //console.log(dom.async)
          dom.async = false;
          dom.loadXML(xdr.responseText);
          
          $jsonObj = JSON.parse(xdr.responseText);
    
          $('#hdpf').val($cpf);
	      $('#userName').html($jsonObj.name);
	
	      /*
	       * Instancia o obj Cupons
	       * Altera o total no html
	       * Altera os tickets no html
	       */
	      var cCupons = new Cupons();
	      $('#tickets-total').html(cCupons.getTotalCupons($jsonObj.coupons));
	      $('#show-tickets tbody').html(cCupons.getCupons($jsonObj.coupons));
      };
      
  } 
  else 
  {  
	  jq182.ajax({
	    type: "GET",
	    url: $urlAjax,
	    data: { cpf: $cpf }
	  }).done(function (data) {
	
	      //converte o result em dados json
	      $jsonObj = data;
	      
	      $('#hdpf').val($cpf);
	      $('#userName').html($jsonObj.name);
	      
	      /*
	       * Instancia o obj Cupons
	       * Altera o total no html
	       * Altera os tickets no html
	       */

	      var cCupons = new Cupons();
	      $('#tickets-total').html(cCupons.getTotalCupons($jsonObj.coupons));
	      $('#show-tickets tbody').html(cCupons.getCupons($jsonObj.coupons));
	
	    });
  }

  return false;
}

function Cupons() {
  /*
   * retorna o total de cupons
   */
  this.getTotalCupons = function ($arrCupons) {
    if ($arrCupons) {
      var total = 0;

      for (i = 0; i < $arrCupons.length; i++) {
    	if($arrCupons[i].numbers)
    	{
    		total += $arrCupons[i].numbers.length;
    	}
      }

      return total;
    }
  }

  this.getCupons = function ($arrCupons) {
    if ($arrCupons) {
      var $html = '';

      for (i = 0; i < $arrCupons.length; i++) {
    	if($arrCupons[i].numbers)
      	{
	        for (j = 0; j < $arrCupons[i].numbers.length; j++) {
	          $data = $arrCupons[i].numbers[j].created_at.substring(0, 10).split('-');
	          $data = $data[2] + '/' + $data[1] + '/' + $data[0];
	
	          $html += '<tr>';
	          $html += '<td class="col2">' + $data + '</td>';
	          $html += '<td class="col2">' + $arrCupons[i].promotional_code + '</td>';
	          $html += '<td class="col2">' + $arrCupons[i].numbers[j].content + '</td>';
	          $html += '</tr>';
	        }
      	}
      }

      return $html;
    }
  }

  /*
   * Cadastra um cupom
   */
  this.setCupom = function () {
    var $answer = '';
    var $promotional_code = $('#promo-promotional_code').val();
    var $purchase = $('#promo-purchase').val();
    var $cpf = $('#hdpf').val();
    var $tagResposta = '#hs-vem-pra-festa .cadastrar-compras .answer';

    $($tagResposta).html('Enviando...');
    $($tagResposta).fadeIn();
    
    if($promotional_code == '' || $purchase == '')
    {
    	$answer = 'Insira o código promocional e o valor da compra.';

    	// Escreve os erros no html
        $($tagResposta).html($answer);

        $($tagResposta).fadeIn();
        setTimeout(function () {
          $($tagResposta).fadeOut();
        }, 10000)
    }
    else
    {
    	$urlAjax = "http://ws.todomundovai.com.br/cupons/novo";
    	
    	/*
    	   * Se for IE ativa o XDR, senão XHR
    	   */
    	  if ($.browser.msie && window.XDomainRequest) 
    	  {
    		  var data = 'coupon[promotional_code]=' + $promotional_code + '&coupon[value]=' + $purchase + '&coupon[user_cpf]=' + $cpf;
    				  
    	      // Use Microsoft XDR
    	      var xdr = new XDomainRequest();
    	      xdr.open("post", $urlAjax);
    	      xdr.send(data);
    	      xdr.onerror = function()
    	      {   	  
    	    	  $($tagResposta).html('No momento não é possível se logar.');
    	      };
    	      xdr.onload = function() 
    	      {
    	          // XDomainRequest doesn't provide responseXml, so if you need it:
    	          var dom = new ActiveXObject("Microsoft.XMLDOM");
    	          //console.log(dom.async)
    	          dom.async = false;
    	          dom.loadXML(xdr.responseText);
    	          
    	          $jsonObj = JSON.parse(xdr.responseText); 
    	    
    	          if ($jsonObj.message_from_external_web_service) {
    		          switch ($jsonObj.message_from_external_web_service) {
    		            case 'cupom already generated':
    		              $answer = 'Cupom já cadastrado.';
    		              break;
    		            case 'cupom not found':
    		              $answer = 'Cupom não encontrado.';
    		              break;
    		          }
    		        } else if ($jsonObj.errors) {
    		          switch ($jsonObj.errors[0]) {
    		            case 'Promotional code já está em uso':
    		              $answer = 'Código promocional já cadastrado.';
    		              break;
    		            default:
    		              $answer = 'Preencha o valor da compra.';
    		              break;
    		          }
    		        } else {
    		          $answer = 'Cupom cadastrado.';
    		        }
    		
    		        // Escreve os erros no html
    		        $($tagResposta).html($answer);
    		
    		        $($tagResposta).fadeIn();
    		        setTimeout(function () {
    		          $($tagResposta).fadeOut();
    		        }, 10000)
    		
    		        /*
    		         * Instancia o obj Cupons
    		         * Altera o total no html
    		         * Altera os tickets no html
    		         */
    		        var cCupons = new Cupons();
    		        if (cCupons.getTotalCupons($jsonObj.coupons))
    		          $('#tickets-total').html(parseInt($('#tickets-total').html()) + cCupons.getTotalCupons($jsonObj.coupons));
    		
    		        $('#show-tickets tbody').append(cCupons.getCupons($jsonObj.coupons));
    	      };
    	      
    	  } //END XDR
    	  else 
    	  { 
		    jq182.ajax({
		      type: "POST",
		      url: $urlAjax,
		      data: { coupon: { promotional_code: $promotional_code, value: $purchase, user_cpf: $cpf } }
		    }).done(function (data) {
		        //converte o result em dados json
		        $jsonObj = data;
		
		        if ($jsonObj.message_from_external_web_service) {
		          switch ($jsonObj.message_from_external_web_service) {
		            case 'cupom already generated':
		              $answer = 'Cupom já cadastrado.';
		              break;
		            case 'cupom not found':
		              $answer = 'Cupom não encontrado.';
		              break;
		          }
		        } else if ($jsonObj.errors) {
		          switch ($jsonObj.errors[0]) {
		            case 'Promotional code já está em uso':
		              $answer = 'Código promocional já cadastrado.';
		              break;
		            default:
		              $answer = 'Preencha o valor da compra.';
		              break;
		          }
		        } else {
		          $answer = 'Cupom cadastrado.';
		        }
		
		        // Escreve os erros no html
		        $($tagResposta).html($answer);
		
		        $($tagResposta).fadeIn();
		        setTimeout(function () {
		          $($tagResposta).fadeOut();
		        }, 10000)
		
		        /*
		         * Instancia o obj Cupons
		         * Altera o total no html
		         * Altera os tickets no html
		         */
		        var cCupons = new Cupons();
		        if (cCupons.getTotalCupons($jsonObj.coupons))
		          $('#tickets-total').html(parseInt($('#tickets-total').html()) + cCupons.getTotalCupons($jsonObj.coupons));
		
		        $('#show-tickets tbody').append(cCupons.getCupons($jsonObj.coupons));
		
		      });//End XHR
    	  }
    }
    return false;
  }
}


//VALIDAR FORMS /JQUERY
function validaFormNiver(formID) {
  var formulario = document.getElementById(formID);
  var f = formulario.getElementsByTagName("input");
  var txtArea = formulario.getElementsByTagName("textarea");
  var error = 'PREENCHA OS CAMPOS\n\n';
  var cont = 0;
  var $classError = 'wrong';
  var $tagResposta = '.box-cadastro .answer';
  var $anotherError = [];
  var $htmlAnswer = '';
  var $emailsToCheck = [];
  var $bullet = '&bull; ';
  var $urlAjax = 'http://ws.todomundovai.com.br/usuarios/novo';
  var $userRegistered = 0;

  if (!$('#promo-declaro').is(':checked')) {
    $htmlAnswer = 'Confirme a leitura do regulamento.';
    $($tagResposta).html($htmlAnswer);
    $($tagResposta).fadeIn();
    setTimeout(function () {
      $($tagResposta).fadeOut();
    }, 10000)
    return false;
  }
  /*
   * instancia os objetos
   */
  $validacao = new validations();

  // Faz o loop pelo textareas do form
  for (i = 0; i < txtArea.length; i++) {
    jQuery(txtArea[i]).removeClass($classError);
    if (txtArea[i].value == txtArea[i].defaultValue || txtArea[i].value == '') {
      jQuery(txtArea[i]).addClass($classError);
      error += txtArea[i].id + '\n';
      cont = 1;
    }
  }

  // Faz o loop pelo inputs do form
  for (i = 0; i < f.length; i++) {
    if (f[i].id != 'submit' && f[i].id != 'file-original' && f[i].id != 'file-falso') {
      jQuery(f[i]).removeClass($classError);

      // Verifica o email
      if (f[i].id == 'promo-user-email' || f[i].id == 'promo-user-email-confirm') {
        if (!$validacao.validateEmail(f[i].value)) {
          jQuery(f[i]).addClass($classError);
          $anotherError.push(f[i].id);
          cont = 1;
        } else {
          $emailsToCheck.push(f[i].value);
        }
      }

      // Verifica o cpf
      /*else if (f[i].id == 'promo-user-cpf') {
        if (!$validacao.validateCPF(f[i].value)) {
          jQuery(f[i]).addClass($classError);
          $anotherError.push('cpf');
          cont = 1;
        }
      }*/

      // Verifica os campos vazios
      else if (f[i].value == f[i].defaultValue || f[i].value == '') {
        jQuery(f[i]).addClass($classError);
        error += f[i].id + '\n';
        cont = 1;
      }

    }
  }

  /*
   * Se não houver erros, será executado o script abaixo
   */
  if (cont == 0) {
    $($tagResposta).html('Enviando...');
    $($tagResposta).fadeIn();

    $recebeEmails = ($('#promo-receber-emails').is(':checked')) ? true : false;
    
    /*
     * Se for IE ativa o XDR, senão XHR
     */
    if ($.browser.msie && window.XDomainRequest) 
    {    
  	  var data = 'user[name]=' + $('#promo-user-nome').val() 
  	  			 + '&user[cpf]=' + clearCPF($('#promo-user-cpf').val()) 
  	  			 + '&user[address]=' + $('#promo-user-endereco').val() 
  	  			 + '&user[state]=' + $('#promo-user-estado').val() 
  	  			 + '&user[cep]=' + $('#promo-user-cep').val() 
  	  			 + '&user[home_phone_number]=' + $('#promo-user-telefone').val() 
  	  			 + '&user[mobile_phone_number]=' + $('#promo-user-celular').val() 
  	  			 + '&user[email]=' + $('#promo-user-email').val() 
  	  			 + '&user[accept_receive_emails]=' + $recebeEmails;
  			  
        // Use Microsoft XDR
        var xdr = new XDomainRequest();
        xdr.open("post", $urlAjax);
        xdr.send(data);
        xdr.onerror = function()
        {   	  
        	var $answer = 'CPF inválido ou usuário já existente';
	    	  $($tagResposta).html('Enviando...');
	    	  $($tagResposta).fadeIn(); 
	    	  // Escreve os erros no html
			    $($tagResposta).html($answer);
			
			    $($tagResposta).fadeIn();
			    setTimeout(function () {
			      $($tagResposta).fadeOut();
			    }, 10000)
        };
        xdr.onload = function() 
        {
            // XDomainRequest doesn't provide responseXml, so if you need it:
            var dom = new ActiveXObject("Microsoft.XMLDOM");
            //console.log(dom.async)
            dom.async = false;
            dom.loadXML(xdr.responseText);
            
            $jsonObj = JSON.parse(xdr.responseText); 
      
            var $answer = '';
	
	        $($tagResposta).fadeOut();
	
	        // add os erros na variável
	        if ($jsonObj.errors) {
	          $.each($jsonObj.errors, function (i, item) {
	            $.each(item, function (subi, subitem) {
	              if ($answer.search(subitem) < 0) {
	                $answer += $bullet + subitem + ' <br />';
	              }
	            });
	          });
	        } else {
	          $answer += $bullet + 'Usuário cadastrado com sucesso.<br />';
	        }
	        
	        // Escreve os erros no html
	        $($tagResposta).html($answer);
	
	        $($tagResposta).fadeIn();
	        
	        if($userRegistered) {
	        	setTimeout(function () {
		        	$($tagResposta).html('Logando usuário, aguarde ...');
		        	
		        	setTimeout(function () {
		        		userLogin(0)
				    }, 1000);
		        	
			    }, 3000);	        	
	        }
	        else {
		        setTimeout(function () {
		          $($tagResposta).fadeOut();
		        }, 10000);
	        }

            
        };
        
    } 
    else 
    { 
	    jq182.ajax({
	      type: 'POST',
	      url: $urlAjax,
	      data: {
	        user: {
	          name: $('#promo-user-nome').val(),
	          cpf: clearCPF($('#promo-user-cpf').val()),
	          address: $('#promo-user-endereco').val(),
	          state: $('#promo-user-estado').val(),
	          cep: $('#promo-user-cep').val(),
	          home_phone_number: $('#promo-user-telefone').val(),
	          mobile_phone_number: $('#promo-user-celular').val(),
	          email: $('#promo-user-email').val(),
	          accept_receive_emails: $recebeEmails
	        }
	      },
	      success: function (data) {
	        var $answer = '';
	        console.log(data);
	        //converte o result em dados json
	        $jsonObj = data;
	
	        $($tagResposta).fadeOut();
	
	        // add os erros na variável
	        if ($jsonObj.errors) {
	          $.each($jsonObj.errors, function (i, item) {
	            $.each(item, function (subi, subitem) {
	              if ($answer.search(subitem) < 0) {
	                $answer += $bullet + subitem + ' <br />';
	              }
	            });
	          });
	        } else {
	          $answer += $bullet + 'Usuário cadastrado com sucesso.<br />';
	          $userRegistered = 1;
	        }
	        
	        // Escreve os erros no html
	        $($tagResposta).html($answer);
	
	        $($tagResposta).fadeIn();
	        
	        if($userRegistered) {
	        	setTimeout(function () {
		        	$($tagResposta).html('Logando usuário, aguarde ...');
		        	
		        	setTimeout(function () {
		        		userLogin(0)
				    }, 1000);
		        	
			    }, 3000);	        	
	        }
	        else {
		        setTimeout(function () {
		          $($tagResposta).fadeOut();
		        }, 10000);
	        }
	
	      },
	      error: function()
	      {
	    	  var $answer = 'CPF inválido ou usuário já existente';
	    	  $($tagResposta).html('Enviando...');
	    	  $($tagResposta).fadeIn(); 
	    	  // Escreve os erros no html
			    $($tagResposta).html($answer);
			
			    $($tagResposta).fadeIn();
			    setTimeout(function () {
			      $($tagResposta).fadeOut();
			    }, 10000)
	      }//End XHR
	    });
    }
    return false;

  } else {
    /*
     * Verifica se existe outros tipos de erros e add a resposta ao output
     */
    if ($anotherError.length > 0) {
      for ($i = 0; $i < $anotherError.length; $i++) {
        switch ($anotherError[$i]) {
          case 'cpf':
            $htmlAnswer += $bullet + 'CPF inválido. <br />';
            break;
          case 'promo-user-email':
            $htmlAnswer += $bullet + 'E-mail inválido. <br />';
            break;
        }
      }
    }

    if ($emailsToCheck.length > 0) {
      if ($validacao.emailsCheck($emailsToCheck[0], $emailsToCheck[1]))
        $htmlAnswer += $bullet + 'E-mails não são correpondentes.<br />';
    }

    $htmlAnswer += $bullet + 'Campos incorretos. Favor verificar campos destacados.<br />';

    $($tagResposta).html($htmlAnswer);
    $($tagResposta).fadeIn();
    setTimeout(function () {
      $($tagResposta).fadeOut();
    }, 10000)
    return false;
  }


}

/* 
 * Classe para validações
 */
function validations() {
  /*
   * Método para validar email
   */
  this.validateEmail = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  /*
   * Método para verificar se emails são equivalentes
   */
  this.emailsCheck = function ($email, $emailConfirm) {
    if ($email == $emailConfirm)
      return false;
    else
      return true;
  }

  /*
   * Método para validar CPF
   */
  this.validateCPF = function (cpf) {
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
      return false;
    for (i = 0; i < cpf.length - 1; i++)
      if (cpf.charAt(i) != cpf.charAt(i + 1)) {
        digitos_iguais = 0;
        break;
      }
    if (!digitos_iguais) {
      numeros = cpf.substring(0, 9);
      digitos = cpf.substring(9);
      soma = 0;
      for (i = 10; i > 1; i--)
        soma += numeros.charAt(10 - i) * i;
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(0))
        return false;
      numeros = cpf.substring(0, 10);
      soma = 0;
      for (i = 11; i > 1; i--)
        soma += numeros.charAt(11 - i) * i;
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(1))
        return false;
      return true;
    }
    else
      return false;
  }

  /*
   * Verifica se a tecla digitada é um número
   */
  this.somenteNumeros = function (e) {
    var key;
    var keychar;
    if (window.event)
      key = window.event.keyCode;
    else if (e)
      key = e.which;
    else
      return true;
    keychar = String.fromCharCode(key);
    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
      return true;
    else if ((("0123456789").indexOf(keychar) > -1))
      return true;
    else if (keychar == ".") {
      return false;
    } else
      return false;
  };

  /*
   * Formatação para máscaras
   */
  this.maskme = function (src, mask, type) {
    var i = src.value.length;
    var saida = mask.substring(0, 1);
    var texto = mask.substring(i);

    if (texto.substring(0, 1) != saida) {
      src.value += texto.substring(0, 1);
    }
  }

  /*
   * Formatação para valores reais
   */
  this.mask_reais = function (fld, milSep, decSep, e) {
	  
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = e.keyCode ? e.keyCode : e.which; //(window.event) ? e.which : e.keyCode;
    
    console.log(whichCode)
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode);  // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false;  // Chave inválida
    len = fld.value.length;
    for (i = 0; i < len; i++)
      if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
    aux = '';
    for (; i < len; i++)
      if (strCheck.indexOf(fld.value.charAt(i)) != -1) aux += fld.value.charAt(i);
    aux += key;
    len = aux.length;
   
    if (len == 0) fld.value = '';
    if (len == 1) fld.value = '0' + decSep + '0' + aux;
    if (len == 2) fld.value = '0' + decSep + aux;
    if (len > 2) {
      aux2 = '';
      for (j = 0, i = len - 3; i >= 0; i--) {
        if (j == 3) {
          aux2 += milSep;
          j = 0;
        }
        aux2 += aux.charAt(i);
        j++;
      }
      fld.value = '';
      len2 = aux2.length;
      for (i = len2 - 1; i >= 0; i--)
        fld.value += aux2.charAt(i);
      fld.value += decSep + aux.substr(len - 2, len);
    }
    return false;
  }
}

/* INIT.js métodos ----------------------------------------------------------------------------------------- */

//adiciona mascara ao CPF
function MascaraCPF(cpf, event)
{
    if(mascaraInteiro(cpf, event)==false){
            event.returnValue = false;
    }       
    return formataCampo(cpf, '000.000.000-00', event);
}

//valida numero inteiro com mascara
function mascaraInteiro(event)
{
    if (event.keyCode < 48 || event.keyCode > 57){
            event.returnValue = false;
            return false;
    }
    return true;
}

//formata de forma generica os campos
function formataCampo(campo, Mascara, evento) 
{ 
	var boleanoMascara; 
	
	var Digitato = evento.keyCode;
	exp = /\-|\.|\/|\(|\)| /g
	campoSoNumeros = campo.value.toString().replace( exp, "" ); 

	var posicaoCampo = 0;    
	var NovoValorCampo="";
	var TamanhoMascara = campoSoNumeros.length;; 
	
	if (Digitato != 8) { // backspace 
			for(i=0; i<= TamanhoMascara; i++) { 
					boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
															|| (Mascara.charAt(i) == "/")) 
					boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(") 
															|| (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " ")) 
					if (boleanoMascara) { 
							NovoValorCampo += Mascara.charAt(i); 
							  TamanhoMascara++;
					}else { 
							NovoValorCampo += campoSoNumeros.charAt(posicaoCampo); 
							posicaoCampo++; 
					  }              
			  }      
			campo.value = NovoValorCampo;
			  return true; 
	}else { 
			return true; 
	}
}

//valida o CPF digitado
function ValidarCPF(Objcpf)
{
    var cpf = Objcpf.value;
    exp = /\.|\-/g
    cpf = cpf.toString().replace( exp, "" );     	
}

// Somente numberos
function numbersonly(myfield, e, dec){
	var key;
	var keychar;
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
	keychar = String.fromCharCode(key);
	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
		return true;
	else if ((("0123456789").indexOf(keychar) > -1))
		return true;
	else if (dec && (keychar == ".")){
		myfield.form.elements[dec].focus();
		return false;
	}else
		return false;
}

function clearCPF($cpf)
{

	$cpf = $cpf.replace(/\./gi, '');
	$cpf = $cpf.replace(/-/gi, '');
	return $cpf
	
}


