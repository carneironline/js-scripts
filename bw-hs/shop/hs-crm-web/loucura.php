
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Loucura do Dia - Shoptime
		</title>
	
	<meta charset="UTF-8" />
	<META NAME="WT.cg_n" CONTENT="www.shoptime.com.br" /><script type="text/javascript">var NREUMQ=NREUMQ||[];NREUMQ.push(["mark","firstbyte",new Date().getTime()]);</script>
	
	<link href="http://ishop.s8.com.br/statics-1.69.1.3/css/shop_components.css" rel="stylesheet" type="text/css" media="all" />
	<!--[if lte IE 7]><link href="http://ishop.s8.com.br/statics-1.69.1.3/css/shop_components_IE.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
	<link href="http://ishop.s8.com.br/statics-1.69.1.3/catalog/css/catalog.css" rel="stylesheet" type="text/css" media="all" />	
	<!--[if lte IE 7]><link href="http://ishop.s8.com.br/statics-1.69.1.3/catalog/css/catalog_IE.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
	
	<link media="only screen and (max-width: 640px)" href="http://m.shoptime.com.br" />
	<link media="handheld" href="http://m.shoptime.com.br" />	
	
	<script type="text/javascript"> // <![CDATA[
		var readyEvents=[]    
		var exceptionEvents=[]
		function onReady(f)  { readyEvents.push(f)  }
		function exceptionEvent(f) { exceptionEvents.push(f) }
		function handleException(x) {
			for(i=0;i<exceptionEvents.length;i++) {
				exceptionEvents[i](x);
			}						 
	  }
		exceptionEvent(function (f) { 
			if(typeof(console)!="undefined") {
					//console.debug(f);
			} 
		})
		
		document.getElementsByTagName("html")[0].className = 
			"js" + ((window.location.hash.indexOf("redir") > 0) ? ' redir' : '');

		// ]]>
	</script>
	<script type="text/javascript" src="http://ishop.s8.com.br/docs/apps/api/0.0.7/jquery.App.min.js?v=2"></script>
	<link href="http://img.shoptime.com.br/imagens/loucura-dia/css/estiloloucura11.css" rel="stylesheet" type="text/css" media="all" />
				<link href="http://img.shoptime.com.br/catalog/skins/skin-temp/css/skin-temp.css" rel="stylesheet" type="text/css" media="all" />
			<meta name="description" content="Na Loucura do Dia do Shoptime você encontra promoções imperdíveis e preços baixos. A promoção começa a meia-noite e termina às 23h e 59min do dia seguinte e o estoque é limitado!" />
	<link rel="canonical" href="http://www.shoptime.com.br"/>

<script type="text/javascript"> 
try {

OAS_url = 'http://oas.shoptime.com.br/RealMedia/ads/';
OAS_listpos = 'Bottom2,Bottom3,x71,x90,x96';
OAS_query = 'loucuradodia?chave=HOME_BANNERLEFT&WT.mc_id=bannerhomeleft-loucuradodia'; 
OAS_sitepage = 'shoptime/loucuradodia';

OAS_version = 10;
OAS_rn = '001234567890'; OAS_rns = '1234567890';
OAS_rn = new String (Math.random()); OAS_rns = OAS_rn.substring (2, 11); 

} catch(exception) {
	handleException(exception);	
}

function OAS_NORMAL(pos) {
	document.write('<A HREF="' + OAS_url + 'click_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + OAS_query + '" TARGET=_top>');
 	document.write('<IMG SRC="' + OAS_url + 'adstream_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + OAS_query + '" BORDER=0></A>');
}  
</script>

<script type="text/javascript">
try {
OAS_version = 11;
if (navigator.userAgent.indexOf('Mozilla/3') != -1)
	OAS_version = 10;
if (OAS_version >= 11)
	document.write('<SCR'+ 'IPT LANGUAGE=JavaScript1.1 SRC="' + OAS_url + 'adstream_mjx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '?' + OAS_query + '"></SC'+'RIPT>');
} catch(exception) {
	handleException(exception);	
}

</script>

<script type="text/javascript">
try {
function OAS_AD(pos) {
	if (OAS_version >= 11)
		OAS_RICH(pos);
	else
		OAS_NORMAL(pos);
}
} catch(exception) {
	handleException(exception);	
}

</script>
	
</head>

	<body>
	
		<div id="page">
			<div id="header">
	<!--jQuery Banners-->
<link type="text/css" rel="stylesheet"  href="http://apps.shoptime.com.br/media/global/jquery.banners/jquery.banners.css" />
<script type="text/javascript" src="http://apps.shoptime.com.br/media/global/jquery.banners/jquery.banners.js"></script>
<script type="text/javascript" src="http://apps.shoptime.com.br/media/global/jquery.banners/jquery.banners.tv.js"></script>
<!--End jQuery Banners-->



<!-- Responsys -->
<script src="http://img.shoptime.com.br/mktshop/site/header/js/cr.js" type="text/javascript"></script>
<!-- -->

<!-- mobile -->
<script src="//ishop.s8.com.br/js/mobileRedir.js"></script>
<!-- -->


<!--link rel="stylesheet" type="text/css" href="http://img.shoptime.com.br/mktshop/site/header/css/estilo-header-home-internas.css" media="all" /-->

<link rel="stylesheet" type="text/css" href="http://ishop.s8.com.br/mktshop/site/header/css/estilo-header-home-internas.css" media="all" />

<style type="text/css">
#shoptimeMenuServicos {margin-top: 25px;}
</style>

<!--link href="http://img.shoptime.com.br/catalog/skins/outubro-rosa-2013/css/outubro-rosa-shop4.css" rel="stylesheet" type="text/css" media="all" /-->

<style>
.loucura{
	overflow: visible !important;
}
.headerlayer-aside{
	margin-right: -278px !important;
	padding: 0 !important;
	height: 552px !important;
	border-left: 0 !important;
}
.loucura .produto-destaque .selo-item {
    display:none !important;
}
</style>

<!--308548 Header Internas-->
<div id="header-top">
	<div id="header-logo">
		<a id="logo-site" href="http://www.shoptime.com.br">Shoptime.com</a>
	</div>
	<div id="header-search">
		<form action="http://busca.shoptime.com.br/busca.php?q=" id="header-search-form">
		<fieldset>
			<label id="header-search-label">
			<span id="header-search-lbl">Buscar</span>
			<input type="text" name="q" id="header-search-input" value="" autocomplete="off" default-placeholder="Digite o que você procura" placeholder="Digite o que você procura" data-position="header" data-neemu="autocomplete-search-top" onBlur="this.placeholder = this.getAttribute('default-placeholder');" onFocus="this.placeholder = '';"/>
			</label>
		</fieldset>
		<button type="submit" value="buscar" id="header-search-submit" class="bt bt-submit" data-position="header">Buscar</button>
		</form>
	</div>

	<div id="header-userbox">
		<p id="header-userbox-container">
		Bem vindo, <span id="header-userbox-username">Visitante</span><br>
			<span id="header-userbox-loggedin">
			Se este não for você, <a href="https://carrinho.shoptime.com.br/CustomerWeb/pages/Logout">clique
			aqui</a>.
			</span>
			<span id="header-userbox-loggedout">
				<a href="https://carrinho.shoptime.com.br/CustomerWeb/pages/Login?placeOrigin=homecadastro&WT.mc_id=homecadastro">Registre-se ou faça login</a>.
			</span>
		</p>
	</div>
	
	<div id="header-cart">
		<a id="header-cart-button" href="http://carrinho.shoptime.com.br/checkout/" class="headerlayer-controller">
			<span class="shoptime-icones ic-header-cart">.</span>
			<span id="header-cart-title">Meu Carrinho</span>
			<span id="header-cart-counter">0 produtos</span>
		</a>
	</div>
	
	<div id="header-orders">
		<a href="https://carrinho.shoptime.com.br/ControlPanelWeb/panel/AllOrders?placeOrigin=homepedidos&WT.mc_id=homepedidos">Meus
		Pedidos</a>
		<a href="https://carrinho.shoptime.com.br/ControlPanelWeb/panel/home/unear/true">Minha Conta</a>
	</div>

	<!--script type="text/javascript">
	(function($){
	$('#area01 .headerCPanel').after('<p style="color:#f00; font-size:14px;">Informamos que os dias 24 e 31 de dezembro e 20 e 25 de janeiro não são considerados dias úteis para cálculo do prazo de entrega.</p>');
	})(jQuery);
	</script-->





	<div id="header-mainnav">
	<div id="header-mainnav-all" class="headerlayer-controller">
	<a href="http://www.shoptime.com.br/ShopSeeAll?sc_m=in|lt|todas_as_lojas_.|_" id="header-mainnav-button">
	<span id="header-mainnav-title">Todas as Lojas</span>
	<span class="shoptime-icones ic-mainnav-all">.</span>
	</a>

	<div class="headernavlayer layerleft headerlayer-container has-headernavlayer-aside">
	<div class="headerlayer-aside">
	<div class="loucura" style="width:auto!important;">
	<div style="width: 280px; height: 577px; position: relative; top: -15px;">
		<script type="text/javascript">
		try {
		OAS_AD('x28')
		} catch (exp) {
		handleException(exp)
		}
		</script>
	</div>

	</div>
	</div>
	<div class="headerlayer-top">
	<ul class="headerlayer-columns">


	<li class="headerlayer-column">
	<ul class="headerlayerlist">

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235552/acessorios-de-informatica">Acessórios
	de Informática</a></li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/344368/alimentos-e-bebidas">Alimentos
	e Bebidas</a></li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/320875/ar-condicionado-e-aquecedores">Ar-Condicionado
	e Aquecedores</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/260708/audio">Áudio</a>
	</li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/237460/automotivo">Automotivo</a>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/322528/bebes">Bebês</a>
	</li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235378/brinquedos">Brinquedos</a>
	</li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235127/cama-mesa-e-banho">Cama,
	Mesa e Banho</a></li>


	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235450/cameras-e-filmadoras">Câmeras
	e Filmadoras</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235938/casa-e-conforto">Casa
	&amp; Conforto</a></li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235576/celulares-e-telefones">Celulares
	e Telefones</a></li>
	
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/365174/decoracao">Decoração </a><span class="shoptime-icones-expansivel flag-new">Novo</span>
	</li>
	
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/328591/dvds">DVDs</a>
	</li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235209/eletrodomesticos">Eletrodomésticos</a>
	</li>


	</ul>
	</li>
	<li class="headerlayer-column">
	<ul class="headerlayerlist">

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/236009/eletronicos">Eletrônicos</a>
	</li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235632/eletroportateis">Eletroportáteis</a>
	</li>
	<!--li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/247288/natal">Enfeites de Natal</a><span class="shoptime-icones-expansivel flag-new">Novo</span></li-->
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/236099/esporte-e-lazer">Esporte
	e Lazer</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235513/ferramentas-e-jardim">Ferramentas
	e Jardim</a></li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235781/fun-kitchen">Fun
	Kitchen</a></li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/236888/games">Games</a>
	</li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/236165/informatica">Informática</a>
	</li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235060/instrumentos-musicais">Instrumentos
	Musicais</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235910/la-cuisine">La
	Cuisine</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235997/life-zone">Life
	Zone</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/339770/livros">Livros</a>
	</li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235741/malas-e-acessorios">Malas
	e Acessórios</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/247848/moveis-e-decoracao">Móveis
	</a></li>


	</ul>
	<!-- <div class="headerlayer-seemore"><a href="#"><span class="header-ico ic-header-seemore">.</span>Veja mais</a></div> -->
	</li>
	<li class="headerlayer-column">
	<ul class="headerlayerlist">

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/361802/musica">M&uacute;sica</a><span class="shoptime-icones-expansivel flag-new">Novo</span></li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/347028/papelaria-e-escritorio">Papelaria
	e Escritório</a></li>


	<!--li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/loja/256248/pascoa">Páscoa</a></li-->

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/339808/perfumaria">Perfumaria</a><span class="shoptime-icones-expansivel flag-new">Novo</span></li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/322149/pet-shop">Pet
	Shop</a></li>

	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235086/relogios">Relógios</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235261/saude-e-beleza">Saúde
	e Beleza</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/loja/235314/utilidades-domesticas">Utilidades
	Domésticas</a></li>
	</ul>
	<div class="headerlayer-seemore-02">Veja Também</div>
	<ul class="headerlayerlist">
	<!--li class="headerlayerlist-item"><a href="http://www.blockbusteronline.com.br">Blockbuster</a>
	</li-->
	<li class="headerlayerlist-item"><a
	href="http://www.shoptime.com.br/HomeEstatica/Catalogo_Click_2012?epar=111209&opn=CATALOGOCLICK&WT.mc_id=catalogoclick&un=1300&submidia=12">Catálogo</a>
	</li>
	<li class="headerlayerlist-item"><a href="http://www.ingresso.com">Ingresso</a></li>
	<li class="headerlayerlist-item"><a href="http://viagens.shoptime.com.br/">Shoptime
	Viagens</a></li>
	<li class="headerlayerlist-item"><a
	href="http://www.soubarato.com.br/?epar=ds_bs_00_sh_siteshopsbshop2&opn=SBSHOP2">Outlet
	- Sou Barato</a></li>
	<li class="headerlayerlist-item"><a
	href="http://blog.shoptime.com.br/">Blog Fica Dica</a><span class="shoptime-icones-expansivel flag-new">Novo</span></li>
	</ul>
	</li>
	</ul>
	</div>
	<div class="headerlayer-bottom">
	<div id="mainnavbrands">
	<div id="mainnavbrands-title">
	<span id="mainnavbrands-titlespan">Marcas exclusivas</span>
	</div>
	<ul id="mainnavbrands-list">
	<li class="mainnavbrands-item">
	<a href="http://www.shoptime.com.br/loja/235938/casa-e-conforto"
	class="shoptime-icones-expansivel ic-headerbrand-casaconforto">Casa &amp; Conforto</a>
	</li>
	<li class="mainnavbrands-item"  style="top: 1px;">
	<a href="http://www.shoptime.com.br/loja/235781/fun-kitchen"
	class="shoptime-icones-expansivel  ic-headerbrand-funkitchen">Fun Kitchen</a>
	</li>
	<li class="mainnavbrands-item" style="top: -12px;">
	<a href="http://www.shoptime.com.br/loja/235910/la-cuisine"
	class="shoptime-icones-expansivel ic-headerbrand-lacuisine">La Cuisinea>
	</li>
	<li class="mainnavbrands-item" style="top: -10px;">
	<a href="http://www.shoptime.com.br/loja/235997/life-zone"
	class="shoptime-icones-expansivel  ic-headerbrand-lifezone">Life Zone</a>
	</li>
	</ul>
	</div>
	</div>
	</div>
	</div>
	<div id="header-mainnav-aux">


	<!--CSS com modificação das categorias-->
	<!--link rel="stylesheet" type="text/css" href="http://img.shoptime.com.br/mktshop/site/header/css/header.css" /-->

	<ul id="mainnavaux">
	<!--ITEM 01-->
	<li class="mainnavaux-item headerlayer-controller mnaux-it-cameba">
	<a href="http://www.shoptime.com.br/loja/235127/cama-mesa-e-banho" class="mainnavaux-item-button">
	<span class="mainnavaux-item-title">Cama, Mesa e Banho</span>
	</a>
	<div class="headernavlayer layerleft headerlayer-container">
	<div class="headerlayer-top">
	<ul class="headerlayer-columns">
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/235127/cama-mesa-e-banho">Cama, Mesa e Banho</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/327330/cama-mesa-e-banho/manta-e-capa-para-sofa">Capa de Sofá</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/327372/cama-mesa-e-banho/edredom">Edredom</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/327415/cama-mesa-e-banho/jogo-de-banho">Jogo de Banho</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/327343/cama-mesa-e-banho/jogo-de-cama">Jogo de Cama</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/327407/cama-mesa-e-banho/roupao">Roupão</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/235127/cama-mesa-e-banho"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/235938/casa-e-conforto" class="shoptime-icones-expansivel ic-headerbrand-casaconforto">Marca Casa &amp; Conforto</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235980/casa-e-conforto/enxoval-de-cama">Enxoval de Cama</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235953/casa-e-conforto/jogo-de-cama">Jogo de Cama</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/sublinha/235968/casa-e-conforto/jogo-de-banho/jogo-de-banho-5-pecas">Jogo de Banho 5 Peças</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235959/casa-e-conforto/travesseiro">Travesseiro</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/sublinha/235971/casa-e-conforto/acessorios-de-cama/capa-de-travesseiro-impermeavel">Capa de Travesseiro</a></li>

	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235988/casa-e-conforto/cobertores-e-mantas">Cobertor e Manta</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235948/casa-e-conforto/edredom">Edredom</a></li>        
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/sublinha/253312/casa-e-conforto/mesa/toalha-de-mesa">Toalha de Mesa</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235974/casa-e-conforto/casa-e-conforto-kids">Casa & Conforto Kids</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/HomeEstatica/Hotsite_Catalogo-Banho">Catálogo de Banho</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/HomeEstatica/CatalogoEstampas">Catálogos de Estampa</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/235938/casa-e-conforto"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column bannerOAS">
	<script type="text/javascript">
	try {
	OAS_AD('x22')
	} catch (exp) {
	handleException(exp)
	}
	</script>
	</li>
	</ul>
	</div>
	</div>
	</li>
	<!--ITEM 01 FIM-->

	<!--ITEM 02-->
	<li class="mainnavaux-item headerlayer-controller mnaux-it-telefones">
	<a href="http://www.shoptime.com.br/loja/235576/celulares-e-telefones" class="mainnavaux-item-button">
	<span class="mainnavaux-item-title">Celulares e Telefones</span>
	</a>
	<div class="headernavlayer layerleft headerlayer-container">
	<div class="headerlayer-top">
	<ul class="headerlayer-columns">
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/235576/celulares-e-telefones">Telefonia Móvel</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/318646/celulares-e-telefones/celular">Celulares</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/350415/celulares-e-telefones/smartphone">Smartphones</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/318671/celulares-e-telefones/acessorios-para-celular">Acessórios</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/318703/celulares-e-telefones/nextel">Nextel</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/348612/celulares-e-telefones/iphone">Iphone</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/linha/318646/celulares-e-telefones/celular"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/linha/318731/celulares-e-telefones/telefone-sem-fio">Telefonia Fixa</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/318731/celulares-e-telefones/telefone-sem-fio">Telefone sem Fio</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/318743/celulares-e-telefones/telefone-com-fio">Telefone com Fio</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/sublinha/318737/celulares-e-telefones/telefone-sem-fio/telefones-sem-fio-com-ramal">Telefone com Ramal</a></li>

	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/linha/318731/celulares-e-telefones/telefone-sem-fio"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column bannerOAS">
	<script type="text/javascript">
	try {
	OAS_AD('x23')
	} catch (exp) {
	handleException(exp)
	}
	</script>
	</li>
	</ul>
	</div>
	</div>
	</li>
	<!--ITEM 02 FIM-->

	<!--ITEM 03-->
	<li class="mainnavaux-item headerlayer-controller mnaux-it-eletro">
	<a href="http://www.shoptime.com.br/loja/236009/eletronicos" class="mainnavaux-item-button">
	<span class="mainnavaux-item-title">Eletrônicos</span>
	</a>
	<div class="headernavlayer layerleft headerlayer-container">
	<div class="headerlayer-top">
	<ul class="headerlayer-columns">
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/236009/eletronicos">Vídeo</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/321062/eletronicos/tv">TV</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/321151/eletronicos/home-theater">Home Theater</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/321130/eletronicos/blu-ray-player">Blu-Ray Player</a>li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/321139/eletronicos/dvd-player">DVD Player</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/236009/eletronicos"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/260708/audio">Áudio</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320811/audio/micro-system">Micro System</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320814/audio/mini-system">Mini System</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/351592/audio/soundbar">Soundbar</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320832/audio/som-portatil">Som Portátil</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320841/audio/ipod-e-acessorios">iPods e Acessórios</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320817/audio/dock-station-e-caixas-bluetooth">Dock Station</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320869/audio/fones-de-ouvido">Fone de Ouvido</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320826/audio/mp-players">MP Player</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/260708/audio"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column bannerOAS">
	<script type="text/javascript">
	try {
	OAS_AD('x24')
	} catch (exp) {
	handleException(exp)
	}
	</script>
	</li>
	</ul>
	</div>
	</div>
	</li>
	<!--ITEM 03 FIM-->


	<!--ITEM 04-->
	<li class="mainnavaux-item headerlayer-controller mnaux-it-eletropo">
	<a href="http://www.shoptime.com.br/loja/235632/eletroportateis?chave=HOME_MENU_ELETROPORTA&WT.mc_id=home-menuprincipal-eletroportateis" class="mainnavaux-item-button">
	<span class="mainnavaux-item-title">Eletroportáteis</span>      
	</a>
	<div class="headernavlayer layerright headerlayer-container">
	<div class="headerlayer-top">
	<ul class="headerlayer-columns">

	<li class="headerlayer-column space-hdlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/235632/eletroportateis">Eletroportáteis</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/322822/eletroportateis/bebedouros-e-purificadores">Bebedouros</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/322897/eletroportateis/aspirador-de-po-vassoura-e-acessorios">Aspiradores</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/322872/eletroportateis/maquinas-de-costura">Máquinas de Costura</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/sublinha/322853/eletroportateis/cafeteira-e-chaleira/cafeteira-expresso">Cafeteiras Expresso</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/322800/eletroportateis/mixer-e-processador-de-alimentos">Mixers</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/322915/eletroportateis/ventilador-e-circulador-de-ar">Ventiladores</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/322888/eletroportateis/liquidificador-e-acessorios">Liquidificadores</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/322954/eletroportateis/batedeira">Batedeiras</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/322797/eletroportateis/panificadora-maquina-de-pao">Panificadoras</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/235632/eletroportateis"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	<!--a href="http://www.shoptime.com.br/jamieoliver"><img src="http://img.shoptime.com.br/imagens/banners/lojas/eletroportateis/bt-menu-expansivel/btn_jamie.jpg" alt="Philips Walita | Linha de Produtos Jamie Oliver" style="margin-top:15px;" /></a-->
	</li>
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/235781/fun-kitchen" class="shoptime-icones-expansivel ic-headerbrand-funkitchen">Marca Fun Kitchen</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235837/fun-kitchen/fritadeira">Fritadeiras</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235850/fun-kitchen/omeleteiras">Omeleteiras</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/336648/fun-kitchen/maquina-de-crepe">Máquinas de Crepe</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235860/fun-kitchen/panela-de-pressao">Panelas de Pressão</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235784/fun-kitchen/processador-de-alimentos">Processadores</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235852/fun-kitchen/panela-de-arroz">Panelas de Arroz</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235839/fun-kitchen/grill">Grills</a></li> 
	</ul>
	</li>
	<li class="headerlayer-column">
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235898/fun-kitchen/ferros-e-passadeiras">Ferros e Passadeiras</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235862/fun-kitchen/panela-eletrica">Panelas Elétricas</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235903/fun-kitchen/purificador-de-agua">Purificadores</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/portal/hotsite/ultimos_lancamentos/0/lancamentos_fk/235781">Últimos Lançamentos</a></li>   
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/235781/fun-kitchen"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column bannerOAS">
	<script type="text/javascript">
	try {
	OAS_AD('x25')
	} catch (exp) {
	handleException(exp)
	}
	</script>
	</li>
	</ul>
	</div>
	</div>
	</li>
	<!--ITEM 04 FIM-->


	<!--ITEM 05-->
	<li class="mainnavaux-item headerlayer-controller mnaux-it-info">
	<a href="http://www.shoptime.com.br/loja/236165/informatica" class="mainnavaux-item-button">
	<span class="mainnavaux-item-title">Informática</span>
	</a>
	<div class="headernavlayer layerright headerlayer-container">
	<div class="headerlayer-top">
	<ul class="headerlayer-columns">
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/236165/informatica">Informática</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320352/informatica/notebooks">Notebook</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/349401/informatica/ultrabooks">Ultrabook</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320633/informatica/tablets-e-ipad">Tablet e iPad</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320480/informatica/computadores-e-all-in-one">Computadores</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320453/informatica/multifuncionais">Impressoras e Multifuncionais</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/236165/informatica"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/235552/acessorios-de-informatica">Acessórios de Informática</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/sublinha/320343/informatica/armazenamento/hd-externo">HDs</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/sublinha/320328/informatica/armazenamento/pen-drives">Pen Drive</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320388/informatica/equipamentos-de-rede-wireless">Roteadores</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/320565/informatica/acessorios-para-notebook">Acessórios para Notebook</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/portal/hotsite/mouses_e_teclados/0/mouse-teclado/235552">Mouses & Teclados</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/235552/acessorios-de-informatica"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column bannerOAS">
	<script type="text/javascript">
	try {
	OAS_AD('x26')
	} catch (exp) {
	handleException(exp)
	}
	</script>
	</li>
	</ul>
	</div>
	</div>
	</li>
	<!--ITEM 05 FIM-->


	<!--ITEM 06-->
	<li class="mainnavaux-item headerlayer-controller mnaux-it-ud">
	<a href="http://www.shoptime.com.br/loja/235314/utilidades-domesticas" class="mainnavaux-item-button">
	<span class="mainnavaux-item-title">Utilidades Domésticas</span>
	</a>
	<div class="headernavlayer layerright headerlayer-container">
	<div class="headerlayer-top">
	<ul class="headerlayer-columns">
	<li class="headerlayer-column space-hdlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/235314/utilidades-domesticas">Utilidades Domésticas</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/323016/utilidades-domesticas/panela-de-pressao">Panelas de Pressão</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/323071/utilidades-domesticas/panelas-avulsas">Panelas Avulsas</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/322994/utilidades-domesticas/conjunto-de-panelas">Conjuntos de Panelas</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/323124/utilidades-domesticas/chopeira">Chopeiras</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/323120/utilidades-domesticas/churrasqueira">Churrasqueiras</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/323114/utilidades-domesticas/cooler">Cooler</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235923/la-cuisine/travessas">Baixelas e Travessas</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/323340/utilidades-domesticas/sobremesa-cafe-e-cha">Sobremesa, Chá e Café</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/235314/utilidades-domesticas"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column">
	<div class="headerlayerlist-title"><a href="http://www.shoptime.com.br/loja/235910/la-cuisine" class="shoptime-icones-expansivel ic-headerbrand-lacuisine">Marca La Cuisine</a></div>
	<ul class="headerlayerlist">
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235934/la-cuisine/panelas-inox">Panelas Inox</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235920/la-cuisine/panela-antiaderente">Panelas Antiaderentes</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/sublinha/326774/la-cuisine/panela-de-ferro-fundido/panela-de-ferro">Panelas de Ferro Fundido</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/sublinha/326768/la-cuisine/aparelhos-de-jantar/aparelho-de-jantar-20-pecas">Aparelhos de Jantar</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235914/la-cuisine/faqueiros">Faqueiros</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/326790/la-cuisine/facas">Facas e Cepos</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235930/la-cuisine/tigelas-e-potes">Tigelas e Potes</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/235918/la-cuisine/utensilios-de-inox">Utensílios de Inox</a></li>
	<li class="headerlayerlist-item"><a href="http://www.shoptime.com.br/linha/236698/la-cuisine/utensilios-de-silicone">Utensílios de Silicone</a></li>
	</ul>
	<div class="headerlayer-seemore"><a href="http://www.shoptime.com.br/loja/235910/la-cuisine"><span class="shoptime-icones-expansivel ic-header-seemore">.</span>Veja mais</a></div>
	</li>
	<li class="headerlayer-column bannerOAS">
	<script type="text/javascript">
	try {
	OAS_AD('x27')
	} catch (exp) {
	handleException(exp)
	}
	</script>
	</li>
	</ul>
	</div>
	</div>
	</li>
	<!--ITEM 06 FIM-->

	</ul><!--#mainnavaux-->

	<!-- 308205 inovacao-header-geral-menuaux-20131025 fim-->
	</div>
	</div>
	<div id="header-auxnav" class="headerlayer-controller">
	<a id="header-auxnav-button" href="http://www.shoptime.com.br/loucuradodia">
	<span class="shoptime-icones ic-auxnax-logoloucura">.</span>
	<span id="header-auxnav-title">Loucura do Dia</span>
	<span class="shoptime-icones ic-auxnax-seta">.</span>
	</a>

	<div class="headernavlayer layerright headerlayer-container">
	<div class="loucura">
	<script type="text/javascript">
	try {
	OAS_AD('x96')
	} catch (exp) {
	handleException(exp)
	}
	</script>
	</div>
	</div>
	</div>
	</div>
<div id="header-bottom">

<!--308067 inovacao-header-home-20131025-->

<div id="shoptime-header-top-13">		
<div id="shoptime-header-heldesk">

<div class="helpdesk-box helpdesk-tv">
<span class="shoptime-icones icone-helpdesk-tv">.</span>
<span class="helpdesk-tv-content">24 horas no Ar | SKY 19 | Parabólica</span>
<span>NET 29 (RJ/POA/BH/SP) 31 (DEMAIS REGIÕES)</span>
</div>

<div class="helpdesk-box helpdesk-ligue-compre">
<a class="hda-link" href="http://www.shoptime.com.br/central-de-atendimento?WT.mc_id=atendimentoInterna&amp;WT.mc_ev=click">
</a><a class="hdl-link lightbox" href="http://www.shoptime.com.br/estaticapop/232590/"><span class="shoptime-icones icone-ligue-compre">.</span>
<span class="helpdesk-ligue-phone">4003-1020</span>
<span class="helpdesk-ligue-24h">24h</span>
<span class="helpdesk-ligue-content">Ligue e compre!</span></a>
</div>

<div class="helpdesk-box helpdesk-atendimento">
<a class="hda-link" href="http://www.shoptime.com.br/central-de-atendimento?WT.mc_id=atendimentoInterna&amp;WT.mc_ev=click"><span class="shoptime-icones icone-atendimento">.</span>
<span class="helpdesk-atendimento-phone">Atendimento</span>
<div style="background: transparent; width: 175px; height: 40px; position: absolute; top: 0; left: 0;"></div><style>
#header-bottom, #shoptime-header-top-13, 
#shoptime-header-heldesk, .helpdesk-box.helpdesk-atendimento{
	overflow: visible !important;
}
.tooltip-atendimento{
	background-color: #fff;
	width: 165px;
	position: absolute;
	top: 40px;
	left: -35px;
	z-index: 100;
	padding: 10px;
	display: none;
	border: 1px solid #6e9bff;
	text-transform: none;
}
.tooltip-atendimento:before {
  content: "";
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
  width: 0; 
  height: 0; 
  position: absolute;
  top: -10px;
  left: 51px;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-bottom: 10px solid #fff;
}
.tooltip-atendimento  h2{
	font-size: 12px;
	color: #505050;
	margin: 0;
}
.tooltip-atendimento  p{
	font-size: 11px;
	color: #505050;
	margin: 7px 0 0;
	display: inline-block;
	line-height: 1.2;
}
.hda-link{
	position: relative;
}
.hda-link:hover  .tooltip-atendimento{ 
	display: block;
}
</style>
<div class="tooltip-atendimento">
    <h2>Precisa de ajuda?</h2>
    <p>Atendimento por telefone, e-mail e dúvidas frequentes. <span style="text-decoration: underline;">clique aqui</span></p>                     
</div>
</a>
</div>

<div class="helpdesk-box helpdesk-banner-oas">
<script type="text/javascript">	
try { 
OAS_AD('x71')
} catch(exp) {
handleException(exp)
}	
</script>			
</div>		

</div>
</div>
</div>
<script type="text/Javascript" src="http://img.shoptime.com.br/mktshop/site/header/js/header-shoptime.js"></script>

<!--308067 inovacao-header-home-20131025 Fim-->
	<script type="text/javascript">
cr = new CR({
m:"SHOP",
origin:4,
eventName:'CR_OFERTA_DIA',
eventId:'1002',
opt:'out', 
send_email:true,
name:'',
mail:'email',
response:function(index,msg){
 var msg = document.getElementById('form-mensagem-oferta');
 _Form('none','block');
 index = Number(index);
switch(index){
  case 1: msg.innerHTML='<p class="sucessocr"><strong>Quase lá!</strong> <br />Acesse seu email para confirmar seu cadastro.</p>'; break;
  case 2: msg.innerHTML='<p class="errorcr">Ocorreu um erro ao tentar cadastrar seu e-mail.<br />Por favor, <a href="javascript:void(0);" onclick="_Form(\'block\',\'none\');">tente novamente.</a></p>'; break;
case 3: msg.innerHTML='<p class="errorcr">Ocorreu um erro ao tentar cadastrar seu e-mail.<br />Por favor, <a href="javascript:void(0);" onclick="_Form(\'block\',\'none\');">tente novamente.</a></p>'; break;
  case 4: msg.innerHTML='<p class="sucessocr"><strong>Quase lá!</strong> <br />Acesse seu email para confirmar seu cadastro.</p>'; break;
}
 document.getElementById('email').value="";
}
});
function _Form(a,b){
document.getElementById('form-default-oferta').style.display=a;
document.getElementById('form-mensagem-oferta').style.display=b;
}
</script>

<style>
.sucessocr{
	color:#999999!important;
	font-size:16px!important;
}
.errorcr{
	color:#999999!important;
	font-size:14px!important;
}
.errorcr a{
color:#ff6600!important;
}
.sucessocr strong{
	color:#ff6600!important;
	font-size:22px!important;
	}
</style>
			</div>
	
		<div id="content">
			<p class="hc">
				<a id="conteudo" accesskey="1">você está no conteúdo</a>						
				<a href="#rodape">ir para o rodapé <strong>(atalho + 2)</strong></a>
			</p>		
			
			<div id="area01">
	<style>
#oferta-dia{ position:relative;}

#shoptimeNovoLoucuraDia .loucura-icones{
		background: url(http://img.shoptime.com.br/imagens/loucura-dia/img/sprite-loucura-dia1.png);
    display: block;
    text-indent: -9999em;
		}
		.loucura-decoracao{
	background-position: -311px -107px !important;
    height: 162px;
    margin: 25px auto 30px;
    width: 206px;
		}

#shoptimeNovoLoucuraDia .loucura-texto1 {
    background-position: -308px -61px;
    height: 17px;
    margin: 0 auto 10px;
    width: 264px;
}
#shoptimeNovoLoucuraDia .loucura-texto2 {
    background-position: -308px -86px;
    height: 16px;
    margin: 0 auto;
    width: 262px;
}
		</style>
<!-- inicio oferta dia -->
     <div id="oferta-dia">
<a href="http://www.shoptime.com.br/HomeEstatica/politica-de-privacidade" style="
    position: absolute;
    right: 25px;
    top: 10px;
    font-size: 10px;
">política de privacidade</a>
		
		
		
		<div id="shoptimeNovoLoucuraDia">
	<div class="snld-top">	
		<div class="snld-top-esquerda left">
			<p class="loucura-icones loucura-logo">Loucura do Dia Shoptime</p>
			<p class="loucura-icones loucura-texto1">Todo dia uma oferta imbat�vel!</p>
			<p class="loucura-icones loucura-texto2">Pre�o in�dito com quantidade limitada.</p>
		</div><!--.snld-top-esquerda-->
	
		<div class="snld-top-direita left">
			<div class="snld-td-box1 left">
			
				<div class="snld-counter left">
					<div class="app-timer timer-ready" data-timer="26/08/2014 00:00:00">
						<p class="timer-frase">Compre agora! Essa oferta acaba em:</p>
						
							  <div class="timer-hour">
								<div class="timer-number">
									<div class="timer-tens">0</div>
									<div class="timer-units">8</div>
									<div class="timer-mark">:</div>
									</div>
								<div class="timer-label">horas</div>
							</div>
							<div class="timer-minute">
								<div class="timer-number">
									<div class="timer-tens">5</div>
									<div class="timer-units">3</div>
									<div class="timer-mark">:</div>
									</div>
								<div class="timer-label">minutos</div>
							</div>
							<div class="timer-second">
								<div class="timer-number">
									<div class="timer-tens">2</div>
									<div class="timer-units">0</div>
								</div>
								<div class="timer-label">segundos</div>
							</div>
						</div>
					<div class="both"></div>
				</div><!--.snld-counter-->
				
				<div class="snld-subscribe left">
					<div class="default" id="form-default-oferta">
					  <h2 class="subsc-tit">Cadastre-se agora</h2>
					  <form onsubmit="javascript:return cr.create();" class="sub-fr" method="get">
						<fieldset>
						  <ul class="subsc-lt">
							<li class="subsc-it-01 subsc-it left">
							 <input type="text" class="typ-01 add-field placeholder" id="email" name="email" placeholder="seu e-mail">
							</li>
							<li class="subsc-it-02 subsc-it left">
							  <input type="submit" class="loucura-icones ofe-cad" value="enviar">
							</li>
						  </ul>
						</fieldset>
					  </form>
					</div>
					<div style="display: none;" id="form-mensagem-oferta"></div>
				
				</div><!--.snld-subscribe-->
				
				
			
			
			</div><!--.snld-box1-->

			<script type="text/javascript">
				App.brand = {name: "shoptime", ref: "SHOP", host: "shoptime.com.br"}
			</script>

			<div class="app-product" data-template="#template1Produto" data-product-id="115309278">			
				</div>
			
			<script id="template1Produto" type="text/template"><%
				var parcel = _.last(_.keys(product.get('installments')));
				var default_price =  product.get('price').from.split('R$ ')[1].replace(',', '.');
				var sales_price = product.get('price').sale.split('R$ ')[1].replace(',', '.');
				var discount_price = Math.ceil(default_price - sales_price);
				%>
				<div class="snld-td-box2 left">
					<img class="loucura-produto-foto left" src="<%= product.get('image') %>" alt="<%= product.get('title_short') %>" />
					<div class="loucura-produto-dados left">
						<p class="loucura-produto-titulo"><%= product.get('title_short') %></p>
						<!--ul class="loucura-produto-lista">
							<li class="loucura-produto-item"><%= product.get('description') %></li>
						</ul-->
						<% if(product.get('freight')){ %>
						<div class="loucura-produto-tags">
							<img class="loucura-produto-tagfrete left" src="<%= product.get('freight') %>" alt="Frete" />
						</div>
						<%}%>
						<div class="loucura-produto-valores left">
							<p class="loucura-precode">De R$ <span class="precode-reais"><%= product.get('price').from && product.get('price').from != 'null'? product.get('price').from.split('R$ ')[1].split(',')[0] + '</span><sup class="precode-centavos">,' + product.get('price').from.split('R$ ')[1].split(',')[1] + '</sup>' : product.get('price').sale.split('R$ ')[1].split(',')[0] + '</span><sup class="precode-centavos">,' + product.get('price').sale.split('R$ ')[1].split(',')[1] + '</sup>' %></p>
							<p class="loucura-sohoje">S&oacute; Hoje!</p>
							<p class="loucura-precopor">R$ <span class="precopor-reais"><%= product.get('price').invoice.split('R$ ')[1].split(',')[0] %></span><sup class="precopor-centavos">,<%= product.get('price').invoice.split('R$ ')[1].split(',')[1] %></sup><span class="precopor-condicao"> no boleto</span></p>
							<p class="loucura-precoparcela"><%= parcel %>x R$ <span class="precoparcela-reais"><%= product.get('installments')[parcel].split('R$ ')[1].split(',')[0] %></span><sup class="precoparcela-centavos">,<%= product.get('installments')[parcel].split('R$ ')[1].split(',')[1] %></sup> sem juros</p>
							<p class="loucura-precoprazo">Total a prazo <span class="precoprazo-reais"><%= product.get('price').sale.split(',')[0] %></span><sup class="precoprazo-centavos">,<%= product.get('price').sale.split(',')[1] %></sup></p>
							<a class="loucura-icones loucura-comprar" href="<%= $el.data('href') || product.get('url') %>">Comprar</a>
                                                        <!--a class="loucura-icones loucura-comprar" href="http://www.shoptime.com.br/ofertas/HomeLandingPage/0/0/118739409/112591757/112563967">Comprar</a-->
						</div>
						<p class="loucura-produto-desconto left">Economize R$ <%= discount_price %></span> (<%= parseFloat((discount_price/default_price)*100).toFixed(0).replace(/\./g,',') %>% de desconto)</p>
					</div><!--loucura-produto-descricao-->
				</div><!--.snld-box2-->
			</script>

		</div><!--.snld-top-direita-->
	</div>
		
		<div class="snld-down">
			<div class="down-text">
					<p>Aqui, na "Loucura do Dia", você encontra promoções imperdíveis e preços baixíssimos. Nesse espaço, a Família Shoptime está sempre escolhendo uma promoção inédita para você. As ofertas começam a meia-noite e terminam às 23h e 59min do mesmo dia. Não deixe para depois porque o estoque é limitado. Com uma loucura dessas os produtos acabam rápido, por isso garanta já o seu!</p>
					
					<p>Não quer perder nenhuma oportunidade? Curta a nossa página no <a href="https://www.facebook.com/CanalShoptime?chave=bannervert-facebook&amp;WT.mc_id=bannervert-facebook">Facebook</a> ou siga a gente no <a href="https://twitter.com/canalshoptime?chave=bannervert-twitter&amp;WT.mc_id=bannervert-twitter">Twitter</a>.</p>
					<div class="down-img loucura-icones"></div>
			</div>	
			
		<ul class="snld-social">
				<li class="left"><a class="loucura-icones bt-midia midia1" href="https://www.facebook.com/CanalShoptime?chave=bannervert-facebook&amp;WT.mc_id=bannervert-facebook ">Facebook</a></li>
				<li class="left"><a class="loucura-icones bt-midia midia2" href="https://twitter.com/canalshoptime?chave=bannervert-twitter&amp;WT.mc_id=bannervert-twitter">Twitter</a></li>
				<li class="left"><a class="loucura-icones bt-midia midia3" href="https://plus.google.com/+canalshoptime/posts">Google Plus</a></li>
				<li class="left"><a class="loucura-icones bt-midia midia4" href="http://instagram.com/canalshoptime">Instagram</a></li>
				<li class="left"><a class="loucura-icones bt-midia midia5" href="http://www.youtube.com/CanalShoptime">YouTube</a></li>
		</ul>	
		</div><!--.snld-down-->
	
	</div>
		
	
      </div>
<!-- fim oferta dia -->
			</div>
	
		</div>
	<style type="text/css">

#new-shoptimeMenuServicos{
	width: 850px;
	overflow: hidden;
        margin:13px auto 10px auto;
}
#new-shoptimeMenuServicos ul.new-sms-list{
	height:150px;			
	overflow:hidden;
}
#new-shoptimeMenuServicos ul.new-sms-list li{
	float:left;	
	width:170px !important;
	height:150px;
}
#new-shoptimeMenuServicos ul.new-sms-list li a{
	background:url("http://img.shoptime.com.br/mktshop/home/servicos-slider/img/sprite-servicos.jpg") no-repeat 12px 0;
	display:block;
	width:170px !important;
	height:150px;	
	overflow:hidden;
	text-indent:-9999px;
}



#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-kmVantagens{
	background-position:-847px 0;
}
#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-garantiaEstendida{
	background-position:-1005px 0;
}
#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-praJa{
	background-position:-1189px 0;
}
#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-seguroPet{
	background-position:-1376px 0;
}
#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-viagens{
	background-position:-1551px 0;
}
#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-casamento{
	background-position:4px 0;
}
#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-catalogo{
	background-position:-196px 0;
}
#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-blog{
	background-position:-362px 0;
}
#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-aplicativoMobile{
	background-position:-540px 0;
}
#new-shoptimeMenuServicos ul.new-sms-list li .new-list-sevicos-compraRapida{
	background-position:-689px 0;
}
.slide-pagination{
	height:5px;
}
.slide-pagination .slide-pagination-prev, .slide-pagination .slide-pagination-next{
	background:url("http://img.shoptime.com.br/mktshop/home/servicos-slider/img/seta.jpg") no-repeat 0 0;
	width:22px;
	height:46px;
	display:block;
	text-indent:-9999px;
	overflow:hidden;
	position:relative;	
	cursor:pointer;
}
.slide-pagination .slide-pagination-prev{
	left: 8px;
	top: -84px;
}
.slide-pagination .slide-pagination-next{	
	background:url("http://img.shoptime.com.br/mktshop/home/servicos-slider/img/seta.jpg") no-repeat -29px 0;
	top: -131px;
	right: -960px;
}
</style>

<div id="box-shoptimeMenuServicos">
	<div id="new-shoptimeMenuServicos">
		<ul class="new-sms-list">
			<li>
				<a href="https://carrinho.shoptime.com.br/lista-de-casamento/pages/HomePage" class="new-list-sevicos-casamento">casamento</a>
			</li>
			
			<li>
				<a href="http://www.shoptime.com.br/HomeEstatica/Catalogo_Click_2012?epar=111209&opn=CATALOGOCLICK&WT.mc_id=catalogoclick&un=1300&submidia=12" class="new-list-sevicos-catalogo">catalogo</a>
			</li>
			
			<li>
				<a href="http://blog.shoptime.com.br/" class="new-list-sevicos-blog">blog</a>
			</li>
			
			<li>
				<a href="http://www.shoptime.com.br/aplicativo-mobile" class="new-list-sevicos-aplicativoMobile">aplicativo mobile</a>
			</li>
			
			<li>
				<a href="http://www.shoptime.com.br/compra-rapida" class="new-list-sevicos-compraRapida">compra rapida</a>
			</li>
			
			<li>
				<a href="http://www.shoptime.com.br/km-de-vantagens" class="new-list-sevicos-kmVantagens">km de vantagens</a>
			</li>
			
			<li>
				<a href="http://www.shoptime.com.br/portal/hotsite/entrega_praja_shop/295153/entrega_praja_shop" class="new-list-sevicos-praJa">praja</a>
			</li>
			
			<li>
				<a href="http://www.shoptime.com.br/planopetamigo" class="new-list-sevicos-seguroPet">seguro pet</a>				                                                         
			</li>
			
			<li>
				<a href="http://viagens.shoptime.com.br/default.aspx?utm_source=shoptime&utm_medium=MALA&utm_campaign=viagens" class="new-list-sevicos-viagens">viagens</a>
			</li>
			
			<!--li>
				<a href="#" class="new-list-sevicos-garantiaEstendida">garantia estendida</a>
			</li-->
		</ul>					
	</div>
		<div class="slide-pagination"> 
			<span class="slide-pagination-prev">Anterior</span> 
			<span class="slide-pagination-next">Próxima</span> 
		</div>	
</div>

<script type="text/javascript">
	onReady(function(){
		$.getScript("http://img.shoptime.com.br/hotsites/acessorio-vidro/js/jquery.carouFredSel-5.6.4-packed.js").done(function(script, textStatus) {
			var carrossel = $(".new-sms-list");
			var time = 200;
			carrossel.show();
			carrossel.carouFredSel({								
				items: 5,
				auto: false,
				circular: false,
				infinite: true,
				scroll:{					
					duration: 400,
					items:1
				},
				prev	: {
					button	: ".slide-pagination-prev",
					key		: "left",
					
				},
				next	: {
					button	: ".slide-pagination-next",
					key		: "right",
					
					
				}
			},{
				wrapper: {
					element: "div",
					classname: "slide-items-wrapper"
				}
			});			
		});
	});
</script>
	<style type="text/css">
.mnSrch fieldset.form{width:67%;}
div#sugestao{color: rgb(255, 255, 255); font-weight: bold; position: absolute; top: 13px; right: 21px; font-size: 13px;display:none;}
div#sugestao a{color:#FFFFFF;}

		/*BUSCA*/
	.left{
	float:left;
	}

	.fSearch{
	background: none;
	background: url (http://img.shoptime.com.br/novoshop/img/shoptime-sprite-header.png) !important;
	margin-bottom: 20px;
	}
	
	.fSearch .mnSrch fieldset {
	width:620px;
	}
	
	.fSearch .mnSrch .a02 .srchStr {
    padding: 10px 4px;
    width: 600px;
	}
	
	.fSearch .mnSrch .dptFilter, .mnSrch .srchStr {
    background: none repeat scroll 0 0 #FFF;
    border: 1px solid #6e9bff;
    padding: 1px;
	}
	
	.fSearch .mnSrch .srchSbmt{
	background: url(http://img.shoptime.com.br/novoshop/img/shoptime-sprite-header.png) repeat scroll 0 0 #6E9BFF !important;
	background-position: -172px -47px !important;
	border: none;
    cursor: pointer;
    height: 36px;
    left: 609px;
    width: 27px;
	top:0;
	zoom:1;
	}
	
	.fSearch  .mnSrch #sugestao{
    background-color: #6E9BFF;
    padding: 9px 30px;
	top: 10px !important;
	font-weight: normal;
	right: 4px !important;
	}
	
	.fSearch  .mnSrch #sugestao .sugestao{
	color: #FFFFFF;
    font-size: 14px;
    text-decoration: none;
	font-weight: normal;
	}	
</style>

<script type="text/javascript">
	setTimeout(function(){
			$(".sugestao").fancybox({width:500,height:475,autoDimensions:false});
$("#fsearch fieldset").addClass("form");
$("#sugestao").fadeIn("slow");
	},3000);
</script>
<div id="searchBox">	
	<div class="fSearch">
		<div class="box5 rBox2"><div class="boxR1"></div><div class="boxR2"></div><div class="boxR3"></div><div class="boxR4"></div>
			<div class="hb">
				<form action="http://busca.shoptime.com.br/busca.php" method="get" class="mnSrch" id="fsearch">
					<fieldset class="form left"><legend>Faça sua busca</legend>
						<div class="a02"><label><span class="lbl">Para buscar, digite aqui o código do produto ou palavra-chave</span>
						<input type="text" value="" data-position="footer"title="Para buscar, digite aqui o código do produto ou palavra-chave" name="q" class="srchStr">
						</label>
						</div>
						<input type="submit" class="srchSbmt shoptime-icones" value="buscar" data-position="footer">
					</fieldset>
<div id="sugestao" style="display: block;" class="left"><a href="/estaticapop/257430" class="sugestao">Quer sugerir algum produto? Clique aqui.</a></div>
				</form>
			</div>
		</div>
	</div>
</div>
	<script type="text/javascript">
	onReady(function(){	
		$(".new-lightbox").click(function(){
			var obj	= $(this);
			var args = {
				ajax : {
					data : {ax: 1}
				},
				centerOnScroll	: true
			};
			for(k in obj.data()){
				args[k]			= obj.data(k);
			}
			if(obj.data('href') === undefined){
				if(obj.attr('href') === undefined){
					return true;
				}else{
					args['href'] = obj.attr('href');
				}
			}
			$.fancybox(args);
			return false;
		});
	});
</script>
<style type="text/css">
.left{
float:left;
}
#shoptimeNovoFooter a{
	color:#828282;
}
	.both{
	clear:both;
	}
	.mobileVersion{
        width: 100%;
        clear: both;
        margin: 10px 0;
       text-align:center;
    }
    .mobileVersion p { width:100%; text-align:center;}
    .mobileVersion a {
        color: #000;
        font-weight: bold;
        display: block;
        text-align: center;
    }
	#shoptimeNovoFooter {
	overflow: hidden;
	width:1004px;
	padding: 10px;
	font-size:12px;
	}
		
	#shoptimeNovoFooter .shoptime-icones-footer{
	background: url(http://img.shoptime.com.br/mktshop/home/img/shoptime-sprite-footer3.png);
	display:block;
	text-indent: -9999em;
	}
	
	#shoptimeNovoFooter .footer-setinha{
    background-position: -232px -77px;
    height: 12px;
    width: 4px;
}

	*#shoptimeNovoFooter .footer-setinha{
    background-position: -232px -77px;
    height: 12px;
    width: 4px;
}
	
	#shoptimeNovoFooter .snf-top,
	#shoptimeNovoFooter .snf-middle,
	#shoptimeNovoFooter .snf-middle-left,
	#shoptimeNovoFooter .snf-middle-right,
	#shoptimeNovoFooter .snf-bottom,
	#shoptimeNovoFooter .snf-bottom-left,
	#shoptimeNovoFooter .snf-bottom-right{
	overflow:hidden;	
	}
	
	#shoptimeNovoFooter .snf-middle-left,
	#shoptimeNovoFooter .snf-bottom-left{
	width: 590px;	
	}
		
	#shoptimeNovoFooter .snf-middle-right,
	#shoptimeNovoFooter .snf-bottom-right{
	margin-left: 60px;
    width: 350px;
	}
	
	#shoptimeNovoFooter .snf-top{
	margin-bottom: 25px;
    }
	
	#shoptimeNovoFooter .snf-middle,
	#shoptimeNovoFooter .snf-bottom{
	border-bottom: 1px solid #C9C9C9;
    margin-bottom: 25px;
    padding-bottom: 20px;
	}
	
	#shoptimeNovoFooter .snf-bt-atendimento{
	background-position: 0 -146px;
    height: 19px;
    width: 143px;
	}
	
	#shoptimeNovoFooter .snf-bt-liguecompre{
	background-position: 0 -114px;
    height: 23px;
    width: 247px;
	margin-left: 40px;
	}
	
	#shoptimeNovoFooter .snf-middle .col-institucional, 
	#shoptimeNovoFooter .snf-middle .col-central {
	margin-right:15px;
	}
	
	#shoptimeNovoFooter .col-institucional strong, 
	#shoptimeNovoFooter .col-central strong,
	#shoptimeNovoFooter .col-viagens strong,
	#shoptimeNovoFooter .col-pagamento strong,
	#shoptimeNovoFooter .col-seguranca strong,
	#shoptimeNovoFooter .col-midias strong{
	color:#ff9c5a;
	margin-left:5px;
	}
	
	#shoptimeNovoFooter a:hover{
	text-decoration: none;
	color:#ff9c5a;
	}
	
	#shoptimeNovoFooter .col-institucional ul, 
	#shoptimeNovoFooter .col-central ul,
	#shoptimeNovoFooter .col-viagens ul{
	margin:5px 0 0 10px;
	}
	
	#shoptimeNovoFooter .col-pagamento ul{
	width: 270px;
	overflow: hidden;
	margin: 0 5px 0 3px;
	}
	
	#shoptimeNovoFooter .col-pagamento p{
	font-size: 11px;
    margin-left: 10px;
    width: 340px;
	}
	
	#shoptimeNovoFooter .col-institucional li, 
	#shoptimeNovoFooter .col-central li,
	#shoptimeNovoFooter .col-viagens li{
	margin-bottom:5px;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pagamento{
	height: 31px;
    width: 43px;
	margin:5px;	
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-01{
	background-position: 0 0;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-02{
	background-position: -50px 0;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-03{
	background-position: -98px 0;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-04{
	background-position: -146px 0;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-05{
	background-position: -194px 0;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-06{
	background-position: 0 -37px;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-07{
	background-position: -50px -37px;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-08{
	background-position: -98px -37px;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-09{
	background-position: -146px -37px;
	}
	
	#shoptimeNovoFooter .col-pagamento .item-pag-10{
	background-position: -194px -37px;
	}
	
	#shoptimeNovoFooter .item-seg-01{
	margin-top:10px;
	}	
	
	#shoptimeNovoFooter .item-seg-02{
    background-position: -122px -79px;
    height: 28px;
    margin: 16px 0 0 10px;
    width: 52px;
	}
	
	#shoptimeNovoFooter .item-seg-03{
	background-position: -183px -80px;
    height: 17px;
    margin: 20px 0 0 10px;
    width: 43px;
	}
	
	#shoptimeNovoFooter .item-seg-05{
	margin: 20px 0 0 10px;
	}
	
	#shoptimeNovoFooter .list-segura {
	padding-left: 10px;
    border-left: 1px solid #c9c9c9;
    margin-left: 10px;
	}
	
	#shoptimeNovoFooter .item-seg-04{
	background-position: -245px -5px;
    height: 50px;
    width: 32px;
	}
	
	#shoptimeNovoFooter .list-midia{
	padding: 5px 10px;
    margin-top: 10px;
    border-right: 1px solid #c9c9c9;
	}
	
	#shoptimeNovoFooter .item-midia-01{
	background-position: -148px -141px;
	width: 21px;
	height: 21px;
	}
	
	#shoptimeNovoFooter .item-midia-02{
	background-position: -173px -141px;
	width: 21px;
	height: 21px;
	}
	
	#shoptimeNovoFooter .item-midia-03{
	background-position: -197px -141px;
	width: 21px;
	height: 21px;
	}
	
	#shoptimeNovoFooter .item-midia-04{
	background-position: -221px -141px;
	width: 21px;
	height: 21px;
	}
	
	#shoptimeNovoFooter .item-midia-05{
	background-position: -247px -141px;
	width: 21px;
	height: 21px;	
	}
	
	#shoptimeNovoFooter .item-midia-06{
	background-position: -247px -168px;
	width: 21px;
	height: 21px;
	margin: 15px 10px;
	}
	
	p.footer-txt-final,
	div.footer-sitemap-final{
	font-size:11px;
	text-align: center;
	}
	
	div.footer-sitemap-final a{
	color: #FF9C5A;
	text-decoration: underline;
	}

#shoptimeNovoFooter .snf-errata{
border-bottom: 1px solid #C9C9C9;
margin-bottom: 25px;
padding-bottom: 20px;
overflow: hidden;
}

#shoptimeNovoFooter .snf-errata strong{
color: #FF9C5A;
margin-left: 5px;
}
/*FIM FOOTER MSG ERRATA*/


.justformobile{
	display:none;
	}
	
@media screen and (max-width: 500px){
    .justformobile{
	display:block;
	}
}
@media handheld{
	.justformobile{
	display:block;
	}
}

.flag-new {
     background-position: -243px 0;
    display: inline-block;
    height: 25px;
    margin-left: 3px;
    text-indent: -9999px;
    vertical-align: middle;
    width: 25px;
    }
	
	.shoptime-vantagens{
		margin:14px 0 0 10px; 
		width:251px; height:52px; 
		background:url(http://img.shoptime.com.br/mktshop/home/img/bg-vantagens.jpg) 0 0 no-repeat; 
		position:relative;
	}
	
	.shoptime-vantagens a {
		text-indent:-9999px; 
		overflow:hidden; 
		position:absolute; 
		bottom:0; left:86px; 
		display:block; 
		width:55px; 
		height:13px;
	}
	
	.bannerDinamicoFooter{
		margin:0 auto;
		width:728px; 
		margin-bottom:25px;	
	}
	
	
	.bannerDinamicoFooter span{
		color:#828282;
	}
	</style>
	
	
	
		<div id="shoptimeNovoFooter">
		
		
			<div class="banner-oas">
		<script type="text/javascript">try {OAS_AD('Position1')} catch (exp) {handleException(exp)}</script>		</div>
			
		
		
		<div class="snf-top">
			<a class="shoptime-icones-footer left snf-bt-atendimento" target="_top" href="http://www.shoptime.com.br/central-de-atendimento?WT.mc_id=atendimentoFooter&WT.mc_ev=click">Atendimento</a>
			
			<a class="shoptime-icones-footer left snf-bt-liguecompre" target="_top" href="http://www.shoptime.com.br/estaticapop/232590" onclick="return window.top.abreLightBox(this, 'iframe');">Ligue e Compre 4003-1020</a>
			
		</div><!--.snf-top-->
		
		<div class="snf-middle">
			<div class="snf-middle-left left">
				<div class="left col-institucional">
						<span class="shoptime-icones-footer footer-setinha left">.</span><strong>Institucional:</strong>
						<ul>
							<li><a target="_top" href="http://www.shoptime.com.br/HomeEstatica/politica-de-privacidade">Política de Privacidade</a></li>
							<li><a target="_top" href="http://faq.shoptime.com.br/?A=MTIz&B=&all=true&T=VHJvY2FzIGUgRGV2b2x1w6fDtWVz#MTI0+MTAyMzg=">Política de Troca e Devolução</a></li>
							<li><a target="_top" href="http://faq.shoptime.com.br/faq/?A=MTIz&T=VHJvY2FzIGUgRGV2b2x1w6fDtWVz#MTI0+MjEyMjg=">Direito de Arrependimento</a></li>
<li><a target="_blank" href="http://www.procon.rj.gov.br/">Procon-RJ</a></li>
							<li><a target="_top" href="http://faq.shoptime.com.br/?A=MTg5&B=&all=true&T=UHJvY2Vzc28gZGUgRW50cmVnYQ==#MTkw">Processo de Entrega</a></li>
							<li><a target="_top" href="http://www.vagas.com.br/b2w">Trabalhe Conosco</a></li>
							<li><a target="_top" class="lbIframe02" href="http://www.shoptime.com.br/estaticapop/246350" onclick="return window.top.abreLightBox(this, 'iframe');" data-width="420" data-height="500">Garantia Mais</a></li>
							<li><a target="_top" href="http://www.b2winc.com/">Relação com Investidores</a></li>
							<li><a target="_top" href="http://carrinho.shoptime.com.br/lista-de-casamento/pages/HomePage">Lista de Casamento</a></li>
<li><a class="lightbox" href="http://www.shoptime.com.br/estaticapop/assessoria-imprensa">Assessoria de Imprensa</a></li>
<li><a target="_top" href="http://www.afiliados.com.br/shoptime/ ">Programa de Afiliados</a><span class="shoptime-icones-expansivel flag-new">Novo</span></li>
<li><a target="_top" href="http://www.shoptime.com.br/contrato-compra-e-venda" crmwa_mdc="Home|Footer|contrato_compra_e_venda|">Termos e Condições de <br>  compra e venda de produtos</a></li>
						</ul>
					</div>
					<div class="left col-central">
							<span class="shoptime-icones-footer footer-setinha left">.</span><strong>Central de Negócios:</strong>
						<ul>
							<li><a target="_top" href="http://www.shoptime.com.br/HomeEstatica/ST2010_Publicidade-e-Media-Pack">Publicidade / Media Pack</a></li>
							<li><a target="_top" href="http://www.shoptime.com.br/HomeEstatica/ST2010_Publicidade-e-Media-Pack">Negócios para Empresas</a></li>
							<li><a target="_top" href="http://www.shoptime.com.br/HomeEstatica/Assistencia_Tecnica">Contato dos Nossos Principais Fornecedores</a></li>
						</ul>
					</div>
					<div class="left col-viagens">
							<span class="shoptime-icones-footer footer-setinha left">.</span><strong>Shoptime Viagens:</strong>
						<ul>
							<li><a target="_top" href="http://viagens.shoptime.com.br/passagens-aereas.aspx?utm_source=shoptime&utm_medium=mala&utm_campaign=pas&CPId=linkbuilding_pas">Passagens Áereas</a></li>
							<li><a target="_top" href="http://viagens.shoptime.com.br/hoteis/hoteis.aspx?utm_source=shoptime&utm_medium=link_building&utm_campaign=hot&CPId=linkbuilding_hot">Hotéis</a></li>
							<li><a target="_top" href="http://viagens.shoptime.com.br/pacotes/pacotes-turisticos.aspx?utm_source=shoptime&utm_medium=link_building&utm_campaign=pct&CPId=linkbuilding_pct">Pacotes Turísticos</a></li>
							<li><a target="_top" href="http://viagens.shoptime.com.br/cruzeiros/cruzeiros-maritimos.aspx?utm_source=shoptime&utm_medium=link_building&utm_campaign=cru&CPId=linkbuilding_cru">Cruzeiros Marítimos</a></li>
							<li><a target="_top" href="http://viagens.shoptime.com.br/resorts/resorts.aspx?utm_source=shoptime&utm_medium=link_building&utm_campaign=res&CPId=linkbuilding_res">Resorts</a></li>
						</ul>
					</div>
			</div><!--.snf-middle-left-->
			
			<div class="snf-middle-right left">
				<div class="col-pagamento">
						<span class="shoptime-icones-footer footer-setinha left">.</span><strong>Formas de Pagamento:</strong>
						<ul>
						<li class="left item-pagamento shoptime-icones-footer item-pag-01">Cartão Shoptime</li>
						<li class="left item-pagamento shoptime-icones-footer item-pag-02">Cartão Visa</li>
						<li class="left item-pagamento shoptime-icones-footer item-pag-03">Cartão Master</li>
						<li class="left item-pagamento shoptime-icones-footer item-pag-04">Cartão American Express</li>
						<li class="left item-pagamento shoptime-icones-footer item-pag-05">Cartão Diners Club</li>
						<li class="left item-pagamento shoptime-icones-footer item-pag-06">Cartão Hipercard</li>
						<li class="left item-pagamento shoptime-icones-footer item-pag-07">Cartão Aura</li>
						<li class="left item-pagamento shoptime-icones-footer item-pag-08">Cartão Visa Electron</li>
						<li class="left item-pagamento shoptime-icones-footer item-pag-09">Cartão Banco do Brasil</li>
						<li class="left item-pagamento shoptime-icones-footer item-pag-10">Boleto Bancário </li>
					</ul>
					<div class="both"></div>
					<p>A inclusão de um produto no &quot;carrinho&quot; não garante o preço do produto. No caso de alteração de preço entre a inclusão no &quot;carrinho&quot; e a finalização do pedido, prevalecerá o preço vigente na &quot;finalização&quot; da compra. Carrinho.</p>
				</div>
					<div class="shoptime-vantagens"><a href="http://www.shoptime.com.br/km-de-vantagens" >Clique aqui</a></div>					
			</div><!--.snf-middle-right-->		
		</div><!--.snf-middle-->

					<div>
<script type="text/javascript">try {OAS_AD("Bottom1")} catch (exp) {handleException(exp);}</script>
</div>
		
		<div class="snf-bottom">
			<div class="snf-bottom-left left">
					<div class="col-seguranca">
						<span class="shoptime-icones-footer footer-setinha left">.</span><strong>Segurança e Certificações:</strong>
						<ul>
							<li class="left item-seg-01">
							<div id="armored_website">
							<param id="aw_preload" value="true" />
							<param id="aw_use_cdn" value="true" />
							</div>
							<script type="text/javascript" src="//selo.siteblindado.com/aw.js"></script>
							</li>
							<li class="left shoptime-icones-footer item-seg-02">Site Seguro - Certsign</li>
							<li class="left shoptime-icones-footer item-seg-03">Internet Segura</li>
							<li class="left item-seg-05">
							<script type="text/javascript">try {OAS_AD("Bottom2")} catch (exp) {handleException(exp);}</script>
							</li>
							
							<li class="left list-segura"><a class="shoptime-icones-footer item-seg-04" href="http://www.b2wdigital.com/institucional/companhia-verde" target="_blank">Companhia Verde</a></li>
						</ul>
					</div>
			</div><!--.snf-bottom-left-->
			
			<div class="snf-bottom-right left">
					<div class="col-midias">
						<span class="shoptime-icones-footer footer-setinha left">.</span><strong>Nossas Redes Sociais:</strong>
						<ul>
							<li class="left list-midia"><a class="shoptime-icones-footer item-midia item-midia-01" href="https://www.facebook.com/CanalShoptime?chave=bannervert-facebook&WT.mc_id=bannervert-facebook">Facebook</a></li>
							<li class="left list-midia"><a class="shoptime-icones-footer item-midia item-midia-02" href="https://twitter.com/canalshoptime?chave=bannervert-twitter&WT.mc_id=bannervert-twitter">Twitter</a></li>
							<li class="left list-midia"><a class="shoptime-icones-footer item-midia  item-midia-03" href="https://plus.google.com/+canalshoptime/posts">Google Plus</a></li>
							<li class="left list-midia"><a class="shoptime-icones-footer item-midia item-midia-04" href="http://instagram.com/canalshoptime">Instagram</a></li>
							<li class="left list-midia"><a class="shoptime-icones-footer item-midia item-midia-05" href="http://www.youtube.com/CanalShoptime">YouTube</a></li>
							<li class="left"><a class="shoptime-icones-footer item-midia item-midia-06" href="http://www.shoptime.com/ficadica">Fica a Dica</a></li>
						</ul>
				</div>
			</div><!--.snf-bottom-right-->

			</div><!--.snf-bottom-->


			
			<p class="footer-txt-final">B2W - Companhia Digital / CNPJ: 00.776.574/0006-60 / Inscrição Estadual: 492.513.778.117 / Endereço: Rua Sacadura Cabral, 102 - Rio de Janeiro, RJ - 20081-902 / <a target="_top" href="http://www.shoptime.com.br/HomeEstatica/Atendimento-Form"><img src="http://img.shoptime.com.br/mktshop/home/email-atendimento-shop.png" style="position:relative; top:2px;" /></a><a target="_top" href="http://www.shoptime.com.br/HomeEstatica/atendimento">atendimento ao cliente</a></p>
			
			<div class="mobileVersion">
	
			</div>
			
	</div><!--shoptimeNovoFooter-->

<script type="text/javascript" src="http://busca.shoptime.com.br/js/neemu_plugin.js"></script>
<script>
	 var isMobile = /iP(hone|od)|Android.*Mobile/.test(navigator.userAgent);
	if (/iP(hone|od)|Android.*Mobile/.test(navigator.userAgent)) {
		var sitemapLink = document.getElementsByClassName('mobileVersion');
		if (sitemapLink && sitemapLink.length > 0) {
		sitemapLink[0].innerHTML = sitemapLink[0].innerHTML + "<p> <a href=\"javascript:void(0);\" onclick=\"deleteCookie('NewMobileOptOut');  window.location = window.location.href.replace('http://www.shoptime.com.br','http://m.shoptime.com.br'); return false;\"> Acessar a Versao Mobile </a></p>";
		
		}
	}
	
</script>
				<div class="x68">
				<script type="text/javascript">
                            try {
                                OAS_AD('x68')
                            } catch (exp) {
                                handleException(exp)
                            }
                        </script>
					</div>
	<script type="text/javascript">
    onReady(function () {
        $(function () {
            $(".placeholder").placeholder();
        });
    });
</script>
	<script src="http://img.shoptime.com.br/imagens/loucura-dia/js/app.js" type="text/javascript"></script><div class="sitemap-link">
Produtos exclusivos e demonstração ao vivo. <a href="http://www.shoptime.com.br/mapa-do-site">Mapa do site</a>
</div>
			
		</div>
    <script>
    	function OAS_AD(pos) {} // Impede processamento de banner uma vez que o JQuery foi carregado.
	  </script>
	    <script type="text/javascript" src="http://ishop.s8.com.br/statics-1.69.1.3//catalog/js/jquery-1.10.1.min.js"></script>
	    <script type="text/javascript" src="http://ishop.s8.com.br/statics-1.69.1.3//catalog/js/jquery-p.js"></script>
		<script type="text/javascript" src="http://ishop.s8.com.br/statics-1.69.1.3/catalog/js/catalog.js"></script>
		<script type="text/javascript">
			$(function(){
					$(".lightbox").fancybox({width:420,height:500,autoDimensions:false});
			});
		</script>
	<!-- 296575 general static content shop-->

	
	
<!-- Selo -->
<script type="text/javascript" src="http://apps.shoptime.com.br/media/selo/js/selo.js"></script>
<!-- End -->

<script type="text/javascript" src="http://apps.shoptime.com.br/media/site/helper.script.js"></script>

<script type="text/javascript">
(function() {
    try {
        var viz = document.createElement('script');
        viz.type = 'text/javascript';
        viz.async = true;
        viz.src = ('https:' == document.location.protocol ?'https://ssl.vizury.com' : 'http://www.vizury.com')+ '/analyze/pixel.php?account_id=VIZVRM713';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(viz, s);
        viz.onload = function() {
            pixel.parse();
        };
        viz.onreadystatechange = function() {
            if (viz.readyState == "complete" || viz.readyState == "loaded") {
                pixel.parse();
            }
        };
    } catch (i) {
    }
})();
</script>

<!-- Adobe Marketing Cloud Tag Loader Code -->
<script type="text/javascript">//<![CDATA[
var amc=amc||{};if(!amc.on){amc.on=amc.call=function(){}};
document.write("<scr"+"ipt type=\"text/javascript\" src=\"//www.adobetag.com/d3/v2/ZDMtYjJ3LTYwMC0yNDYt/amc.js\"></sc"+"ript>");
//]]></script>
<!-- End Adobe Marketing Cloud Tag Loader Code -->


<!-- ClickTale Bottom part -->

<script type='text/javascript'>
// The ClickTale Balkan Tracking Code may be programmatically customized using hooks:
// 
//   function ClickTalePreRecordingHook() { /* place your customized code here */  }
//
// For details about ClickTale hooks, please consult the wiki page http://wiki.clicktale.com/Article/Customizing_code_version_2

document.write(unescape("%3Cscript%20src='"+
(document.location.protocol=='https:'?
"https://clicktalecdn.sslcs.cdngc.net/www14/ptc/ef43d8eb-4951-4875-baf0-f78b72d153ab.js":
"http://cdn.clicktale.net/www14/ptc/ef43d8eb-4951-4875-baf0-f78b72d153ab.js")+"'%20type='text/javascript'%3E%3C/script%3E"));
</script>

<!-- ClickTale end of Bottom part -->


<!--Maxymiser script start -->
<script type="text/javascript"src="//service.maxymiser.net/cdn/pakua/submarino/js/mmcore.js"></script>
<!--Maxymiser script end -->

<!--BEGIN RICH RELEVANCE-->
<script type="text/javascript" src="http://media.richrelevance.com/rrserver/js/1.0/p13n.js">
</script>
<script defer type="text/javascript" src="http://ishop.s8.com.br/js/richRelevanceScript.js">
</script>
<!-- END RICH RELEVANCE--><script type="text/javascript">if (!NREUMQ.f) { NREUMQ.f=function() {NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:") + "//" + "js-agent.newrelic.com/nr-100.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-1.newrelic.com","0699e41a46","3104773,3113053","M1NXNxEFXEtRUkRZVgoZZhcREUZLcVJEWVYKGVkMFgdHSlFVX1RQBQ==",0,36,new Date().getTime(),"","","","",""]);</script>
	</body>
</html>