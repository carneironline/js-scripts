onReady(function() {
	
	$.getScript('js/jquery.colorbox-min.js', function(){
		$('.shoplzv-princ-vid-a').click(function(){ 
			var videoId = $(this).data().videoId; 
			$(this).html('<iframe width="853" height="544" src="//www.youtube.com/embed/'+videoId+'?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>');
		});
		
		$('a.shoplzv-vid-a').click(function(){
			var videoId = $(this).data().videoId; 
			var btnBuyUrl = $(this).parent().siblings('.shoplzv-videos-linkprod').find('.shoplzv-prod-a').attr('href');
			
			var html = '<iframe width="560" height="310" src="//www.youtube.com/embed/'+videoId+'?rel=0" frameborder="0" allowfullscreen></iframe><a href="'+btnBuyUrl+'" target="_blank" class="btn buy"></a>';
			
			$.colorbox({html: html, width: '600px', height: '440px', opacity:0.8, className: 'video-p'});
		});

	});

});

