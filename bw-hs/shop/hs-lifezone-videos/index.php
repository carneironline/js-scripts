<?php
include_once('shop-hs-tpl/header.php');

//define('BASEURL', 'http://o.apps.shoptime.com.br/media/hotsites/hs-delicia-cozinha/aprovacao/'); #PRODUÇÃO
define('BASEURL', ''); #HOMOLOGAÇÃO
?>

<!--HS BEGIN-->

	<!--SCRIPTS HS-->
	<link href="<?php echo BASEURL; ?>css/estilo.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>js/colorbox/colorbox.css" />
	<script type="text/javascript" src="<?php echo BASEURL; ?>js/scripts.js"></script>
	<!--END SCRIPTS HS-->



	<div id="shoptimeLifeZoneVideos">
		<div class="shoplzv-topo">
			<p class="irepla shoplzv-logo">Life Zone</p>
			<p class="irepla shoplzv-frase1">Não perca tempo! Conheça alguns dos sucessos Life Zone e leve uma vida muito mais saudavél.</p>
			<p class="irepla shoplzv-frase2">Assista aos nossos vídeos!</p>			
		</div>
		
		<div class="shoplzv-miolo">
		
			<div class="shoplzv-videoprincipal limite">
				<div class="shoplzv-videos-linkvidprinc">
					<a class="shoplzv-princ-vid-a" data-video-id="_vcz6GomePI" /><img src="img/videoprincipal.jpg" /></a>
				</div>	
				<div class="shoplzv-videos-linkprodprinc">
					<a class="shoplzv-princ-prod-a" href="#" /><img src="<?php echo BASEURL; ?>img/comprar-gd.jpg" /></a>
				</div>
			</div><!--.shoplzv-videoprincipal-->
			
					
			<div class="shoplzv-videos limite">
			
				<div class="left shoplzv-videos-box">
					<div class="shoplzv-videos-linkvid">
						<a class="shoplzv-vid-a" data-video-id="AGkUOA6tuew" /><img class="shoplzv-videos-img" src="img/video01.jpg" alt="#" /></a>
					</div>	
					<div class="shoplzv-videos-linkprod">
						<a class="shoplzv-prod-a" href="http://www.google.com.br" /><img src="<?php echo BASEURL; ?>img/comprar-pq.jpg" /></a>
					</div>
				</div><!--.shoplzv-videos-box"-->
				
				<div class="left shoplzv-videos-box">
					<div class="shoplzv-videos-linkvid">
						<a class="shoplzv-vid-a" data-video-id="_vcz6GomePI" /><img class="shoplzv-videos-img" src="img/video02.jpg" alt="#" /></a>
					</div>	
					<div class="shoplzv-videos-linkprod">
						<a class="shoplzv-prod-a" href="#" /><img src="<?php echo BASEURL; ?>img/comprar-pq.jpg" /></a>
					</div>
				</div><!--.shoplzv-videos-box"-->
				
				<div class="left shoplzv-videos-box">
					<div class="shoplzv-videos-linkvid">
						<a class="shoplzv-vid-a" data-video-id="sAYrAu-jnMY" /><img class="shoplzv-videos-img" src="img/video03.jpg" alt="#" /></a>
					</div>	
					<div class="shoplzv-videos-linkprod">
						<a class="shoplzv-prod-a" href="#" /><img src="<?php echo BASEURL; ?>img/comprar-pq.jpg" /></a>
					</div>
				</div><!--.shoplzv-videos-box"-->
				
				<div class="left shoplzv-videos-box">
					<div class="shoplzv-videos-linkvid">
						<a class="shoplzv-vid-a" data-video-id="#" /><img class="shoplzv-videos-img" src="img/video04.jpg" alt="#" /></a>
					</div>	
					<div class="shoplzv-videos-linkprod">
						<a class="shoplzv-prod-a" href="#" /><img src="<?php echo BASEURL; ?>img/comprar-pq.jpg" /></a>
					</div>
				</div><!--.shoplzv-videos-box"-->
				
				<div class="left shoplzv-videos-box">
					<div class="shoplzv-videos-linkvid">
						<a class="shoplzv-vid-a" data-video-id="#" /><img class="shoplzv-videos-img" src="img/video05.jpg" alt="#" /></a>
					</div>	
					<div class="shoplzv-videos-linkprod">
						<a class="shoplzv-prod-a" href="#" /><img src="<?php echo BASEURL; ?>img/comprar-pq.jpg" /></a>
					</div>
				</div><!--.shoplzv-videos-box"-->
				
				<div class="left shoplzv-videos-box">
					<div class="shoplzv-videos-linkvid">
						<a class="shoplzv-vid-a" data-video-id="#" /><img class="shoplzv-videos-img" src="img/video06.jpg" alt="#" /></a>
					</div>	
					<div class="shoplzv-videos-linkprod">
						<a class="shoplzv-prod-a" href="#" /><img src="<?php echo BASEURL; ?>img/comprar-pq.jpg" /></a>
					</div>
				</div><!--.shoplzv-videos-box"-->
				
				<div class="left shoplzv-videos-box">
					<div class="shoplzv-videos-linkvid">
						<a class="shoplzv-vid-a" data-video-id="#" /><img class="shoplzv-videos-img" src="img/video07.jpg" alt="#" /></a>
					</div>	
					<div class="shoplzv-videos-linkprod">
						<a class="shoplzv-prod-a" href="#" /><img src="<?php echo BASEURL; ?>img/comprar-pq.jpg" /></a>
					</div>
				</div><!--.shoplzv-videos-box"-->
				
				<div class="left shoplzv-videos-box">
					<div class="shoplzv-videos-linkvid">
						<a class="shoplzv-vid-a" data-video-id="#" /><img class="shoplzv-videos-img" src="img/video08.jpg" alt="#" /></a>
					</div>	
					<div class="shoplzv-videos-linkprod">
						<a class="shoplzv-prod-a" href="#" /><img src="<?php echo BASEURL; ?>img/comprar-pq.jpg" /></a>
					</div>
				</div><!--.shoplzv-videos-box"-->
				
				
				
				
				
			</div>
		</div>
		
		<div class="shoplzv-rodape">
		</div>
	
	</div><!--#shoptimeLifeZoneVideos-->

<!--HS END-->

<?php
include_once('shop-hs-tpl/footer.php');
?>
