<?php
	class ACESSO{
		var $usuario,
			$marcas,
			$permissao,
			$funcoes,
			$paginacao;
		public function __construct(){
			include_once("usuario.php");
			include_once("marcas.php");
			include_once("permissao.php");
			include_once("funcoes.php");
			include_once("paginacao.php");
			$this->usuario = new USUARIO();
			$this->marcas = new MARCAS();
			$this->permissao = new PERMISSAO();
			$this->funcoes = new FUNCOES();
			$this->paginacao = new PAGINACAO();
		}
		public function start(){
			ini_set("session.use_trans_sid",1);
			ini_set("session.name","p");
			session_start();
		}
		public function login(){
			$this->start();
			if(isset($_SESSION['uid'])){
				if($this->usuario->define((int)$_SESSION['uid']) === false){
					header("Location:index.php");
				}
			}else{
				header("Location:index.php");
			}
		}
		public function deslogar($nomeVariavelSessao){
			$this->start();
			session_destroy();
			session_unregister($nomeVariavelSessao);
			header("Location:index.php");
		}
		public function administrador(){
			if($this->usuario->admin != 1){
				header("Location:logout.php");
			}
		}
	}
?>