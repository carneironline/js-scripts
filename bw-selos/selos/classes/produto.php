<?php
	class PRODUTO{
		var $id,$id_marca,$id_produto;
		private $conecta,$nome_tabela;
		
		public function __construct(){
			include_once("conecta.php");
			$this->nome_tabela = "produto";
			$this->conecta = new CONECTA();
		}
		public function define($id){
			if(is_array($id)){
				$this->id = $id['id'];
				$this->id_marca = $id['id_marca'];
				$this->id_produto = $id['id_produto'];
			}else{
				$this->conecta->start();
				
				$sql = "SELECT * FROM $this->nome_tabela WHERE id = $id";
				
				$query = mysql_query($sql);
				while($linha = mysql_fetch_array($query)){
					$this->id = $linha['id'];
					$this->id_marca = $linha['id_marca'];
					$this->id_produto = $linha['id_produto'];
					return true;
				}
				return false;
			}
		}
		public function defineIdProduto($id_produto,$id_marca){
			$this->conecta->start();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE id_produto = '$id_produto' AND id_marca = $id_marca";
			
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$this->id = $linha['id'];
				$this->id_marca = $linha['id_marca'];
				$this->id_produto = $linha['id_produto'];
				return true;
			}
			return false;
		}
		public function retornaLimit($index,$limit,$id_marca,$q=NULL,$filtro=NULL,$op1=NULL,$op2=NULL){
			$this->conecta->start();
			$array_de_objetos = array();
			
			if($filtro === NULL){
			$sql = "SELECT * FROM $this->nome_tabela WHERE id_marca = {$id_marca} ";
			}else{
				$sql = "SELECT DISTINCT produto.* FROM $this->nome_tabela";
				$sql .= (isset($op2)) ? " LEFT" : " INNER";
				$sql .= "  JOIN cod_selo ON $this->nome_tabela.id = cod_selo.cod_id";
				if(isset($op2)){
					if(empty($op2)){
						$sql .= " WHERE cod_selo.cod_id IS NULL";
					}elseif(!empty($op2)){
						$sql .= " WHERE cod_selo.id_selo = {$op2}";
					}
				}else{
					$sql .= " AND cod_selo.id_marca = {$id_marca}";
				}
				$sql.="  AND $this->nome_tabela.id_marca = {$id_marca}";
			}
			if(!empty($q)){
				$sql.=" AND(";
				$arg = str_replace(array("/"," ",";",".","-","\t","\n"),",",$q);
				$parms = explode(",",trim($arg,","));
				$sq = " $this->nome_tabela.id_produto = ";
				$i=0;
				foreach($parms as $p){
				$i++;
					$con = ($i > 1)	 ? "OR" : "";
					$sql.=" ".$con.$sq.$p." ";
				}
				$sql.=")";
			}
			$sql.=" ORDER BY produto.id DESC";
			if($index !== NULL && $limit !== NULL){
				$sql.= " LIMIT $index,$limit";
			}
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$PRODUTO = new PRODUTO();
				$PRODUTO->define($linha);
				$array_de_objetos[] = $PRODUTO;
			}
			return $array_de_objetos;
		}
		public function retornaTodos($id_marca){
			$this->conecta->start();
			$array_de_objetos = array();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE id_marca = $id_marca";
			
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$PRODUTO = new PRODUTO();
				$PRODUTO->define($linha);
				$array_de_objetos[] = $PRODUTO;
			}
			return $array_de_objetos;
		}
		public function retornaQTD($id_marca){
			$this->conecta->start();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE id_marca = $id_marca";
			$query = mysql_query($sql);
			$num_rows = mysql_num_rows($query);
			
			return (int)$num_rows;
		}
		public function adiciona(){
			$this->conecta->start();
			
			$id_marca = $this->id_marca;
			$id_produto = "'".$this->id_produto."'";
			
			$sql = "INSERT INTO $this->nome_tabela(id,id_marca,id_produto) VALUES (NULL,$id_marca,$id_produto)";
			mysql_query($sql);
		}
		public function edita($id){
			$this->conecta->start();
			
			$id_marca = $this->id_marca;
			$id_produto = "'".$this->id_produto."'";
			
			$sql = "UPDATE $this->nome_tabela
			SET
			id_marca = $id_marca,
			id_produto = $id_produto
			WHERE id = $id";
			mysql_query($sql);
		}
		public function remove($id){
			$this->conecta->start();
			
			$sql = "DELETE FROM $this->nome_tabela WHERE id = $id";
			mysql_query($sql);
		}
		public function removeIdMarca($id_marca){
			$this->conecta->start();
			
			$sql = "DELETE FROM $this->nome_tabela WHERE id_marca = $id_marca";
			mysql_query($sql);
		}
	}
?>