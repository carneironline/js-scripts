<?php
	class PAGINACAO{
		var $p_r,
			$paginas,
			$qtd_max,
			$Prev,
			$Next,
			$resultado,
			$pages,
			$content;
		
		public function __construct(){
			$this->p_r = array();
			$this->p_l = array();
			$this->paginas = array();
		}
		public function ordenacaoPorPaginas($limit,$qtd,$GET=1,$offset=0){
		$valores = array();
		$grupo = $qtd/$limit;
		$round = ceil($grupo);
		$qtd_grupo = ($qtd == 1 && $qtd > 5) ? $round+1 : $round;
		$this->qtd_max = $round;
		$qtdL = 2;
		$p1=0;
		for($i=1;$i<=$qtd_grupo;$i++){
			$valores[] = $i;
			if($i < 2){
				
				$this->Prev = $GET-1;	
			}
			if($i < ($qtdL+3) && $GET < ($qtdL+2)){
			$p1++;
				$this->p_r[] = $i;
				$this->paginas[] = $i;
			}else
				if(isset($GET) && $GET == $valores[$GET]-1 || $GET == $qtd_grupo){
					if($i < $valores[$GET]+1){
						$p2=0;
						for($y=$GET-1;$y<=$GET+$qtdL;$y++){
						$p2++;
							if($y <= $qtd_grupo){
								$this->p_r[] = $y;
								$this->paginas[] = $y;
							}
						}
					}
					if($i < $valores[$GET]+1){
						for($w=$GET-1;$w>=$GET-$qtdL;$w--){
							$this->p_l[] = $w;
						}
					}elseif($GET == $qtd_grupo){
						$this->p_r[0] = $GET-1;
						$this->p_r[1] = $GET;
						$this->paginas[0] = $GET-1;
						$this->paginas[1] = $GET;
						for($w=$GET-1;$w>=$GET-$qtdL;$w--){	
							if($i <= 1){
								$this->p_l[] = $w;
							}	
						}
					}
				}
			}
			if($GET < $round){
				$this->resultado = ($GET*$limit-$limit+1)." - ".($GET*$limit);
				$this->Next = $GET+1;
			}else{
				$this->resultado = ($GET*$limit-$limit+1)." - ".$qtd;
				$this->Next = $GET;
			}
			$valor = $GET;
			$this->pages = '
			<div class="paginacao">
				<ul class="results">
					<li>Resultado(s) '. $this->resultado.' de '.$qtd .'</li>
				</ul>';
				if($qtd > $limit){    
					$this->content.= '<ul class="pageList">';
						$prev = ($valor > 1) ? $this->Prev : 1;
						$this->content.= '<li class="previous"><a onClick="page.refer('. $prev .','. ($set = ($offset > 0) ? ($offset-$limit) : $offset).');"><img alt="Seta para Esquerda" src="skins/images/arrow-left.png" /></a></li>
							<li>P&aacute;ginas:</li>';
						for($a=(sizeof($this->p_l)-1);$a>0;$a--){
           					$this->content.= '<li '; if($valor == $this->paginas[$t]){ $this->content.= 'class="selected"';  } 
						   	$this->content.= '><a onClick="page.refer('.$this->p_l[$a].','. ($this->p_l[$a]*$limit-$limit) .');">'.$this->p_l[$a].'</a></li>';
                        }	 
						for($t=0;$t<sizeof($this->paginas);$t++){
						 
						   $this->content.= '<li '; if($valor == $this->paginas[$t]){ $this->content.= 'class="selected"';  } 
						   $this->content.= '><a onClick="page.refer('.$this->paginas[$t].','. ($this->p_r[$t]*$limit-$limit) .');">'.$this->p_r[$t].'</a></li>';
						   
						}
							$this->content.= '<li class="after"><a onClick="page.refer('. $this->Next .','. ($set = ($offset < ($this->qtd_max*$limit-$limit)) ? ($offset+$limit) : $offset) .');"> <img alt="Seta para Direita" src="skins/images/arrow-right.png" /> </a></li>
					</ul>';
				 }   
			$this->content.= '
			</div>';
		}
	}	
?>