<?php
	class FUNCOES{
		var $classedaVez,
			$validacaoErro;
		public function __construct(){
			$this->classedaVez = 1;		
		}
		public function revezaClasseCss($nomeClasseCss1,$nomeClasseCss2){
			if($this->classeDaVez == 1){
				$this->classeDaVez = 2;
				return $nomeClasseCss1;
			}else{
				$this->classeDaVez = 1;
				return $nomeClasseCss2;
			}
		}
		public function validacaoForm($POSTS,$msgsPosts){	
			for($i=0;$i<sizeof($POSTS);$i++){
				if($POSTS[$i] == ''){
					$this->validacaoErro = $msgsPosts[$i];
					return false;
				}
			}	
		}
		public function nameExt(){
			$extensoes = array(
				0 => 'Excel'
			);
			return $extensoes;
		}
		public function exportFile($dir_path){
			header("Cache-Control: public, must-revalidate");
			header("Pragma:no-cache");
			header("Content-Type:application/vnd.ms-excel");
			header('Content-Length: '.filesize($dir_path));
			header('Content-Disposition:attachment; filename="'.basename($dir_path).'"');
			header("Content-Transfer-Encoding:binary");
			readfile($dir_path);
			exit;
		}
	}
?>