<?php
	class PERMISSAO{
		var $id,$id_usuario,$id_marca,$nivel;
		var $p;
		private $conecta,$nome_tabela;
		
		public function __construct(){
			include_once("conecta.php");
			$this->nome_tabela = "permissao";
			$this->conecta = new CONECTA();
			$this->name = 
			array(
				1 => "Associado",
				//2 => "Intermedi&aacute;rio",
				//3 => "Estagi&aacute;rio"
			);
		}
		public function define($id){
			$this->conecta->start();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE id = $id";
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$this->id = $linha['id'];
				$this->id_usuario = $linha['id_usuario'];
				$this->id_marca = $linha['id_marca'];
				$this->nivel = $linha['nivel'];
				return true;
			}
			return false;
		}
		public function definePorIdUsuario($id_usuario){
			$this->conecta->start();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE id_usuario = $id_usuario";
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$this->id = $linha['id'];
				$this->id_usuario = $linha['id_usuario'];
				$this->id_marca = $linha['id_marca'];
				$this->nivel = $linha['nivel'];
				return true;
			}
			return false;
		}
		public function definePorIdUsuarioEmarca($id_usuario,$id_marca){
			$this->conecta->start();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE id_usuario = $id_usuario AND id_marca = $id_marca";

			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$this->id = $linha['id'];
				$this->id_usuario = $linha['id_usuario'];
				$this->id_marca = $linha['id_marca'];
				$this->nivel = $linha['nivel'];
				return true;
			}
			return false;
		}
		public function retornaPorIdUsuario($id_usuario){
			$this->conecta->start();
			
			$array_de_objetos = array();
			$sql = "SELECT id FROM $this->nome_tabela WHERE id_usuario = $id_usuario ORDER BY id_marca";
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$permissao = new PERMISSAO();
				$permissao->define($linha['id']);
				$array_de_objetos[] = $permissao;
			}
			return $array_de_objetos;
		}
		public function adiciona(){
			$this->conecta->start();
			
			$sql = "INSERT INTO $this->nome_tabela(id,id_usuario,id_marca,nivel) VALUES (NULL,$this->id_usuario,$this->id_marca,$this->nivel)";
			mysql_query($sql);
			$this->define(mysql_insert_id());
		}
		public function edita($id){
			$this->conecta->start();
			
			$sql = "UPDATE $this->nome_tabela
			SET
			id_usuario = $this->id_usuario,
			id_marca = $this->id_marca,
			nivel = $this->nivel
			WHERE id = $id";
			echo $sql."<br>";
			mysql_query($sql);
		}
		public function editaPorIdUsuarioEmarca($id_usuario,$id_marca){
			$this->conecta->start();
			
			$sql = "UPDATE $this->nome_tabela
			SET
			id_usuario = $this->id_usuario,
			id_marca = $this->id_marca,
			nivel = $this->nivel
			WHERE id_usuario = $id_usuario
			AND id_marca = $id_marca";
			mysql_query($sql);
		}
		public function removePorIdUsuario($id_usuario){
			$this->conecta->start();
			
			$sql = "DELETE FROM $this->nome_tabela WHERE id_usuario = $id_usuario";
			mysql_query($sql);
		}
		public function removePorIdUsuarioEmarca($id_usuario,$id_marca){
			$this->conecta->start();
			
			$sql = "DELETE FROM $this->nome_tabela WHERE id_usuario = $id_usuario AND id_marca = $id_marca";
			mysql_query($sql);
		}
	}
?>