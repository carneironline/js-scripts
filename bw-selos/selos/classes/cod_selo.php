<?php
	class COD_SELO{
		var $id,$id_selo,$cod_id,$id_marca,$marca;
		private $conecta,$nome_tabela;
		
		public function __construct($id_marca){
			include_once("conecta.php");
			$this->nome_tabela = "cod_selo";
			$this->conecta = new CONECTA();
			$this->marca = $id_marca;
		}
		public function define($id){
			$this->conecta->start();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE id = $id AND id_marca = $this->marca";
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$this->id = $linha['id'];
				$this->id_selo = $linha['id_selo'];
				$this->cod_id = $linha['cod_id'];
				$this->id_marca = $linha['id_marca'];
				return true;
			}
			return false;
		}
		public function defineCodSelo($cod_id){
			$this->conecta->start();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE cod_id = '$cod_id' AND id_marca = $this->marca";
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$this->id = $linha['id'];
				$this->id_selo = $linha['id_selo'];
				$this->cod_id = $linha['cod_id'];
				$this->id_marca = $linha['id_marca'];
				return true;
			}
			return false;
		}
		public function defineIdSelo($id_selo){
			$this->conecta->start();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE id_selo = $id_selo AND id_marca = $this->marca";
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$this->id = $linha['id'];
				$this->id_selo = $linha['id_selo'];
				$this->cod_id = $linha['cod_id'];
				$this->id_marca = $linha['id_marca'];
				return true;
			}
			return false;
		}
		public function defineCodSeloIdSelo($id_selo,$cod_id){
			$this->conecta->start();
			
			$sql = "SELECT * FROM $this->nome_tabela WHERE id_selo = $id_selo AND cod_id = '$cod_id' AND id_marca = $this->marca";
			
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$this->id = $linha['id'];
				$this->id_selo = $linha['id_selo'];
				$this->cod_id = $linha['cod_id'];
				$this->id_marca = $linha['id_marca'];
				return true;
			}
			return false;
		}
		public function retornaTodosCodId($cod_id){
			$this->conecta->start();
			$array_de_objetos = array();
			
			$sql = "SELECT id FROM $this->nome_tabela WHERE cod_id = '$cod_id' AND id_marca = $this->marca ORDER BY id DESC";
			
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$cod_selo = new COD_SELO($this->marca);
				$cod_selo->define($linha['id']);
				$array_de_objetos[] = $cod_selo;
			}
			return $array_de_objetos;
		}
		public function retornaTodosIdSelo($id_selo){
			$this->conecta->start();
			$array_de_objetos = array();
			
			$sql = "SELECT id FROM $this->nome_tabela WHERE id_selo = $id_selo AND id_marca = $this->marca";
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$cod_selo = new COD_SELO($this->marca);
				$cod_selo->define($linha['id']);
				$array_de_objetos[] = $cod_selo;
			}
			return $array_de_objetos;
		}
		public function retornaCountIdSelo($id_selo){
			$this->conecta->start();
			
			$sql = "SELECT id FROM $this->nome_tabela WHERE id_selo = $id_selo AND id_marca = $this->marca";
			$query = mysql_query($sql);
			$count = mysql_num_rows($query);
			
			return (int)$count;
		}
		public function retornaTodos(){
			$this->conecta->start();
			
			$sql = "SELECT id FROM $this->nome_tabela ORDER by id ASC";
			$query = mysql_query($sql);
			$linha = mysql_num_rows($query);
			$qtd_objetos = (int)$linha;
			
			return $qtd_objetos;
		}
		public function retornaTodosDistinct(){
			$this->conecta->start();
			$array_de_objetos = array();
			
			$sql = "SELECT distinct cod_id FROM $this->nome_tabela WHERE id_marca = $this->marca ORDER BY id DESC";
			$query = mysql_query($sql);
			while($linha = mysql_fetch_array($query)){
				$cod_selo = new COD_SELO($this->marca);
				$cod_selo->defineCodSelo($linha['cod_id']);
				$array_de_objetos[] = $cod_selo;
			}
			return $array_de_objetos;
		}
		public function adiciona(){
			$this->conecta->start();
			
			$id_selo = "'".$this->id_selo."'";
			$cod_id = "'".$this->cod_id."'";
			
			$sql = "INSERT INTO $this->nome_tabela(id,id_selo,cod_id,id_marca) VALUES (NULL,$id_selo,$cod_id,$this->marca)";
			mysql_query($sql);
		}
		public function edita($id){
			$this->conecta->start();
			
			$id_selo = "'".$this->id_selo."'";
			$cod_id = "'".$this->cod_id."'";
			
			$sql = "UPDATE $this->nome_tabela
			SET
			id_selo = $id_selo,
			cod_id = $cod_id,
			id_marca = $this->marca
			WHERE id = $id AND id_marca = $this->marca";
			mysql_query($sql);
		}
		public function removeCodId($cod_id){
			$this->conecta->start();
			
			$sql = "DELETE FROM $this->nome_tabela WHERE cod_id = '$cod_id' AND id_marca = $this->marca";
			mysql_query($sql);
		}
		public function removeIdSelo($id_selo){
			$this->conecta->start();
			
			$sql = "DELETE FROM $this->nome_tabela WHERE id_selo = $id_selo AND id_marca = $this->marca";
			mysql_query($sql);
		}
		public function removeIdMarca(){
			$this->conecta->start();
			
			$sql = "DELETE FROM $this->nome_tabela WHERE id_marca = $this->marca";
			mysql_query($sql);
		}
	}
?>