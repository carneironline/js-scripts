$(function(){
	$('input[name="associar"]').click(function(){
		$("div.box").css('display','block');
	});
	$('.x-tool-close,input[name="ok"]').click(function(){
		$("div.box").css('display','none');
	});
	$('#mAssociar tr').toggle(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	
	/*Mensagens - box obs*/
	$('div.box-response .more').toggle(function(){
		$(".box-obs",this).show("fast");
	},function(){
		$(".box-obs",this).hide("fast");
	});
	/**/
	
	$("input[type=text]").attr("autocomplete","off");
});
function removeUser(id,idDecode){
	var confirma = confirm("Tem certeza que deseja excluir este usuario?");
	if(confirma === true){
		$.ajax({
		   type: "POST",
		   url: 'remove.php',
		   data:'uid='+id,
		   success:function(result){
			   $("#t"+idDecode).hide('slow',function(){
			   		window.location.href=window.location.href;
				});
		   }
		});
		return false;
	}
}
function removeUserLogado(id){
	var confirma = confirm('Voce tem certeza que deseja excluir sua propria conta?');
	if(confirma === true){
		window.location.href="remove.php?uid="+id+"&user=true";
	}
}
function callLogin(event) {
	if(event.keyCode==13) {
		document.forms[0].submit();
	}
}
page={
	refer:function(valor,offset){
		window.location.href="?page="+valor+"&offset="+offset;
	}
}