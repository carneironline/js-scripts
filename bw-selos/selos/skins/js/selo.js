obj={
	mes:["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
	nome_dias:["D", "S", "T", "Q", "Q", "S", "S"],
	init:function(){
		$("#panel ul li").draggable({
			scroll:false,
			zIndex:'21',
			opacity:"0.95",
			helper:"clone",
			start:function(){
				$(this).addClass("selected");
				obj.selected();
			},
			stop:function(){
				$("#panel ul li.selected").removeClass('selected-drag');
				$(this).removeClass('selected');
			}
		});
		$("body,html").keydown(function(e){
			if(e.shiftKey){
				if(e.which == 65 || e.which == 68){
					$("#panel ul li").each(function(){
						if($(this).css("display") != "none"){
							if(e.which == 65){	
								$(this).addClass("selected");
								$("a.selecao").text("Desmarcar todos");
								$("a.selecao").attr("title","(SHIFT+D)");
							}else if(e.which == 68){
								$(this).removeClass("selected");
								$("a.selecao").text("Selecionar todos");
								$("a.selecao").attr("title","(SHIFT+A)");
							}
						}
					});
				}
			}
		});
		$("#panel ul li.content").click(function(e){
			var removed = false;
			if(!e.ctrlKey){
				$("#panel ul li.content").removeClass("selected");
			}else{
				if($(this).attr("class").indexOf("selected") != -1){
					$(this).removeClass("selected");
					removed = true;
				}
			}
			if(removed === false){
				$(this).addClass("selected");
			}
		});
		$("#toolbar li.submenu").mouseover(function(){
			$("ul",this).css("display","block");
		}).mouseout(function(){
			$("ul",this).css("display","none");
		});
		
		$(".breadcump div.op").delegate(".icon-action","click",function(){
			var param = $("input",this).attr("name");
			$("div#"+param).remove();
			$("input#go").click();	
		});
		
		$(".paginacao .pageList").delegate("li","click",function(){
			obj.loader();
			var event =  $("a",this).attr("onclick").split(',');
			
			var eventI = event[0].split('(');
			var index = eventI[1];
			
			var eventO = event[1].split(')');
			var offset = eventO[0];
			
			$("input[name=page]").val(index);
			$("input[name=offset]").val(offset);
			$("input#go").click();
		});
		
		/*Date*/
		$("#dateBegin").datepicker({
			monthNames:obj.mes,
			dayNamesMin:obj.nome_dias,
			dateFormat:"dd/mm/yy",
			changeMonth: false,
			numberOfMonths: 1,
			onClose:function(selectedDate){
				var date = selectedDate.split("/");
				var dateSelect = "31/12/"+date[2];
				$("#dateEnd").datepicker("option",{"minDate":selectedDate,"maxDate":dateSelect});
			}
		});
		$("#dateEnd").datepicker({
			monthNames:obj.mes,
			dayNamesMin:obj.nome_dias,
			dateFormat:"dd/mm/yy",
			changeMonth: false,
			numberOfMonths:1,
			maxDate:"+11m",
			onClose:function(selectedDate){
				$("#dateBegin").datepicker("option","maxDate",selectedDate);
			}
		});
		/**/
		
		obj.readSelo();
		obj.selecao();
	},
	openCodId:function(element){
		$("#"+element).dialog({
			width:800,
			height:600,
			scroll:false,
			resize:function(){
				var size = parseInt($(this).height())-20;
				$("textarea",this).css("height",size+"px");
			},
			buttons:{
				"OK":function(){
					obj.loader();
					obj.saveCodId($("textarea",this).val());
				},
				"Cancelar":function(){
					$(this).dialog("close");
				}
			}
		});
	},
	openSelo:function(element){
		$("#"+element).dialog({
			scroll:false,
			resizable:false,
			buttons:{
				"OK":function(){
					$("form",this).submit();
					$(this).dialog("close");
					obj.loader();
					setTimeout(function(){
						if(element == "edit_selo"){
							var id = $("#edit_selo input[name=id_selo]").val();
							$(".selos_cad img.selo"+id).attr("src",$("#edit_selo input[name=url_imagem]").val());
						}
						obj.readSelo();
					},2000);
				},
				"Cancelar":function(){
					$(this).dialog("close");
				}
			}
		});
	},
	editSelo:function(id,name,url_imagem){
		$(".ui-dialog-titlebar-close").css("display","block");
		$("#edit_selo input[name=nome]").val(name);
		$("#edit_selo input[name=url_imagem]").val(url_imagem);
		$("#edit_selo input[name=id_selo]").val(id);
		obj.openSelo("edit_selo");
	},
	saveCodId:function(content){
		$.ajax({
		   type: "POST",
		   url: "ajax/write_id.php",
		   data:"id_marca="+$("#id_marca").text()+"&id="+content,
		   cache:true,
		   success:function(response){
			   top.location="selo.php";
		   }
		});
	},
	readSelo:function(){
		$.ajax({
		   type: "POST",
		   url: "ajax/read_selo.php",
		   data:"id_marca="+$("#id_marca").text(),
		   cache:true,
		   success:function(response){
			   $("#selos").html(response);
			   obj.activeIcon();
			   obj.dropSelo();
			   obj.closeBoxMsg();
		   }
		});
	},
	saveSelo:function(id_selo,cod_id){
		$.ajax({
		   type: "POST",
		   url: "ajax/write_selo.php",
		   data:"id_selo="+id_selo+"&cod_id="+cod_id+"&id_marca="+$("#id_marca").text(),
		   cache:true,
		   success:function(msg){
			   if(msg != "false"){
				   $("."+cod_id+" .selos_cad").prepend('<img src="'+$("#selos li#"+id_selo+" .img img").attr("src")+'"/>');
					var sumqtd = parseInt($("#selos li#"+id_selo+" .qtd-prods").html())+1;
					$("#selos li#"+id_selo+" .qtd-prods").html(sumqtd);
			   }
		   }
		});
	},
	selecao:function(){
		$("a.selecao").toggle(function(){
			$(this).text("Desmarcar todos");
			$(this).attr("title","(SHIFT+D)");
			$("#panel ul li").each(function(){
				if($(this).css("display") != "none"){
					$(this).addClass("selected");
				}
			});
		},function(){
			$("#panel ul li").removeClass("selected");
			$(this).text("Selecionar todos");
			$(this).attr("title","(SHIFT+A)");
		});
		$("a.selecao").text("Selecionar todos");
	},
	boxMsg:function(msg,w,h){
		$('<div id="boxmsg" title="Mensagem"><p>'+msg+'</p></div>').appendTo('body');
		$("#boxmsg").dialog({
			scroll:false,
			resizable:false,
			draggable:false,
			width:w,
			height:h,
			modal:true,
			close:function(){$(this).remove();}
		});
		if(msg.indexOf("loading.gif") != -1){
			$(".ui-dialog-titlebar-close").css("display","none");
		}
	},
	loader:function(){
		obj.boxMsg('<img class="loader" src="http://admsite.b2w/SiteManagerWeb/resources/images/default/grid/loading.gif" /> Aguarde...',150,110);
	},
	closeBoxMsg:function(){
		$("#boxmsg").remove();
	},
	dropSelo:function(){
		$(".dropSelo").droppable({
			hoverClass: "activeSelo",
			drop:function(event,selo_drop){
				var id_selo = selo_drop.draggable.find(".idprod").text();
				var id = this.id;
				if(id_selo != ""){
					$(".ui-draggable-dragging").hide('slow');
					if($("#panel ul li.selected").length > 1){
						$("#panel ul li.selected").each(function(){
							obj.saveSelo(id,$(".idprod",this).text());
						});
					}else{
						obj.saveSelo(id,id_selo);
					}
				}
			}
		});
	},
	selected:function(){
		$(".ui-draggable-dragging").removeClass("selected");
		var base = $("#panel ul li.selected");
		var text = (base.length == 1) ? "" : "s";
		$(".ui-draggable-dragging .aba-op").prepend('<div class="count">'+base.length+' selecionado'+text+'</div>').css("background-color","#CC0000");
		base.addClass('selected-drag');
	},
	activeIcon:function(){
		$(".icon-action").mouseover(function(){
			$(this).addClass("ui-state-hover");
		}).mouseout(function(){
			$(this).removeClass("ui-state-hover");
		});
	},
	configSelo:function(element,title){
		element = element.replace("ID-","");
		$('<div id="'+element+'" title="ID - '+$("li."+element+" .idprod2").text()+'"><p></p></div>').appendTo('body');
		$("#"+element).dialog({
			width:500,
			scroll:false,
			resize:function(){
				var size = parseInt($(this).height())-20;
				$("table",this).css("height",size+"px");
			},
			buttons:{
				"OK":function(){
					obj.checkedConfigSelo(element);
				},
				"Cancelar":function(){
					$(this).remove();
				}
			},
			close:function(){
				$(this).remove();
			},
			open:function(){
				$(this).css("height","200px");
				obj.readConfigSelo(element);
			}
		});
	},
	readConfigSelo:function(id){
		$.ajax({
		   type: "POST",
		   url: "ajax/config_selo.php",
		   data:"id_marca="+$("#id_marca").text()+"&cod_id="+id,
		   cache:true,
		   success:function(response){
			$("#"+id).html(response);
			$("div#"+id+" .checkall").toggle(function(){
				$(this).attr("checked","checked");
				$("div#"+id+" .checkbox").each(function(){
					$(this).attr("checked","checked");
				});
			},function(){
				$(this).removeAttr("checked");
				$("div#"+id+" .checkbox").each(function(){
					$(this).removeAttr("checked");
				});
			});
			
		   }
		});
	},
	checkedConfigSelo:function(id){
		obj.loader();
		$.ajax({
		   type: "POST",
		   url: "ajax/remove_cod_selo.php",
		   data:"remove=true&cod_id="+id+"&id_marca="+$("#id_marca").text(),
		   cache:true,
		   success:function(response){
			   	$("#panel li."+id+" .selos_cad").html("");
				$(".config-selo input:checked").each(function(){
					if($(this).val() != 0){
						obj.saveSelo($(this).val(),id);
					}
				});
				obj.readSelo();
				$("#"+id).remove();
				obj.closeBoxMsg();
		   }
		});
	},
	removeSelo:function(id){
		var confirma = confirm("Tem certeza que deseja fazer isso?");
		if(confirma === true){
			$.ajax({
			   type: "POST",
			   url: "ajax/remove_selo.php",
			   data:"id_selo="+id+"&id_marca="+$("#id_marca").text(),
			   cache:true,
			   success:function(response){
					$("#selos li#"+id).fadeOut("slow",function(){
						$(this).remove();
						$(".selos_cad .selo"+id).remove();
						if($("#selos ul li").length == 1){
						   obj.seloPriority($("#selos ul li").attr("id"));
						}
					});
			   }
			});
		}
	},
	seloPriority:function(id){
		$.ajax({
		   type: "POST",
		   url: "ajax/edita_prioridade.php",
		   data:"id_selo="+id+"&id_marca="+$("#id_marca").text(),
		   cache:true,
		   success:function(response){
				obj.readSelo();
		   }
		});
	},
	removeAll:function(){
		var confirma = confirm("Tem certeza que deseja remover todos os produtos cadastrados?");
		if(confirma === true){
			obj.loader();
			$.ajax({
			   type: "POST",
			   url: "ajax/remove_todos.php",
			   data:"prod=true&id_marca="+$("#id_marca").text(),
			   cache:true,
			   success:function(response){
				   window.location.href="selo.php";
			   }
			});
		}
	},
	removeAllSelected:function(){
		if($("#panel li.selected").length > 0){
			var confirma = confirm("Tem certeza que deseja fazer isso?");
			if(confirma === true){
				obj.loader();
				var id_produto = "";
				$("#panel li.selected").each(function(){
					id_produto+=$(".idprod2",this).text()+",";
				});
				$.ajax({
				   type: "POST",
				   url: "ajax/remove_todos_produtos_selected.php",
				   data:"id_marca="+$("#id_marca").text()+"&id_produto="+id_produto,
				   cache:true,
				   success:function(response){
					   window.location.href="selo.php";
				   }
				});
			}else{
				$("#panel li").removeClass("selected");
			}
		}else{
			obj.boxMsg('Selecione pelo menos 1 produto',250,100);
		}
	},
	removeAllListed:function(){
		$("#panel li").addClass("selected");
		obj.removeAllSelected();
	},
	removeAllSelos:function(){
		var confirma = confirm("Tem certeza que deseja remover todos os selos incluidos em cada produto?");
		if(confirma === true){
			obj.loader();
			$.ajax({
			   type: "POST",
			   url: "ajax/remove_todos.php",
			   data:"prod=false&id_marca="+$("#id_marca").text(),
			   cache:true,
			   success:function(response){
				   $("#panel .selos_cad").html("");
				   obj.closeBoxMsg();
				   obj.readSelo();
			   }
			});
		}
	},
	removeAllSelosSelected:function(){
		if($("#panel li.selected").length > 0){
			var confirma = confirm("Tem certeza que deseja fazer isso?");
			if(confirma === true){
				obj.loader();
				var cod_id = "";
				$("#panel li.selected").each(function(){
					cod_id+=$(".idprod",this).text()+",";
					$(".selos_cad",this).html("");
				});
				$.ajax({
				   type: "POST",
				   url: "ajax/remove_cod_selo_selecionados.php",
				   data:"id_marca="+$("#id_marca").text()+"&cod_id="+cod_id,
				   cache:true,
				   success:function(response){
					   obj.closeBoxMsg();
					   obj.readSelo();
				   }
				});
			}else{
				$("#panel li").removeClass("selected");
			}
		}else{
			obj.boxMsg('Selecione pelo menos 1 produto',250,100);
		}
	},
	removeAllSelosListed:function(){
		$("#panel li").addClass("selected");
		obj.removeAllSelosSelected();
	},
	admSeloInclude:function(){
		$('<div id="adm-selo" class="adm-selo" title=""><p></p></div>').appendTo('body');
		$("#adm-selo").dialog({
			width:500,
			scroll:false,
			resize:function(){
				var size = parseInt($(this).height())-20;
				$("table",this).css("height",size+"px");
			},
			buttons:{
				"OK":function(){
					var s = ($("#panel li.selected").length > 1) ? "s" : "";
					obj.boxMsg(''
					+'<select onchange="javascript:if(this.value != \'\')obj.closeBoxMsg();obj.admSeloAction(this.value);$(\'#adm-selo\').remove();">'
						+'<option value="">Selecione</option>'
						+'<option value="1">No'+s+' produto'+s+' selecionado'+s+'</option>'
						+'<option value="2">Em todos os produtos</option>'
					+'</select>',250,100);
				},
				"Cancelar":function(){
					$(this).remove();
				}
			},
			close:function(){
				$(this).remove();
			},
			open:function(){
				obj.readConfigSelo('adm-selo');
			}
		});
	},
	admSeloAction:function(action){
		if(action == 1){
			if($("#panel li.selected").length > 0){
				$("#panel li.selected").each(function(){
					var id = $(".idprod",this).text();
					$(".adm-selo").attr("id",id);
					obj.checkedConfigSelo(id);
				});
			}else{
				obj.boxMsg('Selecione pelo menos 1 produto',250,100);
			}
		}else if(action == 2){
			obj.loader();
			var id_selo = "";
			$("#panel .selos_cad").html("");
			$(".config-selo input:checked").each(function(){
				if($(this).val() != 0){
					id_selo += $(this).val()+",";
					var selo = $("#selos #"+$(this).val()+" .img").html();
					$("#panel .selos_cad").prepend(selo);
				}
			});
			$.ajax({
			   type: "POST",
			   url: "ajax/add_selo_all.php",
			   data:"id_marca="+$("#id_marca").text()+"&id_selo="+id_selo,
			   cache:true,
			   success:function(response){
					obj.readSelo();
					obj.closeBoxMsg();
			   }
			});
			
		}
	},
	removeProd:function(cod_id){
		var confirma = confirm("Tem certeza que deseja remover este produto?");
		if(confirma === true){
			$.ajax({
			   type: "POST",
			   url: "ajax/remove_produto.php",
			   data:"id_marca="+$("#id_marca").text()+"&cod_id="+cod_id,
			   cache:true,
			   success:function(response){
				if(response != ""){
					$("textarea.in_cod_id").val(response);
				}
				$("#panel ul li."+cod_id).fadeOut('slow',function(){
					$(this).remove();
					obj.readSelo();
				});
			   }
			});
		}
	},
	sincron:function(){
		obj.loader();
		$.ajax({
		   type: "POST",
		   url: "ajax/sincronizar.php",
		   data:"id_marca="+$("#id_marca").text(),
		   cache:true,
		   success:function(response){
			   obj.closeBoxMsg();
			   obj.boxMsg('Sincroniza&ccedil;&atilde;o efetuada com sucesso, esse processo pode demorar até 24hrs para refletir no site.',400,100);
		   }
		});
	},
	int:function(number){
		var tecla=(window.event)?event.keyCode:number.which;
		if(tecla >= 47 && tecla <= 58 || tecla == 8 || tecla == 0 || tecla == 44 || tecla == 13){
			return true;
		}else{
			return false;
		}
	},
	filter:function(op){
		var url = window.location.href;
		var geturl = (url.indexOf("?")!=-1) ? "&" : "?";
		window.location.href = url+geturl+op+"&filtro";
	}
};