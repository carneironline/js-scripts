<?php include_once("cadastro_action.php") ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cadastrar usuário</title>
<?php include_once("include/style.php") ?>
</head>

<body>
<?php 
include_once("include/top.php");
include_once("include/menu.php");
include_once("include/bread.php");
h_a("Cadastro");

/*BOX*/
if($msg || $_GET['sucess']){
	include_once("include/box.php");
	if($msg){
		l_a('Error',$msg,'<div align="right"><input type="button" name="ok" value="OK" /></div>');
	}else{
		l_a('Aviso',"Cadastro efetuado com sucesso",'<div align="right"><input type="button" name="ok" value="OK" /></div>');
	}
}
/**/

include_once("include/content.php");
c_a("Cadastrar usuário");
?>
<div id="base-form">        
    <form method="post">
        <table>
            <tr>
                <td><label for="nome">Nome:</label></td>
                <td width="100%"><input type="text" name="nome" id="nome" value="<?php echo $_POST['nome'] ?>" /></td>
            </tr>
            <tr>
                <td><label for="login">Login:</label></td>
                <td><input type="text" name="login" id="login" value="<?php echo $_POST['login'] ?>" /></td>
            </tr>
            <tr>
                <td><label for="senha">Senha:</label></td>
                <td><input type="password" name="senha" id="senha" value="<?php echo $_POST['senha'] ?>" /></td>
            </tr>
            <tr>
                <td><label for="adm">Administrador:</label></td>
                <td>
                    <select id="adm" name="adm">
                        <option value="" selected="selected">Selecione</option>
                        <option value="1" <?php if($_POST['adm'] == "1"){ ?> selected="selected" <?php } ?>>Sim</option>
                        <option value="0" <?php if($_POST['adm'] == "0"){ ?> selected="selected" <?php } ?>>Não</option>
                    </select>
                </td>
            </tr>
        </table>
        <div id="area2">
            <div id="top">
                Associar
            </div>
            <table id="mAssociada">
                <tr id="titulo">
                	<td align="center">::</td>
                    <td>Nome</td>
                    <td>Permissão</td>
                </tr>
                <?php $i=0;foreach($p->marcas->nomes as $nomes){ $i++; ?>
                <tr class="mA">
                	<td><?php echo $p->marcas->icon[$i] ?></td>
                    <td> <?php echo $nomes ?></td>
                    <td width="100%">
                        <select name="<?php echo $nomes ?>">
                            <option value="">Selecione</option>
                            <?php foreach($p->permissao->name as $chave=>$obj){ ?>
                            <option value="<?php echo $chave ?>" <?php if($_POST[$nomes] == $chave){ ?> selected="selected" <?php } ?>><?php echo $obj ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <?php } ?>
            </table>
        </div>
     </form>   
</div>
<?php
c_b('<div align="right"><input type="submit" name="submit" onclick="javascript:document.forms[0].submit();" value="Salvar" /></div>');
include_once("include/footer.php");
?>   
</body>
</html>
