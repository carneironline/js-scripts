<?php include_once("entrada_action.php") ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Saudações <?php echo $p->usuario->nome ?></title>
<?php include_once("include/style.php") ?>
<style>
.noL:hover{background-color:transparent;}
.noL td{text-transform:none; padding:10px;}
</style>
<script>
function nextLocation(marca,a){
	window.location.href="?m="+marca+"&e="+a;
}
</script>
</head>

<body>
<?php 
include_once("include/top.php");
include_once("include/menu.php");
include_once("include/bread.php");
h_a("Home");
include_once("include/content.php");
c_a();
?>

<table width="100%" cellpadding="0" cellspacing="2">
	<?php 
	if(sizeof($array_de_marcas)>0){
	foreach($array_de_marcas as $m){ 
	?>
	<tr>
    	<td width="5"><?php echo $p->marcas->icon[$m->id_marca] ?></td>
        <td onclick="javascript:nextLocation('<?php echo base64_encode($m->id_marca) ?>','<?php echo md5($m->id_marca) ?>')"> <?php echo $p->marcas->nomes[$m->id_marca] ?></td>
    </tr>
	<?php }}else{ ?>
    <tr class="noL">
    	<td>Nenhuma marca no perfil cadastrado.
    	<?php if($p->usuario->admin == 1){ ?>
        Vá até <a href="editar.php?uid=<?php echo base64_encode($p->usuario->id) ?>">editar usuário</a> e associe a marca e comece a administrar.
        <?php }else{ ?>
        Contate um administrador da conta.
        <?php } ?>
        </td>
    </tr>
    <?php } ?>
</table>

<?php
c_b();
include_once("include/footer.php");
?>            
</body>
</html>
