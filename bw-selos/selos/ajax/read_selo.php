<?php
include_once("read_selo_action.php"); 
if(sizeof($array_selo)>0){
?>
    <ul>
    <?php foreach($array_selo as $selo_obj){ ?>
        <li class="dropSelo" id="<?php echo $selo_obj->id ?>" data-name="selo<?php echo $selo_obj->id ?>">
        	<div class="pinpoint <?php if($selo_obj->prioridade == 1){ ?> point-yes <?php }else{ ?> point-no <?php } ?>" title="Prioridade" onclick="javascript:obj.seloPriority('<?php echo $selo_obj->id ?>');"></div>
        	<div class="aba-op">
                <div class="ops">
                    <div class="icon-action ui-state-default ui-corner-all" title="Remover" onclick="obj.removeSelo('<?php echo $selo_obj->id ?>');">
                        <span class="ui-icon ui-icon-trash"></span>
                    </div>
                    <div class="icon-action ui-state-default ui-corner-all" title="Editar" onclick="obj.editSelo('<?php echo $selo_obj->id ?>','<?php echo $selo_obj->nome ?>','<?php echo $selo_obj->url_imagem ?>');">
                        <span class="ui-icon ui-icon-pencil"></span>
                    </div>
                    <div class="icon-action ui-state-default ui-corner-all" title="Buscar por <?php echo $selo_obj->nome ?>" onclick="javascript:obj.loader();window.location.href='?op2=<?php echo $selo_obj->id ?>&filtro';">
                        <span class="ui-icon ui-icon-search"></span>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="img">
                    <img src="<?php echo $selo_obj->url_imagem ?>" style="max-width:30px;max-height:30px;" />
                </div>
                <div class="info">
                    <b class="qtd-prods"><?php echo $cod_selo->retornaCountIdSelo((int)$selo_obj->id) ?></b><br /><?php echo $selo_obj->nome ?><br />
                </div>
                <div class="selos_cad"></div>
            </div>
        </li>
    <?php } ?>
    </ul>
<?php } ?>