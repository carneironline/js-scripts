<?php
	if($_POST){
		include_once("../config.php");
		include_once("../classes/cod_selo.php");
		include_once("../classes/produto.php");
		
		$cod_selo = new COD_SELO($_POST['id_marca']);
		$produto = new PRODUTO;

		$id = trim($_POST['id_produto'],",");
		$ids = explode(",",$id);
		foreach($ids as $id){
			$produto->defineIdProduto((int)$id,$_POST['id_marca']);
			$produto->remove((int)$produto->id);
			$cod_selo->removeCodId((int)$produto->id);
		}
	}
?>