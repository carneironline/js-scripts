<?php
	if($_POST){
		
		include_once("../config.php");
		include_once("../classes/cod_selo.php");
		include_once("../classes/produto.php");
		
		$id_marca = $_POST['id_marca'];
		
		$cod_selo = new COD_SELO($id_marca);
		$produto = new PRODUTO;
		
		$cod_selo->removeIdMarca();
		
		$array_de_produtos = array();
		$array_de_produtos = $produto->retornaTodos($id_marca);
		
		$id_selo = explode(",",trim($_POST['id_selo'],","));
		if(!empty($id_selo)){
			foreach($id_selo as $selo){
				echo $selo."\n";
				foreach($array_de_produtos as $prod){
					$cod_selo->id_selo = $selo;
					$cod_selo->cod_id = $prod->id;
					$cod_selo->id_marca = $id_marca;
					$cod_selo->adiciona();
				}
			}
		}
	}
?>