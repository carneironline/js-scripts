<?php
	include_once("../config.php");
	include_once("../classes/produto.php");
	
	$produto = new PRODUTO();
	
	$ids = explode("\n",trim(str_replace(array('"',' ',"\t",","),'',$_POST['id']),"\n"));
	
	foreach($ids as $i){
		if(!empty($i)){
			if($produto->defineIdProduto($i,$_POST['id_marca']) === false){
				$produto->id_produto = $i;
				$produto->id_marca = $_POST['id_marca'];
				$produto->adiciona();
			}
		}
	}
	echo "success";
?>