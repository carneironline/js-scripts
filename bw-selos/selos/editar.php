<?php include_once("editar_action.php") ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Editar usuário</title>
<?php include_once("include/style.php") ?>
</head>

<body>
<?php 
include_once("include/top.php");
include_once("include/menu.php");
include_once("include/bread.php");
h_a("Editar");

/*Marcas*/
include_once("include/box.php");
if($msg){
	l_a('Error',$msg,'<div align="right"><input type="button" name="ok" value="OK" /></div>');
}elseif($sucess){
	l_a('Aviso',$sucess,'<div align="right"><input type="button" name="ok" value="OK" /></div>');
}
/**/

include_once("include/content.php");
c_a("Editar minha conta");
?>
<div id="base-form">        
    <form method="post">
        <table>
            <tr>
                <td><label for="nome">Nome:</label></td>
                <td width="100%"><input type="text" name="nome" id="nome" value="<?php echo $p2->usuario->nome ?>" /></td>
            </tr>
            <tr>
                <td><label for="login">Login:</label></td>
                <td><input type="text" name="login" id="login" value="<?php echo $p2->usuario->login ?>" /></td>
            </tr>
            <tr>
                <td><label for="senha">Senha:</label></td>
                <td><input type="password" name="senha" id="senha" value="" /></td>
            </tr>
            <?php if($p->usuario->admin == 1){ ?>
            <tr>
                <td><label for="adm">Administrador:</label></td>
                <td>
                    <select id="adm" name="adm">
                        <option value="1" <?php if($p2->usuario->admin == 1){ ?> selected="selected" <?php } ?>>Sim</option>
                        <option value="0" <?php if($p2->usuario->admin == 0){ ?> selected="selected" <?php } ?>>Não</option>
                    </select>
                </td>
            </tr>
            <?php } ?>
        </table>
        <?php if($p->usuario->admin == 1){ ?>
        <div id="area2">
            <div id="top">
                Associados
            </div>
            <table id="mAssociada">
                <tr id="titulo">
                	<td align="center">::</td>
                    <td>Nome</td>
                    <td>Permissão</td>
                </tr>
                <?php foreach($p2->permissao->retornaPorIdUsuario((int)$p2->usuario->id) as $obj){ ?>
                <tr class="mA">
                	<td><?php echo $p2->marcas->icon[$obj->id_marca] ?></td>
                    <td> <?php echo $p2->marcas->nomes[$obj->id_marca] ?></td>
                    <td width="100%">
                        <select name="<?php echo $p2->marcas->nomes[$obj->id_marca] ?>">
                        	<option value="">desassociar</option>
                            <?php foreach($p2->permissao->name as $chave=>$b){ ?>
                            <option value="<?php echo $chave ?>" <?php if($obj->nivel == $chave){ ?> selected="selected" <?php } ?>><?php echo $b ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <?php } 
					foreach($p2->marcas->nomes as $chave=>$_obj){
						if($p2->permissao->definePorIdUsuarioEmarca($p2->usuario->id,$chave) === false){
				?>
                <tr class="mA" bgcolor="#CCCCCC" style="opacity:0.5;">
                    <td><?php echo $p2->marcas->icon[$chave] ?></td>
                    <td><?php echo $_obj ?></td>
                    <td width="100%">
                        <select name="<?php echo $p2->marcas->nomes[$chave] ?>">
                        	<option value="">Não associado</option>
                            <?php foreach($p2->permissao->name as $chaveID=>$b){ ?>
                            <option value="<?php echo $chaveID ?>" <?php if($_POST[$p2->marcas->nomes[$chave]] == $chaveID){ ?> selected="selected" <?php } ?>><?php echo $b ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <?php
						}
					}
				?>
                
            </table>
        </div>
        <?php } ?>
     </form>   
</div>
<?php
c_b('<div align="right"><input type="submit" name="submit" onclick="javascript:document.forms[0].submit();" value="Alterar" /></div>');
include_once("include/footer.php");
?>  
</body>
</html>
