<?php
	include_once("config.php");
	//$p->administrador();
	$p2 = new ACESSO();
	$uid = ($p->usuario->admin == 1) ? base64_decode($_GET['uid']) : $p->usuario->id;
	$p2->usuario->define((int)$uid);
	$p2->permissao->define((int)$p2->usuario->id);
	
	if($_POST){
		if($p->funcoes->validacaoForm(
		array(
		$_POST['nome'],
		$_POST['login'],
		$_POST['senha']
		),
		array(
		"O campo <b>nome</b> é obrigatório",
		"O campo <b>login</b> é obrigatório",
		"O campo <b>senha</b> é obrigatório"
		))!== false){
			if($p->usuario->definePorLogin($_POST['login'],$uid) === false){
				$p2->usuario->nome = $_POST['nome'];
				$p2->usuario->login = $_POST['login'];
				$p2->usuario->senha = $_POST['senha'];
				$p2->usuario->admin = ($_POST['adm'] == "") ? $p->usuario->admin : $_POST['adm'];
				$p2->usuario->edita($uid);
		
				for($i=1;$i<=sizeof($p2->marcas->nomes);$i++){
					if(!empty($_POST[$p2->marcas->nomes[$i]])){
						if($p2->permissao->definePorIdUsuarioEmarca($uid,$i) === false){
							$p2->permissao->id_usuario = $uid;
							$p2->permissao->id_marca = $i;
							$p2->permissao->nivel = $_POST[$p2->marcas->nomes[$i]];
							$p2->permissao->adiciona();
						}else{
							$p2->permissao->id_usuario = $uid;
							$p2->permissao->id_marca = $i;
							$p2->permissao->nivel = $_POST[$p2->marcas->nomes[$i]];
							$p2->permissao->editaPorIdUsuarioEmarca((int)$uid,$i);
						}
					}else{
						$p2->permissao->removePorIdUsuarioEmarca((int)$uid,$i);
					}
					if($i == sizeof($p->marcas->nomes)){
						$sucess = "Alteração efetuada com sucesso.";
					}
				}
			}else{
				$msg = "Este login já existe, use outro.";
			}	
		}else{
			$msg = $p->funcoes->validacaoErro;
		}
	}
?>