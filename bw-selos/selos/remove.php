<?php
	include_once("config.php");
	$p->administrador();
	if($_REQUEST['uid']){
		include_once("classes/usuario.php");
		include_once("classes/permissao.php");
		
		$usuario = new USUARIO();
		$permissao = new PERMISSAO();
		
		$uid = base64_decode($_REQUEST['uid']);
		$usuario->define($uid);
		$permissao->removePorIdUsuario((int)$usuario->id);
		$usuario->remove($uid);
		
		if(isset($_GET['user'])){
			header("Location:logout.php");
		}
	}
?>