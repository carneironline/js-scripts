<?php include_once("index_action.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<?php include_once("include/style.php") ?>
</head>

<body>
<?php 
include_once("include/top.php");
include_once("include/bread.php");
h_a("Login");
?>
<div id="mainContent">
    <div id="toolsLoginForm">
        <form class="boxLogin" method="post">
        	<?php if($msg){ ?>
        	<h4 class="error"><?php echo $msg ?></h4>
            <?php } ?>
            <div class="wide">
                <label for="j_username">Seu Login</label>
                <input type="text" name="login" value="<?php echo $_POST['login'] ?>" id="j_username">
            </div>
            <div class="wide">
                <label for="j_password">Sua Senha</label>
                <input type="password" onkeypress="return callLogin(event)" name="senha" id="j_password">
            </div>
            <div class="buttons">
                <a class="button2" href="javascript:document.forms[0].submit();"><strong>Enviar</strong></a>
            </div>
        </form>
    </div>
</div>
<?php
include_once("include/footer.php");
?> 
</body>
</html>
