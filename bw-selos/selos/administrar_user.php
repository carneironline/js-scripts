<?php include_once("config.php") ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Minha conta</title>
<?php include_once("include/style.php") ?>
<?php if($p->usuario->admin == 1){ ?>
<?php } ?>
<style>
tr{cursor:default}
tr td{text-transform:none;}
</style>
</head>

<body>
<?php 
include_once("include/top.php");
include_once("include/menu.php");
include_once("include/bread.php");
h_a("Minha conta");

include_once("include/content.php");
?>
<div id="base-form">      
    <div id="area2">
        <table width="100%" cellpadding="0" cellpadding="0" id="mAssociada">
            <tr id="titulo">
                <td>Nome</td>
                <td>Acesso ao sistema</td>
                <td>Permissão de acesso a marca</td>
                <td>Administrar</td>
            </tr>
            <tr class="mA" id="t<?php echo $p->usuario->id ?>">
                <td><?php echo $p->usuario->nome ?></td>
                <td><?php echo $adm = ($p->usuario->admin == 1) ? "Administrador" : "Usuário" ?></td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <?php 
                                foreach($p->marcas->nomes as $chave=>$_obj){
                                if($p->permissao->definePorIdUsuarioEmarca((int)$p->usuario->id,$chave) !== false){
                            ?>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td><?php echo $p->marcas->icon[$chave] ?></td>
                                    </tr>
                                </table>
                            </td>
                            <?php }} ?>
                            <?php if($p->permissao->definePorIdUsuario((int)$p->usuario->id) === false){ ?>
                            <td>Nenhuma marca associada</td>
                            <?php } ?>
                        </tr>
                    </table>
                </td>
                <td>
                    <input type="button" name="editar" value="Editar" onclick="javascript:window.location.href='editar.php?uid=<?php echo base64_encode($p->usuario->id) ?>'" />			
                    <?php if($p->usuario->admin == 1){ ?>
                    <input type="button" name="excluir" value="Excluir" onclick="removeUserLogado('<?php echo base64_encode($p->usuario->id) ?>')" />
                    <?php } ?>
                </td>
            </tr>
        </table>
    </div>  
</div>
<?php
include_once("include/footer.php");
?>   
</body>
</html>