<?php 
	$array_de_marcas = array();
	$array_de_marcas = $p->permissao->retornaPorIdUsuario((int)$_SESSION['uid']);
?>
<ul id="navMenu" class="mainMenu" style="position:relative;z-index:21;">
	<li><a href="entrada.php">Home</a></li>
	<?php if($p->usuario->admin == 1){ ?>
    <li><a href="administrar.php">Usu&aacute;rio</a>
        <ul>
            <li><a href="cadastro.php">Cadastrar</a></li>
            <li><a href="administrar.php">Administrar</a></li>
        </ul>
    </li>
    <?php } ?>
    <li><a href="administrar_user.php">Minha conta</a>
        <ul>
            <li><a href="editar.php?uid=<?php echo base64_encode($p->usuario->id) ?>">Editar</a></li>
            <?php if($p->usuario->admin == 1){ ?>
            <li><a href="javascript:removeUserLogado('<?php echo base64_encode($p->usuario->id) ?>');">Remover</a></li>
            <?php } ?>
        </ul>
    </li>
    <?php if(sizeof($array_de_marcas)>0){?>
    <li><a href="entrada.php">Acesso</a>
        <ul>
        	<?php foreach($array_de_marcas as $m){ ?>
            <li style="text-transform:capitalize;"><a href="entrada.php?m=<?php echo base64_encode($m->id_marca) ?>&e=<?php echo md5($m->id_marca) ?>"><?php echo $p->marcas->nomes[$m->id_marca] ?></a></li>
            <?php } ?>
        </ul>
    </li>
    <?php } ?>
</ul>