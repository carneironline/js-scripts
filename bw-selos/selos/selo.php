<?php include_once("selo_action.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Saudações <?php echo $p->usuario->nome ?></title>
<?php include_once("include/style.php") ?>
<link rel="stylesheet" type="text/css" href="skins/css/selo.css" />
<link rel="stylesheet" type="text/css" href="skins/css/jquery-ui-1.8.21.custom.css" />
<script type="text/javascript" src="skins/js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="skins/js/selo.js"></script>
<script type="text/javascript" >$(function(){obj.init();$(".base-panel").mCustomScrollbar({updateOnContentResize:true});})</script>
<style>
td{border:0;background:none;padding:0;}
tr:hover{background:none;border:0;}
</style>
</head>

<body>
<?php
include_once("include/top.php");
include_once("include/menu.php");
include_once("include/bread.php");
h_a($p->marcas->icon[$id_marca]);
include_once("include/content.php");

if(isset($_GET['filtro'])){
	$breadcump = '<div class="breadcump">';
	if(isset($_GET['op1'])){
		$breadcump .= '
		<div id="op1" class="op">
			<div class="text">Com Selo</div>
			<div class="icon-action ui-state-default ui-corner-all">
				<span class="ui-icon ui-icon-trash"></span>
				<input type="hidden" value="true" name="op1" />
				<input type="hidden" name="filtro" value="true" />
			</div>
		</div>';
	}if(isset($_GET['op2'])){
		$nameOp = (!empty($_GET['op2'])) ? ($selo->define((int)$_GET['op2']) === true) ? '<img src="'.$selo->url_imagem.'" width="15" height="15" style="vertical-align:bottom;" /> Selo' : "Sem Selo" : "Sem Selo";
		$breadcump .= '
		<div id="op2" class="op">
			<div class="text">'.$nameOp.'</div>
			<div class="icon-action ui-state-default ui-corner-all">
				<span class="ui-icon ui-icon-trash"></span>
				<input type="hidden" value="'.$_GET['op2'].'" name="op2" />
				<input type="hidden" name="filtro" value="true" />
			</div>
		</div>';
	}
	$breadcump .= '</div>';
	$style_input = 'style="padding-left:110px;width:94%;"';
}
c_a('
<form method="get">
	<div id="filtro">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="100%">
					<input type="text" name="q" placeholder="Busque por 1 ou mais ids separados por vírgula" value="'.$_GET['q'].'" onkeypress="return obj.int(event);" class="inp" '.$style_input.' />
					'.$breadcump.'
				</td>
				 <td>
					<input style="padding:2px;margin-left:5px;" type="submit" id="go" value="Buscar" />
				</td>
			</tr>
		</table>
		<input type="hidden" name="page" value="1" />
		<input type="hidden" name="offset" value="0" />
	</div>');
?>
<div id="selo-global">
	<span id="id_marca"><?php echo $id_marca ?></span>
    <iframe style="display:none;" name="inselo"></iframe>
    <div id="toolbar">
    	<ul id="navMenu" class="mainMenu">
            <li><a href="#">Produtos (IDs)</a>
                <ul>
                	<li><a onclick="obj.openCodId('in_cod_id');">Incluir ID</a></li>
                    <?php if(sizeof($array_de_produtos)>0){ ?>
                    <?php if($p->usuario->id == 1){ ?>
                    <li><a onclick="obj.boxMsg('<textarea style=border:0;width:100%;height:150px><?php if(sizeof($array_de_produtos)>0){foreach($array_de_produtos2 as $obj){ echo $obj->id_produto."/";}} ?></textarea>');">Visualizar IDs</a></li>
                    <?php } ?>
                    <li class="last"><a onclick="obj.loader();" href="?op1&filtro">Com selo</a></li>
                    <li class="last"><a onclick="obj.loader();" href="?op2&filtro">Sem selo</a></li>
                    <li><a onclick="obj.removeAllSelected();">Remover todos os produtos selecionados</a></li>
                    <li><a onclick="obj.removeAllListed();">Remover todos os produtos desta lista</a></li>
                    <li><a onclick="obj.removeAll();">Remover todos os produtos</a></li>
                    <?php } ?>
                </ul>
            </li>
            <li><a href="#">Selos</a>
                <ul>
                	<li><a onclick="obj.openSelo('in_selo');">Incluir</a></li>
                    <?php if(sizeof($array_de_produtos)>0){ ?>
                    <li><a onclick="obj.admSeloInclude();">Administrar os produtos selecionados</a></li>
                    <li><a onclick="obj.removeAllSelosSelected();">Remover todos os selos selecionados</a></li>
                    <li><a onclick="obj.removeAllSelosListed();">Remover todos os selos desta lista</a></li>
                    <li><a onclick="obj.removeAllSelos();">Remover todos os selos</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php if(sizeof($array_de_produtos)>0){ ?>
            <li><a title="(SHIFT+A)" class="selecao"></a></li>
            <li>
            	<a onclick="obj.sincron();">Sincronizar</a>
            </li>
            <?php } ?>
        </ul>
    </div>
    <?php if(sizeof($array_de_produtos)>0){ ?>
    <?php echo $pages.$htm; ?>
	<div id="panel" class="base-panel">
    	<ul>
        	<?php foreach($array_de_produtos as $prod){ ?>
            <li class="<?php echo $prod->id ?> <?php echo $prod->id_produto ?> content">
            	<div class="aba-op">
                	<div class="ops">
                        <div class="icon-action ui-state-default ui-corner-all" title="Remover" onclick="obj.removeProd('<?php echo $prod->id ?>');">
                            <span class="ui-icon ui-icon-trash"></span>
                        </div>
                        <div class="icon-action ui-state-default ui-corner-all" title="Configurar" onclick="obj.configSelo('ID-<?php echo $prod->id ?>','');">
                            <span class="ui-icon ui-icon-gear"></span>
                        </div>
                    </div>
                </div>
                <div class="ref-id">
                	<p class="idprod" style="display:none;"><?php echo $prod->id ?></p>
                    <p class="idprod2"><?php echo $prod->id_produto ?></p>
                    <div class="selos_cad">
						<?php
                        $array_cod_selos = array();
                        $array_cod_selos = $cod_selo->retornaTodosCodId($prod->id);
                        if(sizeof($array_cod_selos)>0){
                            foreach($array_cod_selos as $obj_cod_selo){
                                $selo->define((int)$obj_cod_selo->id_selo);
                                echo '<span class="selo'.$selo->id.'" style="display:none;">selo'.$selo->id.'</span><img class="selo'.$selo->id.'" src="'.$selo->url_imagem.'" />';
                            }
                        }
                        ?>
                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>
    <?php }else{ ?>
    	<div class="no-result">Nenhum produto cadastrado</div>
    <?php } ?>
    <div id="selos" class="base-panel">
    </div>
</div>
</form>
<div id="in_selo" title="Adicionar Selo">
    <form method="post" action="adiciona_selo.php" target="inselo" name="add_selo">
        <table width="100%">
            <tr>
                <td valign="top" width="10">Nome:</td>
                <td><input type="text" name="nome" value="" /></td>
            </tr>
            <tr>
                <td valign="top" width="10">URL imagem:</td>
                <td>
                <input type="text" name="url_imagem" value="" />
                <input type="hidden" name="id_marca" value="<?php echo $id_marca ?>" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="edit_selo" title="Editar Selo">
    <form method="post" action="edita_selo.php" target="inselo" name="edit_selo">
        <table width="100%">
            <tr>
                <td valign="top" width="10">Nome:</td>
                <td><input type="text" name="nome" value="" /></td>
            </tr>
            <tr>
                <td valign="top" width="10">URL imagem:</td>
                <td>
                <input type="text" name="url_imagem" value="" />
                <input type="hidden" name="id_selo" value="" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="in_cod_id" title="ID produto">
    <form method="post">
        <table width="100%">
            <tr>
                <td valign="top" width="10">IDs:</td>
                <td>
                <textarea name="in_cod_id" class="in_cod_id" style="width:100%;height:498px;"></textarea>
                </td>
            </tr>
        </table>
    </form>
</div>
<?php
c_b();
include_once("include/footer.php");
?>
<script>
setTimeout(function(){
	$(window).scrollTop($(window).height());
},200);
</script>            
</body>
</html>
