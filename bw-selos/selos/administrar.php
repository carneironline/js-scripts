<?php include_once("administrar_action.php") ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrar usuários</title>
<?php include_once("include/style.php") ?>
<style>
tr{cursor:default}
tr td{text-transform:none;}
.td1{
	background-color:#EEEEEE;
}
</style>
</head>

<body>
<?php 
include_once("include/top.php");
include_once("include/menu.php");
include_once("include/bread.php");
h_a("Editar");

include_once("include/content.php");
if(sizeof($array_de_usuarios)>0){
echo $pages;
echo $htm;
?>
<div id="base-form">      
    <div id="area2">
        <table width="100%" cellpadding="0" cellspacing="0" id="mAssociada">
            <tr id="titulo">
                <td>Nome</td>
                <td>Acesso ao sistema</td>
                <td>Permissão de acesso a marca</td>
                <td>Administrar</td>
            </tr>
            <?php
            foreach($array_de_usuarios as $obj){
            ?>
            <tr class="mA <?php echo $p->funcoes->revezaClasseCss("td1","td2") ?>" id="t<?php echo $obj->id ?>" <?php if($obj->id == $p->usuario->id){ ?> style="background-color:#CCCCCC !important;" <?php } ?>>
                <td><?php echo $obj->nome ?></td>
                <td><?php echo $adm = ($obj->admin == 1) ? "Administrador" : "Usuário" ?></td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <?php 
                                foreach($p2->marcas->nomes as $chave=>$_obj){
                                if($p2->permissao->definePorIdUsuarioEmarca((int)$obj->id,$chave) !== false){
                            ?>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td><?php echo $p2->marcas->icon[$chave] ?></td>
                                    </tr>
                                </table>
                            </td>
                            <?php }} ?>
                            <?php if($p2->permissao->definePorIdUsuario((int)$obj->id) === false){ ?>
                            <td>Nenhuma marca associada</td>
                            <?php } ?>
                        </tr>
                    </table>
                </td>
                <td>
                	<?php if($obj->admin == 0 || $p->usuario->id == 1){ ?>
                    <input type="button" name="editar" value="Editar" onclick="javascript:window.location.href='editar.php?uid=<?php echo base64_encode($obj->id) ?>'" />
                    <input type="button" name="excluir" value="Excluir" onclick="<?php if($obj->id != $p->usuario->id){ ?>removeUser('<?php echo base64_encode($obj->id) ?>',<?php echo $obj->id ?>)<?php }else{ ?>removeUserLogado('<?php echo base64_encode($obj->id) ?>');<?php } ?>" />
                    <?php } ?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>   
</div>
<?php
echo $htm;
}else{
	echo 'Nenhum usuário cadastrado';
}
include_once("include/footer.php");
?>   
</body>
</html>