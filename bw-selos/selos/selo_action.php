<?php
	include_once("config.php");
	include_once("classes/selo.php");
	include_once("classes/cod_selo.php");
	include_once("classes/produto.php");
	
	$id_marca = $_SESSION['id_marca'];
	
	$selo = new SELO();
	$cod_selo = new COD_SELO($id_marca);
	$produto = new PRODUTO();
	
	$marca = $p->marcas->nomes[$id_marca];
	
	$valor = (empty($_GET['page'])) ? 1 : $_GET['page'];
	$offset = (empty($_GET['offset'])) ? 0 : $_GET['offset'];
	$q = (empty($_GET['q'])) ? NULL : $_GET['q'];
	$op1 = (!isset($_GET['op1'])) ? NULL : true;
	$op2 = $_GET['op2'];
	$filtro = (!isset($_GET['filtro'])) ? NULL : true;
	
	$limit = 100;
	$array_de_produtos = array();
	$array_de_produtos = $produto->retornaLimit($offset,$limit,$id_marca,$q,$filtro,$op1,$op2);
	
	$array_de_produtos2 = array();
	$array_de_produtos2 = $produto->retornaLimit(NULL,NULL,$id_marca,$q,$filtro,$op1,$op2);
	
	$QTD = sizeof($array_de_produtos2);
	$paginas = $p->paginacao->ordenacaoPorPaginas($limit,$QTD,$valor,$offset);
	$pages = $p->paginacao->pages;
	$htm = $p->paginacao->content;
?>