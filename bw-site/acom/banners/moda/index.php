<?php
include_once('acom-hs-tpl/header.php');

define('BASEURL', 'http://img.americanas.com.br/mktacom/hotsite/hs-marketplace/');
//define('BASEURL', '');
?>

<!--HS BEGIN-->

<style type="text/css">
.app-product {
	margin-bottom:40px;
	border:#000 solid 1px;
}
h2 {
	font-size:40px;
	color:#f00;
}
h3 {
	font-size:20px;
	margin-bottom:20px;
	color:#00f;
	border-bottom:#00f solid 1px;
}

/* TPL MODA 1 */ 
.tpl-moda-1 {
	width:870px;
	height:300px;
	position:relative;
}
.tpl-moda-1 .product-container .product-image {
	height:300px;
	position:absolute;
	top:0;
	left:100px;
}
.tpl-moda-1 .product-container .product-title {
	position:absolute;
	top:57px;
	left:480px;
	font-size:18px;
	font-weight:bold;
}
.tpl-moda-1 .product-container .product-info {
	position:absolute;
	top:128px;
	left:480px;
	font-size:18px;
	font-weight:bold;
}
.tpl-moda-1 .product-container .product-price-value-wrapper {
	color:#ea0104;
	border:none !important;
}
.tpl-moda-1 .product-container .product-price-value {
	font-size:50px;
}
.tpl-moda-1 .product-container .product-price-value .product-price-cents {
	font-size:30px;
}
.tpl-moda-1 .product-container .product-info .product-price-secondary {
	display:none;
}
.tpl-moda-1 .product-info .btn-buy {
	display: block;
	margin-top: 40px;
	font-weight: 700;
	font-size: 25px;
	color: #e60014;
	float: none;
	padding-left:0;
}
.tpl-moda-1 .product-info .btn-buy .icon-shopping-cart {
	background-image: url(http://iacom.s8.com.br/mktacom/apps/v0.1.8/images/icon-s96de46d3ea.png);
	background-position: 0 -207px;
	background-repeat: no-repeat;
	display: block;
	height: 26px;
	width: 28px;
	float: left;
	margin-right: 10px;
	margin-top: -2px;
	margin-left:0;
}

/* TPL MODA 2 */ 
.tpl-moda-2 {
	width:310px;
	height:292px;
	position:relative;
}
.tpl-moda-2 .product-container .product-image {
	width:215px;
	position:relative;
	margin-bottom:18px;
}
.tpl-moda-2 .product-container .product-title {
	width:180px;
	position:relative;
	padding:0;
	height: 48px;
	overflow: hidden;
}
.tpl-moda-2 .product-container .product-info {
	width:110px;
	position: absolute;
	padding: 0;
	top: 234px;
	left: 194px;
}
.tpl-moda-2 .product-container .product-price-secondary {
	display:none;
}
.tpl-moda-2 .product-container .product-price-value-wrapper {
	color:#ea0104;
	border:none !important;
}

/* TPL MODA 3 */ 
.tpl-moda-3 {
	width:230px;
	height:490px;
	position:relative;
}
.tpl-moda-3 .product-container .product-image {
	width:215px;
	position:relative;
	margin-bottom:70px;
}
.tpl-moda-3 .product-container .product-title {
	width:156px;
	position:relative;
	padding:0;
	margin: 0 auto;
	margin-bottom:15px;
}
.tpl-moda-3 .product-container .product-info {
	width:134px;
	position:relative;
	padding:0;
	margin: 0 auto;
}
.tpl-moda-3 .product-container .product-price-secondary {
	display:none;
}
.tpl-moda-3 .product-container .product-price-value-wrapper {
	color:#ea0104;
	border:none !important;
}
.tpl-moda-3 .product-container .product-price-value {
	font-size: 40px !important;
}

/* TPL MODA SUBLOJA 1 */ 
.tpl-moda-subloja-1 {
	width:700px;
	height:290px;
	position:relative;
}
.tpl-moda-subloja-1 .product-container .product-image {
	height:290px;
	position:absolute;
	top:0;
	left:42px;
}
.tpl-moda-subloja-1 .product-container .product-title {
	position:absolute;
	top:54px;
	left:390px;
	font-size:18px;
	font-weight:bold;
}
.tpl-moda-subloja-1 .product-container .product-info {
	position:absolute;
	top:122px;
	left:390px;
	font-size:18px;
	font-weight:bold;
}
.tpl-moda-subloja-1 .product-container .product-price-value-wrapper {
	color:#ea0104;
	border:none !important;
}
.tpl-moda-subloja-1 .product-container .product-price-value {
	font-size:50px;
}
.tpl-moda-subloja-1 .product-container .product-price-value .product-price-cents {
	font-size:30px;
}
.tpl-moda-subloja-1 .product-container .product-info .product-price-secondary {
	display:none;
}
.tpl-moda-subloja-1 .product-info .btn-buy {
	display: block;
	margin-top: 40px;
	font-weight: 700;
	font-size: 25px;
	color: #e60014;
	float: none;
	padding-left:0;
}
.tpl-moda-subloja-1 .product-info .btn-buy .icon-shopping-cart {
	background-image: url(http://iacom.s8.com.br/mktacom/apps/v0.1.8/images/icon-s96de46d3ea.png);
	background-position: 0 -207px;
	background-repeat: no-repeat;
	display: block;
	height: 26px;
	width: 28px;
	float: left;
	margin-right: 10px;
	margin-top: -2px;
	margin-left:0;
}

/* TPL MODA SUBLOJA 2 */ 
.tpl-moda-subloja-2 {
	width:248px;
	height:250px;
	position:relative;
}
.tpl-moda-subloja-2 .product-container .product-image {
	width:171px;
	position:relative;
	margin-bottom:18px;
}
.tpl-moda-subloja-2 .product-container .product-title {
	width:118px;
	position:relative;
	padding:0;
	height: 48px;
	overflow: hidden;
	margin-left: 10px;
}
.tpl-moda-subloja-2 .product-container .product-info {
	width:110px;
	position: absolute;
	padding: 0;
	top: 186px;
	left: 140px;
}
.tpl-moda-subloja-2 .product-container .product-price-secondary {
	display:none;
}
.tpl-moda-subloja-2 .product-container .product-price-value-wrapper {
	color:#ea0104;
	border:none !important;
}
</style>

<h2>HOME---------------------------------------------</h2>

<h3>MODA TEMPALTE 1</h3>
<div class="app-product tpl-moda-1 " data-product-id="119698354"></div>

<h3>MODA TEMPALTE 2</h3>
<div class="app-product  tpl-moda-2 product-price-invoice" data-product-id="119841245"></div>

<h3>MODA TEMPALTE 3</h3>
<div class="app-product  tpl-moda-3 product-price-invoice" data-product-id="119841245"></div>

<h2>SUBLOJA---------------------------------------------</h2>

<h3>MODA SUBLOJA TEMPALTE 1</h3>
<div class="app-product tpl-moda-subloja-1 " data-product-id="119698354"></div>

<h3>MODA SUBLOJA TEMPALTE 2</h3>
<div class="app-product tpl-moda-subloja-2 " data-product-id="118054127"></div>

<!--HS END-->

<?php
include_once('acom-hs-tpl/footer.php');
?>
