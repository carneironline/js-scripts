
<!DOCTYPE html >
<!--[if IE 7 ]><html lang="pt-br" class="ie7"> <![endif]--><!--[if IE 8 ]><html lang="pt-br" class="ie8"> <![endif]--><!--[if gt IE 9]><!--><html lang="pt-br">
<!--<![endif]--><head><script type="text/javascript">var NREUMQ=NREUMQ||[];NREUMQ.push(["mark","firstbyte",new Date().getTime()]);</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="UTF-8" />			
			<title>Moda - Americanas.com</title>
			<meta name="keywords" content="Moda, Americanas.com" />
			<meta name="description" content="Confira as melhores ofertas de Moda na Americanas.com. A Maior loja da Internet com os Menores Precos do Mercado" />
				<link href="http://www.americanas.com.br/loja/358352/moda" rel="canonical" />	
	<meta name="apple-itunes-app" content="app-id=476307592" />
	<!--[if lt IE 9]>
		<script src="http://iacom.s8.com.br/statics-1.14.7/catalog/js/html5shiv.js">
		</script>
	<![endif]-->
	<link rel="stylesheet" href="http://iacom.s8.com.br/statics-1.14.7/catalog/css/v1/catalog.base.min.css" />
	<script src="http://iacom.s8.com.br/statics-1.14.7/catalog/js/v1/vendor/jquery/jquery.js"></script>
	
	<script type="text/javascript">
		$.ajaxSetup({
			cache: true
		});
        var readyEvents = [];
        var app_loaded = false;
        function onReady(func) {
          if (func) readyEvents.push(func);
          if (app_loaded) {
            if (readyEvents.length > 0) {
              for (var f in readyEvents) {
                readyEvents[f].call(this);
              }
              readyEvents = [];
            }
          }
        }
        </script>
        
		<script>
			OAS_query = 'loja/358352/moda'; 
			OAS_sitepage = 'americanas/moda_v2';
			APP_OAS = true;
		</script>
   
	<link rel="stylesheet" href="http://iacom.s8.com.br/mktacom/apps/v0.1.8/styles/main_new.css"/>
<script type="text/javascript">
var APP_DEBUG = true;
    var OAS_url = 'http://oas.americanas.com.br/RealMedia/ads/adstream_mjx.ads/'; //Apenas para desenvolvimento
</script>

<script src="http://iacom.s8.com.br/mktacom/apps/v0.1.8/scripts/app.min.v2.js"></script>
<script src="http://iacom.s8.com.br/mktacom/apps/v0.1.8/scripts/americanas.min.js"></script>
<link rel="stylesheet" href="http://iacom.s8.com.br/mktacom/arquivos/css/lightboxcss.css" />
<script>


        App.on('load', function () {
			

            var App = this, $ = this.$, _ = this._;

            oas.on('print:end', function () {
                if ($.cookie("crhome") != "true") {
                    $('#AbreCRPorteiro').fancybox({
                        width: 650,
                        height: 305,
                        type: 'ajax',
                        afterLoad: function () {
                            $.cookie('crhome', true, { expires: 1, path: '/'  });
                            setTimeout(function () {
                                var form = $("#lightboxCrPorteiro");
                                $.app.trigger('run:cr', form);
                                form.find('> .app-cr').one('complete', function () {
                                    $.cookie('crhome', true, { expires: 365, path: '/'  });
                                    var self = $(this);
                                    self.one('click', '.cr-msg', function (a, b, c) {
                                        $.fancybox.close();
                                    });
                                });
                            }, 500)
                        }
                    }).click();
                }

                if ($.fn.fancybox) {
                    var link = $('#abreCRPorteiro').eq(0);
                    var conf = {
                        width: 520,
                        height: 250,
                        type: 'ajax'
                    };
                    conf[(typeof $.fancybox.version === "string" ? 'afterLoad' : 'onComplete')] = function () {
                        setTimeout(function () {
                            var form = $(".lightboxPorteiro");
                            $.app.trigger('run:cr', form);
                            form.find('> .app-cr').one('complete', function () {
                                var self = $(this);
                                self.one('click', '.cr-msg', function () {
                                    $.fancybox.close();
                                });
                            });
                        }, 500)
                    };
                    link.fancybox(conf).click();
                }
				});
				
			
				
			});
			
    </script>
</head>
	<body><script src="//iacom.s8.com.br/mktacom/arquivos/js/mobileredirect.js"></script>
<link rel="stylesheet" href="http://iacom.s8.com.br/statics-1.14.10/catalog/css/v1/header.min.css"/>
<style>
#footer > .sitemap-link {
display: none;
}
.list-product-linhas {margin-bottom: 20px;}
.home-page .pure-g .pure-u-1-3 div.app-product{ height:196px;}
.home-page .pure-g .pure-u-1-3{ position:relative;}
.home-page .pure-g .app-product .product-price{ position:absolute; bottom:30px;}
.home-page .pure-g .pure-u-1-3 .app-product .btn-buy{ position:absolute; bottom:10px;}
.a-menu-sanzonal .ams-item:first-child, .a-menu-sanzonal .oferta-do-dia{max-width:none;}
.comparator-itens { display:none; }
.paginado .comparator-itens { display:block; }
.highlight-dpto .top-area-product { overflow: hidden;}
span.new-tag {
    background: #99E200;
    padding: 0 5px 2px 5px;
    margin-left: 6px;
    font-weight: bold;
    color: #ffffff;
}
.a-bottom-header .a-menu-header .abh-menu-header .list-depts .pooler:hover span.new-tag {
    background: #fff200;
    color: #e60014;
}
.banner-featured .app-product .product-title{
    color:#666;
}
.busca-fontcolor{
    color:#999;
}
.a-aux-box1 {z-index:2;}
.product-page .prompt-delivery figure:before {right:0;top:0;}
.home-page .banner-featured .app-product {width:612px;}
</style>
<header id="a-header" role="banner">
    <div class="a-top-header">

        <div class="banner-top-header">
            <script type="text/javascript">try {OAS_AD("x71")} catch (exp) {}</script>
        </div>
        <ul class="menu-top-header">
            <li class="mth-item">
               <a href="http://www.americanas.com.br/estaticapop/popCentralAtendimento" data-modal="true" data-modal-maxwidth="750" data-modal-maxheight="430" target="_blank">televendas 24h: <strong>4003-1000</strong></a>
            </li>
            <li class="mth-item size-bt tooltip disabled">
                <a href="http://www.americanas.com.br/central-de-atendimento?WT.mc_id=internas-atendimentoHome&WT.mc_ev=click" class="tool-item">
                    atendimento
                </a>				
				<div class="btip-element-header tip-bottom-header">
                    <h2 class="tip-tit">precisa de ajuda?</h2>
                    <p class="tip-tx">atendimento por telefone, e-mail e d�vidas frequentes. <a href="http://www.americanas.com.br/central-de-atendimento?WT.mc_id=internas-atendimentoHome&amp;WT.mc_ev=click" class="tip-lk">clique aqui</a> </p>
                                
                </div>  			
            </li>       
            <li class="mth-item">
                <a href="https://carrinho.americanas.com.br/CustomerWeb/pages/LoginMeusPedidos">
                    meus pedidos
                </a>
            </li>           
            <li class="mth-item">
                <a href="https://carrinho.americanas.com.br/CustomerWeb/pages/LoginMinhaConta">
                    minha conta
                </a>
            </li>       
            <li class="mth-item">
                <a href="http://www.americanas.com.br/lojamaisproxima">
                    loja mais pr�xima
                </a>
            </li>
        </ul>
    </div>
    <div class="a-content-header">
        <div class="header-control">    
            <a href="http://www.americanas.com.br/"><h1 class="spt-logo h-logo">americanas.com</h1></a>
            <div class="h-cart">
                <div class="cart-itens">
                     <a href="https://carrinho.americanas.com.br/checkout" class="ci-itens-lnk">
                        <span class="spt-cesta"></span>
                        <span class="ci-itens-wp">
                            <span class="ci-label">minha cesta</span>
                            <script type="template/text" id="template-totalProducts">
                                <span class="ci-titens">{{ numberProducts }}</span> {{ numberProductsText }} <span class="spt-seta"></span>
                            </script>
                            <span class="ci-total">
                                <span class="ci-titens">0</span> item <span class="spt-seta"></span>
                            </span>
                        </span>
                    </a>
                    <div class="hidden-area cart-content-opened">
                        <div class="cart-empty">
                            <strong class="cart-empty-title">sua cesta est� vazia</strong>
                            
                        </div>
                        <script type="template/text" id="template-cart-products">
                            <ul class="cart-products-list">
                                {{#products}}
                                    <li class="cart-products-item">
                                        <img src="{{ image }}" class="cart-product-photo"/>
                                        <span class="cart-product-title">
                                            <a href="/produto/{{ id }}">
                                                {{ name }}
                                            </a>
                                        </span>
                                        <span class="cart-product-price">
                                            <del class="ctp-from">R$ {{ price }}</del>
                                            <span class="ctp-sale">R$ {{ discountedPrice }}</span>
                                        </span>
                                    </li>
                                {{/products}}
                            </ul>
                            {{#showLinkCart}}
                            <div class="cart-all-products">
                                <a href="https://carrinho.americanas.com.br/checkout/"> ver todos os {{ numberProducts }} produtos da cesta</a>
                            </div>
                            {{/showLinkCart}}
                            <div class="cart-price">
                                <strong class="box-cart-price">
                                    total:
                                    <span class="cart-price-subtotal">R$ {{ subtotal }}</span>
                                </strong>
                                
                                <a href="https://carrinho.americanas.com.br/checkout/" class="spt-ir-cesta"></a>
                            </div>
                        </script>
                        <div class="cart-not-empty">

                        </div>
                    </div>
                </div>
            </div>
            <div class="h-user-login">
                <span class="spt-smile"></span>
                <div class="user-area">
                    <div id="user-logged">
                        <span class="user-welcome">ol� <span id="acomNick"></span>,</span>
                        <span class="user-menu">
                            <a href="https://carrinho.americanas.com.br/CustomerWeb/pages/Logout">sair</a>
                        </span> 
                    </div>
                    <div id="user-not-logged">
                        <a href="https://carrinho.americanas.com.br/CustomerWeb/pages/Login/">ol�, fa�a seu <span class="it-user-log">login</span>
                            ou <span class="it-user-log">cadastre-se</span>
                        </a>
                    </div>
                    
                </div>
            </div>
            <div class="h-search">
                <div class="ginput">
                    <form action="http://busca.americanas.com.br/busca.php">
                        <input type="search" class="input-search placeholder busca-fontcolor" name="q"  placeholder="buscar" default-placeholder="buscar" onBlur="this.placeholder = this.getAttribute('default-placeholder');" onFocus="this.placeholder = '';" data-position="header" style="padding-left: 7px !important;"/>
                        <input type="submit" class="spt-lupa-buscar" data-position="header"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="a-bottom-header">
        
        <ul class="a-menu-sanzonal">

            <li class="ams-item">
        <div class="a-menu-header">
            <span class="mh-label">compre por departamento <span class="spt-seta-menu"></span></span>
            <nav class="abh-menu-header" role="navigation">
                <!-- <ul class="mh-list list-top">
                    <li class="mh-item bfr-arw">
                        <a href="#"><span class="spt-seta-submenu"></span>mais vendidos</a>
                    </li>
                    <li class="mh-item bfr-arw">
                        <a href="#"><span class="spt-seta-submenu"></span>lan�amentos</a>
                    </li>
                    <li class="mh-item bfr-arw">
                        <a href="#"><span class="spt-seta-submenu"></span>ofertas especiais</a>
                    </li>
                </ul> -->
                <ul class="mh-list list-depts">
             <li class="mh-item afr-arw">
               <span class="pooler">Moda<span class="new-tag">novo</span><span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel">
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/358352/moda" class="a-title">Moda</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/subloja/358354/moda/feminino">Feminino</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/358357/moda/feminino/roupas">Roupas</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/358358/moda/feminino/calcados">Cal�ados</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/358360/moda/feminino/bolsas-e-acessorios">Bolsas e Acess�rios</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/subloja/358355/moda/masculino">Masculino</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/358361/moda/masculino/roupas">Roupas</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/358394/moda/masculino/calcados">Cal�ados</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/358362/moda/masculino/acessorios">Acess�rios</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/subloja/358432/moda/infantil">Infantil</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/358414/moda/infantil/meninas">Para meninas</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/358434/moda/infantil/meninos">Para meninos</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/358352/moda">Veja Mais Moda</a>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x91")} catch (exp) {}</script>

                    </div>
                </div>
            </li>
            <li class="mh-item afr-arw">
                <span class="pooler">celulares e telefones<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/229187/celulares-e-telefones" class="a-title">celulares e telefones</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/350392/celulares-e-telefones/smartphone">Smartphones</a>
                                <ul class="a-snl-sub">
                                    <li>

                                        <a href="http://www.americanas.com.br/linha/345399/celulares-e-telefones/iphone">iPhone</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/345396/celulares-e-telefones/samsung-galaxy">Samsung Galaxy</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/345489/celulares-e-telefones/nokia-lumia">Nokia Lumia</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/350373/celulares-e-telefones/smartphone/smartphone-multichips">Smartphone Multichip</a>
                                    </li>
                                </ul>
                                
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/229196/celulares-e-telefones/celular">Celulares</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/263550/celulares-e-telefones/celular/celular-dual-chip-2-chips">Dual chip</a>
                                    </li>   
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/263568/celulares-e-telefones/celular/celular-tri-chip-3-chips">Tri Chip</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/263608/celulares-e-telefones/acessorios-para-celular">Acess�rios para celular</a>
                                        <ul class="a-snl-sub">
                                            <li>
                                                <a href="http://www.americanas.com.br/sublinha/263569/celulares-e-telefones/acessorios-para-celular/capas-peliculas">Capas / Peliculas</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/264410/celulares-e-telefones/nextel">Nextel</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="a-sn-list">
                                <li class="a-snl-item">
                                    <span class="pooler">TelefonIa fixa</span>
                                    <ul class="a-snl-sub">
                                        <li>
                                            <a href="http://www.americanas.com.br/linha/263555/celulares-e-telefones/telefone-sem-fio">Telefone sem fio</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/linha/263559/celulares-e-telefones/telefone-com-fio">Telefone com fio</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="a-snl-item">
                                    <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/229187/celulares-e-telefones">Veja mais Celulares e Telefones</a>
                                </li>
                            </ul>
                        </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x77")} catch (exp) {}</script>
                    </div>
                </div>
            </li>
            <li class="mh-item afr-arw">
                <span class="pooler">Inform�tica e Tablets<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                        
                        <div class="hd-pure-g">
                            <div class="hd-pure-u-1-2">
                        <a href="http://www.americanas.com.br/loja/228190/informatica" class="a-title">inform�tica</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/267868/informatica/notebook">notebook</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/348528/informatica/ultrabook">ultrabook</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/350792/informatica/2-em-1">2 em 1</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/267908/informatica/tablets-e-ipad">Tablet</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/267865/informatica/apple/ipad">iPad</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/268189/informatica/tablets-e-ipad/tablet-samsung?f_268761=samsung">Tablet Samsung</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/267889/informatica/computadores-e-all-in-one">Computadores</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/267854/informatica/multifuncionais">Multifuncionais</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/228190/informatica">Veja mais Inform�tica</a>
                            </li>
                        </ul>
                        </div>
                        <div class="hd-pure-u-1-2">
                        <a href="http://www.americanas.com.br/loja/228098/informatica-acessorios" class="a-title">Inform�tica e acess�rios</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/268389/informatica/armazenamento/hd-externo">HD externo</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/268370/informatica/armazenamento/pen-drives">Pen drive</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/268385/informatica/equipamentos-de-rede-wireless/roteador">Roteadores</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/267888/informatica/acessorios-para-notebook">Acess�rios para notebook</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/267863/informatica/tablets-e-ipad/acessorios-para-tablets">Acess�rios para tablet</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/228098/informatica-acessorios">Veja mais Acess�rios</a>
                            </li>
                        </ul>
                        </div>
                        </div>
                    </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x78")} catch (exp) {}</script>
                    </div>
                </div>
            </li>       
            <li class="mh-item afr-arw">
                <span class="pooler">TVs, �udio e Home Theater<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/227707/tv-e-home-theater" class="a-title">TVs, �udio e Home Theater</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/262909/tv-e-home-theater/tv">TV</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/262909/tv-e-home-theater/tv-smart-tv-3d-smart-tv?f_357593=smart+tv+3d-smart+tv">Smart TV</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/262870/tv-e-home-theater/tv/tv-led">TV LED</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/262909/tv-e-home-theater/tv-smart-tv-3d-3d?f_357593=smart+tv+3d-3d">TV 3D</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/262871/tv-e-home-theater/tv/tvs-de-plasma">TV de Plasma</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/357292/tv-e-home-theater/suporte-para-tv">Suporte para parede</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/loja/256408/audio">�udio</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/262429/audio/micro-system">Micro system</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/262431/audio/som-portatil">Som port�til</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/262488/audio/ipod-e-acessorios">Ipod e acess�rios</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/262453/audio/fones-de-ouvido">Fone de ouvido</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/351552/audio/soundbar">soundbar</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/263029/tv-e-home-theater/home-theater">home theater</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/263030/tv-e-home-theater/home-theater/home-theater-blu-ray-3d">home theater blu-ray 3d</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/263048/tv-e-home-theater/home-theater/home-theater-dvd-player">home theater dvd player</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227707/tv-e-home-theater">veja mais tvs, �udio e home theater</a>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x79")} catch (exp) {}</script>
                    </div>
                </div>
            </li>
            <li class="mh-item afr-arw">
                <span class="pooler">Eletrodom�sticos<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/227644/eletrodomesticos" class="a-title">Eletrodom�sticos</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/316788/eletrodomesticos/geladeira-refrigerador">Geladeiras / Refrigeradores</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/316689/eletrodomesticos/fogao">Fog�es</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/316848/eletrodomesticos/cooktop">Cooktop</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/317052/eletrodomesticos/forno/forno-de-embutir">Forno de embutir</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/317050/eletrodomesticos/coifa-e-depurador/coifas">Coifas</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/316828/eletrodomesticos/micro-ondas">Micro-ondas</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/316829/eletrodomesticos/adega-de-vinho">adega de vinho</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/316690/eletrodomesticos/lava-loucas">Lava-lou�as</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/316808/eletrodomesticos/lavadora-de-roupa-e-tanquinho">Lavadora de roupas</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/316691/eletrodomesticos/secadora-de-roupa-e-centrifuga">Secadora de roupas</a>
                            </li>
                        </ul>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/loja/317728/ar-condicionado-e-aquecedores">Ar condicionado e Aquecedor</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/317752/ar-condicionado-e-aquecedores/ar-condicionado/ar-condicionado-split">Ar condicionado split</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/sublinha/317750/ar-condicionado-e-aquecedores/ar-condicionado/ar-condicionado-janela">Ar condicionado de janela</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/317789/ar-condicionado-e-aquecedores/climatizador-de-ar">Climatizador de ar</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227644/eletrodomesticos">Veja mais Eletrodom�sticos</a>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x80")} catch (exp) {}</script>
                    </div>
                </div>
            </li>
            <li class="mh-item afr-arw">
                <span class="pooler">Eletroport�teis<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/227763/eletroportateis" class="a-title">Eletroport�teis</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278268/eletroportateis/bebedouros-e-purificadores">Bebedouros e Purificadores</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278248/eletroportateis/batedeiras">Batedeiras</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278191/eletroportateis/cafeteiras-e-chaleiras">Cafeteiras</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278192/eletroportateis/centrifugas-e-espremedores-de-fruta">Centr�fugas e Espremedores de fruta</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278196/eletroportateis/forno-eletrico">Forno el�trico</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278252/eletroportateis/grill-sanduicheira-e-torradeira">Grill, Sanduicheiras e Torradeiras</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278159/eletroportateis/panificadora-maquina-de-pao">Panificadoras</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278197/eletroportateis/liquidificadores">Liquidificadores</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278190/eletroportateis/aspiradores-e-vassouras">Aspiradores de p� e Vassoura el�trica</a>
                            </li>
                        </ul>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278250/eletroportateis/ferro-de-passar">Ferro de passar</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278257/eletroportateis/ventiladores-e-circuladores-de-ar">Ventiladores e Circuladores de ar</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278158/eletroportateis/panelas-eletricas">Panela el�trica</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/278198/eletroportateis/maquinas-de-costura">M�quina de costura</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227763/eletroportateis">Veja mais Eletroport�teis</a>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x81")} catch (exp) {}</script>

                    </div>
                </div>
            </li>
            <li class="mh-item afr-arw">
                <span class="pooler">Brinquedos e Beb�s<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                    <a href="http://www.americanas.com.br/loja/227109/brinquedos" class="a-title">Brinquedos</a>
                    <ul class="a-sn-list">
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/279688/brinquedos/bonecas">Bonecas</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/279649/brinquedos/bonecos">Bonecos</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/279748/brinquedos/brinquedos-eletronicos">Brinquedos eletr�nicos</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/279690/brinquedos/controle-remoto">Controle remoto</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/279674/brinquedos/mini-veiculos">Mini ve�culos</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/279670/brinquedos/bicicleta-infantil">Bicicleta infantil</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/279695/brinquedos/quebra-cabeca">Quebra-cabe�a</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/279657/brinquedos/playground">Playground</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/sublinha/280068/brinquedos/esportes/bolas">Bola de futebol</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/279698/brinquedos/praia-e-piscina">Praia e Piscina</a>
                        </li>
                        <li class="a-snl-item">
                            <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227109/brinquedos">Veja mais Brinquedos</a>
                        </li>
                    </ul>
                </div>
                <div class="a-snl-opts">
                    <a href="http://www.americanas.com.br/loja/226940/bebes" class="a-title">Beb�s</a>
                    <ul class="a-sn-list">
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/sublinha/272687/bebes/passeio/carrinhos-de-passeio">Carrinho de passeio</a>
                        </li>
                        <li class="a-snl-item">
                           <a href="http://www.americanas.com.br/linha/272169/bebes/bercario">Ber�ario</a>
                        </li>
                        <li class="a-snl-item">
                            <a href="http://www.americanas.com.br/linha/272228/bebes/brinquedos-para-bebe">Brinquedos para beb�</a>
                        </li>
                        <li class="a-snl-item">
                            <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/226940/bebes">Veja mais Beb�s</a>
                        </li>
                    </ul>
                </div>
                <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x83")} catch (exp) {}</script>
                    </div>
                </div>
            </li>
            <li class="mh-item afr-arw">
                <span class="pooler">Games, M�sica e Filmes<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/226762/games" class="a-title">Games</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/347990/games/console-xbox-one">Xbox one</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/351258/games/jogos-xbox-one">Jogos Xbox One</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/291045/games/console-xbox-360">Xbox 360</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/291228/games/jogos-xbox-360">Jogos Xbox 360</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/291067/games/console-playstation-3">Playstation 3</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/291236/games/jogos-playstation-3">Jogos Playstation 3</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/351535/games/console-playstation-4">Playstation 4</a>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/356437/games/jogos-playstation-4">Jogos Playstation 4</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/291327/games/jogos-pc">Jogos para pc</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/226762/games">Veja Mais Games</a>
                            </li>
                        </ul>
                    </div>
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/227369/musica" class="a-title">M�sica</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/360919/musica/pop-internacional">pop internacional</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/360965/musica/rock-internacional">rock internacional</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/227373/musica/blu-ray">blu-ray musical</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227369/musica">veja mais m�sica</a>
                            </li>
                        </ul>
                        <a href="http://www.americanas.com.br/loja/227609/dvds-e-blu-ray" class="a-title">Filmes</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/315209/dvds-e-blu-ray/lancamentos">lan�amentos</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/315442/dvds-e-blu-ray/boxes-e-colecoes">box e cole��es</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/315255/dvds-e-blu-ray/series-de-tv">s�ries de tv</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227609/dvds-e-blu-ray">veja mais filmes</a>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x85")} catch (exp) {}</script>
                    </div>
                </div>
            </li>
            <li class="mh-item afr-arw">
                <span class="pooler">Livros e Papelaria<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/228310/livros" class="a-title">Livros</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/230995/livros/mais-vendidos">Mais vendidos</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/231012/livros/lancamentos">Lan�amentos</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/loja/228622/livros-importados">Livros importados</a>
                            </li>
                            <li class="a-snl-item">
                                <span class="pooler">G�neros</span>
                                <ul class="a-snl-sub">
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/228536/livros/literatura-estrangeira">Literatura estrangeira</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/228548/livros/literatura-nacional">Literatura nacional</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/228312/livros/administracao-e-negocios">Administra��o e Neg�cios</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/228559/livros/medicina-e-saude">Medicina e Sa�de</a>
                                    </li>
                                    <li>
                                        <a href="http://www.americanas.com.br/linha/228511/livros/juvenil">Juvenil</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/228310/livros">Veja mais Livros</a>
                            </li>
                        </ul>
                    </div>
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/228804/papelaria" class="a-title">Papelaria</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/330688/papelaria/mochilas-escolares">mochilas escolares</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/228860/papelaria/cadernos">cadernos</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/330968/papelaria/ficharios-e-acessorios">fich�rios</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/330193/papelaria/agendas-e-calendarios">agendas e calend�rios</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/330901/papelaria/estojos-e-necessaires">estojos</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/228804/papelaria">veja mais papelaria</a>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x84")} catch (exp) {}</script>
                    </div>
                </div>
            </li>
             <li class="mh-item afr-arw">
               <span class="pooler">M�veis e Decora��o<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao" class="a-title">M�veis</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282284/moveis-e-decoracao/guarda-roupas-roupeiros">Guarda-roupa</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282569/moveis-e-decoracao/cadeira-de-escritorio">Cadeira de escrit�rio</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282125/moveis-e-decoracao/colchao">Colch�o</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282276/moveis-e-decoracao/cama-box-colchao-box">Colch�o + box</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/282622/moveis-e-decoracao/bar/banquetas-cadeiras-para-bar">Banqueta e cadeira de bar</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282714/moveis-e-decoracao/rack-estante-e-painel">Rack, estante e painel</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282908/moveis-e-decoracao/sofas">Sof�</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282264/moveis-e-decoracao/cozinha-modulada">cozinha modulada</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282614/moveis-e-decoracao/comoda">c�moda</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282279/moveis-e-decoracao/estante-e-livreiro">estante e livreiro</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/332648/moveis-e-decoracao/sala-de-estar-completa">sala de estar completa</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282263/moveis-e-decoracao/cozinha-compacta">cozinha compacta</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282748/moveis-e-decoracao/cama-infantil-e-juvenil">cama infantil e juvenil</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao">Veja mais M�veis</a>
                            </li>
                        </ul>
                        </div>
                        <div class="a-snl-opts">
                            <a href="http://www.americanas.com.br/loja/364673/decoracao" class="a-title">Decora��o</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282621/moveis-e-decoracao/luminarias">lumin�rias</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282598/moveis-e-decoracao/quadros">quadros</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/282597/moveis-e-decoracao/painel-de-fotos">painel de fotos</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/348268/moveis-e-decoracao/adesivos">adesivos</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/364673/decoracao">Veja mais Decora��o</a>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x82")} catch (exp) {}</script>
                    </div>
                </div>
            </li>
            <li class="mh-item afr-arw">
                <span class="pooler">Esporte e Sa�de<span class="spt-seta-submenu"></span></span>
                <div class="a-subnivel two-columns">
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/227821/esporte-e-lazer" class="a-title">Esporte</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/341383/esporte-e-lazer/monitor-cardiaco">monitor card�aco</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/291560/esporte-e-lazer/praia-e-piscina">piscina</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/290286/esporte-e-lazer/bicicleta">bicicleta</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/341445/esporte-e-lazer/aparelho-de-musculacao-e-fitness/esteira-eletrica-ergometrica">esteira el�trica</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/341439/esporte-e-lazer/aparelho-de-musculacao-e-fitness/aparelho-de-musculacao">aparelho de muscula��o</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/341399/esporte-e-lazer/aparelho-de-musculacao-e-fitness/acessorios-para-malhar">acess�rios para malha��o</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/341469/esporte-e-lazer/aparelho-de-musculacao-e-fitness/halteres">halteres</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227821/esporte-e-lazer">Veja mais Esporte</a>
                            </li>
                        </ul>
                    </div>
                    <div class="a-snl-opts">
                        <a href="http://www.americanas.com.br/loja/227014/beleza-e-saude" class="a-title">sa�de</a>
                        <ul class="a-sn-list">
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/292289/beleza-e-saude/umidificadores-de-ar">umidificador de ar</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/sublinha/292190/beleza-e-saude/balancas/balanca-digital">balan�a digital</a>
                            </li>
                            <li class="a-snl-item">
                                <a href="http://www.americanas.com.br/linha/292179/beleza-e-saude/medidores-de-pressao">medidor de press�o</a>
                            </li>
                            <li class="a-snl-item">
                                <a class="hd-mn-see-more" href="http://www.americanas.com.br/loja/227014/beleza-e-saude">veja mais sa�de</a>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-snl-item">
                        <script type="text/javascript">try {OAS_AD("x93")} catch (exp) {}</script>
                    </div>
                </div>
            </li>
            <li class="mh-item afr-arw">
                <span class="pooler">Viagens<span class="spt-seta-submenu"></span></span>
                    <div class="a-subnivel">
                        <div class="a-snl-opts">
                            <a href="http://viagens.americanas.com.br/default.aspx?utm_source=americanas&utm_medium=americanas_cabecalho_menu_principal_varejo_-_-_home_principal-&utm_campaing=-_-_home_principal&s_cid=americanas_cabecalho_menu_principal_varejo_-_-_-_home_principal" class="a-title">viagens</a>
                            <ul class="a-snl-sub">
                                <li>
                                    <a href="http://viagens.americanas.com.br/passagens-aereas.aspx?utm_source=americanas&utm_medium=americanas_menu_principal_varejo_-_-_home_passagens_-&utm_campaing=-_-_home_passagens&s_cid=americanas_menu_principal_varejo_-_-_-_home_passagens">Passagens A�reas</a>
                                </li>
                                <li>
                                    <a href="http://viagens.americanas.com.br/hoteis.aspx?utm_source=americanas&utm_medium=americanas_menu_principal_varejo_-_-_home_hoteis_-&utm_campaing=-_-_home_hoteis&s_cid=americanas_menu_principal_varejo_-_-_-_home_hoteis">Hot�is</a>
                                </li>
                                 <li>
                                    <a href="http://viagens.americanas.com.br/pacotes-turisticos.aspx?utm_source=americanas&utm_medium=americanas_menu_principal_varejo_-_-_home_pacotes_-&utm_campaing=-_-_home_pacotes&s_cid=americanas_menu_principal_varejo_-_-_-_home_pacotes">Pacotes Tur�sticos</a>
                                </li>
                                 <li>
                                    <a href="http://viagens.americanas.com.br/cruzeiros-maritimos.aspx?utm_source=americanas&utm_medium=americanas_menu_principal_varejo_-_-_home_cruzeiros_-&utm_campaing=-_-_home_cruzeiros&s_cid=americanas_menu_principal_varejo_-_-_-_home_cruzeiros">Cruzeiros Mar�timos</a>
                                </li>
                                 <li>
                                    <a href="http://viagens.americanas.com.br/seguros.aspx?utm_source=americanas&utm_medium=americanas_menu_principal_varejo_-_-_home_seguros-&utm_campaing=-_-_home_seguros&s_cid=americanas_menu_principal_varejo_-_-_-_home_seguros">Seguros</a>
                                </li>
                                 <li>
                                    <a href="http://viagens.americanas.com.br/atracoes.aspx?utm_source=americanas&utm_medium=americanas_menu_principal_varejo_-_-_home_atracoes-&utm_campaing=-_-_home_atracoes&s_cid=americanas_menu_principal_varejo_-_-_-_home_atracoes">Atra��es</a>
                                </li>
                                 <li>
                                    <a href="http://viagens.americanas.com.br/resorts.aspx?utm_source=americanas&utm_medium=americanas_menu_principal_varejo_-_-_home_resorts-&utm_campaing=-_-_home_resorts&s_cid=americanas_menu_principal_varejo_-_-_-_home_resorts">Resorts</a>
                                </li>
                                 <li>
                                    <a href="http://viagens.americanas.com.br/default.aspx?utm_source=americanas&utm_medium=americanas_toda_a_loja_do_viagens_menu_principal_varejo_-_-_home_principal&utm_campaing=-_-_home_principal&s_cid=americanas_toda_a_loja_do_viagens_menu_principal_varejo__-_-_home_principal">Toda a loja do Viagens</a>
                                </li>
                            </ul>
                        </div>
                        <div class="banner-snl-item">
                            <script type="text/javascript">try {OAS_AD("x92")} catch (exp) {}</script>
                        </div>
                    </div>
                </li>
                </ul>
                <ul class="mh-list list-bottom">
                    
                    
                    <li class="mh-item afr-arw">
                       <span class="pooler">todos os departamentos<span class="spt-seta-submenu"></span></span>
                        <div class="a-subnivel all-depts">
                            <a href="http://www.americanas.com.br/estatica/todos-os-departamentos/?WT.mc_id=home-todos-departamentos" class="a-title">todos os departamentos</a>
                            <div class="hd-pure-g">
                                <div class="hd-pure-u-1-4">
                                    <ul class="a-snl-sub">
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/226795/alimentos-e-bebidas?WT.mc_id=home-menuLista-alimentos">Alimentos e bebidas</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/256408/audio?WT.mc_id=home-menuLista-audio">�udio</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/226855/automotivo?WT.mc_id=home-menuLista-automotivo">Automotivo</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/317728/ar-condicionado-e-aquecedores">Ar-condicionado e aquecedores</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/226940/bebes?WT.mc_id=home-menuLista-bebes">Beb�s</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227014/beleza-e-saude?WT.mc_id=home-menuLista-beleza">Beleza e sa�de</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227109/brinquedos?WT.mc_id=home-menuLista-brinquedos">Brinquedos</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/315798/blu-ray?WT.mc_id=home-menuIconeOver-bluRay-linha">Blu-ray e blu-ray 3D</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="hd-pure-u-1-4">
                                    <ul class="a-snl-sub">
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227296/cama-mesa-e-banho?WT.mc_id=home-menuLista-cameba">Cama, mesa e banho</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227559/cameras-e-filmadoras?WT.mc_id=home-menuLista-cameras" crmwa_mdc="mp|ver_todos_os_departamentos|cameras_e_filmadoras">C�meras
                                                e filmadoras</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/229187/celulares-e-telefones?WT.mc_id=home-menuLista-celulares" crmwa_mdc="mp|ver_todos_os_departamentos|celulares_e_telefones">Celulares e telefones</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227644/eletrodomesticos?WT.mc_id=home-menuLista-eletrodomesticos" crmwa_mdc="mp|ver_todos_os_departamentos|eletrodomesticos">Eletrodom�sticos</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227763/eletroportateis?WT.mc_id=home-menuLista-eletroportateis" crmwa_mdc="mp|ver_todos_os_departamentos|eletroportateis">Eletroport�teis</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227821/esporte-e-lazer?WT.mc_id=home-menuLista-esporte" crmwa_mdc="mp|ver_todos_os_departamentos|esporte_e_lazer">Esporte e lazer</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227999/ferramentas-e-jardim?WT.mc_id=home-menuLista-ferramentas" crmwa_mdc="mp|ver_todos_os_departamentos|ferramentas_e_jardim">Ferramentas e jardim</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227609/dvds-e-blu-ray?WT.mc_id=home-menuLista-dvdBlu" crmwa_mdc="mp|ver_todos_os_departamentos|filmes_e_series">Filmes e s�ries</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="hd-pure-u-1-4">
                                    <ul class="a-snl-sub">
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/226762/games?WT.mc_id=home-menuLista-games" crmwa_mdc="mp|ver_todos_os_departamentos|games">Games</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/228190/informatica?WT.mc_id=home-menuLista-informatica" crmwa_mdc="mp|ver_todos_os_departamentos|informatica">Inform�tica</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/228098/informatica-acessorios?WT.mc_id=home-menuLista-infoacess" crmwa_mdc="mp|ver_todos_os_departamentos|informatica_e_acessorios">Inform�tica e acess�rios</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/228255/instrumentos-musicais?WT.mc_id=home-menuLista-instrumentos" crmwa_mdc="mp|ver_todos_os_departamentos|instrumentos_musicais">Instrumentos musicais</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/228310/livros?WT.mc_id=home-menuLista-livros" crmwa_mdc="mp|ver_todos_os_departamentos|livros">Livros</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/228641/malas-e-acessorios?WT.mc_id=home-menuLista-malas" crmwa_mdc="mp|ver_todos_os_departamentos|malas_e_acessorios">Malas e acess�rios</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/358352/moda?WT.mc_id=home-menuLista-moda" crmwa_mdc="mp|ver_todos_os_departamentos|moda_e_acessorios">Moda e acess�rios</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/228740/moveis-e-decoracao?WT.mc_id=home-menuLista-moveis" crmwa_mdc="mp|ver_todos_os_departamentos|moveis_e_decoracao">M�veis e decora��o</a>
                                            </li>
                                    </ul>
                                </div>
                                <div class="hd-pure-u-1-4">
                                    <ul class="a-snl-sub">
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/227369/musica" crmwa_mdc="mp|ver_todos_os_departamentos|musica">M�sica</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/228804/papelaria?WT.mc_id=home-menuLista-papelaria" crmwa_mdc="mp|ver_todos_os_departamentos|papelaria">Papelaria</a>
                                        </li>
                                        <!--li>
                                            <a href="http://www.americanas.com.br/loja/234768/pascoa" crmwa_mdc="mp|ver_todos_os_departamentos|pascoa">P�scoa</a>
                                        </li-->
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/228926/perfumaria?WT.mc_id=home-menuLista-perfumaria" crmwa_mdc="mp|ver_todos_os_departamentos|perfumaria_e_cosmeticos">Perfumaria e cosm�ticos</a></li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/228975/pet-shop?WT.mc_id=home-menuLista-petshop" crmwa_mdc="mp|ver_todos_os_departamentos|pet_shop">Pet shop</a></li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/229017/relogios?WT.mc_id=home-menuLista-relogios" crmwa_mdc="mp|ver_todos_os_departamentos|relogios">Rel�gios</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/336268/suplementos-e-vitaminas?WT.mc_id=home-menuLista-suplementos" crmwa_mdc="mp|ver_todos_os_departamentos|suplementos_e_vitaminas">Suplementos e Vitaminas</a></li>
                                         <li>
                                            <a href="http://www.americanas.com.br/loja/227707/tv-e-home-theater?WT.mc_id=home-menuLista-eletronicos" crmwa_mdc="mp|ver_todos_os_departamentos|tvs_e_audio">TVs e �udio</a>
                                        </li>
                                        <li>
                                            <a href="http://www.americanas.com.br/loja/229231/utilidades-domesticas?WT.mc_id=home-menuLista-utilidadeDomestica" crmwa_mdc="mp|ver_todos_os_departamentos|utilidades_domesticas">Utilidades dom�sticas</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="a-snl-bottom-info">
                                <div class="hd-pure-g">
                                    <div class="hd-pure-u-1-2">
                                        <span class="a-title">veja tamb�m</span>
                                        <ul class="a-bt-inf-list">
                                            <li class="a-bt-inf-it">
                                                <a href="http://viagens.americanas.com.br" class="a-bt-inf-lnk amv">Americanas Viagens</a>
                                            </li>
                                            <li class="a-bt-inf-it">
                                                <a href="http://www.ingresso.com.br/" class="a-bt-inf-lnk ing">Ingresso.com</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="hd-pure-u-1-2">
                                        <span class="a-title">parceiros</span>
                                        <ul class="a-bt-inf-list">
                                            <li class="a-bt-inf-it">
                                                <a href="http://www.milevo.com.br/" class="a-bt-inf-lnk aml">Milevo.com</a>
                                            </li>
                                            <li class="a-bt-inf-it">
                                                <a href="http://www.soubarato.com.br/?epar=ds_bs_00_am_siteacomsbacom2&opn=SBACOM2" class="a-bt-inf-lnk soub">Outlet - Sou Barato</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <li class="mh-item afr-arw">
                    <span class="pooler">Servi�os<span class="spt-seta-submenu"></span></span>
                    <div class="a-subnivel">
                        <div class="a-snl-opts">
                        <span class="a-title">servi�os</span>
                        <ul class="a-snl-sub">
                            <li class="a-snl-item">
                               <a href="http://www.americanas.com.br/avaliacao?chave=HM_SV_AVPROD&amp;WT.mc_id=HM_SV_AVPROD">avalia��o de produto</a>
                            </li>
                            <li class="a-snl-item">
                               <a href="http://www.americanas.com.br/garantia-estendida?chave=HM_SV_GE&amp;WT.mc_id=HM_SV_GE">garantia estendida</a>
                            </li>
                            <li class="a-snl-item">
                               <a href="http://www.americanas.com.br/app?chave=HM_SV_APP&WT.mc_id=HM_SV_APP">aplicativo para iPhone e Android</a>
                            </li>
                            <li class="a-snl-item">
                               <a href="http://www.americanas.com.br/negocios-corporativos?chave=HM_SV_NEGCORP&amp;WT.mc_id=HM_SV_NEGCORP">neg�cios corporativos</a>
                            </li>
                            <li class="a-snl-item">
                               <a href="http://www.americanas.com.br/ajato?chave=HM_SV_EAJATO&amp;WT.mc_id=HM_SV_EAJATO">entrega a jato</a>
                            </li>
                            <li class="a-snl-item">
                               <a href="http://www.americanas.com.br/caixa-expresso?chave=HM_SV_CEXPRESSO&amp;WT.mc_id=HM_SV_CEXPRESSO">caixa expresso</a>
                            </li>
                            <li class="a-snl-item">
                               <a href="https://carrinho.americanas.com.br/lista-de-casamento/pages/HomePage?chave=HM_SV_LCASAMENTO&amp;WT.mc_id=HM_SV_LCASAMENTO">lista de casamento</a>
                            </li>
                           <li class="a-snl-item">
                               <a href="http://www.americanas.com.br/planopet?chave=HM_SV_PLANOPET&WT.mc_id=HM_SV_PLANOPET">Plano Pet + Sa�de</a>
                            </li>
                        </ul>
                    </div>
                    <div class="banner-snl-item">
                            <script type="text/javascript">try {OAS_AD("x88")} catch (exp) {}</script>
                        </div>
                    </div>
            </li>
                </ul>
            </nav>
        </div>
            </li>
            <li class="ams-item">
                <a href="http://www.americanas.com.br/loja/358352/moda?chave=HT_TP_1">moda</a>
            </li>
            <!--li class="ams-item">
                <a href="http://www.americanas.com.br/ole-tvs?chave=HT_TP_2&WT.mc_id=d_oletvs_tt">ol� tvs</a>
            </li-->
            <li class="ams-item">
                <a href="http://www.americanas.com.br/festival-de-inverno?chave=HT_TP_3&WT.mc_id=d_inverno2014_tt">festival de inverno</a>
            </li>
            <li class="ams-item">
                <a href="http://www.americanas.com.br/volta-as-aulas-universitarias?chave=HT_TP_4&WT.mc_id=d_topofmind14_topo" target="_blank">volta �s aulas universit�rias</a>
            </li>
            <li class="ams-item"> 
               <a href="http://www.americanas.com.br/feirao-tecnologia?chave=HT_TP_5&WT.mc_id=d_feiraotecnologia_tt">feir�o de tecnologia</a> 
            </li>
            
            <li class="ams-item oferta-do-dia">
                <a href="http://www.americanas.com.br/aofertadodia?WT.ac=ofertadodia" style="padding: 0 18px 0 6px;"><span class="spt-ic-oferta-do-dia"></span>oferta do dia<span class="spt-seta-menu"></span></a>
                <div class="banner-oferta-do-dia">
                    <div class="bnr-ofr-img" style="margin-left: 0;">
                        script type="text/javascript">try {OAS_AD("Top2")} catch (exp) {}</script>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</header>
<script>
    
onReady(function(){
$.getScript("http://iacom.s8.com.br/statics-1.14.7/catalog/js/v1/header.js");
$(".bar-floating .cart-empty").html('<strong class="cart-empty-title">sua cesta est� vazia</strong>');
})
</script>
	<section role="main">
		<div class="grid-control">
			<h1 class="page-title pull-left">Moda</h1><script>
onReady(function(){

$( ".pure-control-group input" ).focusin(function() {
  $( ".newsletter-moda").addClass( "shadow" );
});
$( ".pure-control-group input" ).focusout(function() {
  $( ".newsletter-moda").removeClass( "shadow" );
});

})


</script>
<link rel="stylesheet" href="http://iacom.s8.com.br/mktacom/MODA/css/menusombrafinal.css">
<style>
	.linkFixModa:hover{
		color:#666666 !important;
		text-decoration:none !important;
		cursor:default !important;
	}
        .menunovo > ul > li:hover > ul.inf, .menunovo > ul > li a:hover > ul.inf {
                 left: 270px !important;
        }
        .newsletter-moda .box-success {
                 margin-top: 20px !important;
        }
		.newsletter-moda.shadow{
			webkit-box-shadow: 0px 3px 1px 1px #f8f8f8;
	-moz-box-shadow:    0px 3px 1px 1px #f8f8f8;
	box-shadow:         0px 3px 1px 1px #f8f8f8;
		}
</style>

<div class="menu-top">
	<div class="menunovo">
		<ul class="list">
			<!--Novidades-->
			<li class="it-list-mtf first">
				<a href="http://www.americanas.com.br/Moda-novidades" class="lnk-mtf">novidades</a>
			</li>
			
			
			<!--Feminino-->
			<li class="it-list-mtf has-sub">
				<a href="http://www.americanas.com.br/subloja/358354/feminino?chave=moda_menuhor_f" class="lnk-mtf">feminino</a>
				<span class="selected"></span>
				<!--menu drop-down-->
				 <ul>
					<div class="content_drop">
						<!--Roupas-->
					<div>
					<li class="title"><a href="http://www.americanas.com.br/linha/358357/moda/feminino/roupas?chave=moda_menuhor_f_roupas" title="Roupas">roupas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358494/moda/feminino/roupas/blazer-e-jaqueta?chave=moda_menuhor_f_blazer-e-jaqueta">blazer e jaqueta</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358456/moda/feminino/roupas/calcas-jeans?chave=moda_menuhor_f_calcas-jeans">cal�as jeans</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358493/moda/feminino/roupas/camisas?chave=moda_menuhor_f_camisas">camisas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358492/moda/feminino/roupas/camisetas-e-blusas?chave=moda_menuhor_f_camisetas-e-blusas">camisetas e blusas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358495/moda/feminino/roupas/casaco-e-cardigan?chave=moda_menuhor_f_casaco-e-cardigan">casaco e cardigan</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358533/moda/feminino/roupas/lingerie?chave=moda_menuhor_f_lingerie">lingerie</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358459/moda/feminino/roupas/meias?chave=moda_menuhor_f_meias">meias</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358534/moda/feminino/roupas/moda-praia?chave=moda_menuhor_f_moda-praia">moda praia</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358497/moda/feminino/roupas/saias?chave=moda_menuhor_f_saias">saias</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358457/moda/feminino/roupas/shorts?chave=moda_menuhor_f_shorts">shorts</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358512/moda/feminino/roupas/vestidos?chave=moda_menuhor_f_vestidos">vestidos</a></li>
					</div>
					
					<!--Cal�ados-->
					<div>
					<li class="title"><a href="http://www.americanas.com.br/linha/358358/moda/feminino/calcados?chave=moda_menuhor_f_calcados" title="Cal�ados">cal�ados</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358535/moda/feminino/calcados/alpargatas?chave=moda_menuhor_f_alpargatas">alpargatas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358500/moda/feminino/calcados/anabela?chave=moda_menuhor_f_anabela">anabela</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358460/moda/feminino/calcados/botas?chave=moda_menuhor_f_botas">botas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358504/moda/feminino/calcados/chinelos?chave=moda_menuhor_f_chinelos">chinelos</a></li>
					<!--li><a href="http://www.americanas.com.br/sublinha/358537/moda/feminino/calcados/havaianas?chave=moda_menuhor_f_havaianas">havaianas</a></li-->
					<li><a href="http://www.americanas.com.br/sublinha/358506/moda/feminino/calcados/mocassim?chave=moda_menuhor_f_mocassim">mocassim</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358466/moda/feminino/calcados/scarpin?chave=moda_menuhor_f_scarpin">scarpin</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358539/moda/feminino/calcados/sandalias?chave=moda_menuhor_f_sandalias">sand�lias</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358465/moda/feminino/calcados/sapatilhas?chave=moda_menuhor_f_sapatilhas">sapatilhas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358467/moda/feminino/calcados/tenis?chave=moda_menuhor_f_tenis">t�nis</a></li>
					</div>
					
					<!--Bolsas e Acess�rios-->
					<div>
					<li class="title"><a href="http://www.americanas.com.br/linha/358360/moda/feminino/bolsas-e-acessorios?chave=moda_menuhor_f_bolsas-e-acessorios" title="Bolsas e Acess�rios">bolsas e acess�rios</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358501/moda/feminino/bolsas-e-acessorios/bolsas?chave=moda_menuhor_f_bolsas">bolsas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358502/moda/feminino/bolsas-e-acessorios/cintos?chave=moda_menuhor_f_cintos">cintos</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358463/moda/feminino/bolsas-e-acessorios/joias-e-bijuterias?chave=moda_menuhor_f_joias-e-bijuterias">j�ias e bijuterias</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358503/moda/feminino/bolsas-e-acessorios/lencos?chave=moda_menuhor_f_lencos">len�os</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358462/moda/feminino/bolsas-e-acessorios/oculos-de-sol?chave=moda_menuhor_f_oculos-de-sol">�culos de sol</a></li>
				
					</div>
					
					
					<!--Marcas-->
					<div class="last">
					<li class="title"><a href="#" title="Marcas" class="linkFixModa">marcas</a></li>
					<li><a href="http://www.americanas.com.br/marca/coca%20cola%20clothing?group=%22483949%22">Coca-Cola</a></li>
					<li><a href="http://www.americanas.com.br/marca/284">284</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358456/moda/feminino/roupas/calcas-jeans-levis?f_359022=levis">Levis</a></li>
					<li><a href="http://www.americanas.com.br/marca/ballasox">Ballasox</a></li>
					<li><a href="http://www.americanas.com.br/marca/beira%20rio">Beira Rio</a></li>
				
					</div>
					
					
					
					<div class="last banner">
						 <script type="text/javascript">try {OAS_AD("x20")} catch (exp) {handleException(exp);}</script>
					</div>
					
					</div>
					
					
				  </ul>
				
			</li>
			
			<!--Masculino-->
			<li class="it-list-mtf has-sub">
				<a href="http://www.americanas.com.br/subloja/358355/masculino?chave=moda_menuhor_m" class="lnk-mtf">masculino</a>
				<span class="selected"></span>
				
				<!--menu drop-down-->
				 <ul>
					<div class="content_drop">
					<!--Roupas-->
					<div>
					<li class="title"><a href="http://www.americanas.com.br/linha/358361/moda/masculino/roupas?chave=moda_menuhor_m_roupas" title="Roupas">roupas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358664/moda/masculino/roupas/bermudas-e-shorts?chave=moda_menuhor_m_bermudas-e-shorts">bermudas e shorts</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358684/moda/masculino/roupas/blazer-e-jaqueta?chave=moda_menuhor_m_blazer-e-jaqueta">blazer e jaqueta</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358682/moda/masculino/roupas/calcas?chave=moda_menuhor_m_calcas">cal�as</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358667/moda/masculino/roupas/calcas-jeans?chave=moda_menuhor_m_calcas-jeans">cal�a jeans</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358694/moda/masculino/roupas/camisas?chave=moda_menuhor_m_camisas">camisas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358676/moda/masculino/roupas/camisetas-e-blusas?chave=moda_menuhor_m_camisetas-e-blusas">camisetas e blusas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358712/moda/masculino/roupas/casaco-e-cardigan?chave=moda_menuhor_m_casaco-e-cardigan">casaco e cardigan</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358715/moda/masculino/roupas/cueca?chave=moda_menuhor_m_cueca">cueca</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358687/moda/masculino/roupas/meias?chave=moda_menuhor_m_meias">meias</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358717/moda/masculino/roupas/moda-praia?chave=moda_menuhor_m_moda-praia">moda praia</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358695/moda/masculino/roupas/polo?chave=moda_menuhor_m_polo">polo</a></li>
					</div>
					
					<!--Cal�ados-->
					<div>
					<li class="title"><a href="http://www.americanas.com.br/linha/358394/moda/masculino/calcados?chave=moda_menuhor_m_calcados" title="Cal�ados">cal�ados</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358718/moda/masculino/calcados/botas?chave=moda_menuhor_m_botas">botas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358701/moda/masculino/calcados/chinelos?chave=moda_menuhor_m_chinelos">chinelos</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358719/moda/masculino/calcados/dockside?chave=moda_menuhor_m_dockside">dockside</a></li>
					<!--li><a href="http://www.americanas.com.br/sublinha/358702/moda/masculino/calcados/havaianas?chave=moda_menuhor_m_havaianas">havaianas</a></li-->
					<li><a href="http://www.americanas.com.br/sublinha/358703/moda/masculino/calcados/mocassim?chave=moda_menuhor_m_mocassim">mocassim</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358720/moda/masculino/calcados/sandalia?chave=moda_menuhor_m_sandalia">sand�lia</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358732/moda/masculino/calcados/sapato?chave=moda_menuhor_m_sapato">sapato</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358733/moda/masculino/calcados/tenis?chave=moda_menuhor_m_tenis">t�nis</a></li>
			
					</div>
					
					<!--Bolsas e Acess�rios-->
					<div>
					<li class="title"><a href="http://www.americanas.com.br/linha/358362/moda/masculino/acessorios?chave=moda_menuhor_m_acessorios" title="Acess�rios">acess�rios</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358793/moda/masculino/acessorios/bones-e-chapeus?chave=moda_menuhor_m_bones-e-chapeus">bon�s e chap�us</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/361172/moda/masculino/acessorios/carteiras?chave=moda_menuhor_m_carteiras">carteiras</a></li>					
					<li><a href="http://www.americanas.com.br/sublinha/358794/moda/masculino/acessorios/cintos?chave=moda_menuhor_m_cintos">cintos</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358795/moda/masculino/acessorios/oculos-de-sol?chave=moda_menuhor_m_oculos-de-sol">�culos de sol</a></li>
			
				
					</div>
					
					<!--Marcas-->
					<div class="last">
					<li class="title"><a href="#" title="Marcas" class="linkFixModa">marcas</a></li>
					<li><a href="http://www.americanas.com.br/marca/acostamento?group=%22483947%22">Acostamento</a></li>
					<li><a href="http://www.americanas.com.br/marca/colcci?group=%22483947%22">Colcci</a></li>
					<li><a href="http://www.americanas.com.br/marca/handbook?group=%22483947%22">Handbook</a></li>
					<li><a href="http://www.americanas.com.br/marca/m.officer">M Officer</a></li>
					<li><a href="http://www.americanas.com.br/marca/mr.%20kitsch">Mr Kitsch </a></li>
					<li><a href="http://www.americanas.com.br/marca/polo%20collection?group=%22483947%22">Polo Collection </a></li>
					<li><a href="http://www.americanas.com.br/marca/ferracini">Ferracini</a></li>
					<li><a href="http://www.americanas.com.br/marca/kildare">Kildare</a></li>
				
					</div>
					
					<div class="last banner">
						 <script type="text/javascript">try {OAS_AD("x21")} catch (exp) {handleException(exp);}</script>
					</div>
					
					</div>
				  </ul>
				  

			</li>
			
			<!--Infantil-->
			<li class="it-list-mtf has-sub">
				<a href="http://www.americanas.com.br/subloja/358432/infantil?chave=moda_menuhor_i" class="lnk-mtf">infantil</a>
				<span class="selected"></span>
				<!--menu drop-down-->
				 <ul class="inf">
					<div class="content_drop">
					<!--Roupas-->
					<div>
					<li class="title"><a href="#" title="Roupas" class="linkFixModa">roupas</a></li>
					<li class="subtitle"><a href="http://www.americanas.com.br/sublinha/358417/moda/infantil/meninas/roupas?chave=moda_menuhor_i_f_roupas" title="Roupas">meninas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358735/moda/infantil/meninas/roupas/camisetas-e-blusas?chave=moda_menuhor_i_f_camisetas-e-blusas">camisetas e blusas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358738/moda/infantil/meninas/roupas/body?chave=moda_menuhor_i_f_body">body</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358723/moda/infantil/meninas/roupas/calcas?chave=moda_menuhor_i_f_calcas">cal�as</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358724/moda/infantil/meninas/roupas/macacao?chave=moda_menuhor_i_f_macacao">macac�o</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358726/moda/infantil/meninas/roupas/saias?chave=moda_menuhor_i_f_saias">saias</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358739/moda/infantil/meninas/roupas/vestidos?chave=moda_menuhor_i_f_vestidos">vestidos</a></li>
					<li class="subtitle"><a href="http://www.americanas.com.br/sublinha/358419/moda/infantil/meninos/roupas?chave=moda_menuhor_i_m_roupas" title="Roupas">meninos</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358775/moda/infantil/meninos/roupas/bermudas-e-shorts?chave=moda_menuhor_i_m_bermudas-e-shorts">bermudas e shorts</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358772/moda/infantil/meninos/roupas/camisetas-e-blusas?chave=moda_menuhor_i_m_camisetas-e-blusas">camisetas e blusas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358762/moda/infantil/meninos/roupas/body?chave=moda_menuhor_i_m_body">body</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/360792/moda/infantil/meninos/roupas/calcas?chave=moda_menuhor_i_m_calcas">cal�as</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358774/moda/infantil/meninos/roupas/macacao?chave=moda_menuhor_i_m_macacao">macac�o</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358760/moda/infantil/meninos/roupas/polo?chave=moda_menuhor_i_m_polo">polo</a></li>
					
					</div>
					
					<!--Cal�ados-->
					<div class="last">
					<li class="title"><a href="#" title="Cal�ados" class="linkFixModa">cal�ados</a></li>
					<li class="subtitle"><a href="http://www.americanas.com.br/sublinha/358418/moda/infantil/meninas/calcados?chave=moda_menuhor_i_f_calcados">meninas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358753/moda/infantil/meninas/calcados/botas?chave=moda_menuhor_i_f_botas">botas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358729/moda/infantil/meninas/calcados/chinelos?chave=moda_menuhor_i_f_chinelos">chinelos</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358754/moda/infantil/meninas/calcados/sandalias?chave=moda_menuhor_i_f_sandalias">sand�lias</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358755/moda/infantil/meninas/calcados/sapatilhas?chave=moda_menuhor_i_f_sapatilhas">sapatilhas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358730/moda/infantil/meninas/calcados/sapatos?chave=moda_menuhor_i_f_sapatos">sapatos</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358756/moda/infantil/meninas/calcados/tenis?chave=moda_menuhor_i_f_tenis">t�nis</a></li>
					<li class="subtitle"><a href="http://www.americanas.com.br/sublinha/358436/moda/infantil/meninos/calcados?chave=moda_menuhor_i_m_calcados">meninos</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358771/moda/infantil/meninos/calcados/alpargatas?chave=moda_menuhor_i_m_alpargatas">alpargatas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358777/moda/infantil/meninos/calcados/botas?chave=moda_menuhor_i_m_botas">botas</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358768/moda/infantil/meninos/calcados/chinelos?chave=moda_menuhor_i_m_chinelos">chinelos</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358769/moda/infantil/meninos/calcados/sandalias?chave=moda_menuhor_i_m_sandalias">sand�lias</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358770/moda/infantil/meninos/calcados/sapatos?chave=moda_menuhor_i_m_sapatos">sapatos</a></li>
					<li><a href="http://www.americanas.com.br/sublinha/358778/moda/infantil/meninos/calcados/tenis?chave=moda_menuhor_i_m_tenis">t�nis</a></li>
					</div>
					
							
					<!--Marcas-->
					
					<!--<div>
					<li class="title"><a href="#" title="Marcas">Marcas</a></li>
					<li><a href="">Tip Top</a></li>
					<li><a href="">Green</a></li>
					<li><a href="">Disney</a></li>
					<li><a href="">Levis</a></li>
					<li><a href="">Fakini</a></li>
					<li><a href="">Leete</a></li>
					<li><a href="">Paul Frank </a></li>
					<li><a href="">Puramania </a></li>
					<li><a href="">Tyrol</a></li>
					<li><a href="">Puma</a></li>
					<li><a href="">Sun John</a></li>
				
					</div>-->
					
					<div class="last banner">
						 <script type="text/javascript">try {OAS_AD("x22")} catch (exp) {handleException(exp);}</script>
					</div>
					
					</div>
				  </ul>
		
			</li>
			
			<!--Liquida��o-->
			<li class="it-list-mtf last">
				<a href="http://www.americanas.com.br/Moda-Liquidacao" class="lnk-mtf">liquida��o</a>
			</li>
			
		</ul>
	</div>
</div>
		</div>
		<div class="grid-control grid-fix-m-10">
			<div class="has-divisionbottom">
				<div class="pure-u-1-1">
	<div class="app-oas" data-oas-width="100" data-oas-height="100" data-oas="x94" data-component="banner"></div>
	
	<style type="text/css">
		/* style app-product slide */
		.tpl-moda-1 .product-container {
	width:870px;
	height:300px;
	position:relative;
}
.tpl-moda-1 .product-container .product-image {
	height:300px;
	position:absolute;
	top:0;
	left:100px;
}
.tpl-moda-1 .product-container .product-title {
	position:absolute;
	top:57px;
	left:480px;
	font-size:18px;
	font-weight:bold;
}
.tpl-moda-1 .product-container .product-info {
	position:absolute;
	top:128px;
	left:480px;
	font-size:18px;
	font-weight:bold;
}
.tpl-moda-1 .product-container .product-price-value-wrapper {
	color:#ea0104;
	border:none !important;
}
.tpl-moda-1 .product-container .product-price-value {
	font-size:50px;
}
.tpl-moda-1 .product-container .product-price-value .product-price-cents {
	font-size:30px;
}
.tpl-moda-1 .product-container .product-info .product-price-secondary {
	display:none;
}
.tpl-moda-1 .product-info .btn-buy {
	display: block;
	margin-top: 40px;
	font-weight: 700;
	font-size: 25px;
	color: #e60014;
	float: none;
	padding-left:0;
}
.tpl-moda-1 .product-info .btn-buy .icon-shopping-cart {
	background-image: url(http://iacom.s8.com.br/mktacom/apps/v0.1.8/images/icon-s96de46d3ea.png);
	background-position: 0 -207px;
	background-repeat: no-repeat;
	display: block;
	height: 26px;
	width: 28px;
	float: left;
	margin-right: 10px;
	margin-top: -2px;
	margin-left:0;
}
	</style>
	<div class="app-banner banner-moda">    
    <div class="app-slide">
        <div class="slide-items">         


		
		<div class="app-product tpl-moda-1 " data-product-id="119698354"></div>

         <a href="http://www.americanas.com.br/ofertas/HomeLandingPage3/484247/358352/119590600/119588446?ofertas.order=salesPriceasc&ofertas.dir=asc?chave=moda_home_top1">
                <img src="http://img.americanas.com.br/mktacom/2014/home-dptos/moda/banners_acom/finais/departamento/140811_polos_top_home_branco_Dia_Moda.jpg" alt="" width="870" height="300"/>
         </a>
		 
		 <a class="app-product product-price-invoice" data-product-id="113648532"></a>
		 
		 <a class="app-product " data-product-id="115087100"></a>


         <a href="http://www.americanas.com.br/ofertas/HomeLandingPage5/tag-masc-camisetas80off/358361?ofertas.order=salesPriceasc&ofertas.dir=asc&chave=moda_home_top2">
                <img src="http://img.americanas.com.br/mktacom/2014/home-dptos/moda/banners_acom/finais/departamento/140811_camisetas_top_home_branco_Dia_Moda.jpg" alt="" width="870" height="300"/>
         </a>


         <a href="http://www.americanas.com.br/ofertas/HomeLandingPage5/tag-fashion-camisetastiptopinfa?chave=moda_home_top3">
                <img src="http://img.americanas.com.br/mktacom/2014/home-dptos/moda/banners_acom/finais/departamento/140811_camiseta_tiptop_top_home_branco_Dia_Moda.jpg" alt="" width="870" height="300"/>
         </a>


         <a href="http://www.americanas.com.br/Moda-Liquidacao?chave=moda_home_top4">
                <img src="http://img.americanas.com.br/mktacom/2014/home-dptos/moda/banners_acom/finais/departamento/140714_Liquida_top_home.jpg" alt="" width="870" height="300"/>
         </a>


        </div>
        <div class="slide-pager">
            <div class="slide-pager-next">Pr�ximo</div>
            <div class="slide-pager-prev">Anterior</div>
        </div>
    </div>
</div>
				</div>
				<div class="pure-u-1-1">
	<div class="app-oas" data-oas-width="950" data-oas-height="45" data-oas="Bottom" data-component="banner"></div>
				</div>
				<div class="pure-g">
					<div class="pure-u-1-3">
	<div class="app-oas" data-oas-width="310" data-oas-height="300" data-oas="x01" data-component="banner"></div>
					</div>
					<div class="pure-u-1-3">
	<div class="app-oas" data-oas-width="310" data-oas-height="300" data-oas="x02" data-component="banner"></div>
					</div>
					<div class="pure-u-1-3"><style>
  .newsletter-moda{
    height:90px;
}
</style>
<div class="newsletter-moda">
    <h1 class="tit">novidades que n�o saem de moda</h1>

    <style type="text/css">
	.newsletter-moda{position:relative}.small-news-moda .newsletter-moda{padding:16px;position:relative}.newsletter-moda .pure-control-group{position:relative;margin-bottom:10px;margin-top:-10px}
    </style>

    <form action="http://apps.americanas.com.br/responsys/cr.php" method="get" role="form" data-sucess="obrigado, acesse seu e-mail para confirmar seu cadastro." class="valid-form-newsletter-moda use-tip" novalidate="novalidate">
        <input type="hidden" name="event_id" value="3455" />
        <input type="hidden" name="event_name" value="CR_MODA" />
        <input type="hidden" name="origem" value="45" />
        <input type="hidden" name="opt" value="out" />
        <input type="hidden" name="send_email" value="true" />
        <div class="pure-control-group">
            <input class="ipt" type="email" placeholder="digite seu e-mail" name="cr-email" id="cr-email" required="required"><label for="cr-email" class="error" style="display: inline;">digite seu e-mail</label>
        </div>
        <label>
            <span class="custom-input-radio spt-b-check">
                <input type="radio" name="cr-gender" required="required" value="M" class="error"><label for="cr-gender" class="error" style="display: inline;">selecione um g�nero</label>
            </span>homem
        </label>
        <label>
            <span class="custom-input-radio spt-b-check">
                <input type="radio" name="cr-gender" required="required" value="F" class="error">
            </span>mulher
        </label>

        <a href="/estaticapop/politica-de-privacidade-lightbox" data-modal-maxheight="400" data-modal-maxwidth="550" data-modal="true" style="float:left; clear:both; font-size:10px;color:#676767;position: absolute;bottom: 6px;left: 21px;" class="">pol�tica de privacidade</a>

        <button type="submit" class="spt-b-ok2 pull-right disabled" data-title="digite seu e-mail e selecione um g�nero">Cadastrar</button>
    </form>
</div>
	<div class="app-oas" data-oas-width="310" data-oas-height="150" data-oas="x03" data-component="banner"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-control"><section class="model-box">
    <div class="title">
        <h2 class="t-txt">
            marcas mais buscadas
        </h2>
    </div>
    <div class="content">
<div class="a-carousel brands-carousel">
	<ul class="a-carousel-list">
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/coca-cola%20clothing">
					<span class="spt-brand-coca-cola-clothing"></span>
				</a>
			</li>
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/colcci?group="483947"">
					<span class="spt-brand-colcci"></span>
				</a>
			</li>
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/evoke">
					<span class="spt-brand-evoke"></span>
				</a>
			</li>
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/iodice">
					<span class="spt-brand-iodice"></span>
				</a>
			</li>
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/levi's">
					<span class="spt-brand-levis"></span>
				</a>
			</li>
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/polo%20ralph%20lauren?group="483946"">
					<span class="spt-brand-polo-ralph-lauren"></span>
				</a>
			</li>
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/reserva">
					<span class="spt-brand-reserva"></span>
				</a>
			</li>
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/timberland?group="483946"">
					<span class="spt-brand-timberland"></span>
				</a>
			</li>
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/triton?group="483947"">
					<span class="spt-brand-triton"></span>
				</a>
			</li>
			<li class="a-carousel-item">
				<a href="http://www.americanas.com.br/marca/zapalla">
					<span class="spt-brand-zapalla"></span>
				</a>
			</li>
	</ul>
</div>
 </div>
</section>
		</div>
		<div class="grid-control">
			<div class="has-divisionbottom">
	<div class="app-oas" data-oas-width="950" data-oas-height="110" data-oas="x04" data-component="banner"></div>
			</div>
		</div>
		<div class="grid-control">
			<section class="model-box has-divisionbottom model-banner-carousel">
				<div class="pure-g">
					<div class="pure-u-1">
	<div class="app-oas" data-oas-width="950" data-oas-height="70" data-oas="x10" data-component="banner"></div>
					</div>
					<div class="pure-u-1-4">
	<div class="app-oas" data-oas-width="240" data-oas-height="360" data-oas="x05" data-component="banner"></div>
					</div>
					<div class="pure-u-3-4"><section class="model-box " data-slide-toggle="false" data-slide-child=".content" data-slide-button=".title" data-component="carrossel">
    <div class="content">
        <div class="a-carousel grid">
            <div class="a-carousel-list">
	<article class="single-product vitrine200   fashion-product 0" data-component="single-product">
		<form action="http://carrinho.americanas.com.br/checkout/">
		<input class="productId" name="codProdFusion" type="hidden" value="113648532" />
		<input name="codItemFusion" type="hidden" />
		<div class="productImg">
			<a href="http://www.americanas.com.br/produto/113648532/vestido-iodice-denim-voil-jacquard-c-renda" title="Vestido I�dice Denim Voil Jacquard c/ Renda" class="url">
			<img width="200" height="200" data-alternative-src="" alt="Vestido I�dice Denim Voil Jacquard c/ Renda" src="http://iacom.s8.com.br/statics-1.14.7/catalog/img/blank.gif" data-src="http://iacom.s8.com.br/produtos/01/00/sku/113648/5/113648541G1.jpg" class="photo lazy" />
		</a>
		</div>
		<div class="productInfo">
				<div class="top-area-product">
					<a href="http://www.americanas.com.br/produto/113648532/vestido-iodice-denim-voil-jacquard-c-renda" title="Vestido I�dice Denim Voil Jacquard c/ Renda" class="prodTitle"> Vestido I�dice Denim Voil Jacquard c/ Renda
						<span class="model"></span>
						<span class="size"></span>
					</a>
				</div>
				<div class="product-info">
					<div class="price-area">
							<span class="regular price">
								<del>
									de R$ 361,00 por
								</del>
							</span>
						<span class="sale price">
							<strong>
								R$ 204,90
							</strong>
						</span>
							<div class="interest">
								<span class="parcel">
									10x de R$ 20,49
								</span>
								<span class="condition">
									sem juros
								</span>
							</div>
							<div class="mb-save">
								Economize 43%
							</div>
					</div>
	<div class="freight" data-component="stamp">
						<a href="/estaticapop/regras-do-site" data-modal-maxwidth="760" data-modal="true">
							<img src="http://img.americanas.com.br/img/_staging/frete_sul_sudeste_centro_nordeste.gif" alt="frete_sul_sudeste_centro_nordeste" />
						</a>
	</div>
						<div class="see-more">
							+
							<a href="http://www.americanas.com.br/marca/I�dice%20Denim">
								I�dice Denim
							</a>
						</div>
				</div>
		</div>
		</form>
	</article>
	<article class="single-product vitrine200   fashion-product 0" data-component="single-product">
		<form action="http://carrinho.americanas.com.br/checkout/">
		<input class="productId" name="codProdFusion" type="hidden" value="114821659" />
		<input name="codItemFusion" type="hidden" />
		<div class="productImg">
			<a href="http://www.americanas.com.br/produto/114821659/regata-doc-dog-renda-ii" title="Regata Doc Dog Renda II" class="url">
			<img width="200" height="200" data-alternative-src="" alt="Regata Doc Dog Renda II" src="http://iacom.s8.com.br/statics-1.14.7/catalog/img/blank.gif" data-src="http://iacom.s8.com.br/produtos/01/00/sku/114821/6/114821691G1.jpg" class="photo lazy" />
		</a>
		</div>
		<div class="productInfo">
				<div class="top-area-product">
					<a href="http://www.americanas.com.br/produto/114821659/regata-doc-dog-renda-ii" title="Regata Doc Dog Renda II" class="prodTitle"> Regata Doc Dog Renda II
						<span class="model"></span>
						<span class="size"></span>
					</a>
				</div>
				<div class="product-info">
					<div class="price-area">
							<span class="regular price">
								<del>
									de R$ 104,00 por
								</del>
							</span>
						<span class="sale price">
							<strong>
								R$ 64,90
							</strong>
						</span>
							<div class="interest">
								<span class="parcel">
									3x de R$ 21,63
								</span>
								<span class="condition">
									sem juros
								</span>
							</div>
							<div class="mb-save">
								Economize 37%
							</div>
					</div>
						<div class="see-more">
							+
							<a href="http://www.americanas.com.br/marca/Doc%20Dog">
								Doc Dog
							</a>
						</div>
				</div>
		</div>
		</form>
	</article>
	<article class="single-product vitrine200   fashion-product 0" data-component="single-product">
		<form action="http://carrinho.americanas.com.br/checkout/">
		<input class="productId" name="codProdFusion" type="hidden" value="115087100" />
		<input name="codItemFusion" type="hidden" />
		<div class="productImg">
			<a href="http://www.americanas.com.br/produto/115087100/short-jeans-oh-boy-barra-renda" title="Short Jeans Oh Boy! Barra Renda" class="url">
			<img width="200" height="200" data-alternative-src="" alt="Short Jeans Oh Boy! Barra Renda" src="http://iacom.s8.com.br/statics-1.14.7/catalog/img/blank.gif" data-src="http://iacom.s8.com.br/produtos/01/00/sku/115087/1/115087118G1.jpg" class="photo lazy" />
		</a>
		</div>
		<div class="productInfo">
				<div class="top-area-product">
					<a href="http://www.americanas.com.br/produto/115087100/short-jeans-oh-boy-barra-renda" title="Short Jeans Oh Boy! Barra Renda" class="prodTitle"> Short Jeans Oh Boy! Barra Renda
						<span class="model"></span>
						<span class="size"></span>
					</a>
				</div>
				<div class="product-info">
					<div class="price-area">
							<span class="regular price">
								<del>
									de R$ 275,00 por
								</del>
							</span>
						<span class="sale price">
							<strong>
								R$ 164,90
							</strong>
						</span>
							<div class="interest">
								<span class="parcel">
									8x de R$ 20,61
								</span>
								<span class="condition">
									sem juros
								</span>
							</div>
							<div class="mb-save">
								Economize 40%
							</div>
					</div>
	<div class="freight" data-component="stamp">
						<a href="/estaticapop/regras-do-site" data-modal-maxwidth="760" data-modal="true">
							<img src="http://img.americanas.com.br/img/_staging/frete_sul_sudeste_centro_nordeste.gif" alt="frete_sul_sudeste_centro_nordeste" />
						</a>
	</div>
						<div class="see-more">
							+
							<a href="http://www.americanas.com.br/marca/Oh,%20Boy!">
								Oh, Boy!
							</a>
						</div>
				</div>
		</div>
		</form>
	</article>
	<article class="single-product vitrine200   fashion-product 0" data-component="single-product">
		<form action="http://carrinho.americanas.com.br/checkout/">
		<input class="productId" name="codProdFusion" type="hidden" value="117460369" />
		<input name="codItemFusion" type="hidden" />
		<div class="productImg">
			<a href="http://www.americanas.com.br/produto/117460369/vestido-284-rendada" title="Vestido 284 Rendada" class="url">
			<img width="200" height="200" data-alternative-src="" alt="Vestido 284 Rendada" src="http://iacom.s8.com.br/statics-1.14.7/catalog/img/blank.gif" data-src="http://iacom.s8.com.br/produtos/01/00/sku/117460/3/117460393G1.jpg" class="photo lazy" />
		</a>
		</div>
		<div class="productInfo">
				<div class="top-area-product">
					<a href="http://www.americanas.com.br/produto/117460369/vestido-284-rendada" title="Vestido 284 Rendada" class="prodTitle"> Vestido 284 Rendada
						<span class="model"></span>
						<span class="size"></span>
					</a>
				</div>
				<div class="product-info">
					<div class="price-area">
							<span class="regular price">
								<del>
									de R$ 279,00 por
								</del>
							</span>
						<span class="sale price">
							<strong>
								R$ 254,90
							</strong>
						</span>
							<div class="interest">
								<span class="parcel">
									10x de R$ 25,49
								</span>
								<span class="condition">
									sem juros
								</span>
							</div>
							<div class="mb-save">
								Economize 8%
							</div>
					</div>
	<div class="freight" data-component="stamp">
						<a href="/estaticapop/regras-do-site" data-modal-maxwidth="760" data-modal="true">
							<img src="http://img.americanas.com.br/img/_staging/frete_sul_sudeste_centro_nordeste.gif" alt="frete_sul_sudeste_centro_nordeste" />
						</a>
	</div>
						<div class="see-more">
							+
							<a href="http://www.americanas.com.br/marca/284">
								284
							</a>
						</div>
				</div>
		</div>
		</form>
	</article>
	<article class="single-product vitrine200   fashion-product 0" data-component="single-product">
		<form action="http://carrinho.americanas.com.br/checkout/">
		<input class="productId" name="codProdFusion" type="hidden" value="7032742" />
		<input name="codItemFusion" type="hidden" />
		<div class="productImg">
			<a href="http://www.americanas.com.br/produto/7032742/oculos-mormaii-joaca-chumbo-fosco-brilho" title="�culos Mormaii Joaca Chumbo Fosco Brilho" class="url">
			<img width="200" height="200" data-alternative-src="" alt="�culos Mormaii Joaca Chumbo Fosco Brilho" src="http://iacom.s8.com.br/statics-1.14.7/catalog/img/blank.gif" data-src="http://iacom.s8.com.br/produtos/01/00/item/7032/7/7032742g1.jpg" class="photo lazy" />
		</a>
		</div>
		<div class="productInfo">
				<div class="top-area-product">
					<a href="http://www.americanas.com.br/produto/7032742/oculos-mormaii-joaca-chumbo-fosco-brilho" title="�culos Mormaii Joaca Chumbo Fosco Brilho" class="prodTitle"> �culos Mormaii Joaca Chumbo Fosco Brilho
						<span class="model"></span>
						<span class="size"></span>
					</a>
				</div>
				<div class="product-info">
					<div class="price-area">
						<span class="sale price">
							<strong>
								R$ 199,90
							</strong>
						</span>
							<div class="interest">
								<span class="parcel">
									9x de R$ 22,21
								</span>
								<span class="condition">
									sem juros
								</span>
							</div>
					</div>
						<div class="see-more">
							+
							<a href="http://www.americanas.com.br/marca/Mormaii">
								Mormaii
							</a>
						</div>
				</div>
		</div>
		</form>
	</article>
	<article class="single-product vitrine200   fashion-product 0" data-component="single-product">
		<form action="http://carrinho.americanas.com.br/checkout/">
		<input class="productId" name="codProdFusion" type="hidden" value="116351937" />
		<input name="codItemFusion" type="hidden" />
		<div class="productImg">
			<a href="http://www.americanas.com.br/produto/116351937/shorts-guarana-brasil-renda-classico" title="Shorts Guaran� Brasil Renda Cl�ssico" class="url">
			<img width="200" height="200" data-alternative-src="" alt="Shorts Guaran� Brasil Renda Cl�ssico" src="http://iacom.s8.com.br/statics-1.14.7/catalog/img/blank.gif" data-src="http://iacom.s8.com.br/produtos/01/00/sku/116351/9/116351945G1.jpg" class="photo lazy" />
		</a>
		</div>
		<div class="productInfo">
				<div class="top-area-product">
					<a href="http://www.americanas.com.br/produto/116351937/shorts-guarana-brasil-renda-classico" title="Shorts Guaran� Brasil Renda Cl�ssico" class="prodTitle"> Shorts Guaran� Brasil Renda Cl�ssico
						<span class="model"></span>
						<span class="size"></span>
					</a>
				</div>
				<div class="product-info">
					<div class="price-area">
						<span class="sale price">
							<strong>
								R$ 167,00
							</strong>
						</span>
							<div class="interest">
								<span class="parcel">
									8x de R$ 20,88
								</span>
								<span class="condition">
									sem juros
								</span>
							</div>
					</div>
	<div class="freight" data-component="stamp">
						<a href="/estaticapop/regras-do-site" data-modal-maxwidth="760" data-modal="true">
							<img src="http://img.americanas.com.br/img/_staging/frete_sul_sudeste_centro_nordeste.gif" alt="frete_sul_sudeste_centro_nordeste" />
						</a>
	</div>
						<div class="see-more">
							+
							<a href="http://www.americanas.com.br/marca/Guaran�%20Brasil">
								Guaran� Brasil
							</a>
						</div>
				</div>
		</div>
		</form>
	</article>
            </div>
        </div>
    </div>
</section>
					</div>
				</div>
			</section>
		</div>
		<div class="grid-control grid-fix-m-10">
			<div class="pure-g">
				<div class="pure-u-1-4">
	<div class="app-oas" data-oas-width="230" data-oas-height="500" data-oas="x06" data-component="banner"></div>
				</div>
				<div class="pure-u-1-4">
	<div class="app-oas" data-oas-width="230" data-oas-height="500" data-oas="x07" data-component="banner"></div>
				</div>
				<div class="pure-u-1-4">
	<div class="app-oas" data-oas-width="230" data-oas-height="500" data-oas="x08" data-component="banner"></div>
				</div>
				<div class="pure-u-1-4">
	<div class="app-oas" data-oas-width="230" data-oas-height="500" data-oas="x09" data-component="banner"></div>
				</div>
			</div>
		</div>
	</section>
	<div class="grid-control">
			<div class="pure-g grid-fix-m-10">
				<div class="pure-u-1-1">
	<div class="app-oas" data-oas-width="950" data-oas-height="100" data-oas="BottomLeft" data-component="banner"></div>
				</div>
			</div>
		</div><NOSCRIPT>
<img alt="�" width="1" height="1" src="http://wtb.americanas.com.br/dcs20cr2u0pkytlvcatlar3yv_5v5h/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.0.2" />
</NOSCRIPT><style>
    .bannerCustomFull{
        letter-spacing: 0.001em !important;
    }
    .bannerCustom .label{
        color: #fff;
    }
    .bannerCustom.bannerYellow .label{
        color: #666;
    }
.selo-level{
 float:right;
clear:both;
margin-right:20px;
}
    .bannerCustom .product-price{
        bottom: 43px !important;
        
    }
    .bannerCustom .product-price-value{
        font-size: 20px !important;
        height: 22px !important;
        line-height: 23px !important;
    }
    .bannerCustom .product-price-currency{
        left: -21px !important;
        font-size: 14px !important;
    }
    .bannerCustom.product-price .product-price-value-wrapper{
        margin-left: 20px !important;
    }
    .bannerCustom.app-product .btn-buy{
        margin-top: 78px !important;
        line-height: 19px !important;
    }
    .bannerCustom  .product-price .product-price-cents{
       line-height: 19px !important;
    }
    .bannerCustom.bannerBlue{
        background-color: #0088ce;
    }
    .bannerCustom.bannerRed{
        background-color: #e20f3a;
    }
    .bannerCustom.bannerYellow{
        background-color: #edfe36;
    }
    .bannerCustom.bannerYellow  .product-price{
        color: #e70014;
    }
    .bannerCustom.bannerRed  *, .bannerCustom.bannerBlue  *{
        color: #fff;
    }
    .bannerCustom.bannerRed .btn-buy .icon-shopping-cart,
    .bannerCustom.bannerBlue  .btn-buy .icon-shopping-cart{
        background-position: 0  -408px;
    }
    .bannerCustom.bannerYellow .btn-buy{
        color: #e70014;
    }  
    .bannerCustom.bannerYellow .btn-buy .icon-shopping-cart{
        background-position: 0  -423px;
    }
    </style>

  <script>
     App.on('load', function () {
        var App = this, $ = this.$;
        oas.on('print:end', function(){
            console.log('OAS READY');
             App.$('.bannerCustomFull').parent().parent().parent().parent().removeClass('pure-u-1-2').addClass('pure-1-1');
        });
       /*App.$('.bannerCustom .btn-buy').html("<i class='icon-shopping-cart'></i> Compre");*/
      
     });
    </script>

<style>
.mobileVersion p{
 text-align:center;
}
</style>
<link rel="stylesheet" href="http://iacom.s8.com.br/statics-1.14.7/catalog/css/v1/footer.min.css"/>
<style>
.a-footer .ft-list1{list-style-position:outside;padding-left:15px;}
.a-footer .ft-info .ft-questions, .a-footer .ft-info .ft-institutional {padding: 0 0 0 30px;}
.a-footer .ft-list1 .ft-lt1-it {margin-bottom: 4px;line-height: 1.2;}
.a-footer .ft-info .ft-if-seals {float: right;width: auto;padding-top: 45px;}
</style>

<footer class="a-footer" role="contentinfo" itemscope="itemscope" itemtype="http://data-vocabulary.org/Organization">
    <div class="ft-wp">
        <div class="ft-header">
             <a href="/" title="Americanas.com" class="ft-logo" itemprop="url" crmwa_mdc="Footer|americanas.com|"><span itemprop="name">Americanas.com</span></a>
             <strong class="ft-slogan">a maior loja. os menores pre�os.</strong>
             <span class="ft-txt">
                n�o encontrou o que procurava? <a href="/estaticapop/deixar-sugestao" data-modal="true" data-modal-maxwidth="550" data-modal-maxheight="400" title="deixe aqui a sua sugest�o de produto." class="ft-txt-lnk" crmwa_mdc="Footer|deixe_aqui_a_sua_sugestao_de_produto.|">deixe aqui a sua sugest�o de produto.</a>
             </span>
        </div>
        <div class="ft-info">
            <div class="ft-pure-g">
                <div class="ft-pure-u-1-5">
                    <div class="ft-telephones">
                        <strong class="ft-tit">
                            <a href="http://www.americanas.com.br/estaticapop/popCentralAtendimento/" title="Compre pelo telefone" crmwa_mdc="Footer|compre_pelo_telefone|" data-modal="true" data-modal-maxwidth="750" data-modal-maxheight="430" target="_blank">compre pelo telefone <span itemprop="tel">4003.1000</span></a>
                        </strong>
<strong class="ft-tit">
                            <a href="http://www.americanas.com.br/central-de-atendimento?WT.mc_id=internas-atendimentoHome&amp;WT.mc_ev=click" style="
    text-decoration: underline;
">atendimento</a>
                        </strong>
                       
                    </div>
                </div>
                <div class="ft-pure-u-1-5">
                    <div class="ft-questions">
                        <strong class="ft-tit">d�vidas</strong>
                        <ul class="ft-list1">
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/central-de-atendimento" class="ft-lt1-lnk" title="Central de atendimento">Central de atendimento</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/estatica/como-comprar" class="ft-lt1-lnk" title="Como comprar">Como comprar</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://faq.americanas.com.br/faq/?A=MTU2&amp;B=&amp;all=true&amp;T=VHJvY2FzIGUgZGV2b2x1w6fDtWVz#MTU3" class="ft-lt1-lnk" title="Trocas e devolu��es">Trocas e devolu��es</a>
                            </li>
  <li class="ft-lt1-it">
                                <a href="http://faq.americanas.com.br/faq/?A=MTU2&amp;T=VHJvY2FzIGUgZGV2b2x1w6fDtWVz#MTU3+MjU0MDE=" class="ft-lt1-lnk" title="Direito de arrependimento">Direito de arrependimento</a>
                            </li>
 <li class="ft-lt1-it">
                                <a href="http://www.procon.rj.gov.br/" class="ft-lt1-lnk" target="_blank" title="Procon-RJ">Procon-RJ</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://faq.americanas.com.br/faq/?A=MTU0&amp;B=&amp;all=true&amp;T=UHJvY2Vzc28gZGUgZW50cmVnYQ==#MTU1" class="ft-lt1-lnk" title="Processo de Entrega">Processo de Entrega</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/estatica/recompra" class="ft-lt1-lnk" title="Resultado de promo��es">Resultado de promo��es</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="/estaticapop/politica-de-privacidade-lightbox" title="Pol�tica de privacidade" class="ft-lt1-lnk" data-modal="true" data-modal-maxwidth="550" data-modal-maxheight="400">Politica de privacidade</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/estaticapop/regras-do-site" title="Termos de uso e condi��es" class="ft-lt1-lnk" >Termos de uso e condi��es</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/estatica/alerta-de-emails" title="Alerta de e-mails e sites falsos" class="ft-lt1-lnk">Alerta de e-mails e sites falsos</a>
                            </li>
<li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/contrato-compra-e-venda" title="Termos e Condi��es de compra e venda de produtos" class="ft-lt1-lnk">Termos e Condi��es de compra e venda de produtos</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                <div class="ft-pure-u-1-5">
                    <div class="ft-institutional">
                        <strong class="ft-tit">institucional</strong>
                        <ul class="ft-list1">
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/estatica/sobre-americanas" class="ft-lt1-lnk" title="Sobre a Americanas.com">Sobre a Americanas.com</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.b2wdigital.com" title="Investidores B2W" class="ft-lt1-lnk">Investidores B2W</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://ri.lasa.com.br" title="Lojas Americanas S.A." class="ft-lt1-lnk">Lojas Americanas S.A.</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/lojamaisproxima" class="ft-lt1-lnk" title="Loja mais pr�xima">Loja mais pr�xima</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://vagas.com/lojasamericanas" class="ft-lt1-lnk" target="_blank" title="Trabalhe na Lojas Americanas">Trabalhe na Lojas Americanas</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://vagas.com/b2w" class="ft-lt1-lnk" target="_blank" title="Trabalhe na americanas.com">Trabalhe na americanas.com</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/estatica/assistencia-tecnica-fabricantes" class="ft-lt1-lnk">Nossos principais fornecedores</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/estatica/seja-nosso-fornecedor" class="ft-lt1-lnk" title="Seja nosso fornecedor">Seja nosso fornecedor</a>
                            </li>
                            <!--li class="ft-lt1-it">
                                <a href="#" class="ft-lt1-lnk">Anuncie na Americanas.com</a>
                            </li--> 
                            <li class="ft-lt1-it">
                                <a href="http://www.afiliados.com.br/americanas/" targe="_blank" class="ft-lt1-lnk">Programa de Afiliados</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/garantia-estendida" class="ft-lt1-lnk">Garantia Estendida</a>
                            </li>
<li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/1a-em-atendimento" class="ft-lt1-lnk">1� em atendimento ao cliente</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/estaticapop/assessoria-imprensa" data-modal="true" data-modal-maxWidth="550" class="ft-lt1-lnk">Assessoria de Imprensa</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ft-pure-u-1-5">
                    <div class="ft-services">
                        <strong class="ft-tit">servi�os</strong>
                        <ul class="ft-list1">
                            <li class="ft-lt1-it">
                                <a href="http://viagens.americanas.com.br/default.aspx?utm_source=americanas&amp;utm_medium=link_building&amp;utm_campaign=viagens&amp;s_cid=link_building_viagens" title="Americanas Viagens" class="ft-lt1-lnk">Americanas Viagens</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://carrinho.americanas.com.br/lista-de-casamento/pages/HomePage" title="Lista de casamento" class="ft-lt1-lnk">Lista de Casamento</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.americanas.com.br/negocios-corporativos" title="Vendas Corporativas" class="ft-lt1-lnk">Vendas Corporativas</a>
                            </li>
                        </ul>
                        <strong class="ft-tit">parceiros</strong>
                        <ul class="ft-list1">
                            <li class="ft-lt1-it">
                                <a href="http://novosite.ingresso.com/sao-paulo/home/?default=true" title="Ingresso.com" class="ft-lt1-lnk">Ingresso.com</a>
                            </li>
                            <li class="ft-lt1-it">
                                <a href="http://www.soubarato.com.br/?epar=ds_bs_00_am_siteacomsbacom3&amp;opn=SBACOM3" class="ft-lt1-lnk" title="Sou Barato">Sou Barato</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ft-pure-u-1-5">
                    <div class="ft-banner">
                       <div class="app-oas" data-oas-width="170" data-oas-height="200" data-oas="Right2"></div>
                    </div>
                </div>
            </div>
            <div class="ft-pure-g">
                <div class="ft-pure-u-1-2">
                    <div class="ft-news-social">
                        <strong class="ft-tit side">novidades, ofertas e mais</strong>
                        <ul class="ft-list2">
                            <li class="ft-lt2-it">
                                <a href="https://www.facebook.com/AmericanasCom" target="_blank" title="Facebook" class="ft-lt2-bt bt-fb">Facebook</a>
                            </li>
                            <li class="ft-lt2-it">
                                <a href="https://twitter.com/americanascom" target="_blank" title="Twitter" class="ft-lt2-bt bt-tw">Twitter</a>
                            </li>
                            <li class="ft-lt2-it">
                                <a href="https://plus.google.com/+americanascom/posts" target="_blank" title="Google+" class="ft-lt2-bt bt-gp">Google+</a>
                            </li>
                        </ul>
                    </div>
					<p class="brand-rules">As regras s�o v�lidas somente para produtos vendidos e entregues pela Americanas.com.</p>
                </div>

                <link type="text/css" rel="stylesheet" href="http://iacom.s8.com.br/mktacom/site/footer/css/cr-footer.css" />

                <div class="ft-pure-u-1-2">
                    <div class="ft-newsletter a-newsletter">
                        <form class="ft-form app-cr"
                              data-cr-origin="48"
                              data-cr-event-name="CR_FOOTER"
                              data-cr-event-id="4255"
                              data-cr-opt="out"
                              data-cr-confirmation="true">
                            <label for="cr-form-email-bottom-home">
                                <strong class="ft-tit side">ofertas exclusivas para voc�</strong>
                            </label>
                            <input type="text" placeholder="digite seu e-mail" name="cr-email" class="ft-fr-ipt" id="cr-form-email-bottom-home" />
                            <input type="submit" value="ok" class="ft-fr-bt"/>

                            <div class="cr-msg"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="ft-info">  
            <div class="ft-if-payment">
                <strong class="ft-tit">formas de pagamento</strong>
                <div class="ft-if-pay cards">
                    <span class="ft-small-tit">cart�es</span>
                    <ul class="ft-if-pay-lst">
                        <li>Visa</li>
                        <li>MasterCard</li>
                        <li>AmericanExpress</li>
                        <li>Diners Club</li>
                        <li>Aura</li>
                        <li>Elo</li>
                        <li>Casa</li>
                        <li>Caixa</li>
                    </ul>
                </div>
                <div class="ft-if-pay billet">
                    <span class="ft-small-tit">boleto</span>
                    <ul class="ft-if-pay-lst">
                        <li>Boleto Banc�rio</li>
                    </ul>
                </div>
                <div class="ft-if-pay debt">
                    <span class="ft-small-tit">d�bito online</span>
                    <ul class="ft-if-pay-lst">
                        <li>Visa</li>
                        <li>Ita�</li>
                        <li>Bradesco</li>
                        <li>Banco do Brasil</li>
                    </ul>
                </div>
            </div>
            <div class="ft-if-seals">
                <ul class="ft-list2">

                    <li class="ft-lt2-it">
                        <span class="ft-lt2-bt tim-seal">Movido com Intelig Telecom</span>
                    </li>
                    <li class="ft-lt2-it">
                        <span class="ft-lt2-bt green-campaign-seal">Movido com Intelig Telecom</span>
                    </li>
                </ul>
            </div>
            
            <div class="selo-level">
                <div class="app-oas" data-oas-width="170" data-oas-height="200" data-oas="Frame1"></div>
            </div>
        </div>
        <div class="ft-address">
            <address class="ft-address-txt"><span itemprop="address">B2W - Companhia Digital / CNPJ: 00776574/0001-56 / Inscri��o Estadual:
                492.513.778.117 / Endere�o: Rua Sacadura Cabral, 102 - Rio de Janeiro, RJ - 20081-902</span> <span itemprop="geo" hidden="hidden">latitude:-22.897409, longitude:-43.184372</span>
              <a href="http://www.americanas.com.br/atendimento-email" class="ft-email-lnk" title="email de atendimento" itemprop="email" target="_top"><img src="http://iacom.s8.com.br/mktacom/hotsite/footer/img/email-footer-01.jpg" /></a> - 
            </address>
        </div>
        <div class="mobileVersion">
            
        </div>
    </div>
</footer>
<script>
var isMobile = /iP(hone|od)|Android.*Mobile/.test(navigator.userAgent);
if (isMobile) { 
     var sitemapLink = document.getElementsByClassName('mobileVersion');
     if (sitemapLink && sitemapLink.length > 0) { 
       sitemapLink[0].innerHTML = sitemapLink[0].innerHTML + "<p><a href=\"javascript:void(0);\" onclick=\"javascript:setCookie('NewMobileOptOut','0'); window.location = window.location.href.replace('MobileOptOut=1','MobileOptOut=0'); return false;\">Acessar a Vers�o Mobile</a></p>";
     }
   }
   
if(typeof jQuery != 'undefined') {
if($(".a-main-product").length){
       $(".a-main-product .table-size>a").attr("data-modal-maxwidth","810").attr("data-modal-maxheight","400" );
    }
    $(function() {
    $(".home-page .pure-u-3-4 .single-product").each(function(index, b) {
        var href = $(this).find(".prodTitle").attr("href");
        $(this).find(".prodTitle").attr("href", href+"?chave=HM_SAZ_VT_"+(index+1));
        $(this).find(".url").attr("href", href+"?chave=HM_SAZ_VT_"+(index+1));
    });

    var count = 0;
    $(".home-page [data-oas='x33']").closest(".grid-control").find(".single-product").each(function(index) {
        var href = $(this).find(".prodTitle").attr("href");
        $(this).find(".prodTitle").attr("href", href+"?chave=HM_BL1_VT_"+(index+1));
        $(this).find(".url").attr("href", href+"?chave=HM_BL1_VT_"+(index+1));
        count = index+1;
    }); 
    $(".home-page [data-oas='x26']").closest(".grid-control").find(".single-product").each(function(index) {
        var href = $(this).find(".prodTitle").attr("href");
        count++;
        $(this).find(".prodTitle").attr("href", href+"?chave=HM_BL2_VT_"+(count));
        $(this).find(".url").attr("href", href+"?chave=HM_BL2_VT_"+(count));
    });
});
}

App.on('load', function (){
var str = location.href;
if(str.search("/estatica") > -1 || str.search("/hotsite") > -1){ 
$('.ft-lt1-lnk').click(function(){
App.$('.ft-lt1-lnk').fancybox({
type: 'iframe',
href: $(this).attr('href')
});
});
}
});


</script>

<script>
App.on('load', function(){
    var App = this, $ = this.$, _ = this._, $elemPar = $('.grid-control:nth-of-type(1)'), num = 0;
    $elemPar.addClass('bg-geral bg-0');
    oas.on('print:end', function(){
        var $elm = $('.banner-principal .slide-items');
        $elm.trigger('configuration', {
            scroll: {
                onBefore: function(data){
                    var attrsrc = data.items.visible.find('.product-image').data('original');
                    data.items.visible.find('.product-image').attr('src', attrsrc);
                    $elm.trigger('currentPosition', function(i){
                        $elemPar.removeClass('bg-' + num);
                        num = i;
                        $elemPar.addClass('bg-' + num);
                    });
                }
            }
        });
    });
});
</script><div class="sitemap-link" data-component="mapa_do_site">
        <a href="http://www.americanas.com.br/mapa-do-site/358352" class="ft-address-lnk">Mapa do site</a>
    </div>

	<script type="text/javascript" src="http://iacom.s8.com.br/statics-1.14.7/js/criteo_ld.js" async="true"></script>
		<script type="text/javascript">
			var CRITEO_CONF = [[{
			
			    pageType: 'list',
			    'Product IDs': [$(".single-product:eq(0) .productId").val(), $(".single-product:eq(1) .productId").val(), $(".single-product:eq(2) .productId").val()],
			    'Keywords': ''
			
			}], [8416,'pac','us.','110',[[7725630, 7725631]], {'Product IDs':['i',1],'Keywords':['kw',2]}]];
			if (typeof (CRITEO) != "undefined") { CRITEO.Load(false); }
		</script><script type="text/javascript">			
			var crmWA_dataLayer = crmWA_dataLayer || [];
			crmWA_dataLayer.push({
				'objDepartamento': '358352', 
				'objLinha': '',
				'objFamilia': '',
				'objSublinha': '' 
			});
		</script>
	<!-- general static content acom 296555-->



<!-- Selos -->
<script type="text/javascript" src="http://apps.americanas.com.br/media/selo/js/selo.js"></script>
<!-- End -->

<script type="text/javascript">
(function() {
    try {
        var viz = document.createElement('script');
        viz.type = 'text/javascript';
        viz.async = true;
        viz.src = ('https:' == document.location.protocol ?'https://ssl.vizury.com' : 'http://www.vizury.com')+ '/analyze/pixel.php?account_id=VIZVRM863';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(viz, s);
        viz.onload = function() {
            pixel.parse();
        };
        viz.onreadystatechange = function() {
            if (viz.readyState == "complete" || viz.readyState == "loaded") {
                pixel.parse();
            }
        };
    } catch (i) {
    }
})();
</script>



<!-- Adobe Marketing Cloud Tag Loader Code -->
<script type="text/javascript">//<![CDATA[
var amc=amc||{};if(!amc.on){amc.on=amc.call=function(){}};
document.write("<scr"+"ipt type=\"text/javascript\" src=\"//www.adobetag.com/d3/v2/ZDMtYjJ3LTYwMC0yNDgt/amc.js\"></sc"+"ript>");
//]]></script>
<!-- End Adobe Marketing Cloud Tag Loader Code -->

<!--Maxymiser script start -->
<script type="text/javascript"src="//service.maxymiser.net/cdn/pakua/submarino/js/mmcore.js"></script>
<!--Maxymiser script end -->


<!-- AMERICANAS -->
<!-- Google Code for Smart Pixel tag Remarketing New Implementation Static Content  -->
<script type="text/javascript">
//Tipo de p�gina
var path = location.pathname.toLowerCase();
var host = location.hostname;
if(host == "www.americanas.com.br" && path == "/" || host == "www.americanas.com.br" && path == ""){
  var tipo_de_pagina = "home"
}
else if(location.pathname.indexOf("/produto/") >=0){
   var tipo_de_pagina = "product"
}
else if(location.pathname.indexOf("/loja/") >=0){
  var tipo_de_pagina = "departament"
}
/* <![CDATA[ */
               
                 /*
                 - 
                 - Pname: nome do produto
                 - ProdId: id do produto
                 - Pcat: categoria do produto
                 - Value: pre�o do produto
                 - ET: tipo de evento que o cliente selecionou (ex: carrinho, p�gina do produto, etc)
                 */
              var google_conversion_id = 1033431979;
              var google_conversion_language = "en";
              var google_conversion_format = "3";
              var google_conversion_color = "666666";
              var google_conversion_label = "34J0CLnFkQIQq9fj7AM";
              var google_conversion_value = 0;
              var productId = crmWA_dataLayer[0]['objIdProd']
              var productCategory = crmWA_dataLayer[0]['objDepartamento']
              var productValues = crmWA_dataLayer[0]['objPrecoProd']

                
              var google_custom_params = {
   
              usuario_pok: '$isPreApproved',
              use_case: 'retail',
              prodId : productId, 
              pcat : productCategory, 
              pvalues : productValues, 
              orderId: '',
              g:'$gender',
              age: '$age',
              ET : tipo_de_pagina
              };
              var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1033431979/?value=0&amp;label=34J0CLnFkQIQq9fj7AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script>
if(jQuery(".a-main-product").length) {
	var html = jQuery("#find-cep").html();
	jQuery("#find-cep").remove()
	jQuery("body").append("<div hidden><div id='find-cep'>"+html+"</div></div>");

	jQuery("#find-cep form").on("submit", function(e) {
		e.preventDefault();
		var service_url = "/cep/", params = jQuery(this).serialize(), f = jQuery(this);
		f.find(".customLoadAjax").stop().fadeIn();
		jQuery(".address-not-found").hide();
		jQuery.ajax({
			url: service_url,
			data: params
		}).done(function(data) {
			f.find(".customLoadAjax").fadeOut();
			var mustacheTemplate = document.getElementById("find-cep-result-template").innerHTML;
			var result = {data: data};
			jQuery("#lb-result-cep").html(Mustache.render(mustacheTemplate, result));
			acom.lightbox(jQuery("#lb-result-cep"), "inline", "500", "500");
		}).fail(function() {
			f.find(".customLoadAjax").stop().fadeOut();
			jQuery(".address-not-found").stop().fadeIn();
			f[0].reset();
			setTimeout(function() {
				jQuery(".address-not-found").stop().fadeOut();
			}, 5000)
		});
	});
	jQuery(document).on("click", ".find-cep-result tr", function() {
		jQuery(".input-cep").val(jQuery(this).attr("data-cep")).focus();
		jQuery.magnificPopup.close();
	});
}
</script>
      
	<script src="http://iacom.s8.com.br/statics-1.14.7/catalog/js/v1/injs.js"></script>
	<script>
		injs.jsDir = "http://iacom.s8.com.br/statics-1.14.7/catalog/js/v1/";
	</script>
	<script src="http://iacom.s8.com.br/statics-1.14.7/catalog/js/v1/main.js"></script>
		<script type="text/javascript" src="http://busca.americanas.com.br/js/neemu_plugin.js"></script>
		<script type="text/javascript" src="http://laas-acom.neemu.com/acom/neemu.js" async="true"></script>

	<META NAME="WT.cg_n" CONTENT="Moda" />

	<script type="text/javascript" src="http://iacom.s8.com.br/statics-1.14.7/js/criteo_ld.js" async="true"></script>
		<script type="text/javascript">
			var CRITEO_CONF = [[{
			
			    pageType: 'list',
			    'Product IDs': [$(".single-product:eq(0) .productId").val(), $(".single-product:eq(1) .productId").val(), $(".single-product:eq(2) .productId").val()],
			    'Keywords': ''
			
			}], [8416,'pac','us.','110',[[7725630, 7725631]], {'Product IDs':['i',1],'Keywords':['kw',2]}]];
			if (typeof (CRITEO) != "undefined") { CRITEO.Load(false); }
		</script><script type="text/javascript">if (!NREUMQ.f) { NREUMQ.f=function() {NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:") + "//" + "js-agent.newrelic.com/nr-100.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-1.newrelic.com","0699e41a46","3084391,3112561","M1NXNxEFXEtRUkRZVgoZZhcREUZLcVJEWVYKGVkMCQUdCwUJAwULS1taBwI=",0,314,new Date().getTime(),"","","","",""]);</script>
	<script>
	app_loaded = true;
	onReady();
	</script>
</body>
</html>