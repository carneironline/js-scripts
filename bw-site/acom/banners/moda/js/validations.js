function validations() {
  /*
   * MÃ©todo para validar email
   */
  this.validateEmail = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  
  /*
   * Valida datas
   */
  this.validateDate = function (date){

		var ardt=new Array;
		var ExpReg=new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
		ardt=date.split("/");
		erro=false;
		if ( date.search(ExpReg)==-1){
			erro = true;
			}
		else if (((ardt[1]==4)||(ardt[1]==6)||(ardt[1]==9)||(ardt[1]==11))&&(ardt[0]>30))
			erro = true;
		else if ( ardt[1]==2) {
			if ((ardt[0]>28)&&((ardt[2]%4)!=0))
				erro = true;
			if ((ardt[0]>29)&&((ardt[2]%4)==0))
				erro = true;
		}
		if (erro) {
			return false;
		}
		return true;
  }
  
   this.validateCEP = function (strCEP)
	{
		// Caso o CEP não esteja nesse formato ele é inválido!
		var objER = /^[0-9]{2}.[0-9]{3}-[0-9]{3}$/;

		if(strCEP.length > 0)
		{
			if(objER.test(strCEP))
				return true;
			else
				return false;
		}
		else
			return false;
	}

  /*
   * MÃ©todo para verificar se emails sÃ£o equivalentes
   */
  this.emailsCheck = function ($email, $emailConfirm) {
    if ($email == $emailConfirm)
      return false;
    else
      return true;
  }

  /*
   * Método para validar CPF
   */
  this.validateCPF = function (cpf) {
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
      return false;
    for (i = 0; i < cpf.length - 1; i++)
      if (cpf.charAt(i) != cpf.charAt(i + 1)) {
        digitos_iguais = 0;
        break;
      }
    if (!digitos_iguais) {
      numeros = cpf.substring(0, 9);
      digitos = cpf.substring(9);
      soma = 0;
      for (i = 10; i > 1; i--)
        soma += numeros.charAt(10 - i) * i;
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(0))
        return false;
      numeros = cpf.substring(0, 10);
      soma = 0;
      for (i = 11; i > 1; i--)
        soma += numeros.charAt(11 - i) * i;
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(1))
        return false;
      return true;
    }
    else
      return false;
  }


  /*
   * Verifica se a tecla digitada é um número
   */
  this.somenteNumeros = function (e) {
    var key;
    var keychar;
    if (window.event)
      key = window.event.keyCode;
    else if (e)
      key = e.which;
    else
      return true;
    keychar = String.fromCharCode(key);
    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
      return true;
    else if ((("0123456789").indexOf(keychar) > -1))
      return true;
    else if (keychar == ".") {
      return false;
    } else
      return false;
  };

  /*
   * Formatação para máscaras
   */
  this.addMask = function (campo, Mascara, evento) {
	    var boleanoMascara; 
		var Digitato = evento.keyCode;
		exp = /\-|\.|\/|\(|\)| /g

		campoSoNumeros = campo.value.toString().replace( exp, "" ); 

		var posicaoCampo = 0;    
		var NovoValorCampo="";
		var TamanhoMascara = campoSoNumeros.length;; 

		if (Digitato != 8) { // backspace 
				for(i=0; i<= TamanhoMascara; i++) { 
						boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".") || (Mascara.charAt(i) == "/")) 

						boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(") || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " ")) 

						if (boleanoMascara) { 
								NovoValorCampo += Mascara.charAt(i); 
								  TamanhoMascara++;
						}else { 
								NovoValorCampo += campoSoNumeros.charAt(posicaoCampo); 
								posicaoCampo++; 
						  }              
				  }      

				campo.value = NovoValorCampo;

				return true; 
		}else { 
				return true; 
		}
  }

  /*
   * Formatação para valores reais
   */
  this.mask_reais = function (fld, milSep, decSep, e) {
	  
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = e.keyCode ? e.keyCode : e.which; //(window.event) ? e.which : e.keyCode;
    
    console.log(whichCode)
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode);  // Valor para o cÃ³digo da Chave
    if (strCheck.indexOf(key) == -1) return false;  // Chave invÃ¡lida
    len = fld.value.length;
    for (i = 0; i < len; i++)
      if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
    aux = '';
    for (; i < len; i++)
      if (strCheck.indexOf(fld.value.charAt(i)) != -1) aux += fld.value.charAt(i);
    aux += key;
    len = aux.length;
   
    if (len == 0) fld.value = '';
    if (len == 1) fld.value = '0' + decSep + '0' + aux;
    if (len == 2) fld.value = '0' + decSep + aux;
    if (len > 2) {
      aux2 = '';
      for (j = 0, i = len - 3; i >= 0; i--) {
        if (j == 3) {
          aux2 += milSep;
          j = 0;
        }
        aux2 += aux.charAt(i);
        j++;
      }
      fld.value = '';
      len2 = aux2.length;
      for (i = len2 - 1; i >= 0; i--)
        fld.value += aux2.charAt(i);
      fld.value += decSep + aux.substr(len - 2, len);
    }
    return false;
  }
}

$validations = new validations();
