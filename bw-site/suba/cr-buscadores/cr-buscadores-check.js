//Metodo para pegar uma query da url
var QueryString = function () {
    // This function is anonymous, is executed immediately and
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = pair[1];
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]], pair[1] ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(pair[1]);
        }
    }
    return query_string;
} ();

onReady('jQuery', function(){
    $(document).on('click', '#JanelaCadastroModa .app-cr .btn-brand', function() {
        $('#JanelaCadastroModa .app-cr').submit();
    });

    function crBuscadoresCheck() {
        var origem = ['googlesearch', 'xmlgoogle', 'gorgsuba', 'vizurysub'];
        var opn = QueryString.opn.toLowerCase();

        // Checa se o valor do atributo opn esta dentro dos valores possiveis ou se nao definido
        if ($.inArray(opn, origem) > -1 && opn !== null) {
            var opnStr = "opn="+opn;
            //var url = "estaticapop/CRGERALHOME" + opnStr;
            var url = "http://www.submarino.com.br/estaticapop/CRGERALHOME" + opnStr;

            $.cachedScript = function( url, options ) {
                options = $.extend( options || {}, {
                    dataType: "script",
                    cache: true,
                    url: url
                });

                return $.ajax( options );
            };

            $.cachedScript('http://apps.submarino.com.br/media/site/cr-buscadores/jquery.colorbox-min.js').done(function( script, textStatus ) {

                $.colorbox({
                        href:url,
                        width:'704px',
                        height:'400px'
                });
            });
        }
    }

    crBuscadoresCheck();
});