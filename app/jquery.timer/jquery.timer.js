(function ($) {

    var default_options = {
        alerts: [5, 10, 15, 20, 30, 60, 60 * 5, 60 * 10],
        classes: {
            component: 'timer',
            loading: 'loading',
            complete: 'complete',
            alert: 'alert',
            separator: '-',
            year: 'year',
            day: 'day',
            hour: 'hour',
            minute: 'minute',
            second: 'second',
            hundreds: 'hundreds',
            milliseconds:'milliseconds',
            tens: 'tens',
            units: 'units'
        }
    };

    $.fn.timer = function (_params) {
        return getTimer.call(this, _params);
    };

    function getTimer(_params) {
        var params = $.extend(true, {}, default_options, _params);
        var getClass = function (arg, dotted) {
            dotted = (typeof(dotted) === 'undefined') ? true : false;
            return [dotted ? '.' : '',
                params.classes.component,
                params.classes.separator,
                params.classes[arg]].join('');
        };

        var insertValue = function (arg, value) {
            var elem = this.find(getClass(arg));
            var values = value.split('').reverse();
            var hundreds = elem.find(getClass('hundreds'));
            var tens = elem.find(getClass('tens'));
            var units = elem.find(getClass('units'));

            if (hundreds.length || tens.length || units.length) {
                if(value.length <= 2){
                    units.text(values[0] || '0');
                    tens.text(values[1] || '0');
                    hundreds.text(values[2] || '0');
                } else {
                    tens.text(value);
                }
            } else {
                elem.text(value);
            }
        };

        // ordenando os alertas
        params.alerts = params.alerts.sort(function (a, b) {
            return a - b;
        });

        return this.each(function () {
            var self = $(this);

            var attr_end = $(this).data('timer') ? ($(this).data('timer').match(/(\d{1,4})|(\d{1,2})/gi) || []) : [];

            var objs = {};
            $.each(['day', 'month', 'year', 'hours', 'minutes', 'seconds', 'milliseconds'], function (index, value) {
                objs[value] = attr_end.slice(index, index + 1).length ? attr_end.slice(index, index + 1)[0] : '';
            });

            var checkTime = function (i) {
                if (i < 10 && i >= 0) {
                    i = "0" + i;
                } else if (isNaN(i) || i < 0) {
                    i = "00";
                }
                return i + '';
            };

            var cont = 100;

            var changeDate = function () {
                var date_now = new Date();
                //TODO: O mÃªs ainda estÃ¡ passÃ­vel de erro;
                var date_end = new Date(
                    (objs.year || ''),
                    ((parseInt(objs.month, 10) - 1) || ''),
                    (objs.day || ''),
                    (objs.hours || ''),
                    (objs.minutes || ''),
                    (objs.seconds || '')
                );

                // obter o tempo total em seconds
                var date_difference = (date_end.getTime() - date_now.getTime()) / 1000;
                date_difference = Math.floor(date_difference);

                var oneMininSgs  = 60;    /* = 60sgs */
                var oneHourinSgs = 3600;  /* = 60sgs * 60min */
                var oneDayinSgs  = 86400; /* = 3600sgs * 24hrs */

                // obter os days
                /* basicamente Ã© verificar se a classe .date existe pega esse codigo, caso contrario = 0 */
                var days = 0;
                if(self.find(".timer-day").length > 0){
                    days = date_difference / oneDayinSgs;
                    days = Math.floor(days);
                }
                days = checkTime(days);

                var timeDays = (days * oneDayinSgs);

                // obter as horas
                var hours = (date_difference - timeDays) / oneHourinSgs;
                hours = Math.floor(hours);
                hours = checkTime(hours);
                var hourDays = (hours * oneHourinSgs);

                // obter os minutos
                var minutes = (date_difference - timeDays - hourDays) / 60;
                minutes = Math.floor(minutes);
                minutes = checkTime(minutes);

                // obter os segundos
                var seconds = (date_difference - timeDays - (hours * oneHourinSgs) - (minutes * 60));
                seconds = checkTime(seconds);

                // obter os milliseconds usando mod
                /*var milliseconds = date_now.getMilliseconds() % 100;
                 milliseconds = checkTime(milliseconds);*/

                var milliseconds = (date_now.getMilliseconds() / 10);
                milliseconds = checkTime(Math.floor(milliseconds));

                /* solucao mais performatica */
                /*var milliseconds = (cont <= 0 ? cont=99 : cont--);
                 milliseconds = checkTime(milliseconds);*/

                $.each({'day': days, 'hour': hours, 'minute': minutes, 'second': seconds, 'milliseconds': milliseconds}, function (a, b) {
                    insertValue.call(self, a, b);
                });

                $.each(params.alerts, function (a, b) {
                    var current_class = getClass('alert', false);
                    if (date_difference <= b) {
                        self.addClass([current_class, b].join('-'));
                        if (date_difference <= 0 || date_difference <= params.alerts[a - 1]) {
                            self.removeClass([current_class, b].join('-'));
                        }
                    }
                });

                if (date_difference > 0) {
                    setTimeout(function () {
                        changeDate.call(self);
                    }, 1);
                } else {
                    setTimeout(function () {
                        self.trigger('complete', self);
                        self.addClass(getClass('complete', false));
                    }, 100);
                }
            };
            changeDate.call(self);
            self.removeClass(getClass('loading', false));
        });
    }
})(jQuery);