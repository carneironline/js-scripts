App.on('load', function(){

	(function($){
	    $.fn.fullBanner = function(options) {
	    	if(this.length>0){
		        var elementsData = new Array();
		        
		        for(i=0; i<this.length; i++){ // console.log($(this))
		
		        	elementsData.push(this[i]);
		        };
		        
		        getProduct(elementsData, 0);
	    	}
	    }
	    
	    
	    // Busca o produto no productInfo pelo seu ID
		function getProduct(arrEl, cont){ 
			
			var settings = {
	          'background' : '#fff',
	          'width' : '100%',
	          'height' : '100px',
	          'content_w': '985px',
	          'font_color': '#fff',
	          'invoice_price': true
	        };
			
			data = $(arrEl[cont]).data();
			
			for (p in data) {
	    		settings[p] = data[p];
	    	}
			
			//Trata o background se o valor for uma imagem
	    	if (/\.(jpg|png|gif)$/.test(settings.background)) {
	    		settings.background = 'url('+settings.background+') center center no-repeat';
	    	}
	    	
	    	//Add css ao elemento .app-fullbanner
	    	$(arrEl[cont]).css({
	        	'background': settings.background,
	        	'width': settings.width,
	        	'height': settings.height
	        });
			
			$.ajax({
				 url: 'http://www.americanas.com.br/productinfo?itens=' + settings.product_id + '&callback=?',
				 jsonpCallback: 'fullBannerCallback',
		         cache: true,
		         dataType: 'jsonp',
		         beforeSend: function( xhr ) {
		    	    xhr.overrideMimeType( "text/javascript; charset=UTF-8" );
		    	  },
				 success: function(json){
					
					createHTML(json.products[0], $(arrEl[cont]), settings);
					
					 if(cont < arrEl.length-1){
						getProduct(arrEl, cont+1);
					 }
					 
					
				 },
				 error: function() { console.log('err')
					 getProduct(arrEl, cont);
				}
			});
		}
		
		// Gera o html do banner
		function createHTML(objProd, elWrap, settings){
			
			// Add o elemento "a" ao .app-fullbanner
		$(elWrap).append('<a class="fullBanner-url" href="'+objProd.url+'" style="display:block; width:'+settings.width+'; height:'+settings.height+'; color:'+settings.font_color+'; text-decoration:none; "></a>');
			
			//Add o elemento que envolve o banner e suas informaï¿½ï¿½es
		$('a', elWrap).append('<div class="fullBanner-content" style="display:block; width:'+settings.content_w+'; height:'+settings.height+'; margin:0 auto; "></div>');
			
		// Verifica se é para trazer o preço no boleto ou o preço de venda
		if($(elWrap).hasClass('product-price-invoice'))
		{
			$sales_price = objProd.invoice_price.split(' '); $sales_price = $sales_price[1];
		} else {
			$sales_price = objProd.sales_price.split(' '); $sales_price = $sales_price[1];
		}
			
		// Html do banner
			$html = '';
			$html += '<div class="price" style="padding-left:650px; padding-top:20px; font-size:12px;">';
			$html += '<div class="regularPrice">a partir de</div>';
			$html += '<div class="salePrice" style="font-size:32px;"><span style="font-size:12px;" class="unidade-valor">R$</span>  <strong>'+ $sales_price +'</strong></span></div>';
			$html += '<div class="parcel">ou em até '+ objProd.installment.total_installments +'x de <span> '+ objProd.installment.installment_value +' </span></div>';
			$html += '</div>';
			
			//Insere o html gerado no content do fullbanner
			$('.fullBanner-content', elWrap).append($html);
		}
	    
	    $(function(){
	    	$(".app-fullbanner").fullBanner();
	    });
	})(App.$);

});