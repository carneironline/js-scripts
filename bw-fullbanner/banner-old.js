(function($){
    $.fn.fullBanner = function(options) {
	
 	// Defini��o dos valores padr�es
        var defaults = {
          'background' : '#fff',
          'width' : '100%',
          'height' : '100px',
          'content_w': '985px',
          'font_color': '#fff'
        };
       
 
        var settings = $.extend( {}, defaults, options );
       
        return this.each(function() {
        	// Recebe todos os attr data do objeto
        	var data = $(this).data(); 
        	var elWrap = $(this);
        	
        	// Faz um loop sobre os attr data para substituir os params do objeto defaults
        	for (p in data) {
        		settings[p] = data[p];
        	}
        	
        	//Trata o background se o valor for uma imagem
        	if (/\.(jpg|png|gif)$/.test(settings.background)) {
        		settings.background = 'url('+settings.background+') center center no-repeat';
        	}
        	
        	//Add css ao elemento .app-fullbanner
            $(this).css({
            	'background': settings.background,
            	'width': settings.width,
            	'height': settings.height
            });
            
            getProduct(settings, function(data){
            	createHTML(data[0], elWrap, settings);
            });
            
            // Busca o produto no productInfo pelo seu ID
      		function getProduct(settings, callback){

      			$.ajax({
      				 url: 'http://www.americanas.com.br/productinfo?itens=' + settings.product_id + '&callback=?',
    				 jsonpCallback: 'jsonpCallback',
    	             cache: true,
    	             dataType: 'jsonp',
    	             beforeSend: function( xhr ) {
	            	    xhr.overrideMimeType( "text/javascript; charset=UTF-8" );
	            	  },
    				 success: function(json){
    					 callback(json.products);
    				 },
    				 error: function() { console.log("fb-err");}
    			});
      		}
      		
      		// Gera o html do banner
      		function createHTML(objProd, elWrap, settings){
      			
      			// Add o elemento "a" ao .app-fullbanner
            	$(elWrap).append('<a class="fullBanner-url" href="'+objProd.url+'" style="display:block; width:'+settings.width+'; height:'+settings.height+'; color:'+settings.font_color+'; "></a>');
      			
      			//Add o elemento que envolve o banner e suas informa��es
            	$('a', elWrap).append('<div class="fullBanner-content" style="display:block; width:'+settings.content_w+'; height:'+settings.height+'; margin:0 auto; "></div>');
      			
            	// Verifica se é para trazer o preço no boleto ou o preço de venda
            	if( $(elWrap).hasClass('product-price-invoice'))
            	{
            		$sales_price = objProd.invoice_price.split(' '); $sales_price = $sales_price[1];
            	} else {
            		$sales_price = objProd.sales_price.split(' '); $sales_price = $sales_price[1];
            	}
      			
            	// Html do banner
      			$html = '';
      			$html += '<div class="price" style="padding-left:650px; padding-top:20px; font-size:12px;">';
      			$html += '<div class="regularPrice">a partir de</div>';
      			$html += '<div class="salePrice" style="font-size:32px;"><span style="font-size:12px;" class="unidade-valor">R$</span>  '+ $sales_price +'</span></div>';
      			$html += '<div class="parcel">ou em até '+ objProd.installment.total_installments +'x de <span> '+ objProd.installment.installment_value +' </span></div>';
      			$html += '</div>';
      			
      			//Insere o html gerado no content do fullbanner
      			$('.fullBanner-content', elWrap).append($html);
      		}
        });
    }
    
    $(function(){
    	$(".app-fullbanner").fullBanner();
    });
})(jQuery);

