function Products()
{
	/*
	 * @param productID - Pode receber uma string ou array;
	 * @param callback - Pode receber um callback
	 * Dependencies - Jquery
	 */
	this.getProductsByID = function(productID, callback)
	{	
		var productID = productID.toString();
		
		$.ajax({
			url: 'http://www.'+checkBrand()+'.com.br/productinfo?itens='+productID+'&callback=?',
			jsonpCallback: 'CallbackProductsByID',
			cache: true,
			dataType: 'jsonp',
			success: function(json){
				var objProducts = json.products;
				callback(objProducts);
			},
			error: function() { console.log("gPBID-err");}
		});
	}
	
	/*
	 * @param paramTag - Pode receber um array com as tags de produtos;
	 * @param callback - Pode receber um callback;
	 * Dependencies - Jquery
	 */
	this.getProductsByTag = function(paramTag, callback)
	{	
		var $arrObj = new Array();
		
		// Chama o método que busca os produtos pelas tags
		getTagData(paramTag, $arrObj, function(data){

			callback(data);
		});
	}
	
	/*
	 * @param paramTag - Pode receber um array com as tags de produtos;
	 * @param $execute - Precisa receber o valor 0. É somente um iterador;
	 * @param arrObj - Precisa receber um array vazio. O método se encarrega de preenchê-lo;
	 * @param callback - Método que retornará o array de produtos populado;
	 * Dependencies - Jquery
	 */
	function getTagData(paramTag, arrObj, callback, index, testing){
		
		//index contador
		index = typeof index !== 'undefined' ? index : 0;
		
		// Var para teste de erro
		testing = typeof testing !== 'undefined' ? testing : 0;
		
		var tagName = paramTag[index]; 
		
		$.ajax({
			url: 'http://www.'+checkBrand()+'.com.br/productinfo_bytag?tag='+tagName+'&callback=?',
			jsonpCallback: 'CallbackProductsByTag',
			cache: true,
			dataType: 'jsonp',
			success: function(json){
				
				// Insere os dados da tag dentro do array
				arrObj.push({ 
							products: json.products, 
							tag: tagName 
							});
				 
				// Caso ainda existam mais tags, o iterador será incrementado e o getTagData chamado novamente.
				if(index < paramTag.length-1) {
					getTagData(paramTag, arrObj, callback, index+1);

				} else {
					callback(arrObj);
				}

			},
			complete: function(a, b){
				if(b == 'parsererror'){
					if(testing<2){
						getTagData(paramTag, arrObj, callback, index, testing+1);
					}
					else if(index < paramTag.length-1) {
						getTagData(paramTag, arrObj, callback, index+1, 0);
					} else {
						callback(arrObj);
					}
				}
			}
		});	
		
	}
	
	/*
	 * Retorna a marca atual a partir da url
	 */
	function checkBrand(){
		str = location.host;
		
		if(str.search("americanas") > -1){
			return 'americanas';
		}
		if(str.search("submarino") > -1){
			return 'submarino';
		}
		if(str.search("shoptime") > -1){
			return 'shoptime';
		}
		if(str.search("localhost") > -1){
			return 'submarino';
		}
	}
	
}

Produtos = new Products();
